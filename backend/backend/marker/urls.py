from django.urls import path

from marker.views import AddMarkerView, MarkerView, NearMarkerListView, AddCommentView, CommentListView, LeaderBoardView
from marker.consumers import MarkerConsumer


urlpatterns = [
    path('add', AddMarkerView.as_view()),
    path('<int:marker_pk>', MarkerView.as_view()),
    path('near', NearMarkerListView.as_view()),
    path('comment/add/<int:marker_pk>', AddCommentView.as_view()),
    path('comment/<int:marker_pk>', CommentListView.as_view()),
    path('leader_board/', LeaderBoardView.as_view()),
]

ws_urlpatterns = [
    path('marker', MarkerConsumer),
]
