package com.dps_locator.dpslocator.core.chat

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.dps_locator.dpslocator.MainActivity
import com.dps_locator.dpslocator.R
import com.dps_locator.dpslocator.UserFragment
import com.dps_locator.dpslocator.core.api
import com.squareup.picasso.Picasso
import jp.wasabeef.picasso.transformations.RoundedCornersTransformation


class MessageAdapter(private val mMessageList: List<UserMessage>): RecyclerView.Adapter<MessageAdapter.MessageHolder>() {

    private val VIEW_TYPE_MESSAGE_SENT = 1
    private val VIEW_TYPE_MESSAGE_RECEIVED = 2

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessageAdapter.MessageHolder {
        if (viewType == VIEW_TYPE_MESSAGE_SENT) {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.item_message_sent, parent, false)
            return SentMessageHolder(view)
        } else {  // Received
            val view = LayoutInflater.from(parent.context).inflate(R.layout.item_message_received, parent, false)
            return ReceivedMessageHolder(view)
        }
    }

    override fun onBindViewHolder(messageView: MessageHolder, position: Int) {
        val userMessage = mMessageList[position]

        if (userMessage.sender.id != api.user?.id) {
            messageView as ReceivedMessageHolder
            messageView.nameText.text = userMessage.sender.username
        }

        if (messageView.avatarImageView != null) {
            Picasso.get()
                .load(userMessage.sender.avatar)
                .transform(RoundedCornersTransformation(250, 0))
                .resize(250, 250)
                .into(messageView.avatarImageView)

            messageView.avatarImageView.setOnClickListener {
                MainActivity.replaceFragment(UserFragment.newInstance(userMessage.sender.id, "common_chat"))
            }
        }

        messageView.messageText.text = userMessage.message
//        val sentDate = Date(userMessage.createdAt)
        messageView.timeText.text = userMessage.createdAt // sentDate.toString()
    }

    override fun getItemViewType(position: Int): Int {
        val message = mMessageList[position]

        return when (message.sender.id) {
            api.user!!.id -> VIEW_TYPE_MESSAGE_SENT
            else -> VIEW_TYPE_MESSAGE_RECEIVED
        }
    }

    override fun getItemCount(): Int {
        return mMessageList.size
    }

    open inner class MessageHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        var messageText = itemView.findViewById<TextView>(R.id.text_message_body)
        val timeText = itemView.findViewById<TextView>(R.id.text_message_time)
        val avatarImageView: ImageView? = itemView.findViewById(R.id.image_message_profile)
    }

    inner class ReceivedMessageHolder(itemView: View): MessageHolder(itemView) {
        val nameText = itemView.findViewById<TextView>(R.id.text_message_name)
//        val avatar
    }

    inner class SentMessageHolder(itemView: View): MessageHolder(itemView)
}