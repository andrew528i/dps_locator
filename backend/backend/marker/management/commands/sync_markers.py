from typing import List

from django.core.management.base import BaseCommand
from django.contrib.gis.geos import Point
from django.contrib.gis.measure import Distance
from django.db import transaction
from django.db.models import Max
from telethon import TelegramClient, events, sync
from telethon.tl.types import MessageMediaGeo
from loguru import logger

from user.models import User
from marker.models import Marker


session_name = 'dps_locator'
api_id = 358277
api_hash = 'd5b5626597cf278c0bb2f885c8294a2e'
chat_name = 'anti_reid_msk'
batch_size = 100
seen_markers = []


def sync_markers(markers: List[Marker], force=False):
    if force or (markers and len(markers) > batch_size):
        logger.info(f'Saving {len(markers)} markers')

        with transaction.atomic():
            for m in markers:
                m.save()

        new_marker_pks = [m.pk for m in markers]
        logger.info(f'New marker ids: {new_marker_pks}')

        return new_marker_pks


class Command(BaseCommand):

    help = 'sync marker objects from group'

    @staticmethod
    def _unique_markers(new_marker_pks=None):
        global seen_markers

        if seen_markers:
            logger.info('Markers already unique')
            quit()

        while 1:
            if new_marker_pks:
                logger.info(f'New markers: {len(new_marker_pks)}')
                markers = Marker.objects.filter(pk__in=new_marker_pks).exclude(pk__in=seen_markers)
            else:
                logger.info(f'Seen markers: {len(seen_markers)}')
                markers = Marker.objects.exclude(pk__in=seen_markers)

            if not markers.count():
                logger.info('All done')
                return

            for m in markers:
                lng, lat = m.location.coords
                point = Point(lng, lat)
                near_markers = Marker.objects.filter(location__distance_lt=(point, Distance(m=70)), type=Marker.TYPE_POLICE)
                near_markers_count = near_markers.count()

                logger.info(f'Marker: {m.pk}, near: {near_markers_count}')

                if near_markers_count > 1:
                    with transaction.atomic():
                        for nm in near_markers:
                            seen_markers.append(nm.pk)
                            nm.delete()

                    m.seen_count += near_markers_count
                    m.save()
                    seen_markers.append(m.pk)
                    break

                if m.pk not in seen_markers:
                    seen_markers.append(m.pk)
                else:
                    break

    def _add_new_markers(self):
        user = User.objects.first()
        last_spawn_time = Marker.objects.aggregate(Max('spawn_time')).get('spawn_time__max', 0)
        client = TelegramClient(session_name, api_id, api_hash)
        client.connect()
        markers = []

        if not client.is_connected():
            logger.error('Failed to auth Telegram client')
            quit()

        for message in client.iter_messages(chat_name):
            media = message.media

            if not media or not isinstance(media, MessageMediaGeo):
                continue

            if last_spawn_time and message.date.timestamp() <= last_spawn_time.timestamp():
                new_marker_pks = sync_markers(markers, True)
                logger.info('No new markers')
                return new_marker_pks

            logger.info(f'[{message.sender.username}, {message.sender.first_name}] {message.date}: <{media.geo.long}:{media.geo.lat}>')

            marker = Marker()
            marker.user = user
            marker.type = Marker.TYPE_POLICE
            marker.location = Point(media.geo.long, media.geo.lat)
            marker.spawn_time = message.date
            markers.append(marker)

            if sync_markers(markers):
                markers = []

        marker_pks = sync_markers(markers, True)
        client.disconnect()

        return marker_pks

    def handle(self, *args, **options):
        try:
            new_marker_pks = self._add_new_markers()

            if new_marker_pks:
                self._unique_markers(new_marker_pks)
        except KeyboardInterrupt:
            self._unique_markers()
