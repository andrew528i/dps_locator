package com.dps_locator.dpslocator.core

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.maps.android.clustering.ClusterItem
import com.google.maps.android.clustering.ClusterManager
import com.google.maps.android.clustering.view.DefaultClusterRenderer
import org.json.JSONObject
import android.support.v4.content.ContextCompat
import android.graphics.drawable.Drawable
import com.dps_locator.dpslocator.R


enum class MarkerType {
    POLICE, SPEED, CAMERA, FUELING, UNKNOWN
}

class Marker(
    val pk: Int,
    val type: MarkerType,
    val userId: Int,
    val latlng: LatLng,
    val createTime: String,
    val seenCount: Int,
    val options: HashMap<String, Any?>
): ClusterItem {

    override fun getSnippet() = JSONObject()
        .put("pk", pk)
        .put("type", type)
        .put("userId", userId)
        .put("latlng",
            JSONObject()
                .put("lat", latlng.latitude)
                .put("lng", latlng.longitude)
            )
        .put("createTime", createTime)
        .put("seenCount", seenCount)
        .put("options", options)
        .toString()


    override fun getTitle(): String {
        return when (type) {
            MarkerType.POLICE -> "Police"
            MarkerType.SPEED -> "Speed"
            else -> "Unknown"
        }
    }

    override fun getPosition(): LatLng = latlng

    companion object {
        fun getType(type: String): MarkerType {
            return when(type) {
                "P" -> MarkerType.POLICE
                "S" -> MarkerType.SPEED
                else -> MarkerType.UNKNOWN
            }
        }
    }
}

class MarkerItemRenderer(
    val context: Context,
    map: GoogleMap,
    clusterManager: ClusterManager<Marker>
): DefaultClusterRenderer<Marker>(context, map, clusterManager) {

    private fun bitmapDescriptorFromVector(text: String, backgroundColor: Int): BitmapDescriptor {
        val width = 35
        val height = 35
        val bitmap = Bitmap.createBitmap(width * 2, height * 2, Bitmap.Config.ARGB_8888)

        val canvas = Canvas(bitmap)
        val bgPaint = Paint()
        val textPaint = Paint()
        bgPaint.color = backgroundColor
        textPaint.color = Color.WHITE
        textPaint.textSize = 55F
        canvas.drawCircle(width.toFloat(), height.toFloat(), width / 1f, bgPaint)
        canvas.drawText(text, width / 2F, height * 1.5F, textPaint)

        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }

    private fun bitmapDescriptorFromIcon(context: Context, vectorResId: Int, backgroundColor: Int): BitmapDescriptor {
        val vectorDrawable = ContextCompat.getDrawable(context, vectorResId)
        val width = vectorDrawable!!.intrinsicWidth / 2
        val height = vectorDrawable.intrinsicHeight / 2
        vectorDrawable.setBounds(
            (width * 0.4).toInt(),
            (height * 0.4).toInt(),
            Math.round(width * 1.6).toInt(),
            Math.round(height * 1.6).toInt()
        )

        val bitmap = Bitmap.createBitmap(width * 2, height * 2, Bitmap.Config.ARGB_8888)

        val canvas = Canvas(bitmap)
        val paint = Paint()
        paint.color = backgroundColor
        canvas.drawCircle(width.toFloat(), height.toFloat(), width / 1f, paint)
        vectorDrawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }

    override fun onBeforeClusterItemRendered(item: Marker?, markerOptions: MarkerOptions?) {
        when (item?.type) {
            MarkerType.POLICE -> markerOptions?.icon(
//                bitmapDescriptorFromVector("P", Color.rgb(189, 50, 39))
                bitmapDescriptorFromIcon(context, R.drawable.ic_police_map_icon, Color.rgb(232, 51, 51))
            )
            MarkerType.SPEED -> markerOptions?.icon(
//                bitmapDescriptorFromVector("S", Color.rgb(19, 192, 39))
                bitmapDescriptorFromIcon(context, R.drawable.ic_speed_map_icon, Color.rgb(71, 112, 232))
            )
        }

        markerOptions?.anchor(0.5f, 0.5f)
        super.onBeforeClusterItemRendered(item, markerOptions)
    }

    override fun getColor(clusterSize: Int): Int {
        val hueRange = 150f
        val sizeRange = 100f
        val size = Math.min(clusterSize.toFloat(), sizeRange)
        val hue = 60 + (sizeRange - size) * (sizeRange - size) / (sizeRange * sizeRange) * hueRange
        return Color.HSVToColor(floatArrayOf(hue, 1f, .6f))
    }

}
