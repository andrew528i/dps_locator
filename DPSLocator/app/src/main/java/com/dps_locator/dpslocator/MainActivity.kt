package com.dps_locator.dpslocator

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.provider.Settings
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v4.view.GravityCompat
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.ImageView
import android.widget.TextView
import com.dps_locator.dpslocator.core.api
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import com.squareup.picasso.Picasso
import jp.wasabeef.picasso.transformations.RoundedCornersTransformation
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.async
import java.util.*
import android.support.v4.app.FragmentManager
import android.view.inputmethod.InputMethodManager
import io.sentry.Sentry
import io.sentry.android.AndroidSentryClientFactory
import org.jetbrains.anko.contentView
import android.support.v4.content.ContextCompat.getSystemService
import android.telephony.TelephonyManager
import com.google.android.gms.ads.MobileAds
import org.json.JSONObject


class UpdateDrawerInfoTask(val activity: FragmentActivity, val avatarImageView: ImageView, val usernameTextView: TextView): TimerTask() {
    override fun run() {
        if (api.isAuthorized() && api.user != null && api.user?.username != usernameTextView.text) {
            usernameTextView.text = api.user?.username ?: "error"
        }

        if (api.isAuthorized()) {
            activity.runOnUiThread {
                api.user?.let {
                    Picasso.get()
                        .load(api.user!!.avatar)
                        .transform(RoundedCornersTransformation(250, 0))
                        .resize(350, 350)
//                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                        .into(avatarImageView)
                }
            }
        }
    }
}


class MainActivity:
    AppCompatActivity(),
    NavigationView.OnNavigationItemSelectedListener
{
    private val mMapsFragment = MapsFragment()
    private val mChatFragment = ChatFragment()
    private val mPrivateMessages = MessagesFragment()
    private val mSettingsFragment = SettingsFragment()
    private val mLeaderboardFragment = LeaderboardFragment()
    private var mErrorFragment = ErrorFragment()

    private var updateDrawerTimer: Timer? = null

    @SuppressLint("HardwareIds")
    override fun onCreate(savedInstanceState: Bundle?) {
        mFragmentTransaction = supportFragmentManager
        super.onCreate(savedInstanceState)

        val dsn = if (BuildConfig.DEBUG) {
            "http://1f3fb51d3ed6498bb7811b28052d719a:0e3d014d21124f05b84c978844dc6064@127.0.0.1:9000//4"
        } else {
            "http://1f3fb51d3ed6498bb7811b28052d719a:0e3d014d21124f05b84c978844dc6064@173.249.45.177:9000//4"
        }

        Sentry.init(dsn, AndroidSentryClientFactory(applicationContext))

        setContentView(R.layout.activity_main)
        nav_view.setNavigationItemSelectedListener(this)
        api.context = applicationContext

        runWithPermissions(
            Manifest.permission.READ_PHONE_STATE
        ) {
            val telephonyManager = applicationContext.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
            api.deviceId = telephonyManager.getDeviceId()

            async {
                try {
                    api.authorize()

                    runOnUiThread {
                        if (api.isAuthorized()) {
                            runWithPermissions(
                                Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.ACCESS_COARSE_LOCATION) {

                                replaceFragment(mMapsFragment)
                            }
                        } else {
                            mErrorFragment = ErrorFragment.newInstance("Login error", "You are banned :(")
                            replaceFragment(mErrorFragment)
                        }
                    }
                } catch (e: Exception) {
                    mErrorFragment = ErrorFragment.newInstance("Cannot connect server", "Please try again later")
                    replaceFragment(mErrorFragment)
                }
            }
        }

//        api.deviceId = Settings.Secure.getString(this.contentResolver, Settings.Secure.ANDROID_ID)


//        val avatarImageView = nav_view.getHeaderView(0).findViewById<ImageView>(R.id.nav_bar_avatar)
//        val usernameTextView = nav_view.getHeaderView(0).findViewById<TextView>(R.id.nav_bar_username)
//        try {
//            updateDrawerTimer?.cancel()
//        } catch (e: Throwable) {}
//        updateDrawerTimer = Timer()
//        updateDrawerTimer?.scheduleAtFixedRate(UpdateDrawerInfoTask(this, avatarImageView, usernameTextView), 0, 250)
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
//            super.onBackPressed()
            supportFragmentManager.fragments.last().let {
                when (it) {
                    is MarkerFragment, is SpeedMarkerFragment, is PoliceMarkerFragment -> {
                        it.arguments?.let { args ->
                            val markerStr = args.getString("marker")

                            if (markerStr.isEmpty()) {
                                replaceFragment(
                                    MapsFragment.newInstance(
                                        args.getDouble("latitude"),
                                        args.getDouble("longitude")))
                            } else {
                                val markerObj = JSONObject(markerStr)
                                val locationObj = markerObj.getJSONObject("location")
                                replaceFragment(
                                    MapsFragment.newInstance(
                                        locationObj.getDouble("lat"),
                                        locationObj.getDouble("lng"))
                                )
                            }
                        }
                    }
                    is UserFragment -> {
                        it.arguments?.getString("from")?.let { from ->
                            when (from) {
                                "leaderboard" -> {
                                    replaceFragment(mLeaderboardFragment)
                                }
                                "common_chat" -> {
                                    replaceFragment(mChatFragment)
                                }
                                else -> {}
                            }

                            if (from.startsWith("speed_marker_")) {
                                val marker = JSONObject(from.substring(13))
                                marker.put("latlng", marker.getJSONObject("location"))
                                replaceFragment(MarkerFragment.newInstance(marker))
                            }
                        }
                    }
                    is MessageThreadFragment -> {
                        it.arguments?.getString("from")?.let { from ->
                            when (from) {
                                "common_chat" -> {
                                    replaceFragment(mChatFragment)
                                }
                                "messages" -> {
                                    replaceFragment(mPrivateMessages)
                                }
                            }

                            if (from.startsWith("user_")) {
                                val user_id = from.substring(5)
                                replaceFragment(UserFragment.newInstance(user_id.toInt(), ""))
                            }
                        }
                    }
                    else -> {}
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_settings -> return true
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        var fragment: Fragment? = null

        val imm = this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(contentView?.windowToken, 0)

        when (item.itemId) {
            R.id.nav_maps -> {
                fragment = mMapsFragment
            }
            R.id.nav_chat -> {
                fragment = mChatFragment
            }
            R.id.nav_private_messages -> {
                fragment = mPrivateMessages
            }
            R.id.nav_leaders -> {
                fragment = mLeaderboardFragment
            }
            R.id.nav_settings -> {
                fragment = mSettingsFragment
            }
        }

        fragment?.let {
            mFragmentTransaction
                ?.beginTransaction()
                ?.replace(R.id.fragment_container, it)
                ?.commit()
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    companion object {
        private var mFragmentTransaction: FragmentManager? = null

        fun replaceFragment(fragment: Fragment) {
            mFragmentTransaction
                ?.beginTransaction()
                ?.replace(R.id.fragment_container, fragment)
                ?.commit()
        }
    }
}
