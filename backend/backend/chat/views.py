from django.db.models import QuerySet
from django.core.cache import cache
from rest_framework.generics import ListAPIView
from rest_framework import serializers
from rest_framework.pagination import CursorPagination

from user.models import Message as UserMessage, User
from user.serializers.models import UserSerializer


class ThreadSerializer(serializers.Serializer):

    thread = serializers.CharField()

    def to_representation(self, instance: UserMessage) -> dict:
        data = super(ThreadSerializer, self).to_representation(instance)
        users_cache_key = f'users:{instance.thread}'
        users = cache.get(users_cache_key)
        message = UserMessage.objects.filter(thread=instance.thread).order_by('-id')[0]

        if not users:
            users = User.objects.filter(pk__in=instance.user_ids).all()
            cache.set(users_cache_key, users, 300)

        data['users'] = UserSerializer(users, many=True).data
        data['last_message'] = message.body

        return data


class MarkerPagination(CursorPagination):

    ordering = '-thread'
    page_size = 250


class ThreadListView(ListAPIView):

    serializer_class = ThreadSerializer
    pagination_class = MarkerPagination

    def get_queryset(self) -> QuerySet:
        user = self.request.user

        return UserMessage.objects.filter(user_ids__contains=[user.pk]).order_by('thread').distinct('thread')
