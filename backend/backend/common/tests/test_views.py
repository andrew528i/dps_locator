import pytest
import mock
from rest_framework import serializers
from rest_framework.response import Response

from common.views import BaseApiView


SERIALIZER_PATH = 'common.serializers.views'


class ApiViewPostSerializer(serializers.Serializer):

    char_field = serializers.CharField(required=True)
    integer_field = serializers.IntegerField(required=True)


def api_view_fabric():
    class ApiView(BaseApiView):

        authentication_classes = ()
        permission_classes = ()

        def post(self, request):
            return Response(request.validated_data)

    return ApiView


@pytest.fixture
def patched_base_api_view__get_serializer():
    with mock.patch.object(BaseApiView, '_get_serializer', lambda cls: ApiViewPostSerializer):
        yield


@pytest.fixture
def patched_base_api_view__find_serialiers_package():
    with mock.patch.object(BaseApiView, '_find_serialiers_package', lambda: SERIALIZER_PATH):
        with mock.patch('common.views.importlib') as patched_importlib:
            yield patched_importlib


def test_base_api_view_wo_serializer_package():
    with pytest.raises(NotImplementedError):
        api_view_fabric()


def test_base_api_view_wo__find_serialiers_package(patched_base_api_view__find_serialiers_package):
    api_view_fabric()
    patched_base_api_view__find_serialiers_package.import_module.assert_called_once_with(SERIALIZER_PATH)


@pytest.mark.usefixtures('patched_base_api_view__get_serializer')
@pytest.mark.parametrize('pack,status_code,response_keys', [
    ({'char_field': 'qwerty', 'integer_field': 123456}, 200, ['char_field', 'integer_field']),
    ({'integer_field': 'qwerty'}, 400, ['char_field', 'integer_field']),
    ({'char_field': 123456}, 400, ['integer_field']),
    ({'integer_field': 'qwerty'}, 400, ['char_field']),
    ({}, 400, ['char_field', 'integer_field']),
])
def test_base_api_view(rf, pack, status_code, response_keys):
    cls: serializers.Serializer = api_view_fabric()
    request = rf.post('/api_view', data=pack)
    response = cls.as_view()(request)

    assert cls.request_post_serializer_class == ApiViewPostSerializer
    assert response.status_code == status_code

    if status_code == 400:
        assert all(map(lambda k: k in response.data, response_keys))
