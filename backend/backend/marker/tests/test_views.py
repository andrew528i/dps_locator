import random

import pytest
from django.contrib.gis.geos import Point

from marker.models import Marker, Comment
from marker.serializers.models import MarkerSerializer


@pytest.fixture(params=[
    (Marker.TYPE_POLICE, {'mans': 1, 'cars': 1}),
    (Marker.TYPE_CAMERA, {'limit': 60}),
    (Marker.TYPE_FUELING, {'brand': 'GPN', 'octane': ['d', '98', '100']}),
])
def add_marker_pack(request, g):
    marker_type, options = request.param
    pack = {
        'location': {
            'latitude': g.address.latitude(),
            'longitude': g.address.longitude(),
        },
        'type': marker_type,
        'options': options,
    }

    return marker_type, options, pack


@pytest.fixture
def near_markers(marker, make_marker):
    longitude, latitude = marker.location.coords
    markers = []

    for i in range(100):
        longitude_delta = random.random() / 100
        latitude_delta = random.random() / 100
        marker_point = Point(longitude + longitude_delta, latitude + latitude_delta)
        markers.append(make_marker(marker_point))

    yield markers

    for marker in markers:
        marker.delete()


@pytest.fixture
def comments(make_comment):
    comments = []

    for i in range(50):
        comments.append(make_comment())

    yield comments

    for comment in comments:
        comment.delete()


def test_add_marker_view_200(client, add_marker_pack):
    marker_type, options, pack = add_marker_pack
    resp = client.post('/marker/add', pack)
    data = resp.json()
    marker = Marker.objects.get(pk=data['pk'])

    assert marker.type == marker_type
    assert marker.options == options
    assert marker.seen_count == 1

    resp = client.post('/marker/add', pack)
    data = resp.json()
    marker = Marker.objects.get(pk=data['pk'])
    assert marker.seen_count == 2

    marker.delete()


def test_marker_view_200(client, marker):
    resp = client.get('/marker/{}'.format(marker.pk))
    data = resp.json()
    serialized_marker = MarkerSerializer(marker).data

    assert data == serialized_marker


def test_near_marker_view_200(client, marker, near_markers):
    pack = {'longitude': marker.location.coords[0], 'latitude': marker.location.coords[1]}
    resp = client.get('/marker/near', pack)
    data = resp.json()
    objects = data['results']

    assert resp.status_code == 200
    assert len(objects) == len(near_markers) + 1  # marker + near_markers


def test_add_comment_view_200(client, marker, g):
    body = g.text.text()
    resp = client.post('/marker/comment/add/{}'.format(marker.id), {'body': body})
    data = resp.json()
    comment = Comment.objects.get(pk=data['pk'])

    assert comment.body == body

    comment.delete()


def test_list_comments_view_200(client, marker, comments):
    resp = client.get('/marker/comment/{}'.format(marker.pk))
    data = resp.json()

    assert len(data['results']) == len(comments)
