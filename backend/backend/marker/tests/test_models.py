import pytest
from django.contrib.gis.geos import Point


@pytest.fixture
def near_marker(make_marker, marker):
    longitude, latitude = marker.location.coords
    marker = make_marker(Point(longitude + 0.001, latitude + 0.001, srid=4326))
    yield marker
    marker.delete()


@pytest.mark.usefixtures('near_marker')
def test_marker_get_near(marker):
    near_marker_query = marker.get_near(10)
    assert near_marker_query.count() == 0

    near_marker_query = marker.get_near(1000)
    assert near_marker_query.count() == 1
