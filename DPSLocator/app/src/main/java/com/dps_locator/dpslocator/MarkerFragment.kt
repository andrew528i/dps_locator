package com.dps_locator.dpslocator

import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.dps_locator.dpslocator.core.API_HOST
import com.dps_locator.dpslocator.core.Marker
import com.dps_locator.dpslocator.core.MarkerType
import com.dps_locator.dpslocator.core.api
import com.squareup.picasso.Picasso
import jp.wasabeef.picasso.transformations.RoundedCornersTransformation
import kotlinx.coroutines.async
import org.json.JSONObject
import java.sql.Date
import java.sql.Timestamp


class PoliceMarkerFragment: Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.marker_police, container, false)
        val marker = JSONObject(arguments?.getString("marker"))
        val markerOptions = marker.getJSONObject("options")

        val createTime = Date(Timestamp(marker.getString("create_time").toLong() * 1000).time).toString()

        view.findViewById<TextView>(R.id.create_date_text_view).text = createTime
        view.findViewById<TextView>(R.id.mans_count_text_view).text = markerOptions.optString("mans", "1")
        view.findViewById<TextView>(R.id.cars_count_text_view).text = markerOptions.optString("cars", "1")
        view.findViewById<TextView>(R.id.seen_count_text_view).text = marker.optString("seen_count", "1")

        return view
    }

    companion object {
        fun newInstance(marker: JSONObject): PoliceMarkerFragment {
            val fragment = PoliceMarkerFragment()
            val args = Bundle()
            args.putString("marker", marker.toString())
            fragment.arguments = args

            return fragment
        }
    }
}


class SpeedMarkerFragment: Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.marker_speed, container, false)
        val marker = JSONObject(arguments?.getString("marker"))
        val leaders = marker.getJSONArray("leaders")

        val leaderLayout = view.findViewById<LinearLayout>(R.id.leader_layout)

        for (i in 0..(leaders.length() - 1)) {
            val leader = leaders.getJSONObject(i)

            val leaderUser = leader.getJSONObject("user")
            val velocity = leader.getDouble("velocity")

            val leaderItemView = layoutInflater.inflate(R.layout.leader_item_layout, null)
            leaderItemView.findViewById<ImageView>(R.id.leader_avatar).let {
                val avatar = leaderUser.getString("avatar")
                Picasso.get()
                    .load("http://$API_HOST$avatar")
                    .transform(RoundedCornersTransformation(250, 0))
                    .resize(300, 300)
                    .into(it)
            }
            leaderItemView.findViewById<TextView>(R.id.leader_username).text = leaderUser.getString("username")
            leaderItemView.findViewById<TextView>(R.id.leader_velocity).text = "%.2f км/ч".format(velocity)
            leaderItemView.setOnClickListener {
                MainActivity.replaceFragment(UserFragment.newInstance(
                    leaderUser.getInt("pk"),
                    "speed_marker_" + arguments?.getString("marker")
                ))
            }
            leaderLayout.addView(leaderItemView)
        }

        return view
    }

    companion object {
        fun newInstance(marker: JSONObject): SpeedMarkerFragment {
            val fragment = SpeedMarkerFragment()
            val args = Bundle()
            args.putString("marker", marker.toString())
            fragment.arguments = args

            return fragment
        }
    }
}


class MarkerFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_marker, container, false)

        val progressLayout = view.findViewById<ConstraintLayout>(R.id.progress_layout)
        val markerLayout = view.findViewById<ConstraintLayout>(R.id.marker_layout)

        val toolbar = view.findViewById<Toolbar>(R.id.marker_toolbar)
        toolbar.setNavigationIcon(R.drawable.ic_menu)
        toolbar.setNavigationOnClickListener {
            activity?.findViewById<DrawerLayout>(R.id.drawer_layout)?.openDrawer(GravityCompat.START)
        }

        progressLayout.visibility = View.VISIBLE
        markerLayout.visibility = View.GONE
        view.findViewById<TextView>(R.id.marker_title).text = "Маркер #" + arguments?.getInt("markerId").toString()

        async {
            var markerFragment: Fragment? = null

            arguments?.getInt("markerId")?.let {
                api.getMarker(it) { markerObj ->
                    marker = markerObj

                    activity?.runOnUiThread {
                        progressLayout.visibility = View.GONE
                        markerLayout.visibility = View.VISIBLE

                        when (Marker.getType(markerObj.getString("type"))) {
                            MarkerType.POLICE -> markerFragment = PoliceMarkerFragment.newInstance(markerObj)
                            MarkerType.SPEED -> markerFragment = SpeedMarkerFragment.newInstance(markerObj)
                        }

                        markerFragment?.let { f ->
                            fragmentManager
                                ?.beginTransaction()
                                ?.replace(R.id.marker_fragment_container, f)
                                ?.commit()
                        }
                    }
                }
            }
        }

        return view
    }

    companion object {
        fun newInstance(markerObj: JSONObject): MarkerFragment {
            val fragment = MarkerFragment()
            val args = Bundle()
            val latlng = markerObj.getJSONObject("latlng")
            args.putInt("markerId", markerObj.getInt("pk"))
            args.putDouble("latitude", latlng.getDouble("lat"))
            args.putDouble("longitude", latlng.getDouble("lng"))
            fragment.arguments = args

            return fragment
        }

        var marker: JSONObject? = null
    }
}