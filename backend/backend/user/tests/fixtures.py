import pytest

from user.models import User


@pytest.fixture
def user_factory(g):
    user = None

    def create_user():
        nonlocal user

        user = User.objects.create(
            username=g.person.username(), email=g.person.email(),
            device_id=g.cryptographic.uuid(), ip_address=g.internet.ip_v4(),
            avatar='/upload/avatars/default.png',
        )
        return user

    yield create_user

    if user:
        user.delete()


@pytest.fixture
def user(user_factory):
    return user_factory()
