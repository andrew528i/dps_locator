from django.utils.timezone import now
from django.db.models import QuerySet
from django.contrib.gis.db import models as gis_models
from django.db import models
from django.contrib.postgres import fields
from django.contrib.gis.measure import Distance

from user.models import User
from common.models import Comment as BaseComment
from common.utils.mongo import mongo_client


class Marker(gis_models.Model):

    TYPE_ALL = 'A'
    TYPE_UNKNOWN = 'U'
    TYPE_POLICE = 'P'
    TYPE_SPEED = 'S'
    TYPE_CAMERA = 'C'
    TYPE_FUELING = 'F'

    TYPES = (
        (TYPE_ALL, 'all'),
        (TYPE_UNKNOWN, 'unknown'),
        (TYPE_POLICE, 'police'),
        (TYPE_SPEED, 'speed'),
        (TYPE_CAMERA, 'camera'),
        (TYPE_FUELING, 'fueling'),
    )

    type = gis_models.CharField(choices=TYPES, default=TYPE_UNKNOWN, max_length=8)
    location = gis_models.PointField(geography=True, srid=4326)
    options = fields.JSONField(default=dict)
    user = gis_models.ForeignKey('user.User', on_delete=gis_models.DO_NOTHING)
    create_time = gis_models.DateTimeField(auto_now_add=True)
    modify_time = gis_models.DateTimeField(auto_now=True)
    spawn_time = gis_models.DateTimeField(default=now)
    seen_count = gis_models.IntegerField(null=False, default=1)

    def get_near(self, distance: int = 100, **kwargs: dict) -> QuerySet('Marker'):
        return Marker.objects.exclude(pk=self.pk).filter(location__dwithin=(self.location, Distance(m=distance)), **kwargs)

    @property
    def leaders(self):
        leaders = {}
        gps_logs_query = mongo_client.gps_logs.find({
            'location': {
                '$near': {
                    '$geometry': {
                        'type': 'Point',
                        'coordinates': self.location.coords,
                    },
                    '$maxDistance': 100
                }
            }
        })

        for log_entry in gps_logs_query:
            user_id = log_entry['user_id']
            velocity = log_entry['velocity']

            if user_id not in leaders:
                leaders[user_id] = {
                    'user': User.objects.get(pk=user_id),
                    'velocity': velocity
                }
            elif leaders[user_id]['velocity'] < velocity:
                leaders[user_id]['velocity'] = velocity

        return sorted(list(leaders.values()), key=lambda x: x['velocity'], reverse=True)


class Comment(BaseComment):

    marker = models.ForeignKey('marker.Marker', on_delete=models.DO_NOTHING)
