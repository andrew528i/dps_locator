package com.dps_locator.dpslocator

import android.annotation.SuppressLint
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Context.LOCATION_SERVICE
import android.content.Intent
import android.graphics.Color
import android.location.Criteria
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.provider.Settings
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AlertDialog
import android.view.*
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.dps_locator.dpslocator.core.*
import com.dps_locator.dpslocator.core.Marker
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.maps.android.clustering.ClusterManager
import kotlinx.coroutines.async
import org.json.JSONObject
import java.util.*


class MapsFragment : Fragment(), LocationListener, OnMapReadyCallback, GoogleMap.OnCameraIdleListener {
    @SuppressLint("MissingPermission")
    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
        mLocationManager?.requestLocationUpdates(provider, 500, 1F, this)
    }

    override fun onProviderEnabled(provider: String?) {}

    override fun onProviderDisabled(provider: String?) {}

    override fun onLocationChanged(location: Location?) {
        async {
            location?.let {
                velocity = (location.speed * 3600F) / 1000F
                mCurrentLocation = LatLng(it.latitude, it.longitude)

                if (velocity > 1) {
                    MarkerWS.webSocketManager?.sendMessage(
                        LogGpsCoordsOutPacket(
                            "latitude" to it.latitude,
                            "longitude" to it.longitude,
                            "velocity" to velocity).serialize()
                    )
                }

                if (mRoutePolyline != null) { //followLocation) {
                    activity?.runOnUiThread {
                        mMap.animateCamera(CameraUpdateFactory.newLatLng(LatLng(it.latitude, it.longitude)))
                    }
                }

//                api.getMarkers(LatLng(it.latitude, it.longitude))
            }
        }
    }

    override fun onCameraIdle() {
        mMap.cameraPosition?.let {
            async {
                api.getMarkers(LatLng(it.target.latitude, it.target.longitude)).forEach { marker->
                    if (marker.pk !in mMarkersOnMap) {
                        mMarkersOnMap[marker.pk] = marker
                        mClusterManager?.addItem(marker)
                    }
                }
            }
        }


//        followLocation = false


//        mTargetMarker?.let {
//            it.isVisible = false
//        }

        mClusterManager?.onCameraIdle()
    }

    private lateinit var mMap: GoogleMap
    private lateinit var mSearchEdit: AutoCompleteTextView

    private val mPlacesSuggestionMap = HashMap<String, String>()
    private var mSuggestionList = mutableListOf<String>()

    private var mLocationManager: LocationManager? = null
    private var mClusterManager: ClusterManager<Marker>? = null
    private var mMarkerItemRenderer: MarkerItemRenderer? = null
    private var mTargetMarker: com.google.android.gms.maps.model.Marker? = null
//    private var mRoutePolyline: Polyline? = null
//    private var mRoutePolygon: Polygon? = null
    private var mCurrentLocation: LatLng? = null

    private var velocity = 0f
    private var updateTimer: Timer? = null
    private var followLocation = true
    var provider: String? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_maps, container, false) // super.onCreateView(inflater, container, savedInstanceState)

//        mMarkersOnMap.clear()

        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(this)

        mLocationManager = activity?.applicationContext?.getSystemService(LOCATION_SERVICE) as LocationManager

        view.findViewById<ImageButton>(R.id.open_drawer_button).setOnClickListener {
            activity?.findViewById<DrawerLayout>(R.id.drawer_layout)?.openDrawer(GravityCompat.START)
        }

        if (mLocationManager?.isProviderEnabled(LocationManager.GPS_PROVIDER) != true) {
            showGPSDialog()
        }

        mSearchEdit = view.findViewById(R.id.search_edit)
        mSearchEdit.setOnEditorActionListener { v, actionId, keyEvent ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                val location = LatLng(
                    mMap.cameraPosition.target.latitude,
                    mMap.cameraPosition.target.longitude)

                val imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(view.rootView.windowToken, 0)

                async {
                    val addressList = api.getSuggestions(mSearchEdit.text.toString(), location)

                    if (addressList.isEmpty()) {
                        activity?.runOnUiThread {
                            Toast.makeText(context, "Nothing found :(", Toast.LENGTH_LONG).show()
                        }
                    }

                    mSuggestionList = mutableListOf()

                    for (suggestion in addressList) {
                        suggestion["description"]?.let {
                            mSuggestionList.add(it)
                            suggestion["place_id"]?.let { place_id ->
                                mPlacesSuggestionMap[it] = place_id
                            }
                        }
                    }

                    activity?.runOnUiThread {
                        val adapter = ArrayAdapter<String>(
                            activity,
                            android.R.layout.simple_dropdown_item_1line,
                            mSuggestionList)
                        mSearchEdit.setAdapter(adapter)
                        mSearchEdit.showDropDown()
                    }
                }
                true
            }
            false
        }
        mSearchEdit.threshold = 1
        mSearchEdit.dropDownVerticalOffset = 20
        mSearchEdit.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
            val description = mSuggestionList[position]
            val placeId = mPlacesSuggestionMap[description]

            async {
                val targetLocation = api.getPlace(placeId!!)

                activity?.runOnUiThread {
                    mTargetMarker?.let {
                        it.remove()
                    }

                    val markerOptions = MarkerOptions()
                    markerOptions.position(targetLocation)
                    mTargetMarker = mMap.addMarker(markerOptions)

                    mTargetMarker!!.position = targetLocation
                    mTargetMarker!!.title = description

//                    showTargetDialog()
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(targetLocation, 18f))
                }
            }

            mSearchEdit.clearFocus()
            val imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(mSearchEdit.windowToken, 0)
        }

        val velocityTextView = view.findViewById<TextView>(R.id.velocityTextView)

        class UpdateMapsTask: TimerTask() {
            override fun run() {
                activity?.runOnUiThread {
                    if (BuildConfig.DEBUG) {
                        velocityTextView.text = "$velocity km/h\n$provider"
                    } else {
                        velocityTextView.text = ""
                    }

                    val bestProvider = mLocationManager?.getBestProvider(Criteria(), true)

                    if (bestProvider != provider) {
                        provider = bestProvider
                    }

                    if (mRoutePolyline == null) {
                        activity?.findViewById<ImageButton>(R.id.clear_route)?.visibility = View.INVISIBLE
                    } else {
                        activity?.findViewById<ImageButton>(R.id.clear_route)?.visibility = View.VISIBLE
                    }
                }
            }
        }

        try {
            updateTimer?.cancel()
        } catch(e: Throwable) {}

        updateTimer = Timer()
        updateTimer?.scheduleAtFixedRate(UpdateMapsTask(), 0, 250)

        return view
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.uiSettings.isMyLocationButtonEnabled = false
        mMap.uiSettings.isMapToolbarEnabled = false
        mMap.uiSettings.isZoomControlsEnabled = false
        mMap.uiSettings.isCompassEnabled = false
        mMap.isBuildingsEnabled = false
        mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(activity!!.applicationContext, R.raw.map_style))

        provider = mLocationManager?.getBestProvider(Criteria(), true)

        mClusterManager = ClusterManager(activity?.applicationContext, mMap)
        mMap.setOnCameraIdleListener(this)
        mMap.setOnMarkerClickListener(mClusterManager)

        activity?.let {
            mMarkerItemRenderer = MarkerItemRenderer(it.applicationContext, mMap, mClusterManager as ClusterManager<Marker>)
            mClusterManager?.renderer = mMarkerItemRenderer
        }

        mMap.setOnMarkerClickListener(GoogleMap.OnMarkerClickListener {
            mTargetMarker?.let { targetMarker ->
                if (targetMarker.id == it.id) {
                    showTargetDialog()
                }
            }

            if (it.snippet != null && !it.snippet.contains("\"POLICE\"")) {
                it.snippet?.let { snippet ->
                    val markerObj = JSONObject(snippet)
                    MainActivity.replaceFragment(MarkerFragment.newInstance(markerObj))
                }
            }

            true
        })

        for (marker in mMarkersOnMap.values) {
            mClusterManager?.addItem(marker)
        }

        try {
            mMap.isMyLocationEnabled = true
            mLocationManager?.requestLocationUpdates(provider, 500, 1F, this)

            mRoutePolyline?.let {
                val polylineOptions = PolylineOptions()
                polylineOptions.color(Color.GREEN)
                polylineOptions.width(12f)
                polylineOptions.jointType(JointType.ROUND)
                polylineOptions.addAll(it.points)
                mMap.addPolyline(polylineOptions)
            }

            activity?.findViewById<ImageButton>(R.id.my_location)?.setOnClickListener {
                var location: Location? = mLocationManager?.getLastKnownLocation(provider)

                if (location == null) {
                    location = mMap.myLocation
                }

                location?.let {
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(it.latitude, it.longitude), 17F))
                }
                followLocation = !followLocation
            }

            activity?.findViewById<ImageButton>(R.id.clear_route)?.setOnClickListener {
                mRoutePolyline?.let { it.remove(); mRoutePolyline = null }
                mRoutePolygon?.let { it.remove(); mRoutePolygon = null }
                mTargetMarker?.let { it.remove(); mTargetMarker = null }
                activity?.findViewById<EditText>(R.id.search_edit)?.setText("")
            }

            if (arguments == null) {
                val lastLocation = mLocationManager?.getLastKnownLocation(provider)
                if (lastLocation != null) {
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                        LatLng(lastLocation.latitude, lastLocation.longitude), 17F))
                } else {
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(55.7558, 37.6173), 17F))
                }
            } else {
                arguments?.let { args ->
                    val lat = args.getDouble("latitude")
                    val lng = args.getDouble("longitude")
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(lat, lng), 14F)) // animateCamera
                }
            }
        } catch (e: SecurityException) {
            println("Shit happens")
        }
    }

    private fun showTargetDialog() {
        val dialogBuilder = AlertDialog.Builder(ContextThemeWrapper(activity!!, R.style.dialog))

        dialogBuilder.setMessage(mTargetMarker!!.title)
            .setCancelable(false)
            .setPositiveButton("Yes") { _, _ ->
                buildRoute(mTargetMarker!!.position)
            }
            .setNeutralButton("Yandex Navi") { dialog, _ ->
                dialog.cancel()

                buildRoute(mTargetMarker!!.position) {
                    val intent = Intent("ru.yandex.yandexnavi.action.BUILD_ROUTE_ON_MAP") // , uri, activity?.applicationContext, MainActivity.javaClass)
                    intent.setPackage("ru.yandex.yandexnavi")
                    intent.putExtra("lat_to", mTargetMarker!!.position.latitude)
                    intent.putExtra("lon_to", mTargetMarker!!.position.longitude)

                    mRoutePolyline!!.points.slice(
                        0..(mRoutePolyline!!.points.size - 1) step (mRoutePolyline!!.points.size - 1) / 10
                    ).forEachIndexed { index, latLng ->
                        intent.putExtra("lat_via_$index", latLng.latitude)
                        intent.putExtra("lon_via_$index", latLng.longitude)
                    }

                    try {
                        startActivity(intent)
                        mTargetMarker!!.isVisible = false
//                    mTargetMarker?.remove()
                    } catch(e: ActivityNotFoundException) {
                        Toast.makeText(context, "Yandex.Navi is not installed", Toast.LENGTH_LONG).show()
                    }
                }
            }
            .setNegativeButton("Cancel") { dialog, _ ->
                mTargetMarker!!.isVisible = false
//                mTargetMarker?.remove()
                dialog.cancel()
            }

        val alert = dialogBuilder.create()
        alert.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alert.setTitle("Go to marker?")
        alert.show()
    }

    private fun showGPSDialog() {
        val dialogBuilder = AlertDialog.Builder(ContextThemeWrapper(activity!!, R.style.dialog))

        dialogBuilder.setMessage("Включите GPS")
            .setCancelable(false)
            .setPositiveButton("Да") { _, _ ->
                activity?.startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
            }
            .setNegativeButton("Отмена") { dialog, _ ->
                dialog.cancel()
            }

        val alert = dialogBuilder.create()
        alert.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alert.setTitle("Включите GPS")
        alert.show()
    }

    @SuppressLint("MissingPermission")
    private fun buildRoute(targetLocation: LatLng, func: (() -> Unit)? = null) {
        mRoutePolyline?.let { it.remove() }
        mRoutePolygon?.let { it.remove() }

        val polylineOptions = PolylineOptions()
        val polygonOptions = PolygonOptions()
        var location: Location? = null

        activity?.runOnUiThread {
            location = mLocationManager?.getLastKnownLocation(provider)
        }

        async {
            location?.let {
                async {
                    api.getRoute(
                        LatLng(it.latitude, it.longitude),
                        targetLocation,
                        activity
                    ) { routePoints, polygonPoints ->
                        activity?.runOnUiThread {
                            polylineOptions.color(Color.GREEN)
                            polylineOptions.width(12f)
                            polylineOptions.jointType(JointType.ROUND)
                            polylineOptions.addAll(routePoints)

                            mRoutePolyline = mMap.addPolyline(polylineOptions)

                            if (BuildConfig.DEBUG) {
                                polygonOptions.strokeJointType(JointType.ROUND)
                                polygonOptions.addAll(polygonPoints)

                                mRoutePolygon = mMap.addPolygon(polygonOptions)
                            }

                            func?.let { it() }
                        }
                    }
                }
            }
        }
    }

    companion object {
        private val mMarkersOnMap = HashMap<Int, Marker>()

        fun newInstance(lat: Double, lng: Double): MapsFragment {
            val fragment = MapsFragment()
            val args = Bundle()
            args.putDouble("latitude", lat)
            args.putDouble("longitude", lng)
            fragment.arguments = args

            return fragment
        }

        var mRoutePolyline: Polyline? = null
        var mRoutePolygon: Polygon? = null
    }
}
