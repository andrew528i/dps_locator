import pytest

from user.models import User


def test_user_sign_in_view_200(client, user):
    resp = client.post('/user/sign_in', {'device_id': user.device_id})
    data = resp.json()

    assert resp.status_code == 200
    assert 'user' in data
    assert 'csrf' in data


@pytest.mark.parametrize('pack', [
    {},  # Empty
    {'device_id': 'some_shit'},  # Not matching regexp
    {'device_id': 'a'},  # Too short
    {'device_id': 'a' * 64}  # Too long
])
def test_user_sign_in_view_400(client, pack):
    resp = client.post('/user/sign_in')

    assert resp.status_code == 400


def test_user_sign_up_view_200(client, g):
    resp = client.post('/user/sign_in', {'device_id': g.cryptographic.uuid()})
    data = resp.json()

    assert resp.status_code == 200
    assert 'user' in data
    assert 'csrf' in data


@pytest.mark.parametrize('another_user', [True, False])
def test_get_user_info_view_200(client, user, user_factory, another_user):
    if another_user:
        user = user_factory()

    resp = client.get(f'/user/info/{user.id}')
    data = resp.json()

    assert resp.status_code == 200
    assert data['pk'] == user.id
    assert data['username'] == user.username


def test_get_user_info_view_400(client):
    resp = client.get('/user/info/999')
    data = resp.json()

    assert resp.status_code == 404
    assert 'not found' in data['detail']


@pytest.mark.parametrize('self_username', [True, False])
def test_set_user_info_view_200(client, user, self_username):
    username = 'new_test_username'

    if self_username:
        username = user.username

    resp = client.post('/user/info', {'username': username})
    data = resp.json()

    assert resp.status_code == 200
    assert data['username'] == username
    assert data['pk'] == user.pk
    assert User.objects.get(pk=user.pk).username == username


def test_set_user_info_view_400(client, user, user_factory):
    other_user = user_factory()
    username = other_user.username

    resp = client.post('/user/info', {'username': username})
    data = resp.json()

    assert resp.status_code == 400
    assert 'already taken' in data['detail']
