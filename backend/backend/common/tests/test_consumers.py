import json
import uuid
from unittest.mock import patch

import pytest
from channels.db import database_sync_to_async
from channels.testing import WebsocketCommunicator

from common.consumers import BaseConsumer
from common.packets import Packet
from user.packets import AuthenticatePacket


class PingPacket(Packet):

    def process(self, *args, **kwargs):
        return PingPacket(status=Packet.STATUS_SUCCESS)


class NoReturnPingPacket(Packet):

    def process(self, *args, **kwargs):
        pass


class AsyncPingPacket(Packet):

    async def process(self, *args, **kwargs):
        return AsyncPingPacket(status=Packet.STATUS_SUCCESS)


@pytest.fixture(params=[
    {'type': 'some_shit'},
    {'no_type': 'some_shit'},
])
def invalid_packet(request):
    return json.dumps(request.param)


@pytest.fixture
def invalid_uuid():
    return str(uuid.uuid4())


@pytest.fixture
@pytest.mark.asyncio
@pytest.mark.django_db
async def communicator(user_factory, request):
    class MyConsumer(BaseConsumer):
        packet_types = []

    allowed_packets = next((m for m in request.function.pytestmark if m.name == 'allowed_packets'), None)

    if allowed_packets:
        MyConsumer.packet_types = allowed_packets.args

    communicator = WebsocketCommunicator(MyConsumer, '/test/')
    connected, subprotocol = await communicator.connect()

    assert connected

    if 'authenticate_communicator' in request.keywords:
        user = await database_sync_to_async(user_factory)()
        packet = AuthenticatePacket(traffic_type=Packet.TYPE_TRAFFIC_INCOMING, device_id=user.device_id)
        await communicator.send_json_to(packet.serialize())
        data = await communicator.receive_json_from()
        packet = Packet.deserialize(traffic_type=Packet.TYPE_TRAFFIC_OUTCOMING, **data)

        assert packet.status == Packet.STATUS_SUCCESS
        assert packet.user.pk == user.pk

    yield communicator

    await communicator.disconnect()


@pytest.mark.asyncio
@pytest.mark.parametrize('authenticate', [True, False])
async def test_invalid_packet(invalid_packet, user_factory, communicator, authenticate):
    if authenticate:
        user = await database_sync_to_async(user_factory)()
        packet = AuthenticatePacket(traffic_type=Packet.TYPE_TRAFFIC_INCOMING, device_id=user.device_id)
        await communicator.send_json_to(packet.serialize())
        data = await communicator.receive_json_from()
        packet = Packet.deserialize(traffic_type=Packet.TYPE_TRAFFIC_OUTCOMING, **data)

        assert packet.status == Packet.STATUS_SUCCESS
        assert packet.user.pk == user.pk

    await communicator.send_to(text_data=invalid_packet)
    data = await communicator.receive_json_from()
    packet = Packet.deserialize(traffic_type=Packet.TYPE_TRAFFIC_OUTCOMING, **data)

    assert packet.status == Packet.STATUS_FAILURE


@pytest.mark.asyncio
async def test_authentication(user_factory, communicator):
    user = await database_sync_to_async(user_factory)()
    packet = AuthenticatePacket(traffic_type=Packet.TYPE_TRAFFIC_INCOMING, device_id=user.device_id)
    await communicator.send_json_to(packet.serialize())
    data = await communicator.receive_json_from()
    packet = Packet.deserialize(traffic_type=Packet.TYPE_TRAFFIC_OUTCOMING, **data)

    assert packet.status == Packet.STATUS_SUCCESS
    assert packet.user.pk == user.pk


@pytest.mark.asyncio
async def test_authentication_invalid_user(invalid_uuid, communicator):
    packet = AuthenticatePacket(traffic_type=Packet.TYPE_TRAFFIC_INCOMING, device_id=invalid_uuid)
    await communicator.send_json_to(packet.serialize())
    data = await communicator.receive_json_from()
    packet = Packet.deserialize(traffic_type=Packet.TYPE_TRAFFIC_OUTCOMING, **data)

    assert packet.status == Packet.STATUS_FAILURE
    assert 'no user found' in packet.message.lower()


@pytest.mark.asyncio
async def test_unauthorized_packet(communicator):
    packet = PingPacket()
    await communicator.send_json_to(packet.serialize())
    data = await communicator.receive_json_from()
    packet = Packet.deserialize(traffic_type=Packet.TYPE_TRAFFIC_OUTCOMING, **data)

    assert packet.status == Packet.STATUS_FAILURE
    assert 'authentication required' in packet.message.lower()


@pytest.mark.asyncio
@pytest.mark.authenticate_communicator
async def test_unallowed_packet(user_factory, communicator):
    packet = PingPacket()
    await communicator.send_json_to(packet.serialize())
    data = await communicator.receive_json_from()
    packet = Packet.deserialize(traffic_type=Packet.TYPE_TRAFFIC_OUTCOMING, **data)

    assert packet.status == Packet.STATUS_FAILURE
    assert 'is not allowed' in packet.message


@pytest.mark.asyncio
@pytest.mark.authenticate_communicator
@pytest.mark.allowed_packets(PingPacket.packet_type)
async def test_allowed_packet(communicator):
    packet = PingPacket()
    await communicator.send_json_to(packet.serialize())
    data = await communicator.receive_json_from()
    packet_in = Packet.deserialize(traffic_type=Packet.TYPE_TRAFFIC_OUTCOMING, **data)

    assert packet_in.packet_type == PingPacket.packet_type


@pytest.mark.asyncio
@pytest.mark.authenticate_communicator
@pytest.mark.allowed_packets(NoReturnPingPacket.packet_type)
async def test_packet_wo_process(communicator):
    packet = NoReturnPingPacket()
    await communicator.send_json_to(packet.serialize())
    data = await communicator.receive_json_from()
    packet_in = Packet.deserialize(traffic_type=Packet.TYPE_TRAFFIC_OUTCOMING, **data)

    assert packet_in.status == Packet.STATUS_FAILURE
    assert 'object has no attribute' in packet_in.message


@pytest.mark.asyncio
@pytest.mark.authenticate_communicator
@pytest.mark.allowed_packets(AsyncPingPacket.packet_type)
async def test_async_packet_process(communicator):
    packet = AsyncPingPacket()
    await communicator.send_json_to(packet.serialize())
    data = await communicator.receive_json_from()
    packet_in = Packet.deserialize(traffic_type=Packet.TYPE_TRAFFIC_OUTCOMING, **data)

    assert packet_in.packet_type == AsyncPingPacket.packet_type


@patch('common.utils.redis.redis_client')
def test_base_consumer_redis_client(patched_redis_client):
    class MyConsumer(BaseConsumer):

        pass

    consumer = MyConsumer({})
    assert consumer.redis_client == patched_redis_client
