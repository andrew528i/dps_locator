import json

import pytest
from mimesis import Generic, locales


@pytest.fixture(scope='session')
def g():
    return Generic(locales.EN)


@pytest.fixture
def client(client, user):
    client = _patch_post(client)
    client.force_login(user)
    return client


@pytest.fixture
def rf(rf):
    return _patch_post(rf)


def _patch_post(obj):
    original_post = obj.post

    def post(path, data=None, *args, **kwargs):
        kwargs['content_type'] = 'application/json'
        data = data or kwargs.get('data')

        return original_post(path, data, *args, **kwargs)

    obj.post = post

    return obj
