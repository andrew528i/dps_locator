package com.dps_locator.dpslocator

import android.Manifest
import android.app.Activity.RESULT_OK
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.preference.PreferenceFragmentCompat
import android.support.v7.preference.PreferenceManager
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.dps_locator.dpslocator.core.api
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import com.squareup.picasso.Picasso
import com.yalantis.ucrop.UCrop
import jp.wasabeef.picasso.transformations.RoundedCornersTransformation
import kotlinx.coroutines.async
import java.io.File
import java.util.*


class SettingsFragment : Fragment(), SharedPreferences.OnSharedPreferenceChangeListener {

    val REQUEST_GET_SINGLE_FILE = 1

    var avatarImage: ImageView? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_settings, container, false)
        val avatarImageView = view.findViewById<ImageView>(R.id.settings_avatar)

        MobileAds.initialize(activity, "ca-app-pub-3192553222268535~2630069321")
        val mAdView = view.findViewById<AdView>(R.id.adView)
        val adRequest = AdRequest.Builder().build()
        mAdView.loadAd(adRequest)

        fragmentManager
            ?.beginTransaction()
            ?.replace(R.id.preferences_fragment_container, PreferencesFragemnt.withParams(avatarImageView, activity!!))
            ?.commit()

        val toolbar = view.findViewById<Toolbar>(R.id.settings_toolbar)
        toolbar.setNavigationIcon(R.drawable.ic_menu)
        toolbar.setNavigationOnClickListener {
            activity?.findViewById<DrawerLayout>(R.id.drawer_layout)?.openDrawer(GravityCompat.START)
        }

        avatarImage = view.findViewById(R.id.settings_avatar)

//        Picasso.get()
//            .load("https://i.imgur.com/tGbaZCY.jpg")
//            .transform(RoundedCornersTransformation(250, 0))
//            .resize(400, 400)
//            .into(avatarImage)


        val pickImageTextView = view.findViewById<TextView>(R.id.pick_image)
        pickImageTextView.setOnClickListener {
            runWithPermissions(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA
            ) {
                val intent = Intent(Intent.ACTION_PICK)
                intent.type = "image/*"
                intent.putExtra(Intent.EXTRA_MIME_TYPES, arrayOf("image/jpeg", "image/png"))
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), REQUEST_GET_SINGLE_FILE)
            }
        }

        activity?.let {
            val preferences = PreferenceManager.getDefaultSharedPreferences(it)
            preferences.registerOnSharedPreferenceChangeListener(this)

            api.user?.let { user ->

                val editor = preferences.edit()
                editor.putString("username", user.username)
                editor.apply()
            }
        }

        return view
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        api.user?.let { user ->
            async {
                when (key) {
                    "username" -> api.updateUser(
                        sharedPreferences?.getString(key, null) ?: user.username)
                    "license_plate" -> api.updateUser(
                        sharedPreferences?.getString("username", null) ?: user.username,
                        sharedPreferences?.getString(key, null) ?: user.license_plate)
                    else -> { }
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        try {
            if (resultCode == RESULT_OK) {

                if (requestCode == REQUEST_GET_SINGLE_FILE) {
                    val selectedImageUri = data?.data
//                    val imagePath = getPathFromURI(selectedImageUri!!)
                    val destinationUri = Uri.fromFile(File(activity?.cacheDir, "avatar.jpg"))

                    val uCrop = UCrop.of(selectedImageUri!!, destinationUri)
                        .withAspectRatio(1f, 1f)
                        .withMaxResultSize(400, 400)

                    startActivityForResult(
                        uCrop.getIntent(activity!!.baseContext),
                        UCrop.REQUEST_CROP)
                }

                if (requestCode == UCrop.REQUEST_CROP) {
                    val resultUri = UCrop.getOutput(data!!)

                    async {
                        api.changeAvatar(File(resultUri?.path))
                    }
                }

                if (resultCode == UCrop.RESULT_ERROR) {
                    val cropError = UCrop.getError(data!!)
                }
            }
        } catch (e: Throwable) {}
    }
}


class PreferencesFragemnt: PreferenceFragmentCompat() {

    companion object {
        fun withParams(avatarImageView: ImageView, settingsActivity: FragmentActivity): PreferencesFragemnt {
            val preferencesFragment = PreferencesFragemnt()
            preferencesFragment.avatarImageView = avatarImageView
            preferencesFragment.settingsActivity = settingsActivity

            return preferencesFragment
        }
    }

    var avatarImageView: ImageView? = null
    var settingsActivity: FragmentActivity? = null

    private var updateTimer: Timer? = null

    override fun onCreatePreferences(p0: Bundle?, p1: String?) {
        addPreferencesFromResource(R.xml.app_preferences)

        class SettingsUpdateTask: TimerTask() {
            override fun run() {
                activity?.runOnUiThread {
                    api.user?.let {
                        findPreference("username").summary = it.username
                        findPreference("username").setDefaultValue(it.username)

                        if (it.license_plate.isEmpty()) {
                            findPreference("license_plate").summary = "E001KX777"
                        } else {
                            findPreference("license_plate").summary = it.license_plate
                        }

                        findPreference("license_plate").setDefaultValue(it.license_plate)

                        settingsActivity?.runOnUiThread {
                            Picasso.get()
                                .load(it.avatar)
                                .transform(RoundedCornersTransformation(250, 0))
                                .resize(400, 400)
//                          .memoryPolicy(MemoryPolicy.NO_CACHE)
                                .into(avatarImageView)
                        }
                    }
                }
            }
        }

        try {
            updateTimer?.cancel()
        } catch (e: Throwable) {}
        updateTimer = Timer()
        updateTimer?.scheduleAtFixedRate(SettingsUpdateTask(), 0, 250)
    }
}

