from django.core.cache import cache
from rest_framework import serializers

from chat.models import Message
from user.serializers.models import UserSerializer
from common.serializers import CreateTimeField


class MessageSerializer(serializers.ModelSerializer):

    author = UserSerializer()
    create_time = CreateTimeField()  # TODO: TimestampField

    class Meta:

        model = Message
        fields = ('pk', 'author', 'body', 'create_time')

    def to_internal_value(self, data):
        cache_key = f'message:{data["pk"]}'
        message = cache.get(cache_key)

        if not message:
            data = {k: v['pk'] if isinstance(v, dict) else v for k, v in data.items()}  # TODO: check model for reference
            data.pop('create_time', None)
            message = Message.objects.get(**data)
            cache.set(cache_key, message, 300)

        return message


