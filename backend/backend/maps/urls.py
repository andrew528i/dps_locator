from django.urls import path

from maps.views import BuildRouteView, SuggestView, PlaceView


urlpatterns = [
    path('build_route', BuildRouteView.as_view()),
    path('suggest', SuggestView.as_view()),
    path('place', PlaceView.as_view()),
]
