from datetime import timedelta

from django.db.models import Q
from django.contrib.gis.geos import Point
from django.contrib.gis.measure import Distance
from rest_framework.response import Response
from rest_framework.generics import ListAPIView
from rest_framework.pagination import CursorPagination
from rest_framework.exceptions import APIException

from common.views import BaseApiView
from marker.models import Marker, Comment
from marker.serializers.models import MarkerSerializer, CommentSerializer
from marker.serializers.views import NearMarkerViewSerializer
from user.models import User
from user.serializers.models import UserSerializer


class AddMarkerView(BaseApiView):

    def post(self, request):
        location = request.validated_data['location']
        marker_type = request.validated_data['type']
        options = request.validated_data['options']
        point = Point(location['longitude'], location['latitude'], srid=4326)
        near_markers = Marker.objects.filter(location__dwithin=(point, Distance(m=100)), type=marker_type)

        if near_markers.count():
            marker: Marker = near_markers.first()
            marker.seen_count += 1
            marker.save()
        else:
            marker = Marker.objects.create(location=point, type=marker_type, user=request.user, options=options)

        return Response(MarkerSerializer(marker).data)


class MarkerView(BaseApiView):

    def get(self, request, marker_pk):
        try:
            marker_pk = marker_pk or request.validated_data.get('pk')
            marker = Marker.objects.get(pk=marker_pk)

            return Response(MarkerSerializer(marker).data)
        except Marker.DoesNotExist:
            raise APIException('no_marker', 400)


class MarkerPagination(CursorPagination):

    ordering = '-modify_time'
    page_size = 250


class NearMarkerListView(ListAPIView):

    serializer_class = MarkerSerializer
    pagination_class = MarkerPagination

    def get_queryset(self):
        serializer = NearMarkerViewSerializer(data=self.request.query_params)
        serializer.is_valid(raise_exception=True)
        latitude = serializer.validated_data['latitude']
        longitude = serializer.validated_data['longitude']
        distance = serializer.validated_data['distance']

        last_police_spawn_time = Marker.objects.filter(type=Marker.TYPE_POLICE).order_by('-spawn_time')[0].spawn_time
        marker_ids = Marker.objects.filter(
            Q(spawn_time__gte=last_police_spawn_time - timedelta(hours=8)) |
            Q(seen_count__gte=40, type=Marker.TYPE_POLICE) |
            ~Q(type=Marker.TYPE_POLICE)
        ).values_list('id', flat=True)
        point = Point(longitude, latitude)
        marker_query = Marker.objects.filter(location__distance_lt=(point, Distance(m=distance)), pk__in=marker_ids)

        return marker_query


class AddCommentView(BaseApiView):

    def post(self, request, marker_pk):
        marker = Marker.objects.get(id=marker_pk)
        body = request.validated_data['body']
        comment = Comment.objects.create(marker=marker, user=request.user, body=body)

        return Response(CommentSerializer(comment).data)


class CommentListView(ListAPIView):

    serializer_class = CommentSerializer

    def get_queryset(self):
        return Comment.objects.filter(marker=self.kwargs['marker_pk'])


class LeaderBoardView(ListAPIView):

    class Pagination(CursorPagination):
        ordering = '-velocity_rating'
        page_size = 50

    serializer_class = UserSerializer
    pagination_class = Pagination

    def get_queryset(self):
        return User.objects.filter(velocity_rating__gt=0)
