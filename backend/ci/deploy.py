import os

from telethon import TelegramClient, events, sync
from loguru import logger


session_name = 'dps_locator'
api_id = 358277
api_hash = 'd5b5626597cf278c0bb2f885c8294a2e'
chat_id = 1346702182
commit_message = os.getenv('CI_COMMIT_TITLE', 'no commit message')
commit_sha = os.getenv('CI_COMMIT_SHORT_SHA', 'no commit sha')


if __name__ == '__main__':
    client = TelegramClient(session_name, api_id, api_hash)
    client.connect()

    if not client.is_connected():
        logger.error('Failed to auth Telegram client')
        quit()

    client.send_file(chat_id, '/tmp/app.apk')
    client.send_message(chat_id, f'{commit_sha}\n\n**{commit_message}**')
    client.disconnect()
