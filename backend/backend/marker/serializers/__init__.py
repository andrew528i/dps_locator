from rest_framework import serializers
from django.contrib.gis.geos import Point


class LocationFeild(serializers.Serializer):

    latitude = serializers.FloatField()
    longitude = serializers.FloatField()


class PointField(serializers.Serializer):

    def to_representation(self, instance):
        lng, lat = instance

        return {'lat': lat, 'lng': lng}

    def to_internal_value(self, data):
        lat = data['lat']
        lng = data['lng']

        return Point(lng, lat)

