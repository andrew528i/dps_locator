from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter
from django.urls import path, include

from chat.urls import ws_urlpatterns as chat_urlpatters
from marker.urls import ws_urlpatterns as marker_urlpatters


application = ProtocolTypeRouter({
    'websocket': AuthMiddlewareStack(URLRouter(chat_urlpatters + marker_urlpatters)),
})
