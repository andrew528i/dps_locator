from rest_framework import serializers

from common.packets import Packet
from user.serializers.models import UserSerializer


class BasePacketSerializer(serializers.Serializer):

    type = serializers.CharField(source='packet_type')
    uuid = serializers.UUIDField()
    parent_uuid = serializers.UUIDField(required=False, allow_null=True)


class BasePacketOutSerializer(BasePacketSerializer):

    user = UserSerializer(required=False)
    timestamp = serializers.IntegerField()
    status = serializers.ChoiceField(choices=Packet.statuses())
    message = serializers.CharField(required=False)


class BasePacketInSerializer(BasePacketSerializer):

    pass


class BasePingPacketSerializer(BasePacketSerializer):

    timestamp = serializers.IntegerField(required=False)


class BasePingPacketOutSerializer(BasePingPacketSerializer):

    status = serializers.ChoiceField(choices=Packet.statuses())


class BasePingPacketInSerializer(BasePingPacketSerializer):

    pass
