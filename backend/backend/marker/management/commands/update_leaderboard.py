import collections

from django.core.management.base import BaseCommand
from django.db import transaction
from loguru import logger

from marker.models import Marker
from user.models import User


class Command(BaseCommand):

    def __init__(self, *args, **kwargs):
        super(Command, self).__init__(*args, *kwargs)

        self.user_rating = collections.defaultdict(int)

    def handle(self, *args, **options):
        for marker in Marker.objects.filter(type=Marker.TYPE_SPEED):
            leaders = marker.leaders
            rating_points = 10

            logger.info(f'Marker [{marker.pk}] has {len(leaders)} leaders')

            for i in range(10):
                if not leaders:
                    break

                leader = leaders.pop(0)
                user = leader['user']
                self.user_rating[user.pk] += rating_points
                rating_points -= 1

        self.sync_rating()

    def sync_rating(self):
        if not self.user_rating:
            return

        with transaction.atomic():
            for user in User.objects.filter():
                user.velocity_rating = self.user_rating[user.pk]
                user.save()
