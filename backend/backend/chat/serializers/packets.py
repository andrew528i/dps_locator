from rest_framework import serializers
from rest_framework.compat import MinValueValidator, MaxValueValidator

from chat.serializers.models import MessageSerializer
from common.serializers.packets import BasePacketOutSerializer, BasePacketInSerializer
from user.serializers.models import MessageSerializer as UserMessageSerializer


class GetChatMessagesPacketOutSerializer(BasePacketOutSerializer):

    messages = MessageSerializer(many=True, required=False)


class GetChatMessagesPacketInSerializer(BasePacketInSerializer):

    limit = serializers.IntegerField(default=100, validators=[MinValueValidator(100), MaxValueValidator(250)])
    offset = serializers.IntegerField(default=0, validators=[MinValueValidator(0), MaxValueValidator(5000)])


class SendChatMessagePacketOutSerializer(BasePacketOutSerializer):

    pass  # Only status needed


class SendChatMessagePacketInSerializer(BasePacketInSerializer):

    message_body = serializers.CharField(max_length=512)


class GetCommonChatInfoPacketOutSerializer(BasePacketOutSerializer):

    online_count = serializers.IntegerField()
    name = serializers.CharField()


class GetCommonChatInfoPacketInSerializer(BasePacketInSerializer):

    pass


# User messages

class GetPrivateChatMessagesPacketOutSerializer(BasePacketOutSerializer):

    messages = UserMessageSerializer(many=True, required=False)


class GetPrivateChatMessagesPacketInSerializer(BasePacketInSerializer):

    limit = serializers.IntegerField(default=100, validators=[MinValueValidator(100), MaxValueValidator(1000)])
    offset = serializers.IntegerField(default=0, validators=[MinValueValidator(0), MaxValueValidator(5000)])
    thread = serializers.CharField()


class SendPrivateChatMessagePacketOutSerializer(BasePacketOutSerializer):

    pass  # Only status needed


class SendPrivateChatMessagePacketInSerializer(BasePacketInSerializer):

    message_body = serializers.CharField(max_length=512)
    thread = serializers.CharField()
