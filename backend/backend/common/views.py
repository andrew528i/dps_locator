import importlib
from importlib import util as importlib_util

from rest_framework.views import APIView
from rest_framework.pagination import CursorPagination


class BaseApiView(APIView):

    def __init_subclass__(cls, **kwargs):
        if hasattr(cls, 'post'):
            cls.request_post_serializer_class = cls._get_serializer('post')
            original_post = cls.post

            def post(self, request, *args, **kwargs_):
                serializer = cls.request_post_serializer_class(data=request.data)
                serializer.is_valid(raise_exception=True)
                request.validated_data = serializer.validated_data

                return original_post(self, request, *args, **kwargs_)

            cls.post = post

        if hasattr(cls, 'get'):
            cls.request_get_serializer_class = cls._get_serializer('get')
            original_get = cls.get

            def get(self, request, *args, **kwargs_):
                serializer = cls.request_get_serializer_class(data=request.query_params)
                serializer.is_valid(raise_exception=True)
                request.validated_data = serializer.validated_data

                return original_get(self, request, *args, **kwargs_)

            cls.get = get

    @classmethod
    def _find_serialiers_package(cls):
        """
        Finds serializers.views module in the same django app as a cls
        :return:
        """
        module = cls.__module__

        def check(path):
            serializers_module = '.'.join([path, 'serializers', 'views'])

            try:
                found = importlib_util.find_spec(serializers_module)
            except ModuleNotFoundError:
                found = None

            if found:
                return serializers_module

        module_path = None

        for m in module.split('.'):
            if module_path:
                module_path = '.'.join([module_path, m])
            else:
                module_path = m

            result = check(module_path)

            if result:
                return result

    @classmethod
    def _get_serializer(cls, method: str):
        """
        Returns serializer class based on current class name
        :type method: GET, POST
        :return:
        """
        classname = cls.__name__
        serializers_package_path = cls._find_serialiers_package()
        serializer_class = None

        if serializers_package_path:
            serializers_package = importlib.import_module(serializers_package_path)
            serializer_classname = '{}{}{}'.format(classname, method.capitalize(), 'Serializer')
            serializer_class = getattr(serializers_package, serializer_classname, None)

        if not serializer_class:
            raise NotImplementedError(f'Serializer class not found for view {classname} and method {method}')

        return serializer_class

    def get_ip_address(self, request):
        x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')

        if x_forwarded_for:
            ip_address = x_forwarded_for.split(',')[-1].strip()
        else:
            ip_address = request.META.get('REMOTE_ADDR')

        return ip_address


class Pagination(CursorPagination):

    ordering = '-create_time'
    page_size = 50
