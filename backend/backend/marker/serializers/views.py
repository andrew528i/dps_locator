import operator

from rest_framework import serializers

from marker.serializers import LocationFeild
from marker.models import Marker


class AddMarkerViewPostSerializer(serializers.Serializer):

    location = LocationFeild()
    type = serializers.ChoiceField(choices=[k for k, _ in Marker.TYPES], default=Marker.TYPE_ALL, required=False)
    options = serializers.DictField(default=dict, required=False)


class MarkerViewGetSerializer(serializers.Serializer):

    pk = serializers.IntegerField(required=False)


class NearMarkerViewSerializer(serializers.Serializer):

    distance = serializers.IntegerField(default=40000, required=False)
    latitude = serializers.FloatField()
    longitude = serializers.FloatField()
    type = serializers.ChoiceField(choices=map(operator.itemgetter(1), Marker.TYPES), default=Marker.TYPE_ALL, required=False)


class AddCommentViewPostSerializer(serializers.Serializer):

    body = serializers.CharField(max_length=1024, required=True)
