from datetime import datetime

from channels.db import database_sync_to_async
from django.conf import settings

from common.packets import Packet
from common.utils.mongo import mongo_client


class LogGPSCoordsPacket(Packet):

    async def process(self, *args, **kwargs):
        if self.velocity:
            location = {
                'type': 'Point',
                'coordinates': [self.longitude, self.latitude],
            }
            user = kwargs['consumer'].user
            gps_log_entry = {
                'location': location,
                'velocity': self.velocity,
                'user_id': user.pk,
                'create_time': datetime.now()
            }
            await database_sync_to_async(mongo_client[settings.MONGO_LOGS_GPS_COLLECTION].insert_one)(gps_log_entry)

            return LogGPSCoordsPacket(traffic_type=Packet.TYPE_TRAFFIC_OUTCOMING, status=Packet.STATUS_SUCCESS)

        return LogGPSCoordsPacket(traffic_type=Packet.TYPE_TRAFFIC_OUTCOMING, status=Packet.STATUS_FAILURE, message='zero velocity')
