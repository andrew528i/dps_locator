from django.core.management.base import BaseCommand
from django.contrib.gis.geos import Point
from loguru import logger

from marker.models import Marker
from user.models import User


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('--lng', type=float)
        parser.add_argument('--lat', type=float)

    def handle(self, *args, **options):
        point = Point(options['lng'], options['lat'])
        user = User.objects.get(pk=1)
        marker = Marker.objects.create(type=Marker.TYPE_SPEED, location=point, user=user)
        logger.info(f'Marker added: {marker}')
