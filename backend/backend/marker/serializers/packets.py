from rest_framework import serializers

from common.serializers.packets import BasePacketOutSerializer, BasePacketInSerializer


class LogGPSCoordsPacketOutSerializer(BasePacketOutSerializer):

    pass


class LogGPSCoordsPacketInSerializer(BasePacketInSerializer):

    latitude = serializers.FloatField()
    longitude = serializers.FloatField()
    velocity = serializers.FloatField()
