import re

from django.core.validators import RegexValidator, MinLengthValidator, MaxLengthValidator
from rest_framework import serializers


class UserSignInViewPostSerializer(serializers.Serializer):

    device_id = serializers.CharField(validators=[
            RegexValidator('^[a-fA-F0-9-]*$'),
            MinLengthValidator(10),
            MaxLengthValidator(40),
        ]
    )


class UserInfoViewGetSerializer(serializers.Serializer):

    pass


class PlateNumberField(serializers.Field):

    LICENSE_PLATE_REGEXP = r'([ABCEHKMOPTXYа-я]{1}[0-9]{3}[ABCEHKMOPTXYа-я]{2}[0-9]{2,3})'
    TRANSLATE_TABLE = str.maketrans({
        'А': 'A',
        'В': 'B',
        'С': 'C',
        'Е': 'E',
        'Н': 'H',
        'К': 'K',
        'М': 'M',
        'О': 'O',
        'Р': 'P',
        'Т': 'T',
        'Х': 'X',
        'У': 'Y',
    })

    def _check_license_plate_number(self, number: str) -> str:
        number = number.upper()
        match = re.match(self.LICENSE_PLATE_REGEXP, number.translate(self.TRANSLATE_TABLE))

        if match and match.groups():
            return match.groups()[0]

    def to_internal_value(self, data: str) -> str:
        number = self._check_license_plate_number(data)

        if not number:
            raise serializers.ValidationError('Not valid license plate number')

        return number

    def to_representation(self, value: str) -> str:
        number = self._check_license_plate_number(value)

        if not number:
            raise serializers.ValidationError('Not valid license plate number')

        return number


class UserInfoViewPostSerializer(serializers.Serializer):

    username = serializers.CharField()
    license_plate = PlateNumberField(required=False)


class UploadAvatarViewPostSerializer(serializers.Serializer):

    image = serializers.ImageField()
