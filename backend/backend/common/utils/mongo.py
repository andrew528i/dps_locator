from pymongo import MongoClient
from django.conf import settings


mongo_client = None

if mongo_client is None:
    mongo_client = MongoClient(settings.MONGO_HOST, settings.MONGO_PORT, maxPoolSize=settings.MONGO_POOL_SIZE)
    mongo_client = mongo_client[settings.MONGO_DB]
