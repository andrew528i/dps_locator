import redis
from django.conf import settings


pool = None

if pool is None:
    pool = redis.ConnectionPool(
        host=settings.REDIS_HOST,
        port=settings.REDIS_PORT,
        db=settings.REDIS_INTERNAL_DB,
        max_connections=settings.REDIS_POOL_SIZE,
    )

redis_client = redis.StrictRedis(connection_pool=pool)
