package com.dps_locator.dpslocator

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.dps_locator.dpslocator.core.API_HOST
import com.dps_locator.dpslocator.core.api
import com.squareup.picasso.Picasso
import jp.wasabeef.picasso.transformations.RoundedCornersTransformation
import kotlinx.coroutines.async


class LeaderboardFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_leaderboard, container, false)

        val toolbar = view.findViewById<Toolbar>(R.id.leader_toolbar)
        toolbar.setNavigationIcon(R.drawable.ic_menu)
        toolbar.setNavigationOnClickListener {
            activity?.findViewById<DrawerLayout>(R.id.drawer_layout)?.openDrawer(GravityCompat.START)
        }

        val leaderLayout = view.findViewById<LinearLayout>(R.id.leader_layout)

        async {
            api.getLeaders { user ->
                activity?.runOnUiThread {
                    val velocityRating = user.getDouble("velocity_rating")

                    val leaderItemView = layoutInflater.inflate(R.layout.leaderboard_item_layout, null)
                    leaderItemView.findViewById<ImageView>(R.id.leader_avatar).let {
                        val avatar = user.getString("avatar")
                        Picasso.get()
                            .load(avatar)
                            .transform(RoundedCornersTransformation(250, 0))
                            .resize(300, 300)
                            .into(it)
                    }
                    leaderItemView.findViewById<TextView>(R.id.leader_username).text = user.getString("username")
                    leaderItemView.findViewById<TextView>(R.id.leader_rating).text = "$velocityRating pts"
                    leaderItemView.setOnClickListener {
                        MainActivity.replaceFragment(UserFragment.newInstance(user.getInt("pk"), "leaderboard"))
                    }
                    leaderLayout.addView(leaderItemView)
                }
            }
        }

        return view
    }
}
