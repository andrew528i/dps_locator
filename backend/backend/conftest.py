import warnings

import pytest
from django.core.cache import cache

from common.tests.conftest import client, rf, g
from user.tests.fixtures import user, user_factory
from marker.tests.conftest import make_marker


@pytest.fixture(autouse=True)
def disable_cache():
    cache.clear()
    yield
    cache.clear()


warnings.filterwarnings('ignore', message='DateTimeField', category=RuntimeWarning)
