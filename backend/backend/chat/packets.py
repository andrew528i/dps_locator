from datetime import datetime, timedelta
from typing import Optional, List

from asgiref.sync import async_to_sync
from channels.db import database_sync_to_async
from channels.layers import get_channel_layer

from common.packets import Packet
from common.consumers import BaseConsumer
from chat.models import Message
from user.models import Message as UserMessage, User


class GetChatMessagesPacket(Packet):

    def process(self, *args, **kwargs):
        offset = self.offset
        limit = self.limit
        messages = Message.objects.filter(room=self.room).order_by('-id')[offset:offset + limit][::-1]

        return GetChatMessagesPacket(traffic_type=Packet.TYPE_TRAFFIC_OUTCOMING, status=Packet.STATUS_SUCCESS, messages=messages)


class SendChatMessagePacket(Packet):

    MESSAGE_LIMIT = 15
    LIMIT_PERIOD = 5  # in minutes
    DENIED_MESSAGES = ['text']

    def process(self, *args, **kwargs):
        recent_messages_count = Message.objects.filter(
            author=self.user,
            create_time__gte=datetime.now() - timedelta(minutes=self.LIMIT_PERIOD),
        ).count()

        if recent_messages_count > self.MESSAGE_LIMIT or self.message_body in self.DENIED_MESSAGES:
            return SendChatMessagePacket(traffic_type=Packet.TYPE_TRAFFIC_OUTCOMING, status=Packet.STATUS_FAILURE, message='calm_down')

        Message.objects.create(author=self.user, room=self.room, body=self.message_body)

        async_to_sync(get_channel_layer().group_send)(f'common_chat_{self.room.pk}', {'type': 'update_messages'})

        return SendChatMessagePacket(traffic_type=Packet.TYPE_TRAFFIC_OUTCOMING, status=Packet.STATUS_SUCCESS)


class CommonChatPingPacket(Packet):

    async def process(self, *args, **kwargs):
        consumer: Optional[BaseConsumer] = kwargs.get('consumer')

        if consumer:
            await database_sync_to_async(consumer.redis_client.setex)(f'common_chat:{consumer.room.pk}:{consumer.user.pk}', 30, 1)
            return CommonChatPingPacket(traffic_type=Packet.TYPE_TRAFFIC_OUTCOMING, status=Packet.STATUS_SUCCESS)


class GetCommonChatInfoPacket(Packet):

    async def process(self, *args, **kwargs):
        consumer: Optional[BaseConsumer] = kwargs.get('consumer')

        if consumer:
            online_count = await consumer.online_count()

            return GetCommonChatInfoPacket(
                traffic_type=Packet.TYPE_TRAFFIC_OUTCOMING,
                online_count=online_count,
                name=consumer.room.name,
                status=Packet.STATUS_SUCCESS,
            )


# User message packets

class GetPrivateChatMessagesPacket(Packet):

    async def process(self, *args, **kwargs):
        offset = self.offset
        limit = self.limit
        # consumer = kwargs['consumer']
        # chat_user = await consumer.get_user(pk=self.user_id)
        # user = consumer.user
        # thread = self.thread  # UserMessage.get_thread(user.pk, chat_user.pk)
        messages = UserMessage.objects.filter(thread=self.thread).order_by('-id')[offset:offset + limit][::-1]

        return GetPrivateChatMessagesPacket(traffic_type=Packet.TYPE_TRAFFIC_OUTCOMING, status=Packet.STATUS_SUCCESS, messages=messages)


class SendPrivateChatMessagePacket(Packet):

    MESSAGE_LIMIT = 150
    LIMIT_PERIOD = 5  # in minutes
    DENIED_MESSAGES = ['text']

    def get_chat_user_ids(self, user: User, thread: str) -> List[int]:
        splited_thread = thread.split('_')
        user_ids = []

        for part in splited_thread:
            try:
                user_id = int(part)

                if user_id != user.pk:
                    user_ids.append(user_id)
            except ValueError:
                continue

        return user_ids

    async def process(self, *args, **kwargs):
        consumer = kwargs['consumer']
        chat_user_ids = self.get_chat_user_ids(consumer.user, self.thread)
        user = consumer.user
        recent_messages_count = UserMessage.objects.filter(
            author=user,
            thread=self.thread,
            create_time__gte=datetime.now() - timedelta(minutes=self.LIMIT_PERIOD),
        ).count()

        if recent_messages_count > self.MESSAGE_LIMIT or self.message_body in self.DENIED_MESSAGES:
            return SendPrivateChatMessagePacket(
                traffic_type=Packet.TYPE_TRAFFIC_OUTCOMING,
                status=Packet.STATUS_FAILURE,
                message='calm_down',
            )

        UserMessage.objects.create(author=user, thread=self.thread, body=self.message_body, user_ids=[user.pk, *chat_user_ids])

        await get_channel_layer().group_send(consumer.group_name(user.pk), {'type': 'update_messages', 'thread': self.thread})

        for user_id in chat_user_ids:
            await get_channel_layer().group_send(consumer.group_name(user_id), {'type': 'update_messages', 'thread': self.thread})

        return SendPrivateChatMessagePacket(traffic_type=Packet.TYPE_TRAFFIC_OUTCOMING, status=Packet.STATUS_SUCCESS)
