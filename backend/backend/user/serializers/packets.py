from django.core.validators import RegexValidator
from rest_framework import serializers
from rest_framework.compat import MinLengthValidator, MaxLengthValidator

from common.serializers.packets import BasePacketOutSerializer, BasePacketInSerializer


class AuthenticatePacketOutSerializer(BasePacketOutSerializer):

    pass


class AuthenticatePacketInSerializer(BasePacketInSerializer):

    device_id = serializers.CharField(validators=[
            RegexValidator('^[a-fA-F0-9-]*$'),
            MinLengthValidator(10),
            MaxLengthValidator(40),
        ]
    )
