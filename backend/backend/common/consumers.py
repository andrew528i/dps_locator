from typing import Optional
import inspect

from channels.generic.websocket import AsyncJsonWebsocketConsumer
from channels.db import database_sync_to_async
from redis import StrictRedis

from common.packets import Packet
from user.packets import AuthenticatePacket
from user.models import User


class BaseConsumer(AsyncJsonWebsocketConsumer):

    def __init_subclass__(cls, **kwargs):
        packet_types = getattr(cls, 'packet_types', [])
        packet_types.append(AuthenticatePacket.packet_type)
        cls.packet_types = packet_types

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.user = None

    @property
    def redis_client(self) -> Optional[StrictRedis]:
        from common.utils.redis import redis_client

        return redis_client

    @database_sync_to_async
    def get_user(self, **kwargs):
        return User.objects.get(**kwargs)

    async def receive_json(self, content, **kwargs):
        try:
            packet = Packet.deserialize(**content)
        except Exception as e:
            packet = AuthenticatePacket(status=AuthenticatePacket.STATUS_FAILURE, message=e, parent_uuid=content.get('uuid'))
            await self.send_packet(packet)
        else:
            if packet.packet_type == AuthenticatePacket.packet_type:
                try:
                    self.user = await self.get_user(device_id=packet.device_id)
                    packet = AuthenticatePacket(status=AuthenticatePacket.STATUS_SUCCESS, user=self.user, parent_uuid=packet.uuid)
                    await self.send_packet(packet)
                    await self.authentication_success()
                except User.DoesNotExist:
                    packet = AuthenticatePacket(status=AuthenticatePacket.STATUS_FAILURE, message='No user found', parent_uuid=packet.uuid)
                    await self.send_packet(packet)
            else:
                if self.user:
                    await self.receive_packet(packet)
                else:
                    packet = AuthenticatePacket(
                        status=AuthenticatePacket.STATUS_FAILURE,
                        message='Authentication required',
                        parent_uuid=packet.uuid,
                    )
                    await self.send_packet(packet)

    async def send_packet(self, packet: Packet):
        await self.send_json(packet.serialize())

    async def receive_packet(self, packet: Packet):
        if packet.packet_type not in self.packet_types:
            packet = AuthenticatePacket(status=Packet.STATUS_FAILURE, message=f'Packet with type {packet.packet_type} is not allowed')
            await self.send_packet(packet)
        else:
            try:
                packet.user = self.user
                kwargs = {'consumer': self}

                if inspect.iscoroutinefunction(packet.process):
                    result = await packet.process(**kwargs)
                else:
                    result = await database_sync_to_async(packet.process)(**kwargs)

                result.parent_uuid = packet.uuid
                await self.send_packet(result)
            except Exception as e:  # 'NoneType' object has no attribute 'parent_uuid' becuase result is None from packet.process
                packet = AuthenticatePacket(status=Packet.STATUS_FAILURE, message=e)
                await self.send_packet(packet)
                # TODO: log e

    async def authentication_success(self):
        pass
