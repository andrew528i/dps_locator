from rest_framework import serializers
from django.core.cache import cache

from user.models import User, Message
from common.serializers import CreateTimeField


class UserSerializer(serializers.ModelSerializer):

    class Meta:

        model = User
        fields = ('pk', 'username', 'email', 'avatar', 'velocity_rating', 'license_plate')

    def to_internal_value(self, data):
        cache_key = f'user:{data["pk"]}'

        user = cache.get(cache_key)

        if not user:
            data.pop('avatar', None)
            user = User.objects.get(**data)
            cache.set(cache_key, user, 300)

        return user


class MessageSerializer(serializers.ModelSerializer):

    author = UserSerializer()
    create_time = CreateTimeField()  # TODO: TimestampField

    class Meta:

        model = Message
        fields = ('pk', 'author', 'body', 'create_time', 'thread')

    def to_internal_value(self, data):
        cache_key = f'user_message:{data["pk"]}'
        message = cache.get(cache_key)

        if not message:
            data = {k: v['pk'] if isinstance(v, dict) else v for k, v in data.items()}  # TODO: check model for reference
            data.pop('create_time', None)
            message = Message.objects.get(**data)
            cache.set(cache_key, message, 300)

        return message


