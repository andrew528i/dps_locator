from datetime import datetime, timedelta
import math
from typing import NewType, Tuple

from rest_framework.exceptions import APIException
from rest_framework.response import Response
from rest_framework import status
from django.contrib.gis.geos import Polygon
from django.conf import settings
from googlemaps.convert import decode_polyline
from shapely.geometry import LineString
from geoip2.errors import AddressNotFoundError
import geoip2.database

from common.views import BaseApiView
from common.utils.maps import GMapsClient
from marker.models import Marker


geoip_reader = geoip2.database.Reader(settings.GEOIP_DATABASE_PATH)
Coord = NewType('Coord', Tuple[float, float])


class RouteDataError(APIException):

    status_code = status.HTTP_400_BAD_REQUEST
    default_detail = 'Invalid route data'
    default_code = 'invalid_route_data'


class BuildRouteError(APIException):

    status_code = status.HTTP_400_BAD_REQUEST
    default_detail = 'Cannot build route'
    default_code = 'build_route_error'


def calculate_distance(start: Coord, end: Coord) -> float:
    """
    Calculates distance in meters between two latlng points on map
    :param start:
    :param end:
    :return:
    """
    r = 6372800

    if not all(map(lambda x: isinstance(x, tuple), [start, end])):
        return 0

    lat1, lon1 = start
    lat2, lon2 = end

    phi1, phi2 = math.radians(lat1), math.radians(lat2)
    dphi = math.radians(lat2 - lat1)
    dlambda = math.radians(lon2 - lon1)

    a = math.sin(dphi / 2) ** 2 + math.cos(phi1) * math.cos(phi2) * math.sin(dlambda / 2) ** 2

    return 2 * r * math.atan2(math.sqrt(a), math.sqrt(1 - a))


class BuildRouteView(BaseApiView):

    def post(self, request):
        data = request.validated_data
        data['alternatives'] = True
        distance_between_points = calculate_distance(data['origin'], data['destination'])

        if distance_between_points > 40000:
            return Response({'error': 'Max allowed distance: 40km'})

        routes = GMapsClient.directions(**data)
        route_map = {}
        marker_query = Marker.objects.filter(type=Marker.TYPE_POLICE).order_by('-spawn_time')
        last_spawn_time = marker_query[0].spawn_time if marker_query else datetime.now()
        route = 0

        for idx, route in enumerate(routes):
            coords = []
            seen = set()

            for p in route['legs'][0]['steps']:
                decoded_polyline = [(c['lng'], c['lat']) for c in decode_polyline(p['polyline']['points'])]
                coords.extend(decoded_polyline)

            coords = [c for c in coords if not (c in seen or seen.add(c))]
            line_string = LineString(coords)

            try:
                polygon = [{'longitude': p[0], 'latitude': p[1]} for p in line_string.buffer(0.0000625).boundary.coords]
            except NotImplementedError:
                polygon = [{'longitude': p[0], 'latitude': p[1]} for p in line_string.buffer(0.0000625).exterior.coords]

            # for c in coords:
            #     print(f'<Point><coordinates>{c[0]},{c[1]},0</coordinates></Point>')

            coords = [{'latitude': c[1], 'longitude': c[0]} for c in coords]
            markers = Marker.objects.filter(
                spawn_time__gte=last_spawn_time - timedelta(days=2),
                location__intersects=Polygon([(p['longitude'], p['latitude']) for p in polygon]),
                type=Marker.TYPE_POLICE,
            )

            route_map['route_{}'.format(idx)] = {
                'markers_count': markers.count(),
                'coords': coords,
                'polygon': polygon,
            }

        if not route_map:
            raise BuildRouteError

        min_markers_count = min(c['markers_count'] for c in route_map.values())

        for r, c in route_map.items():
            if c['markers_count'] == min_markers_count:
                route = r
                break

        print(route, {r: c['markers_count'] for r, c in route_map.items()})

        return Response(route_map[route])


class SuggestView(BaseApiView):

    def get(self, request):
        address = request.validated_data['address']
        location = request.validated_data['location']
        ip_address = self.get_ip_address(request)

        try:
            geoip_response = geoip_reader.city(ip_address)
            country_code = geoip_response.country.iso_code.lower()
        except AddressNotFoundError:
            country_code = 'ru'

        result = GMapsClient.places_autocomplete_query(address, location=location) or []

        return Response({
            'results': [{
                'description': r.get('description', ''),
                'place_id': r.get('place_id', {}),
            } for r in result][:4]
        })


class PlaceView(BaseApiView):

    def get(self, request):
        place_id = request.validated_data['place_id']

        result = GMapsClient.place(place_id)

        return Response(result['result']['geometry']['location'])
