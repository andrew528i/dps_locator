import pytest
from channels.testing import WebsocketCommunicator
from channels.db import database_sync_to_async
from mock import patch

from common.packets import Packet
from chat.consumers import ChatConsumer, PrivateChatConsumer
from chat.packets import GetChatMessagesPacket, SendChatMessagePacket, GetPrivateChatMessagesPacket, SendPrivateChatMessagePacket
from user.packets import AuthenticatePacket
from user.models import Message as UserMessage


@pytest.fixture
async def room(room_factory):
    return await database_sync_to_async(room_factory)()


@pytest.fixture
async def user(user_factory):
    return await database_sync_to_async(user_factory)()


@pytest.fixture
async def chat_user(user_factory):
    return await database_sync_to_async(user_factory)()


@pytest.fixture
async def message(message_factory, user, room):
    return await database_sync_to_async(message_factory)(user, room)


@pytest.fixture
async def user_message(user_message_factory, user, chat_user):
    return await database_sync_to_async(user_message_factory)(user, chat_user)


@pytest.fixture
async def messages(message_factory, user, room):
    return await database_sync_to_async(lambda u, r: [message_factory(u, r) for _ in range(10)])(user, room)


@pytest.fixture
async def patched_chat_consumer_get_room(room):
    with patch.object(ChatConsumer, 'get_room_id', return_value=room.pk) as patched_method:
        yield patched_method


@pytest.fixture
@pytest.mark.asyncio
@pytest.mark.django_db
async def communicator(room, user_factory, patched_chat_consumer_get_room):
    communicator = WebsocketCommunicator(ChatConsumer, f'/chat/{room.pk}')
    connected, subprotocol = await communicator.connect()

    assert connected

    user = await database_sync_to_async(user_factory)()
    packet = AuthenticatePacket(traffic_type=Packet.TYPE_TRAFFIC_INCOMING, device_id=user.device_id)
    await communicator.send_json_to(packet.serialize())
    data = await communicator.receive_json_from()
    packet = Packet.deserialize(traffic_type=Packet.TYPE_TRAFFIC_OUTCOMING, **data)

    assert packet.status == Packet.STATUS_SUCCESS
    assert packet.user.pk == user.pk
    patched_chat_consumer_get_room.assert_called_once()

    yield communicator

    await communicator.disconnect()


@pytest.fixture
@pytest.mark.asyncio
@pytest.mark.django_db
async def user_communicator(room, user, chat_user):
    communicator = WebsocketCommunicator(PrivateChatConsumer, f'/chat/private')
    connected, subprotocol = await communicator.connect()

    assert connected

    packet = AuthenticatePacket(traffic_type=Packet.TYPE_TRAFFIC_INCOMING, device_id=user.device_id)
    await communicator.send_json_to(packet.serialize())
    data = await communicator.receive_json_from()
    packet = Packet.deserialize(traffic_type=Packet.TYPE_TRAFFIC_OUTCOMING, **data)

    assert packet.status == Packet.STATUS_SUCCESS
    assert packet.user.pk == user.pk

    yield communicator

    await communicator.disconnect()


@pytest.mark.asyncio
async def test_get_messages(communicator, message):
    packet = GetChatMessagesPacket(traffic_type=Packet.TYPE_TRAFFIC_INCOMING, limit=100, offset=0)
    await communicator.send_json_to(packet.serialize())

    while 1:
        data = await communicator.receive_json_from()
        packet_in = Packet.deserialize(traffic_type=Packet.TYPE_TRAFFIC_OUTCOMING, **data)

        if packet_in.parent_uuid == packet.uuid:
            packet_message, = packet_in.messages
            break

    # Not asserting ids equality because after Message.objects.create id is 0 because of messages partitioning
    assert packet_message.body == message.body
    assert packet_message.author == message.author
    assert packet_message.room == message.room


@pytest.mark.asyncio
async def test_get_user_messages(user_communicator, user_message, chat_user, user):
    thread = UserMessage.get_thread(chat_user.pk, user.pk)
    packet = GetPrivateChatMessagesPacket(traffic_type=Packet.TYPE_TRAFFIC_INCOMING, limit=100, offset=0, thread=thread)
    await user_communicator.send_json_to(packet.serialize())

    while 1:
        data = await user_communicator.receive_json_from()
        packet_in = Packet.deserialize(traffic_type=Packet.TYPE_TRAFFIC_OUTCOMING, **data)

        if packet_in.parent_uuid == packet.uuid:
            packet_message, = packet_in.messages
            break

    # Not asserting ids equality because after Message.objects.create id is 0 because of messages partitioning
    assert packet_message.body == user_message.body
    assert packet_message.author == user_message.author


@pytest.mark.asyncio
@pytest.mark.parametrize('limit,offset', [
    (99, 0),
    (100, 5001),
    (99, 5001),
])
async def test_get_messages_invalid_limit_offset(communicator, message, limit, offset):
    packet = GetChatMessagesPacket(traffic_type=Packet.TYPE_TRAFFIC_INCOMING, limit=limit, offset=offset)
    await communicator.send_json_to(packet.serialize())

    while 1:
        data = await communicator.receive_json_from()
        packet_in = Packet.deserialize(traffic_type=Packet.TYPE_TRAFFIC_OUTCOMING, **data)

        if packet_in.parent_uuid == packet.uuid:  # AuthenticationPacket because it has failure status
            break

    assert packet_in.status == Packet.STATUS_FAILURE


@pytest.mark.asyncio
async def test_send_message(communicator, message, g):
    message_body = g.text.sentence()
    packet = SendChatMessagePacket(traffic_type=Packet.TYPE_TRAFFIC_INCOMING, message_body=message_body)
    await communicator.send_json_to(packet.serialize())

    while 1:
        data = await communicator.receive_json_from()
        packet_in = Packet.deserialize(traffic_type=Packet.TYPE_TRAFFIC_OUTCOMING, **data)

        if packet_in.parent_uuid == packet.uuid:
            break

    assert packet_in.status == Packet.STATUS_SUCCESS

    packet = GetChatMessagesPacket(traffic_type=Packet.TYPE_TRAFFIC_INCOMING, limit=100, offset=0)
    await communicator.send_json_to(packet.serialize())

    while 1:
        data = await communicator.receive_json_from()
        packet_in = Packet.deserialize(traffic_type=Packet.TYPE_TRAFFIC_OUTCOMING, **data)

        if packet_in.parent_uuid == packet.uuid:
            first_message, *_ = packet_in.messages[::-1]
            break

    assert first_message.body == message_body


@pytest.mark.asyncio
async def test_send_user_message(user_communicator, user_message, g, chat_user, user):
    message_body = g.text.sentence()
    thread = UserMessage.get_thread(chat_user.pk, user.pk)
    packet = SendPrivateChatMessagePacket(traffic_type=Packet.TYPE_TRAFFIC_INCOMING, message_body=message_body, thread=thread)
    await user_communicator.send_json_to(packet.serialize())

    while 1:
        data = await user_communicator.receive_json_from()
        packet_in = Packet.deserialize(traffic_type=Packet.TYPE_TRAFFIC_OUTCOMING, **data)

        if packet_in.parent_uuid == packet.uuid:
            break

    assert packet_in.status == Packet.STATUS_SUCCESS

    packet = GetPrivateChatMessagesPacket(traffic_type=Packet.TYPE_TRAFFIC_INCOMING, limit=100, offset=0, thread=thread)
    await user_communicator.send_json_to(packet.serialize())

    while 1:
        data = await user_communicator.receive_json_from()
        packet_in = Packet.deserialize(traffic_type=Packet.TYPE_TRAFFIC_OUTCOMING, **data)

        if packet_in.parent_uuid == packet.uuid:
            first_message, *_ = packet_in.messages[::-1]
            break

    assert first_message.body == message_body
