package com.dps_locator.dpslocator.core.chat

import com.dps_locator.dpslocator.core.User


class UserMessage(var pk: Int, var message: String, var sender: User, var createdAt: String)