package com.dps_locator.dpslocator

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.dps_locator.dpslocator.core.CommonChatWS
import com.dps_locator.dpslocator.core.CommonChatWS.Companion.messageAdapter
import kotlinx.coroutines.async
import java.util.*


class CommonChatUpdateInfoTask(val activity: FragmentActivity, val titleEdit: TextView, val subtitleEdit: TextView): TimerTask() {
    override fun run() {
        activity.runOnUiThread {
            titleEdit.text = "Общий чат"  // CommonChatWS.title
            subtitleEdit.text = "Онлайн: ${CommonChatWS.onlineCount}"
        }
    }
}


class ChatFragment : Fragment() {

    var timer: Timer? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_chat, container, false)

        val titleEdit = view.findViewById<TextView>(R.id.toolbar_title)
        val subtitleEdit = view.findViewById<TextView>(R.id.toolbar_subtitle)
        startUpdateInfoTimer(titleEdit, subtitleEdit)

        val recyclerView = view.findViewById<RecyclerView>(R.id.recyclerview_message_list)
        val messageEdit  = view.findViewById<EditText>(R.id.messageEdit)
        CommonChatWS.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        CommonChatWS.layoutManager?.stackFromEnd = true
        recyclerView.layoutManager = CommonChatWS.layoutManager
        view.findViewById<ImageView>(R.id.sendChat).setOnClickListener {
            val messageText = messageEdit.text.toString()
            async {
                CommonChatWS.sendChatMessage(messageText)

                activity?.runOnUiThread {
                    messageEdit.text.clear()
//                    val imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
//                    imm.hideSoftInputFromWindow(messageEdit.windowToken, 0)
                }
            }
        }

        recyclerView.adapter = messageAdapter
        val toolbar = view.findViewById<Toolbar>(R.id.toolbar)
//        toolbar.inflateMenu(R.menu.chat_menu)  Another chat room
        toolbar.setNavigationIcon(R.drawable.ic_menu)
        toolbar.setNavigationOnClickListener {
            activity?.findViewById<DrawerLayout>(R.id.drawer_layout)?.openDrawer(GravityCompat.START)
        }

        return view
    }

    private fun startUpdateInfoTimer(titleEdit: TextView, subtitleEdit: TextView) {
        try {
            timer?.cancel()
        } catch(e: IllegalStateException) {}

        timer = Timer()
        timer?.scheduleAtFixedRate(activity?.let { CommonChatUpdateInfoTask(it, titleEdit, subtitleEdit) }, 0, 250)
    }
}
