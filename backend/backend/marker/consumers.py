from common.consumers import BaseConsumer
from common.packets import Packet
from marker.packets import LogGPSCoordsPacket


class MarkerConsumer(BaseConsumer):

    packet_types = [
        LogGPSCoordsPacket.packet_type
    ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    async def connect(self):
        await self.accept()

    async def disconnect(self, close_code):
        await self.channel_layer.group_discard(self.group_name, self.channel_name)
        await super().disconnect(close_code)

    async def authentication_success(self):
        await self.channel_layer.group_add(self.group_name, self.channel_name)

    @property
    def group_name(self):
        return 'common_marker'
