from django.conf import settings

import googlemaps


_client = None


def _get_client():
    global _client

    if not _client:
        _client = googlemaps.Client(settings.GOOGLE_API_KEY)

    return _client


class _GMapsClientMeta(type):

    def __getattribute__(self, name):
        return getattr(_get_client(), name)


class GMapsClient(metaclass=_GMapsClientMeta):

    pass
