from rest_framework import serializers


class CreateTimeField(serializers.CharField):

    def _format_result(self, value: str) -> str:
        if value.count('.') == 1:
            value = value.replace(value.split('.')[-1], '')

        return value.strip('.')

    def to_internal_value(self, data: str):
        res = super(CreateTimeField, self).to_internal_value(data)

        return self._format_result(res)

    def to_representation(self, value: str):
        res = super(CreateTimeField, self).to_representation(value)

        return self._format_result(res)
