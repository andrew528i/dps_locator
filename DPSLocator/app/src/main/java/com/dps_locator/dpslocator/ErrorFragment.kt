package com.dps_locator.dpslocator

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

class ErrorFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_error, container, false)

        view.findViewById<TextView>(R.id.error_title).text = arguments?.getString("title")
        view.findViewById<TextView>(R.id.error_body).text = arguments?.getString("body")

        return view
    }

    companion object {
        fun newInstance(title: String, body: String): ErrorFragment {
            val fragment = ErrorFragment()
            val args = Bundle()
            args.putString("title", title)
            args.putString("body", body)
            fragment.arguments = args

            return fragment
        }
    }
}
