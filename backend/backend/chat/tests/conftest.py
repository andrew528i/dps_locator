import pytest

from chat.models import Room, Message
from user.models import Message as UserMessage


@pytest.fixture
def room_factory(g):
    room = None

    def create_room():
        nonlocal room

        room = Room.objects.create(name=g.address.city())

        return room

    yield create_room

    room.delete()


@pytest.fixture
def message_factory(g):
    messages = []

    def create_message(author, room):
        nonlocal messages

        message = Message.objects.create(body=g.text.sentence(), author=author, room=room)
        messages.append(message)

        return message

    yield create_message

    for m in messages:
        m.delete()


@pytest.fixture
def user_message_factory(g):
    messages = []

    def create_message(author, chat_user):
        nonlocal messages

        message = UserMessage.objects.create(
            body=g.text.sentence(),
            author=author,
            thread=UserMessage.get_thread(chat_user.pk, author.pk),
            user_ids=[chat_user.pk, author.pk],
        )
        messages.append(message)

        return message

    yield create_message

    for m in messages:
        m.delete()
