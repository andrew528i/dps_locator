from rest_framework.parsers import MultiPartParser
from rest_framework.response import Response
from rest_framework.exceptions import APIException
from rest_framework import status
from django.contrib.auth import login
from django.middleware.csrf import get_token
from django.utils.crypto import get_random_string
from django.db.models import Q

from common.views import BaseApiView
from user.models import User
from user.serializers.models import UserSerializer


class UserNotFoundError(APIException):

    status_code = status.HTTP_404_NOT_FOUND
    default_detail = 'User not found'
    default_code = 'no_user'


class UsernameError(APIException):

    status_code = status.HTTP_400_BAD_REQUEST
    default_detail = 'Username is already taken'
    default_code = 'username_taken'


class LicensePlateError(APIException):

    status_code = status.HTTP_400_BAD_REQUEST
    default_detail = 'License plate number is already taken'
    default_code = 'license_plate_taken'


class UserSignInView(BaseApiView):

    authentication_classes = ()
    permission_classes = ()

    def post(self, request):
        device_id = request.validated_data['device_id']
        ip_address = self.get_ip_address(request)

        try:
            user = User.objects.get(device_id=device_id)
            user.ip_address = ip_address
        except User.DoesNotExist:
            user = User.objects.create_user(
                username=get_random_string(),
                password=get_random_string(),
                device_id=device_id,
                ip_address=ip_address,
            )
            user.username = 'user_{}'.format(user.pk)

        user.save()
        login(request, user)

        return Response({
            'user': UserSerializer(user).data,
            'csrf': get_token(request),
        })


class UserInfoView(BaseApiView):

    def get(self, request, user_id=None):
        user_id = user_id or request.user.id

        try:
            user = User.objects.get(pk=user_id)
        except User.DoesNotExist:
            raise UserNotFoundError

        return Response(UserSerializer(user).data)

    def post(self, request):
        username = request.validated_data['username']
        license_plate = request.validated_data.get('license_plate')
        existing_user = User.objects.filter(Q(username=username) | Q(license_plate=license_plate)).exclude(pk=request.user.pk).first()

        if existing_user:
            if existing_user.username == username:
                raise UsernameError

            if existing_user.license_plate == license_plate:
                raise LicensePlateError

        user = User.objects.get(pk=request.user.pk)
        user.username = username

        if license_plate:
            user.license_plate = license_plate

        user.save()

        return Response(UserSerializer(user).data)


class UploadAvatarView(BaseApiView):

    parser_classes = MultiPartParser,

    def post(self, request):
        user = request.user
        user.avatar = request.validated_data['image']
        user.save()

        return Response(UserSerializer(user).data)
