package com.dps_locator.dpslocator.core

import android.widget.Toast
import com.dps_locator.dpslocator.core.chat.UserMessage
import org.json.JSONObject
import java.util.*


fun String.toSnakeCase(): String {
    var text = ""
    var isFirst = true
    this.forEach {
        if (it.isUpperCase()) {
            if (isFirst) isFirst = false
            else text += "_"
            text += it.toLowerCase()
        } else {
            text += it
        }
    }
    return text
}


sealed class Packet(
    pairs: Array<out Pair<String, Any?>>? = null,
    private vararg val fields: String
) {
    enum class Status {
        SUCCESS,
        FAILURE
    }

    companion object {
        fun getUuid(): String { return UUID.randomUUID().toString() }

        val knownValues = mapOf(
            "device_id" to api.deviceId
        )

        val packetTypes: List<String>
            get() {
                return Packet::class.sealedSubclasses.map { className2PacketType(it.toString()) }
            }

        fun className2PacketType(classname: String): String {
            classname.split(".").let {
                val className = it[it.size - 1].replace("Packet", "")

                return className.toSnakeCase()
            }
        }

        fun processIncome(packet: JSONObject) {
            val type = packet.getString("type").let { "${it}_in" }
            var processor: Packet? = null

            for (sealedClass in  Packet::class.sealedSubclasses) {
                if (className2PacketType(sealedClass.toString()) == type) {
                    processor = sealedClass.constructors.first().call()
                    processor.process(packet)
                    break
                }
            }

            if (processor == null) {
                throw NotImplementedError("Packet $type is not implemented")
            }
        }
    }

    private var valuesMap = (pairs?.toMap()?.toMutableMap()) ?: HashMap()

    val packetType: String
        get() {
            return className2PacketType(javaClass.name)
        }

    fun serialize(): String {
        val packet = JSONObject()
        packet.put("uuid", getUuid())
        packet.put("type", packetType.removeSuffix("_out").removeSuffix("_in"))

        for ((key, value) in knownValues) {
            if (!valuesMap.containsKey(key) && fields.contains(key)) {
                valuesMap[key] = value
            }
        }

        for (field in fields) {
            val value = valuesMap[field]
            packet.put(field, value)
        }

        return packet.toString()
    }

    operator fun set(idx: String, value: Any?) {
        valuesMap[idx] = value
    }

    operator fun get(idx: String): Any? {
        return valuesMap[idx]
    }

    open fun process(packet: JSONObject) {
        throw NotImplementedError("process method should be implemented")
    }
}

class AuthenticateOutPacket(vararg pairs: Pair<String, Any?>): Packet(pairs, "device_id")

// Common chat
class GetChatMessagesOutPacket(vararg pairs: Pair<String, Any?>): Packet(pairs, "limit", "offset")
class SendChatMessageOutPacket(vararg pairs: Pair<String, Any?>): Packet(pairs, "message_body")
class CommonChatPingOutPacket(vararg pairs: Pair<String, Any?>): Packet(pairs, "timestamp")
class GetCommonChatInfoOutPacket(vararg pairs: Pair<String, Any?>): Packet(pairs)

// Private chat
class GetPrivateChatMessagesOutPacket(vararg pairs: Pair<String, Any?>): Packet(pairs, "thread", "limit", "offset")
class SendPrivateChatMessageOutPacket(vararg pairs: Pair<String, Any?>): Packet(pairs, "thread", "message_body")

// Marker
class LogGpsCoordsOutPacket(vararg pairs: Pair<String, Any?>): Packet(pairs, "latitude", "longitude", "velocity")


class AuthenticateInPacket: Packet() {
    override fun process(packet: JSONObject) {
        when (packet.opt("status")) {
            Packet.Status.SUCCESS.name.toLowerCase() -> {
                CommonChatWS.webSocketManager?.sendMessage(GetChatMessagesOutPacket(
                    "limit" to 250,
                    "offset" to 0
                ).serialize())

                CommonChatWS.webSocketManager?.sendMessage(GetCommonChatInfoOutPacket().serialize())
                CommonChatWS.timer = Timer()
                CommonChatWS.timer?.scheduleAtFixedRate(PingTask(), 0, 5000)
            }
            Packet.Status.FAILURE.name.toLowerCase() -> {}
        }
    }
}

class GetChatMessagesInPacket: Packet() {
    override fun process(packet: JSONObject) {
        when (packet.opt("status")) {
            Packet.Status.SUCCESS.name.toLowerCase() -> {
                val messages = packet.getJSONArray("messages")

                for (i in 0..(messages.length() - 1)) {
                    val message = messages.getJSONObject(i)
                    val user = message.getJSONObject("author")

                    if (CommonChatWS.messages.none { it.pk == message.getInt("pk") }) {
                        CommonChatWS.messages.add(UserMessage(
                            message.getInt("pk"),
                            message.getString("body"),
                            User(
                                user.getInt("pk"),
                                user.getString("username"),
                                "http://${API_HOST}" + user.getString("avatar")
                            ),
                            message.getString("create_time")
                        ))
                    }
                }

                CommonChatWS.messageAdapter.notifyDataSetChanged()
                if (CommonChatWS.messages.size > 0) {
                    CommonChatWS.layoutManager?.scrollToPosition(CommonChatWS.messages.size - 1)
                }
            }
            Packet.Status.FAILURE.name.toLowerCase() -> {}
        }
    }
}

class SendChatMessageInPacket: Packet() {
    override fun process(packet: JSONObject) {
        when (packet.opt("status")) {
            Packet.Status.SUCCESS.name.toLowerCase() -> {
//                CommonChatWS.webSocketManager?.sendMessage(GetChatMessagesOutPacket(
//                    "limit" to 250,
//                    "offset" to 0
//                ).serialize())
            }
            Packet.Status.FAILURE.name.toLowerCase() -> {
                packet.optString("message")?.let {
                    Toast.makeText(api.context, it, Toast.LENGTH_LONG).show()
                }
            }
        }
    }
}


class CommonChatPingInPacket: Packet() {
    override fun process(packet: JSONObject) {
        when (packet.opt("status")) {
            Packet.Status.SUCCESS.name.toLowerCase() -> {}
            Packet.Status.FAILURE.name.toLowerCase() -> {}
        }
    }
}


class GetCommonChatInfoInPacket: Packet() {
    override fun process(packet: JSONObject) {
        when (packet.opt("status")) {
            Packet.Status.SUCCESS.name.toLowerCase() -> {
                CommonChatWS.onlineCount = packet.getInt("online_count")
                CommonChatWS.title = packet.getString("name")
            }
            Packet.Status.FAILURE.name.toLowerCase() -> {}
        }
    }
}


class LogGpsCoordsInPacket: Packet() {
    override fun process(packet: JSONObject) {
        when (packet.opt("status")) {
            Packet.Status.SUCCESS.name.toLowerCase() -> {}
            Packet.Status.FAILURE.name.toLowerCase() -> {}
        }
    }
}


class GetPrivateChatMessagesInPacket: Packet() {
    override fun process(packet: JSONObject) {
        when (packet.opt("status")) {
            Packet.Status.SUCCESS.name.toLowerCase() -> {
                val messages = packet.getJSONArray("messages")

                for (i in 0..(messages.length() - 1)) {
                    val message = messages.getJSONObject(i)
                    val user = message.getJSONObject("author")

                    if (CommonChatWS.privateMessages.none { it.pk == message.getInt("pk") }) {
                        CommonChatWS.privateMessages.add(UserMessage(
                            message.getInt("pk"),
                            message.getString("body"),
                            User(
                                user.getInt("pk"),
                                user.getString("username"),
                                "http://${API_HOST}" + user.getString("avatar")
                            ),
                            message.getString("create_time")
                        ))
                    }
                }

                CommonChatWS.privateMessageAdapter.notifyDataSetChanged()
                if (CommonChatWS.privateMessages.size > 0) {
                    CommonChatWS.privateLayoutManager?.scrollToPosition(CommonChatWS.privateMessages.size - 1)
                }
            }
            Packet.Status.FAILURE.name.toLowerCase() -> {
                print(123)
            }
        }
    }
}


class SendPrivateChatMessageInPacket: Packet() {
    override fun process(packet: JSONObject) {
        when (packet.opt("status")) {
            Packet.Status.SUCCESS.name.toLowerCase() -> {}
            Packet.Status.FAILURE.name.toLowerCase() -> {}
        }
    }
}
