from typing import Iterable

import pytz

from django.contrib.auth.models import AbstractUser
from django.contrib.postgres.fields import ArrayField
from django.db import models
from django.utils import timezone

from common.models import CommonMessage


class User(AbstractUser):

    class Meta:
        verbose_name = 'user'
        verbose_name_plural = 'users'

    REQUIRED_FIELDS = ['ip_address', 'device_id', 'email']

    GENDER_UNKNOWN = 'u'
    GENDER_MALE = 'm'
    GENDER_FEMALE = 'f'

    GENDERS = (
        (GENDER_UNKNOWN, 'unknown'),
        (GENDER_MALE, 'male'),
        (GENDER_FEMALE, 'female'),
    )

    TYPE_UNKNOWN = 'u'
    TYPE_CAR_OWNER = 'co'
    TYPE_PEDESTRIAN = 'p'
    TYPE_MOTOCYCLIST = 'm'
    TYPE_CYCLIST = 'c'

    TYPES = (
        (TYPE_UNKNOWN, 'unknown'),
        (TYPE_CAR_OWNER, 'car_owner'),
        (TYPE_PEDESTRIAN, 'pedestrian'),
        (TYPE_MOTOCYCLIST, 'motocyclist'),
        (TYPE_CYCLIST, 'cyclist'),
    )

    device_id = models.CharField(max_length=40, blank=False, unique=True)
    ip_address = models.GenericIPAddressField()
    gender = models.CharField(max_length=1, choices=GENDERS, default=GENDER_UNKNOWN)
    type = models.CharField(max_length=2, choices=TYPES, default=TYPE_UNKNOWN)
    license_plate = models.CharField(max_length=9, null=False, default='')
    birthdate = models.DateTimeField(null=False, default=timezone.datetime(1900, 1, 1, tzinfo=pytz.UTC))
    biography = models.CharField(max_length=256, null=False, default='')
    avatar = models.ImageField(upload_to='avatars', default='avatars/default.png')
    velocity_rating = models.IntegerField(default=0, null=False)


class Message(CommonMessage):

    author = models.ForeignKey('user.User', on_delete=models.DO_NOTHING, related_name='private_messages')
    thread = models.CharField(max_length=127, blank=False, unique=True, null=False)  # user_1:user_2:...:thread
    user_ids = ArrayField(models.IntegerField(), default=list)

    @staticmethod
    def get_thread(*user_ids: Iterable[int]):
        return '_'.join(map(str, sorted(user_ids) + ['thread']))
