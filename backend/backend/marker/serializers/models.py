from rest_framework import serializers

from marker.serializers import PointField
from user.serializers.models import UserSerializer


class VelocityField(serializers.FloatField):

    def to_internal_value(self, data: float):
        res = super(VelocityField, self).to_internal_value(data)

        return round(float(res), 2)

    def to_representation(self, value: float):
        res = super(VelocityField, self).to_internal_value(value)

        return round(float(res), 2)


class _LeaderSerializer(serializers.Serializer):

    user = UserSerializer()
    velocity = VelocityField()


class MarkerSerializer(serializers.Serializer):

    pk = serializers.IntegerField(source='id')
    user = UserSerializer()
    type = serializers.CharField()
    location = PointField()
    options = serializers.DictField()
    create_time = serializers.DateTimeField()
    seen_count = serializers.IntegerField()
    leaders = _LeaderSerializer(many=True, required=False)


class CommentSerializer(serializers.Serializer):

    pk = serializers.IntegerField(source='id')
    user = UserSerializer()
    marker = MarkerSerializer()
    body = serializers.CharField(max_length=1024)
    create_time = serializers.DateTimeField()
