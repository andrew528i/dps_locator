import pytest

from user.models import Message as UserMessage


@pytest.fixture
def chat_user_1(user_factory):
    return user_factory()


@pytest.fixture
def chat_user_2(user_factory):
    return user_factory()


@pytest.fixture
def private_messages(user_message_factory, user, chat_user_1, chat_user_2):
    user_message_factory(user, chat_user_1)
    user_message_factory(chat_user_1, user)
    user_message_factory(chat_user_2, user)
    user_message_factory(user, chat_user_2)


@pytest.mark.usefixtures('private_messages')
def test_get_private_chat_threads_200(client, chat_user_1, chat_user_2, user):
    resp = client.get('/chat/private/threads')
    data = resp.json()
    threads = set([UserMessage.get_thread(user.pk, u.pk) for u in (chat_user_1, chat_user_2)])

    assert threads == set(i['thread'] for i in data['results'])
