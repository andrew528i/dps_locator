from django.db import models, connections, DEFAULT_DB_ALIAS

from common.models import CommonMessage


class Room(models.Model):

    name = models.CharField(max_length=128)
    create_time = models.DateTimeField(auto_now_add=True)
    modify_time = models.DateTimeField(auto_now=True)


class Message(CommonMessage):

    author = models.ForeignKey('user.User', on_delete=models.DO_NOTHING, related_name='messages')
    room = models.ForeignKey('chat.Room', on_delete=models.DO_NOTHING, related_name='chat_messages')
