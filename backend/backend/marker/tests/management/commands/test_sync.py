import pytest
from mock import patch, call, MagicMock
from django.core.management import call_command
from telethon.tl.types import MessageMediaGeo

TELEGRAM_SESSION_NAME = 'test_session_name'
TELEGRAM_API_ID = 'test_api_id'
TELEGRAM_API_HASH = 'test_api_hash'


@pytest.fixture
def patch_telegram_config():
    with patch('marker.management.commands.sync_markers.session_name', TELEGRAM_SESSION_NAME):
        with patch('marker.management.commands.sync_markers.api_id', TELEGRAM_API_ID):
            with patch('marker.management.commands.sync_markers.api_hash', TELEGRAM_API_HASH):
                yield


@pytest.mark.usefixtures('patch_telegram_config')
@patch('marker.management.commands.sync_markers.TelegramClient')
@patch('marker.management.commands.sync_markers.sync_markers')
def test_sync_command(patched_sync_markers, patched_telegram_client):
    args = ()
    opts = {}

    m = MagicMock()
    m.media = MessageMediaGeo(m)
    m.long = 1.0
    m.lat = 1.0
    patched_telegram_client.return_value = m
    m.iter_messages = lambda *_: [m]

    call_command('sync_markers', *args, **opts)

    assert call(TELEGRAM_SESSION_NAME, TELEGRAM_API_ID, TELEGRAM_API_HASH) in patched_telegram_client.mock_calls
    assert call().connect() in patched_telegram_client.mock_calls
    assert call().is_connected() in patched_telegram_client.mock_calls
    assert call().disconnect() in patched_telegram_client.mock_calls
    assert patched_sync_markers.call_count
