package com.dps_locator.dpslocator.core

import android.app.Activity
import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.widget.Toast
import com.dps_locator.dpslocator.BuildConfig
import com.dps_locator.dpslocator.MainActivity
import com.dps_locator.dpslocator.core.chat.MessageAdapter
import com.dps_locator.dpslocator.core.chat.UserMessage
import com.eclipsesource.json.JsonObject
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.android.core.Json
import com.github.kittinunf.fuel.android.extension.responseJson
import com.github.kittinunf.fuel.core.DataPart
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.core.Request
import com.github.kittinunf.fuel.core.Response
import com.github.kittinunf.result.Result
import com.google.android.gms.maps.model.LatLng
import com.rabtman.wsmanager.WsManager
import com.rabtman.wsmanager.listener.WsStatusListener
import kotlinx.coroutines.async
import okhttp3.OkHttpClient
import okhttp3.WebSocket
import org.jetbrains.anko.runOnUiThread
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.File
import java.util.*
import java.util.concurrent.TimeUnit
import com.danneu.json.Encoder as JE


var API_HOST = if (BuildConfig.DEBUG) "127.0.0.1:8000" else "173.249.45.177"



class PingTask: TimerTask() {
    override fun run() {
        if (CommonChatWS.webSocketManager!!.isWsConnected) {
            val timestamp = System.currentTimeMillis() / 1000
            CommonChatWS.webSocketManager?.sendMessage(CommonChatPingOutPacket("timestamp" to timestamp).serialize())
        }
    }
}


class MarkerWS: WsStatusListener() {
    companion object {
        var webSocketManager: WsManager? = null
    }

    override fun onOpen(response: okhttp3.Response?) {
        webSocketManager?.sendMessage(AuthenticateOutPacket().serialize())
    }

    override fun onMessage(text: String?) {
        Packet.processIncome(JSONObject(text))
    }
}


class CommonChatWS: WsStatusListener() {
    companion object {
        private const val NORMAL_CLOSURE_STATUS = 1000
        var webSocket: WebSocket? = null
        var webSocketManager: WsManager? = null
        var webSocketManagerPrivate: WsManager? = null
        val messages = ArrayList<UserMessage>()
        val messageAdapter = MessageAdapter(messages)
        val privateMessages = ArrayList<UserMessage>()
        val privateMessageAdapter = MessageAdapter(privateMessages)
        var layoutManager: LinearLayoutManager? = null
        var privateLayoutManager: LinearLayoutManager? = null
        var timer: Timer? = null
        var onlineCount = 0
        var title = ""

        fun sendChatMessage(messageBody: String) {
            webSocketManager?.sendMessage(SendChatMessageOutPacket("message_body" to messageBody).serialize())
        }

        fun sendPrivateChatMessage(thread: String, messageBody: String) {
            webSocketManagerPrivate?.sendMessage(SendPrivateChatMessageOutPacket(
                "thread" to thread,
                "message_body" to messageBody).serialize())
        }

        fun fetchPrivateMessages(thread: String) {
            webSocketManagerPrivate?.sendMessage(GetPrivateChatMessagesOutPacket(
                "thread" to thread,
                "limit" to 1000
            ).serialize())
        }
    }

    override fun onOpen(response: okhttp3.Response?) {
        webSocketManager?.sendMessage(AuthenticateOutPacket().serialize())
        webSocketManagerPrivate?.sendMessage(AuthenticateOutPacket().serialize())
    }

    override fun onClosed(code: Int, reason: String?) {
        try {
            timer?.cancel()
        } catch(e: IllegalStateException) {}
    }

    override fun onMessage(text: String?) {
        Packet.processIncome(JSONObject(text))
    }
}


class API {
    private var csrfToken: String? = null
    var deviceId: String? = null
    var user: User? = null
    private val cookies = mutableMapOf<String, String>()
    var context: Context? = null

    fun getThread(vararg user_ids: Int): String {
        user_ids.sort()

        return user_ids.map { it.toString() }.joinToString("_") + "_thread"
    }

    fun isAuthorized(): Boolean {
        csrfToken?.let {
            return it.isNotEmpty()
        }

        return false
    }

    fun authorize(): Boolean {
        if (isAuthorized()) {
            return true
        }

        val pack = JSONObject().put("device_id", deviceId)
        val (_, _, result) = post("/user/sign_in", pack)
        val data = result.get().obj()

        try {
            csrfToken = data.getString("csrf")
            val userObj = data.getJSONObject("user")

            user = User(
                userObj.getInt("pk"),
                userObj.getString("username"),
                "http://$API_HOST" + userObj.getString("avatar"),
                userObj.getString("license_plate"))
        } catch (e: JSONException) {
            println("Shit happens")
        }

        if (isAuthorized() && CommonChatWS.webSocketManager == null) {
            startCommonChatListener()
            startPrivateChatListener()
        }

        if (isAuthorized() && MarkerWS.webSocketManager == null) {
            startMarkerListener()
        }

        return isAuthorized()
    }

    private fun startCommonChatListener() {
        async {
            val okHttpClient = OkHttpClient().newBuilder()
                .pingInterval(15, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true)
                .build()
            CommonChatWS.webSocketManager = WsManager.Builder(api.context)
                .wsUrl("ws://$API_HOST/chat/0")
                .needReconnect(true)
                .client(okHttpClient)
                .build()
            CommonChatWS.webSocketManager?.startConnect()
            CommonChatWS.webSocketManager?.setWsStatusListener(CommonChatWS())
        }
    }

    private fun startPrivateChatListener() {
        async {
            val okHttpClient = OkHttpClient().newBuilder()
                .pingInterval(15, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true)
                .build()
            CommonChatWS.webSocketManagerPrivate = WsManager.Builder(api.context)
                .wsUrl("ws://$API_HOST/chat/private")
                .needReconnect(true)
                .client(okHttpClient)
                .build()
            CommonChatWS.webSocketManagerPrivate?.startConnect()
            CommonChatWS.webSocketManagerPrivate?.setWsStatusListener(CommonChatWS())
        }
    }

    private fun startMarkerListener() {
        async {
            val okHttpClient = OkHttpClient().newBuilder()
                .pingInterval(15, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true)
                .build()
            MarkerWS.webSocketManager = WsManager.Builder(api.context)
                .wsUrl("ws://$API_HOST/marker")
                .needReconnect(true)
                .client(okHttpClient)
                .build()
            MarkerWS.webSocketManager?.startConnect()
            MarkerWS.webSocketManager?.setWsStatusListener(MarkerWS())
        }
    }

    fun getThreads(cusror: String = "", func: (user: JSONObject) -> Unit) {
        val (_, _, result) = get("/chat/private/threads")
        val data = result.get().obj()

        val results = data.getJSONArray("results")

        for (i in 0..(results.length() - 1)) {
            func(results.getJSONObject(i))
        }
    }

    fun getLeaders(func: (user: JSONObject) -> Unit) {
        val (_, _, result) = get("/marker/leader_board")
        val data = result.get().obj()

        val results = data.getJSONArray("results")

        for (i in 0..(results.length() - 1)) {
            val user = results.getJSONObject(i)

            func(user)
        }
    }

    fun getMarker(markerId: Int, callback: (marker: JSONObject) -> Unit) {
        val (_, _, result) = get("/marker/$markerId")

        callback(result.get().obj())
    }

    fun getUser(userId: Int, callback: (user: JSONObject) -> Unit) {
        val (_, _, result) = get("/user/info/$userId")

        callback(result.get().obj())
    }

    fun getMarkers(latlng: LatLng, distance: Int? = null): ArrayList<Marker> {
        // TODO: next in response
        val markers = ArrayList<Marker>()
        val pack = mutableListOf<Pair<String, Any?>>(
            "longitude" to latlng.longitude,
            "latitude" to latlng.latitude)

        distance?.let {
            pack.add("distance" to it)
        }

        val (_, _, result) = get("/marker/near", pack)
        val data = result.get().obj()

        val results = data.getJSONArray("results")

        for (i in 0..(results.length() - 1)) {
            val markerObj = results.getJSONObject(i)
            val markerLocation = markerObj.getJSONObject("location")
            val markerOptions = HashMap<String, Any?>()
            markerObj.getJSONObject("options").let {
                it.keys().forEach { key ->
                    markerOptions[key] = it[key]
                }
            }
            val marker = Marker(
                markerObj.getInt("pk"),
                when (markerObj.getString("type")) {
                    "P" -> MarkerType.POLICE
                    "S" -> MarkerType.SPEED
                    "C" -> MarkerType.CAMERA
                    "F" -> MarkerType.FUELING
                    else -> MarkerType.UNKNOWN
                },
                markerObj.getJSONObject("user").getInt("pk"),
                LatLng(
                    markerLocation.getDouble("lat"),
                    markerLocation.getDouble("lng")
                ),
                markerObj.getString("create_time"),
                markerObj.getInt("seen_count"),
                markerOptions
            )

            markers.add(marker)
        }

        return markers
    }

    fun getSuggestions(address: String, latlng: LatLng): ArrayList<HashMap<String, String>> {
        val addresses = ArrayList<HashMap<String, String>>()
        val pack = mutableListOf<Pair<String, Any?>>(
            "address" to address,
            "location" to "${latlng.latitude},${latlng.longitude}")

        val (_, _, result) = get("/maps/suggest", pack)
        val data = result.get().obj()
        val results = data.getJSONArray("results")

        for (i in 0..(results.length() - 1)) {
            val addressObj = results.getJSONObject(i)

            val address = HashMap<String, String>()
            address["description"] = addressObj.getString("description")
            address["place_id"] = addressObj.getString("place_id")

            addresses.add(address)
        }

        return addresses
    }

    fun getPlace(placeId: String): LatLng {
        val pack = mutableListOf<Pair<String, Any?>>("place_id" to placeId)
        val (_, _, result) = get("/maps/place", pack)
        val data = result.get().obj()

        return LatLng(data.getDouble("lat"), data.getDouble("lng"))
    }

    fun getRoute(
        origin: LatLng,
        destination: LatLng,
        activity: Activity?,
        callback: (routePoints: ArrayList<LatLng>, polygonPoints: ArrayList<LatLng>) -> Unit
    ) {
        val routePoints = ArrayList<LatLng>()
        val polygonPoints = ArrayList<LatLng>()

        val pack = JSONObject()
            .put("origin", JSONArray(listOf(origin.latitude, origin.longitude)))
            .put("destination", JSONArray(listOf(destination.latitude, destination.longitude)))

        val (_, _, result) = post("/maps/build_route", pack)
        val data = result.get().obj()
        val error = data.optString("error")

        if (!error.isEmpty()) {
            activity?.let {
                it.runOnUiThread {
                    Toast.makeText(context, error, Toast.LENGTH_LONG).show()
                }
            }
        } else {
            val coords = data.getJSONArray("coords")
            val polygonCoords = data.getJSONArray("polygon")

            for (i in 0..(coords.length() - 1)) {
                val location = coords.getJSONObject(i)
                routePoints.add(LatLng(location.getDouble("latitude"), location.getDouble("longitude")))
            }

            for (i in 0..(polygonCoords.length() - 1)) {
                val location = polygonCoords.getJSONObject(i)
                polygonPoints.add(LatLng(location.getDouble("latitude"), location.getDouble("longitude")))
            }

            callback(routePoints, polygonPoints)
        }
    }

    fun updateUser(username: String, license_plate: String = "") {
        val pack = JSONObject().put("username", username)

        if (!license_plate.isEmpty()) {
            pack.put("license_plate", license_plate)
        }

        val (_, resp, result) = post("/user/info", pack)
        val data = result.get().obj()

        if (resp.statusCode == 200) {
            user = User(
                data.getInt("pk"),
                data.getString("username"),
                "http://$API_HOST" + data.getString("avatar"),
                data.getString("license_plate"))
        }
    }

    fun changeAvatar(avatarFile: File) {
        val request = Fuel.upload("http://$API_HOST/user/upload_avatar").dataParts { request, url ->
            listOf(DataPart(avatarFile, type="file", name="image"))
        }

        getCookie().let {
            if (!it.isEmpty()) {
                request.headers["Cookie"] = it
            }
        }

        csrfToken?.let {
            request.headers["X-CSRFToken"] = it
        }
        val (request_, response, result) = request.responseJson()

        response.headers["Set-Cookie"]?.forEach { cookie ->
            cookie.split(";").let {
                if (!it.isEmpty()) {
                    val (key, value) = it[0].split("=")

                    cookies[key] = value
                }
            }
        }

        val data = result.get().obj()

        user = User(data.getInt("pk"), data.getString("username"), "http://$API_HOST" + data.getString("avatar"))
    }

    private fun getCookie(): String {
        val cookiesList = ArrayList<String>()

        cookies.forEach { i ->
            cookiesList.add("${i.key}=${i.value}")
        }

        return cookiesList.joinToString("; ")
    }

    private fun post(path: String, pack: JSONObject): Triple<Request, Response, Result<Json, FuelError>> {
        val request = Fuel.post("http://$API_HOST$path")
            .body(pack.toString())
        request.headers["Content-Type"] = "application/json"
        getCookie().let {
            if (!it.isEmpty()) {
                request.headers["Cookie"] = it
            }
        }

        csrfToken?.let {
            request.headers["X-CSRFToken"] = it
        }

        val (request_, response, result) = request.responseJson()

        response.headers["Set-Cookie"]?.forEach { cookie ->
            cookie.split(";").let {
                if (!it.isEmpty()) {
                    val (key, value) = it[0].split("=")

                    cookies[key] = value
                }
            }
        }

        try {
            val data = JSONObject(String(response.data))

            data.optString("detail")?.let {
                if (!it.isEmpty()) {
                    context?.runOnUiThread {
                        Toast.makeText(context, it, Toast.LENGTH_LONG).show()
                    }
                }
            }
        } catch (e: Throwable) { }

        return Triple(request_, response, result)
    }

    private fun get(path: String, parameters: List<Pair<String, Any?>>? = null): Triple<Request, Response, Result<Json, FuelError>> {
        val request = Fuel.get("http://$API_HOST$path", parameters)
        request.headers["Content-Type"] = "application/json"
        getCookie().let {
            if (!it.isEmpty()) {
                request.headers["Cookie"] = it
            }
        }

        val (request_, response, result) = request.responseJson()

        response.headers["Set-Cookie"]?.forEach { cookie ->
            cookie.split(";").let {
                if (!it.isEmpty()) {
                    val (key, value) = it[0].split("=")

                    cookies[key] = value
                }
            }
        }

        return Triple(request_, response, result)
    }
}

val api = API()
