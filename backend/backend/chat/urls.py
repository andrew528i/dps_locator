from django.urls import path

from chat.consumers import ChatConsumer, PrivateChatConsumer
from chat.views import ThreadListView


urlpatterns = [
    path('private/threads', ThreadListView.as_view())
]


ws_urlpatterns = [
    path('chat/<int:room_id>', ChatConsumer),
    path('chat/private', PrivateChatConsumer),
]
