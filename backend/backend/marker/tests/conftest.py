import random
import operator

import pytest
from django.contrib.gis.geos import Point

from marker.models import Marker, Comment


@pytest.fixture
def make_marker(user, g):
    marker = None

    def make_marker_(point: Point = None, marker_type: str = None, options: dict = None):
        nonlocal marker

        point = point or Point(g.address.longitude(), g.address.latitude(), srid=4326)
        type_getter = operator.itemgetter(0)
        marker_type = marker_type or type_getter(random.choice(Marker.TYPES))
        options = options or {'cars': 1, 'mans': 1}
        marker = Marker.objects.create(location=point, type=marker_type, user=user, options=options)

        return marker

    return make_marker_


@pytest.fixture
def make_comment(user, marker, g):
    comment = None

    def make_comment_(body: str = None):
        nonlocal comment

        body = body or g.text.text()
        comment = Comment.objects.create(user=user, marker=marker, body=body)

        return comment

    return make_comment_


@pytest.fixture
def marker(make_marker, g):
    marker = make_marker()
    yield marker
    marker.delete()
