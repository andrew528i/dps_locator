from rest_framework import serializers


class InvalidGeoValue(ValueError):

    pass


class GeoField(serializers.Field):

    def to_representation(self, value):
        if not isinstance(value, (list, str)):
            raise InvalidGeoValue

        return value

    def to_internal_value(self, data):
        if not isinstance(data, (list, str)):
            raise InvalidGeoValue

        return data


class BuildRouteViewPostSerializer(serializers.Serializer):

    origin = GeoField()
    destination = GeoField()


class SuggestViewGetSerializer(serializers.Serializer):

    address = serializers.CharField()
    location = serializers.CharField()


class PlaceViewGetSerializer(serializers.Serializer):

    place_id = serializers.CharField()
