--
-- PostgreSQL database dump
--

-- Dumped from database version 10.5 (Debian 10.5-2.pgdg90+1)
-- Dumped by pg_dump version 10.5 (Debian 10.5-2.pgdg90+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: DATABASE postgres; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON DATABASE postgres IS 'default administrative connection database';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: citext; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS citext WITH SCHEMA public;


--
-- Name: EXTENSION citext; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION citext IS 'data type for case-insensitive character strings';


--
-- Name: sentry_increment_project_counter(bigint, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.sentry_increment_project_counter(project bigint, delta integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
        declare
          new_val int;
        begin
          loop
            update sentry_projectcounter set value = value + delta
             where project_id = project
               returning value into new_val;
            if found then
              return new_val;
            end if;
            begin
              insert into sentry_projectcounter(project_id, value)
                   values (project, delta)
                returning value into new_val;
              return new_val;
            exception when unique_violation then
            end;
          end loop;
        end
        $$;


ALTER FUNCTION public.sentry_increment_project_counter(project bigint, delta integer) OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: auth_authenticator; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_authenticator (
    id integer NOT NULL,
    user_id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    last_used_at timestamp with time zone,
    type integer NOT NULL,
    config text NOT NULL,
    CONSTRAINT auth_authenticator_type_check CHECK ((type >= 0))
);


ALTER TABLE public.auth_authenticator OWNER TO postgres;

--
-- Name: auth_authenticator_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_authenticator_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_authenticator_id_seq OWNER TO postgres;

--
-- Name: auth_authenticator_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_authenticator_id_seq OWNED BY public.auth_authenticator.id;


--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO postgres;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO postgres;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_group_id_seq OWNED BY public.auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO postgres;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_group_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO postgres;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_group_permissions_id_seq OWNED BY public.auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO postgres;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO postgres;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_permission_id_seq OWNED BY public.auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_user (
    password character varying(128) NOT NULL,
    last_login timestamp with time zone NOT NULL,
    id integer NOT NULL,
    username character varying(128) NOT NULL,
    first_name character varying(200) NOT NULL,
    email character varying(75) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    is_superuser boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL,
    is_managed boolean NOT NULL,
    is_password_expired boolean NOT NULL,
    last_password_change timestamp with time zone,
    session_nonce character varying(12),
    last_active timestamp with time zone
);


ALTER TABLE public.auth_user OWNER TO postgres;

--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_id_seq OWNER TO postgres;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_user_id_seq OWNED BY public.auth_user.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    user_id integer NOT NULL,
    content_type_id integer,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO postgres;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.django_admin_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO postgres;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.django_admin_log_id_seq OWNED BY public.django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO postgres;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.django_content_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO postgres;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.django_content_type_id_seq OWNED BY public.django_content_type.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO postgres;

--
-- Name: django_site; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_site (
    id integer NOT NULL,
    domain character varying(100) NOT NULL,
    name character varying(50) NOT NULL
);


ALTER TABLE public.django_site OWNER TO postgres;

--
-- Name: django_site_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.django_site_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_site_id_seq OWNER TO postgres;

--
-- Name: django_site_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.django_site_id_seq OWNED BY public.django_site.id;


--
-- Name: jira_ac_tenant; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.jira_ac_tenant (
    id bigint NOT NULL,
    organization_id bigint,
    client_key character varying(50) NOT NULL,
    secret character varying(100) NOT NULL,
    base_url character varying(60) NOT NULL,
    public_key character varying(250) NOT NULL
);


ALTER TABLE public.jira_ac_tenant OWNER TO postgres;

--
-- Name: jira_ac_tenant_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.jira_ac_tenant_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.jira_ac_tenant_id_seq OWNER TO postgres;

--
-- Name: jira_ac_tenant_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.jira_ac_tenant_id_seq OWNED BY public.jira_ac_tenant.id;


--
-- Name: nodestore_node; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.nodestore_node (
    id character varying(40) NOT NULL,
    data text NOT NULL,
    "timestamp" timestamp with time zone NOT NULL
);


ALTER TABLE public.nodestore_node OWNER TO postgres;

--
-- Name: sentry_activity; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_activity (
    id bigint NOT NULL,
    project_id bigint NOT NULL,
    group_id bigint,
    type integer NOT NULL,
    ident character varying(64),
    user_id integer,
    datetime timestamp with time zone NOT NULL,
    data text,
    CONSTRAINT sentry_activity_type_check CHECK ((type >= 0))
);


ALTER TABLE public.sentry_activity OWNER TO postgres;

--
-- Name: sentry_activity_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_activity_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_activity_id_seq OWNER TO postgres;

--
-- Name: sentry_activity_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_activity_id_seq OWNED BY public.sentry_activity.id;


--
-- Name: sentry_apiapplication; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_apiapplication (
    id bigint NOT NULL,
    client_id character varying(64) NOT NULL,
    client_secret text NOT NULL,
    owner_id integer NOT NULL,
    name character varying(64) NOT NULL,
    status integer NOT NULL,
    allowed_origins text,
    redirect_uris text NOT NULL,
    homepage_url character varying(200),
    privacy_url character varying(200),
    terms_url character varying(200),
    date_added timestamp with time zone NOT NULL,
    CONSTRAINT sentry_apiapplication_status_check CHECK ((status >= 0))
);


ALTER TABLE public.sentry_apiapplication OWNER TO postgres;

--
-- Name: sentry_apiapplication_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_apiapplication_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_apiapplication_id_seq OWNER TO postgres;

--
-- Name: sentry_apiapplication_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_apiapplication_id_seq OWNED BY public.sentry_apiapplication.id;


--
-- Name: sentry_apiauthorization; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_apiauthorization (
    id bigint NOT NULL,
    application_id bigint,
    user_id integer NOT NULL,
    scopes bigint NOT NULL,
    date_added timestamp with time zone NOT NULL,
    scope_list text[]
);


ALTER TABLE public.sentry_apiauthorization OWNER TO postgres;

--
-- Name: sentry_apiauthorization_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_apiauthorization_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_apiauthorization_id_seq OWNER TO postgres;

--
-- Name: sentry_apiauthorization_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_apiauthorization_id_seq OWNED BY public.sentry_apiauthorization.id;


--
-- Name: sentry_apigrant; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_apigrant (
    id bigint NOT NULL,
    user_id integer NOT NULL,
    application_id bigint NOT NULL,
    code character varying(64) NOT NULL,
    expires_at timestamp with time zone NOT NULL,
    redirect_uri character varying(255) NOT NULL,
    scopes bigint NOT NULL,
    scope_list text[]
);


ALTER TABLE public.sentry_apigrant OWNER TO postgres;

--
-- Name: sentry_apigrant_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_apigrant_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_apigrant_id_seq OWNER TO postgres;

--
-- Name: sentry_apigrant_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_apigrant_id_seq OWNED BY public.sentry_apigrant.id;


--
-- Name: sentry_apikey; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_apikey (
    id bigint NOT NULL,
    organization_id bigint NOT NULL,
    label character varying(64) NOT NULL,
    key character varying(32) NOT NULL,
    scopes bigint NOT NULL,
    status integer NOT NULL,
    date_added timestamp with time zone NOT NULL,
    allowed_origins text,
    scope_list text[],
    CONSTRAINT sentry_apikey_status_check CHECK ((status >= 0))
);


ALTER TABLE public.sentry_apikey OWNER TO postgres;

--
-- Name: sentry_apikey_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_apikey_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_apikey_id_seq OWNER TO postgres;

--
-- Name: sentry_apikey_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_apikey_id_seq OWNED BY public.sentry_apikey.id;


--
-- Name: sentry_apitoken; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_apitoken (
    id bigint NOT NULL,
    user_id integer NOT NULL,
    token character varying(64) NOT NULL,
    scopes bigint NOT NULL,
    date_added timestamp with time zone NOT NULL,
    application_id bigint,
    refresh_token character varying(64),
    expires_at timestamp with time zone,
    scope_list text[]
);


ALTER TABLE public.sentry_apitoken OWNER TO postgres;

--
-- Name: sentry_apitoken_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_apitoken_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_apitoken_id_seq OWNER TO postgres;

--
-- Name: sentry_apitoken_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_apitoken_id_seq OWNED BY public.sentry_apitoken.id;


--
-- Name: sentry_auditlogentry; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_auditlogentry (
    id bigint NOT NULL,
    organization_id bigint NOT NULL,
    actor_id integer,
    target_object integer,
    target_user_id integer,
    event integer NOT NULL,
    data text NOT NULL,
    datetime timestamp with time zone NOT NULL,
    ip_address inet,
    actor_label character varying(64),
    actor_key_id bigint,
    CONSTRAINT sentry_auditlogentry_event_check CHECK ((event >= 0)),
    CONSTRAINT sentry_auditlogentry_target_object_check CHECK ((target_object >= 0))
);


ALTER TABLE public.sentry_auditlogentry OWNER TO postgres;

--
-- Name: sentry_auditlogentry_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_auditlogentry_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_auditlogentry_id_seq OWNER TO postgres;

--
-- Name: sentry_auditlogentry_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_auditlogentry_id_seq OWNED BY public.sentry_auditlogentry.id;


--
-- Name: sentry_authidentity; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_authidentity (
    id bigint NOT NULL,
    user_id integer NOT NULL,
    auth_provider_id bigint NOT NULL,
    ident character varying(128) NOT NULL,
    data text NOT NULL,
    date_added timestamp with time zone NOT NULL,
    last_verified timestamp with time zone NOT NULL,
    last_synced timestamp with time zone NOT NULL
);


ALTER TABLE public.sentry_authidentity OWNER TO postgres;

--
-- Name: sentry_authidentity_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_authidentity_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_authidentity_id_seq OWNER TO postgres;

--
-- Name: sentry_authidentity_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_authidentity_id_seq OWNED BY public.sentry_authidentity.id;


--
-- Name: sentry_authprovider; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_authprovider (
    id bigint NOT NULL,
    organization_id bigint NOT NULL,
    provider character varying(128) NOT NULL,
    config text NOT NULL,
    date_added timestamp with time zone NOT NULL,
    sync_time integer,
    last_sync timestamp with time zone,
    default_role integer NOT NULL,
    default_global_access boolean NOT NULL,
    flags bigint NOT NULL,
    CONSTRAINT sentry_authprovider_default_role_check CHECK ((default_role >= 0)),
    CONSTRAINT sentry_authprovider_sync_time_check CHECK ((sync_time >= 0))
);


ALTER TABLE public.sentry_authprovider OWNER TO postgres;

--
-- Name: sentry_authprovider_default_teams; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_authprovider_default_teams (
    id integer NOT NULL,
    authprovider_id bigint NOT NULL,
    team_id bigint NOT NULL
);


ALTER TABLE public.sentry_authprovider_default_teams OWNER TO postgres;

--
-- Name: sentry_authprovider_default_teams_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_authprovider_default_teams_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_authprovider_default_teams_id_seq OWNER TO postgres;

--
-- Name: sentry_authprovider_default_teams_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_authprovider_default_teams_id_seq OWNED BY public.sentry_authprovider_default_teams.id;


--
-- Name: sentry_authprovider_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_authprovider_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_authprovider_id_seq OWNER TO postgres;

--
-- Name: sentry_authprovider_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_authprovider_id_seq OWNED BY public.sentry_authprovider.id;


--
-- Name: sentry_broadcast; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_broadcast (
    id bigint NOT NULL,
    message character varying(256) NOT NULL,
    link character varying(200),
    is_active boolean NOT NULL,
    date_added timestamp with time zone NOT NULL,
    title character varying(32) NOT NULL,
    upstream_id character varying(32),
    date_expires timestamp with time zone
);


ALTER TABLE public.sentry_broadcast OWNER TO postgres;

--
-- Name: sentry_broadcast_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_broadcast_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_broadcast_id_seq OWNER TO postgres;

--
-- Name: sentry_broadcast_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_broadcast_id_seq OWNED BY public.sentry_broadcast.id;


--
-- Name: sentry_broadcastseen; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_broadcastseen (
    id bigint NOT NULL,
    broadcast_id bigint NOT NULL,
    user_id integer NOT NULL,
    date_seen timestamp with time zone NOT NULL
);


ALTER TABLE public.sentry_broadcastseen OWNER TO postgres;

--
-- Name: sentry_broadcastseen_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_broadcastseen_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_broadcastseen_id_seq OWNER TO postgres;

--
-- Name: sentry_broadcastseen_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_broadcastseen_id_seq OWNED BY public.sentry_broadcastseen.id;


--
-- Name: sentry_commit; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_commit (
    id bigint NOT NULL,
    organization_id integer NOT NULL,
    repository_id integer NOT NULL,
    key character varying(64) NOT NULL,
    date_added timestamp with time zone NOT NULL,
    author_id bigint,
    message text,
    CONSTRAINT sentry_commit_organization_id_check CHECK ((organization_id >= 0)),
    CONSTRAINT sentry_commit_repository_id_check CHECK ((repository_id >= 0))
);


ALTER TABLE public.sentry_commit OWNER TO postgres;

--
-- Name: sentry_commit_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_commit_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_commit_id_seq OWNER TO postgres;

--
-- Name: sentry_commit_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_commit_id_seq OWNED BY public.sentry_commit.id;


--
-- Name: sentry_commitauthor; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_commitauthor (
    id bigint NOT NULL,
    organization_id integer NOT NULL,
    name character varying(128),
    email character varying(75) NOT NULL,
    external_id character varying(164),
    CONSTRAINT sentry_commitauthor_organization_id_check CHECK ((organization_id >= 0))
);


ALTER TABLE public.sentry_commitauthor OWNER TO postgres;

--
-- Name: sentry_commitauthor_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_commitauthor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_commitauthor_id_seq OWNER TO postgres;

--
-- Name: sentry_commitauthor_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_commitauthor_id_seq OWNED BY public.sentry_commitauthor.id;


--
-- Name: sentry_commitfilechange; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_commitfilechange (
    id bigint NOT NULL,
    organization_id integer NOT NULL,
    commit_id bigint NOT NULL,
    filename text NOT NULL,
    type character varying(1) NOT NULL,
    CONSTRAINT sentry_commitfilechange_organization_id_check CHECK ((organization_id >= 0))
);


ALTER TABLE public.sentry_commitfilechange OWNER TO postgres;

--
-- Name: sentry_commitfilechange_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_commitfilechange_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_commitfilechange_id_seq OWNER TO postgres;

--
-- Name: sentry_commitfilechange_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_commitfilechange_id_seq OWNED BY public.sentry_commitfilechange.id;


--
-- Name: sentry_deploy; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_deploy (
    id bigint NOT NULL,
    organization_id integer NOT NULL,
    release_id bigint NOT NULL,
    environment_id integer NOT NULL,
    date_finished timestamp with time zone NOT NULL,
    date_started timestamp with time zone,
    name character varying(64),
    url character varying(200),
    notified boolean,
    CONSTRAINT sentry_deploy_environment_id_check CHECK ((environment_id >= 0)),
    CONSTRAINT sentry_deploy_organization_id_check CHECK ((organization_id >= 0))
);


ALTER TABLE public.sentry_deploy OWNER TO postgres;

--
-- Name: sentry_deploy_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_deploy_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_deploy_id_seq OWNER TO postgres;

--
-- Name: sentry_deploy_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_deploy_id_seq OWNED BY public.sentry_deploy.id;


--
-- Name: sentry_distribution; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_distribution (
    id bigint NOT NULL,
    organization_id integer NOT NULL,
    release_id bigint NOT NULL,
    name character varying(64) NOT NULL,
    date_added timestamp with time zone NOT NULL,
    CONSTRAINT sentry_distribution_organization_id_check CHECK ((organization_id >= 0))
);


ALTER TABLE public.sentry_distribution OWNER TO postgres;

--
-- Name: sentry_distribution_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_distribution_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_distribution_id_seq OWNER TO postgres;

--
-- Name: sentry_distribution_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_distribution_id_seq OWNED BY public.sentry_distribution.id;


--
-- Name: sentry_dsymapp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_dsymapp (
    id bigint NOT NULL,
    project_id bigint NOT NULL,
    app_id character varying(64) NOT NULL,
    sync_id character varying(64),
    data text NOT NULL,
    platform integer NOT NULL,
    last_synced timestamp with time zone NOT NULL,
    date_added timestamp with time zone NOT NULL,
    CONSTRAINT sentry_dsymapp_platform_check CHECK ((platform >= 0))
);


ALTER TABLE public.sentry_dsymapp OWNER TO postgres;

--
-- Name: sentry_dsymapp_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_dsymapp_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_dsymapp_id_seq OWNER TO postgres;

--
-- Name: sentry_dsymapp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_dsymapp_id_seq OWNED BY public.sentry_dsymapp.id;


--
-- Name: sentry_email; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_email (
    id bigint NOT NULL,
    email public.citext NOT NULL,
    date_added timestamp with time zone NOT NULL
);


ALTER TABLE public.sentry_email OWNER TO postgres;

--
-- Name: sentry_email_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_email_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_email_id_seq OWNER TO postgres;

--
-- Name: sentry_email_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_email_id_seq OWNED BY public.sentry_email.id;


--
-- Name: sentry_environment; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_environment (
    id bigint NOT NULL,
    project_id integer,
    name character varying(64) NOT NULL,
    date_added timestamp with time zone NOT NULL,
    organization_id integer NOT NULL,
    CONSTRAINT ck_organization_id_pstv_217ef821157f703 CHECK ((organization_id >= 0)),
    CONSTRAINT sentry_environment_project_id_check CHECK ((project_id >= 0))
);


ALTER TABLE public.sentry_environment OWNER TO postgres;

--
-- Name: sentry_environment_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_environment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_environment_id_seq OWNER TO postgres;

--
-- Name: sentry_environment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_environment_id_seq OWNED BY public.sentry_environment.id;


--
-- Name: sentry_environmentproject; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_environmentproject (
    id bigint NOT NULL,
    project_id bigint NOT NULL,
    environment_id bigint NOT NULL
);


ALTER TABLE public.sentry_environmentproject OWNER TO postgres;

--
-- Name: sentry_environmentproject_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_environmentproject_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_environmentproject_id_seq OWNER TO postgres;

--
-- Name: sentry_environmentproject_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_environmentproject_id_seq OWNED BY public.sentry_environmentproject.id;


--
-- Name: sentry_environmentrelease; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_environmentrelease (
    id bigint NOT NULL,
    project_id integer,
    release_id integer NOT NULL,
    environment_id integer NOT NULL,
    first_seen timestamp with time zone NOT NULL,
    last_seen timestamp with time zone NOT NULL,
    organization_id integer NOT NULL,
    CONSTRAINT ck_organization_id_pstv_4f21eb33d1f59511 CHECK ((organization_id >= 0)),
    CONSTRAINT sentry_environmentrelease_environment_id_check CHECK ((environment_id >= 0)),
    CONSTRAINT sentry_environmentrelease_project_id_check CHECK ((project_id >= 0)),
    CONSTRAINT sentry_environmentrelease_release_id_check CHECK ((release_id >= 0))
);


ALTER TABLE public.sentry_environmentrelease OWNER TO postgres;

--
-- Name: sentry_environmentrelease_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_environmentrelease_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_environmentrelease_id_seq OWNER TO postgres;

--
-- Name: sentry_environmentrelease_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_environmentrelease_id_seq OWNED BY public.sentry_environmentrelease.id;


--
-- Name: sentry_eventmapping; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_eventmapping (
    id bigint NOT NULL,
    project_id bigint NOT NULL,
    group_id bigint NOT NULL,
    event_id character varying(32) NOT NULL,
    date_added timestamp with time zone NOT NULL
);


ALTER TABLE public.sentry_eventmapping OWNER TO postgres;

--
-- Name: sentry_eventmapping_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_eventmapping_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_eventmapping_id_seq OWNER TO postgres;

--
-- Name: sentry_eventmapping_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_eventmapping_id_seq OWNED BY public.sentry_eventmapping.id;


--
-- Name: sentry_eventprocessingissue; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_eventprocessingissue (
    id bigint NOT NULL,
    raw_event_id bigint NOT NULL,
    processing_issue_id bigint NOT NULL
);


ALTER TABLE public.sentry_eventprocessingissue OWNER TO postgres;

--
-- Name: sentry_eventprocessingissue_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_eventprocessingissue_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_eventprocessingissue_id_seq OWNER TO postgres;

--
-- Name: sentry_eventprocessingissue_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_eventprocessingissue_id_seq OWNED BY public.sentry_eventprocessingissue.id;


--
-- Name: sentry_eventtag; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_eventtag (
    id bigint NOT NULL,
    project_id bigint NOT NULL,
    event_id bigint NOT NULL,
    key_id bigint NOT NULL,
    value_id bigint NOT NULL,
    date_added timestamp with time zone NOT NULL,
    group_id bigint
);


ALTER TABLE public.sentry_eventtag OWNER TO postgres;

--
-- Name: sentry_eventtag_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_eventtag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_eventtag_id_seq OWNER TO postgres;

--
-- Name: sentry_eventtag_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_eventtag_id_seq OWNED BY public.sentry_eventtag.id;


--
-- Name: sentry_eventuser; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_eventuser (
    id bigint NOT NULL,
    project_id bigint NOT NULL,
    ident character varying(128),
    email character varying(75),
    username character varying(128),
    ip_address inet,
    date_added timestamp with time zone NOT NULL,
    hash character varying(32) NOT NULL,
    name character varying(128)
);


ALTER TABLE public.sentry_eventuser OWNER TO postgres;

--
-- Name: sentry_eventuser_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_eventuser_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_eventuser_id_seq OWNER TO postgres;

--
-- Name: sentry_eventuser_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_eventuser_id_seq OWNED BY public.sentry_eventuser.id;


--
-- Name: sentry_featureadoption; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_featureadoption (
    id bigint NOT NULL,
    organization_id bigint NOT NULL,
    feature_id integer NOT NULL,
    date_completed timestamp with time zone NOT NULL,
    complete boolean NOT NULL,
    applicable boolean NOT NULL,
    data text NOT NULL,
    CONSTRAINT sentry_featureadoption_feature_id_check CHECK ((feature_id >= 0))
);


ALTER TABLE public.sentry_featureadoption OWNER TO postgres;

--
-- Name: sentry_featureadoption_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_featureadoption_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_featureadoption_id_seq OWNER TO postgres;

--
-- Name: sentry_featureadoption_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_featureadoption_id_seq OWNED BY public.sentry_featureadoption.id;


--
-- Name: sentry_file; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_file (
    id bigint NOT NULL,
    name character varying(128) NOT NULL,
    path text,
    type character varying(64) NOT NULL,
    size integer,
    "timestamp" timestamp with time zone NOT NULL,
    checksum character varying(40),
    headers text NOT NULL,
    blob_id bigint,
    CONSTRAINT sentry_file_size_check CHECK ((size >= 0))
);


ALTER TABLE public.sentry_file OWNER TO postgres;

--
-- Name: sentry_file_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_file_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_file_id_seq OWNER TO postgres;

--
-- Name: sentry_file_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_file_id_seq OWNED BY public.sentry_file.id;


--
-- Name: sentry_fileblob; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_fileblob (
    id bigint NOT NULL,
    path text,
    size integer,
    checksum character varying(40) NOT NULL,
    "timestamp" timestamp with time zone NOT NULL,
    CONSTRAINT sentry_fileblob_size_check CHECK ((size >= 0))
);


ALTER TABLE public.sentry_fileblob OWNER TO postgres;

--
-- Name: sentry_fileblob_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_fileblob_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_fileblob_id_seq OWNER TO postgres;

--
-- Name: sentry_fileblob_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_fileblob_id_seq OWNED BY public.sentry_fileblob.id;


--
-- Name: sentry_fileblobindex; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_fileblobindex (
    id bigint NOT NULL,
    file_id bigint NOT NULL,
    blob_id bigint NOT NULL,
    "offset" integer NOT NULL,
    CONSTRAINT sentry_fileblobindex_offset_check CHECK (("offset" >= 0))
);


ALTER TABLE public.sentry_fileblobindex OWNER TO postgres;

--
-- Name: sentry_fileblobindex_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_fileblobindex_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_fileblobindex_id_seq OWNER TO postgres;

--
-- Name: sentry_fileblobindex_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_fileblobindex_id_seq OWNED BY public.sentry_fileblobindex.id;


--
-- Name: sentry_filterkey; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_filterkey (
    id bigint NOT NULL,
    project_id bigint NOT NULL,
    key character varying(32) NOT NULL,
    values_seen integer NOT NULL,
    label character varying(64),
    status integer NOT NULL,
    CONSTRAINT ck_status_pstv_56aaa5973127b013 CHECK ((status >= 0)),
    CONSTRAINT ck_values_seen_pstv_12eab0d3ff94a35c CHECK ((values_seen >= 0)),
    CONSTRAINT sentry_filterkey_status_check CHECK ((status >= 0)),
    CONSTRAINT sentry_filterkey_values_seen_check CHECK ((values_seen >= 0))
);


ALTER TABLE public.sentry_filterkey OWNER TO postgres;

--
-- Name: sentry_filterkey_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_filterkey_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_filterkey_id_seq OWNER TO postgres;

--
-- Name: sentry_filterkey_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_filterkey_id_seq OWNED BY public.sentry_filterkey.id;


--
-- Name: sentry_filtervalue; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_filtervalue (
    id bigint NOT NULL,
    key character varying(32) NOT NULL,
    value character varying(200) NOT NULL,
    project_id bigint,
    times_seen integer NOT NULL,
    last_seen timestamp with time zone,
    first_seen timestamp with time zone,
    data text,
    CONSTRAINT ck_times_seen_pstv_10c4372f28cef967 CHECK ((times_seen >= 0)),
    CONSTRAINT sentry_filtervalue_times_seen_check CHECK ((times_seen >= 0))
);


ALTER TABLE public.sentry_filtervalue OWNER TO postgres;

--
-- Name: sentry_filtervalue_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_filtervalue_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_filtervalue_id_seq OWNER TO postgres;

--
-- Name: sentry_filtervalue_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_filtervalue_id_seq OWNED BY public.sentry_filtervalue.id;


--
-- Name: sentry_groupasignee; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_groupasignee (
    id bigint NOT NULL,
    project_id bigint NOT NULL,
    group_id bigint NOT NULL,
    user_id integer NOT NULL,
    date_added timestamp with time zone NOT NULL
);


ALTER TABLE public.sentry_groupasignee OWNER TO postgres;

--
-- Name: sentry_groupasignee_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_groupasignee_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_groupasignee_id_seq OWNER TO postgres;

--
-- Name: sentry_groupasignee_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_groupasignee_id_seq OWNED BY public.sentry_groupasignee.id;


--
-- Name: sentry_groupbookmark; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_groupbookmark (
    id bigint NOT NULL,
    project_id bigint NOT NULL,
    group_id bigint NOT NULL,
    user_id integer NOT NULL,
    date_added timestamp with time zone
);


ALTER TABLE public.sentry_groupbookmark OWNER TO postgres;

--
-- Name: sentry_groupbookmark_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_groupbookmark_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_groupbookmark_id_seq OWNER TO postgres;

--
-- Name: sentry_groupbookmark_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_groupbookmark_id_seq OWNED BY public.sentry_groupbookmark.id;


--
-- Name: sentry_groupcommitresolution; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_groupcommitresolution (
    id bigint NOT NULL,
    group_id integer NOT NULL,
    commit_id integer NOT NULL,
    datetime timestamp with time zone NOT NULL,
    CONSTRAINT sentry_groupcommitresolution_commit_id_check CHECK ((commit_id >= 0)),
    CONSTRAINT sentry_groupcommitresolution_group_id_check CHECK ((group_id >= 0))
);


ALTER TABLE public.sentry_groupcommitresolution OWNER TO postgres;

--
-- Name: sentry_groupcommitresolution_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_groupcommitresolution_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_groupcommitresolution_id_seq OWNER TO postgres;

--
-- Name: sentry_groupcommitresolution_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_groupcommitresolution_id_seq OWNED BY public.sentry_groupcommitresolution.id;


--
-- Name: sentry_groupedmessage; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_groupedmessage (
    id bigint NOT NULL,
    logger character varying(64) NOT NULL,
    level integer NOT NULL,
    message text NOT NULL,
    view character varying(200),
    status integer NOT NULL,
    times_seen integer NOT NULL,
    last_seen timestamp with time zone NOT NULL,
    first_seen timestamp with time zone NOT NULL,
    data text,
    score integer NOT NULL,
    project_id bigint,
    time_spent_total integer NOT NULL,
    time_spent_count integer NOT NULL,
    resolved_at timestamp with time zone,
    active_at timestamp with time zone,
    is_public boolean,
    platform character varying(64),
    num_comments integer,
    first_release_id bigint,
    short_id bigint,
    CONSTRAINT ck_num_comments_pstv_44851d4d5d739eab CHECK ((num_comments >= 0)),
    CONSTRAINT sentry_groupedmessage_level_check CHECK ((level >= 0)),
    CONSTRAINT sentry_groupedmessage_num_comments_check CHECK ((num_comments >= 0)),
    CONSTRAINT sentry_groupedmessage_status_check CHECK ((status >= 0)),
    CONSTRAINT sentry_groupedmessage_times_seen_check CHECK ((times_seen >= 0))
);


ALTER TABLE public.sentry_groupedmessage OWNER TO postgres;

--
-- Name: sentry_groupedmessage_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_groupedmessage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_groupedmessage_id_seq OWNER TO postgres;

--
-- Name: sentry_groupedmessage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_groupedmessage_id_seq OWNED BY public.sentry_groupedmessage.id;


--
-- Name: sentry_groupemailthread; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_groupemailthread (
    id bigint NOT NULL,
    email character varying(75) NOT NULL,
    project_id bigint NOT NULL,
    group_id bigint NOT NULL,
    msgid character varying(100) NOT NULL,
    date timestamp with time zone NOT NULL
);


ALTER TABLE public.sentry_groupemailthread OWNER TO postgres;

--
-- Name: sentry_groupemailthread_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_groupemailthread_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_groupemailthread_id_seq OWNER TO postgres;

--
-- Name: sentry_groupemailthread_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_groupemailthread_id_seq OWNED BY public.sentry_groupemailthread.id;


--
-- Name: sentry_grouphash; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_grouphash (
    id bigint NOT NULL,
    project_id bigint,
    hash character varying(32) NOT NULL,
    group_id bigint,
    state integer,
    group_tombstone_id integer,
    CONSTRAINT ck_group_tombstone_id_pstv_17e9b9f9962c3c62 CHECK ((group_tombstone_id >= 0)),
    CONSTRAINT ck_state_pstv_556c62431651a0b1 CHECK ((state >= 0)),
    CONSTRAINT sentry_grouphash_group_tombstone_id_check CHECK ((group_tombstone_id >= 0)),
    CONSTRAINT sentry_grouphash_state_check CHECK ((state >= 0))
);


ALTER TABLE public.sentry_grouphash OWNER TO postgres;

--
-- Name: sentry_grouphash_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_grouphash_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_grouphash_id_seq OWNER TO postgres;

--
-- Name: sentry_grouphash_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_grouphash_id_seq OWNED BY public.sentry_grouphash.id;


--
-- Name: sentry_grouplink; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_grouplink (
    id bigint NOT NULL,
    group_id bigint NOT NULL,
    project_id bigint NOT NULL,
    linked_type integer NOT NULL,
    linked_id bigint NOT NULL,
    relationship integer NOT NULL,
    data text NOT NULL,
    datetime timestamp with time zone NOT NULL,
    CONSTRAINT sentry_grouplink_linked_type_check CHECK ((linked_type >= 0)),
    CONSTRAINT sentry_grouplink_relationship_check CHECK ((relationship >= 0))
);


ALTER TABLE public.sentry_grouplink OWNER TO postgres;

--
-- Name: sentry_grouplink_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_grouplink_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_grouplink_id_seq OWNER TO postgres;

--
-- Name: sentry_grouplink_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_grouplink_id_seq OWNED BY public.sentry_grouplink.id;


--
-- Name: sentry_groupmeta; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_groupmeta (
    id bigint NOT NULL,
    group_id bigint NOT NULL,
    key character varying(64) NOT NULL,
    value text NOT NULL
);


ALTER TABLE public.sentry_groupmeta OWNER TO postgres;

--
-- Name: sentry_groupmeta_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_groupmeta_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_groupmeta_id_seq OWNER TO postgres;

--
-- Name: sentry_groupmeta_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_groupmeta_id_seq OWNED BY public.sentry_groupmeta.id;


--
-- Name: sentry_groupredirect; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_groupredirect (
    id bigint NOT NULL,
    group_id bigint NOT NULL,
    previous_group_id bigint NOT NULL
);


ALTER TABLE public.sentry_groupredirect OWNER TO postgres;

--
-- Name: sentry_groupredirect_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_groupredirect_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_groupredirect_id_seq OWNER TO postgres;

--
-- Name: sentry_groupredirect_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_groupredirect_id_seq OWNED BY public.sentry_groupredirect.id;


--
-- Name: sentry_grouprelease; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_grouprelease (
    id bigint NOT NULL,
    project_id integer NOT NULL,
    group_id integer NOT NULL,
    release_id integer NOT NULL,
    environment character varying(64) NOT NULL,
    first_seen timestamp with time zone NOT NULL,
    last_seen timestamp with time zone NOT NULL,
    CONSTRAINT sentry_grouprelease_group_id_check CHECK ((group_id >= 0)),
    CONSTRAINT sentry_grouprelease_project_id_check CHECK ((project_id >= 0)),
    CONSTRAINT sentry_grouprelease_release_id_check CHECK ((release_id >= 0))
);


ALTER TABLE public.sentry_grouprelease OWNER TO postgres;

--
-- Name: sentry_grouprelease_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_grouprelease_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_grouprelease_id_seq OWNER TO postgres;

--
-- Name: sentry_grouprelease_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_grouprelease_id_seq OWNED BY public.sentry_grouprelease.id;


--
-- Name: sentry_groupresolution; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_groupresolution (
    id bigint NOT NULL,
    group_id bigint NOT NULL,
    release_id bigint NOT NULL,
    datetime timestamp with time zone NOT NULL,
    status integer NOT NULL,
    type integer,
    actor_id integer,
    CONSTRAINT ck_actor_id_pstv_750a8dad4187faba CHECK ((actor_id >= 0)),
    CONSTRAINT ck_status_pstv_375a4efcf0df73b9 CHECK ((status >= 0)),
    CONSTRAINT ck_type_pstv_15c3a9fd0180fff9 CHECK ((type >= 0)),
    CONSTRAINT sentry_groupresolution_actor_id_check CHECK ((actor_id >= 0)),
    CONSTRAINT sentry_groupresolution_status_check CHECK ((status >= 0)),
    CONSTRAINT sentry_groupresolution_type_check CHECK ((type >= 0))
);


ALTER TABLE public.sentry_groupresolution OWNER TO postgres;

--
-- Name: sentry_groupresolution_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_groupresolution_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_groupresolution_id_seq OWNER TO postgres;

--
-- Name: sentry_groupresolution_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_groupresolution_id_seq OWNED BY public.sentry_groupresolution.id;


--
-- Name: sentry_grouprulestatus; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_grouprulestatus (
    id bigint NOT NULL,
    project_id bigint NOT NULL,
    rule_id bigint NOT NULL,
    group_id bigint NOT NULL,
    status smallint NOT NULL,
    date_added timestamp with time zone NOT NULL,
    last_active timestamp with time zone,
    CONSTRAINT sentry_grouprulestatus_status_check CHECK ((status >= 0))
);


ALTER TABLE public.sentry_grouprulestatus OWNER TO postgres;

--
-- Name: sentry_grouprulestatus_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_grouprulestatus_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_grouprulestatus_id_seq OWNER TO postgres;

--
-- Name: sentry_grouprulestatus_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_grouprulestatus_id_seq OWNED BY public.sentry_grouprulestatus.id;


--
-- Name: sentry_groupseen; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_groupseen (
    id bigint NOT NULL,
    project_id bigint NOT NULL,
    group_id bigint NOT NULL,
    user_id integer NOT NULL,
    last_seen timestamp with time zone NOT NULL
);


ALTER TABLE public.sentry_groupseen OWNER TO postgres;

--
-- Name: sentry_groupseen_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_groupseen_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_groupseen_id_seq OWNER TO postgres;

--
-- Name: sentry_groupseen_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_groupseen_id_seq OWNED BY public.sentry_groupseen.id;


--
-- Name: sentry_groupshare; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_groupshare (
    id bigint NOT NULL,
    project_id bigint NOT NULL,
    group_id bigint NOT NULL,
    uuid character varying(32) NOT NULL,
    user_id integer,
    date_added timestamp with time zone NOT NULL
);


ALTER TABLE public.sentry_groupshare OWNER TO postgres;

--
-- Name: sentry_groupshare_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_groupshare_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_groupshare_id_seq OWNER TO postgres;

--
-- Name: sentry_groupshare_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_groupshare_id_seq OWNED BY public.sentry_groupshare.id;


--
-- Name: sentry_groupsnooze; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_groupsnooze (
    id bigint NOT NULL,
    group_id bigint NOT NULL,
    until timestamp with time zone,
    count integer,
    "window" integer,
    user_count integer,
    user_window integer,
    state text,
    actor_id integer,
    CONSTRAINT ck_actor_id_pstv_1174f21750349df2 CHECK ((actor_id >= 0)),
    CONSTRAINT ck_count_pstv_2e729d18cc059d41 CHECK ((count >= 0)),
    CONSTRAINT ck_user_count_pstv_26e6359669512ec8 CHECK ((user_count >= 0)),
    CONSTRAINT ck_user_window_pstv_5621099b76ac766a CHECK ((user_window >= 0)),
    CONSTRAINT ck_window_pstv_2569b2f0095a6e41 CHECK (("window" >= 0)),
    CONSTRAINT sentry_groupsnooze_actor_id_check CHECK ((actor_id >= 0)),
    CONSTRAINT sentry_groupsnooze_count_check CHECK ((count >= 0)),
    CONSTRAINT sentry_groupsnooze_user_count_check CHECK ((user_count >= 0)),
    CONSTRAINT sentry_groupsnooze_user_window_check CHECK ((user_window >= 0)),
    CONSTRAINT sentry_groupsnooze_window_check CHECK (("window" >= 0))
);


ALTER TABLE public.sentry_groupsnooze OWNER TO postgres;

--
-- Name: sentry_groupsnooze_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_groupsnooze_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_groupsnooze_id_seq OWNER TO postgres;

--
-- Name: sentry_groupsnooze_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_groupsnooze_id_seq OWNED BY public.sentry_groupsnooze.id;


--
-- Name: sentry_groupsubscription; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_groupsubscription (
    id bigint NOT NULL,
    project_id bigint NOT NULL,
    group_id bigint NOT NULL,
    user_id integer NOT NULL,
    is_active boolean NOT NULL,
    reason integer NOT NULL,
    date_added timestamp with time zone,
    CONSTRAINT sentry_groupsubscription_reason_check CHECK ((reason >= 0))
);


ALTER TABLE public.sentry_groupsubscription OWNER TO postgres;

--
-- Name: sentry_groupsubscription_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_groupsubscription_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_groupsubscription_id_seq OWNER TO postgres;

--
-- Name: sentry_groupsubscription_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_groupsubscription_id_seq OWNED BY public.sentry_groupsubscription.id;


--
-- Name: sentry_grouptagkey; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_grouptagkey (
    id bigint NOT NULL,
    project_id bigint,
    group_id bigint NOT NULL,
    key character varying(32) NOT NULL,
    values_seen integer NOT NULL,
    CONSTRAINT sentry_grouptagkey_values_seen_check CHECK ((values_seen >= 0))
);


ALTER TABLE public.sentry_grouptagkey OWNER TO postgres;

--
-- Name: sentry_grouptagkey_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_grouptagkey_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_grouptagkey_id_seq OWNER TO postgres;

--
-- Name: sentry_grouptagkey_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_grouptagkey_id_seq OWNED BY public.sentry_grouptagkey.id;


--
-- Name: sentry_grouptombstone; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_grouptombstone (
    id bigint NOT NULL,
    previous_group_id integer NOT NULL,
    project_id bigint NOT NULL,
    level integer NOT NULL,
    message text NOT NULL,
    culprit character varying(200),
    data text,
    actor_id integer,
    CONSTRAINT sentry_grouptombstone_actor_id_check CHECK ((actor_id >= 0)),
    CONSTRAINT sentry_grouptombstone_level_check CHECK ((level >= 0)),
    CONSTRAINT sentry_grouptombstone_previous_group_id_check CHECK ((previous_group_id >= 0))
);


ALTER TABLE public.sentry_grouptombstone OWNER TO postgres;

--
-- Name: sentry_grouptombstone_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_grouptombstone_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_grouptombstone_id_seq OWNER TO postgres;

--
-- Name: sentry_grouptombstone_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_grouptombstone_id_seq OWNED BY public.sentry_grouptombstone.id;


--
-- Name: sentry_hipchat_ac_tenant; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_hipchat_ac_tenant (
    id character varying(40) NOT NULL,
    room_id character varying(40) NOT NULL,
    room_name character varying(200),
    room_owner_id character varying(40),
    room_owner_name character varying(200),
    secret character varying(120) NOT NULL,
    homepage character varying(250) NOT NULL,
    token_url character varying(250) NOT NULL,
    capabilities_url character varying(250) NOT NULL,
    api_base_url character varying(250) NOT NULL,
    installed_from character varying(250) NOT NULL,
    auth_user_id integer
);


ALTER TABLE public.sentry_hipchat_ac_tenant OWNER TO postgres;

--
-- Name: sentry_hipchat_ac_tenant_organizations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_hipchat_ac_tenant_organizations (
    id integer NOT NULL,
    tenant_id character varying(40) NOT NULL,
    organization_id bigint NOT NULL
);


ALTER TABLE public.sentry_hipchat_ac_tenant_organizations OWNER TO postgres;

--
-- Name: sentry_hipchat_ac_tenant_organizations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_hipchat_ac_tenant_organizations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_hipchat_ac_tenant_organizations_id_seq OWNER TO postgres;

--
-- Name: sentry_hipchat_ac_tenant_organizations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_hipchat_ac_tenant_organizations_id_seq OWNED BY public.sentry_hipchat_ac_tenant_organizations.id;


--
-- Name: sentry_hipchat_ac_tenant_projects; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_hipchat_ac_tenant_projects (
    id integer NOT NULL,
    tenant_id character varying(40) NOT NULL,
    project_id bigint NOT NULL
);


ALTER TABLE public.sentry_hipchat_ac_tenant_projects OWNER TO postgres;

--
-- Name: sentry_hipchat_ac_tenant_projects_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_hipchat_ac_tenant_projects_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_hipchat_ac_tenant_projects_id_seq OWNER TO postgres;

--
-- Name: sentry_hipchat_ac_tenant_projects_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_hipchat_ac_tenant_projects_id_seq OWNED BY public.sentry_hipchat_ac_tenant_projects.id;


--
-- Name: sentry_identity; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_identity (
    id bigint NOT NULL,
    idp_id bigint NOT NULL,
    external_id character varying(64) NOT NULL,
    data text NOT NULL,
    status integer NOT NULL,
    scopes text[],
    date_verified timestamp with time zone NOT NULL,
    date_added timestamp with time zone NOT NULL,
    CONSTRAINT sentry_identity_status_check CHECK ((status >= 0))
);


ALTER TABLE public.sentry_identity OWNER TO postgres;

--
-- Name: sentry_identity_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_identity_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_identity_id_seq OWNER TO postgres;

--
-- Name: sentry_identity_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_identity_id_seq OWNED BY public.sentry_identity.id;


--
-- Name: sentry_identityprovider; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_identityprovider (
    id bigint NOT NULL,
    type character varying(64) NOT NULL,
    instance character varying(64) NOT NULL
);


ALTER TABLE public.sentry_identityprovider OWNER TO postgres;

--
-- Name: sentry_identityprovider_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_identityprovider_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_identityprovider_id_seq OWNER TO postgres;

--
-- Name: sentry_identityprovider_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_identityprovider_id_seq OWNED BY public.sentry_identityprovider.id;


--
-- Name: sentry_integration; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_integration (
    id bigint NOT NULL,
    provider character varying(64) NOT NULL,
    external_id character varying(64) NOT NULL,
    name character varying(200) NOT NULL,
    metadata text NOT NULL
);


ALTER TABLE public.sentry_integration OWNER TO postgres;

--
-- Name: sentry_integration_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_integration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_integration_id_seq OWNER TO postgres;

--
-- Name: sentry_integration_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_integration_id_seq OWNED BY public.sentry_integration.id;


--
-- Name: sentry_lostpasswordhash; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_lostpasswordhash (
    id bigint NOT NULL,
    user_id integer NOT NULL,
    hash character varying(32) NOT NULL,
    date_added timestamp with time zone NOT NULL
);


ALTER TABLE public.sentry_lostpasswordhash OWNER TO postgres;

--
-- Name: sentry_lostpasswordhash_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_lostpasswordhash_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_lostpasswordhash_id_seq OWNER TO postgres;

--
-- Name: sentry_lostpasswordhash_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_lostpasswordhash_id_seq OWNED BY public.sentry_lostpasswordhash.id;


--
-- Name: sentry_message; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_message (
    id bigint NOT NULL,
    message text NOT NULL,
    datetime timestamp with time zone NOT NULL,
    data text,
    group_id bigint,
    message_id character varying(32),
    project_id bigint,
    time_spent integer,
    platform character varying(64)
);


ALTER TABLE public.sentry_message OWNER TO postgres;

--
-- Name: sentry_message_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_message_id_seq OWNER TO postgres;

--
-- Name: sentry_message_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_message_id_seq OWNED BY public.sentry_message.id;


--
-- Name: sentry_messagefiltervalue; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_messagefiltervalue (
    id bigint NOT NULL,
    group_id bigint NOT NULL,
    times_seen integer NOT NULL,
    key character varying(32) NOT NULL,
    value character varying(200) NOT NULL,
    project_id bigint,
    last_seen timestamp with time zone,
    first_seen timestamp with time zone,
    CONSTRAINT sentry_messagefiltervalue_times_seen_check CHECK ((times_seen >= 0))
);


ALTER TABLE public.sentry_messagefiltervalue OWNER TO postgres;

--
-- Name: sentry_messagefiltervalue_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_messagefiltervalue_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_messagefiltervalue_id_seq OWNER TO postgres;

--
-- Name: sentry_messagefiltervalue_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_messagefiltervalue_id_seq OWNED BY public.sentry_messagefiltervalue.id;


--
-- Name: sentry_messageindex; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_messageindex (
    id bigint NOT NULL,
    object_id integer NOT NULL,
    "column" character varying(32) NOT NULL,
    value character varying(128) NOT NULL,
    CONSTRAINT sentry_messageindex_object_id_check CHECK ((object_id >= 0))
);


ALTER TABLE public.sentry_messageindex OWNER TO postgres;

--
-- Name: sentry_messageindex_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_messageindex_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_messageindex_id_seq OWNER TO postgres;

--
-- Name: sentry_messageindex_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_messageindex_id_seq OWNED BY public.sentry_messageindex.id;


--
-- Name: sentry_minidumpfile; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_minidumpfile (
    id bigint NOT NULL,
    file_id bigint NOT NULL,
    event_id character varying(36) NOT NULL
);


ALTER TABLE public.sentry_minidumpfile OWNER TO postgres;

--
-- Name: sentry_minidumpfile_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_minidumpfile_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_minidumpfile_id_seq OWNER TO postgres;

--
-- Name: sentry_minidumpfile_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_minidumpfile_id_seq OWNED BY public.sentry_minidumpfile.id;


--
-- Name: sentry_option; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_option (
    id bigint NOT NULL,
    key character varying(64) NOT NULL,
    value text NOT NULL,
    last_updated timestamp with time zone NOT NULL
);


ALTER TABLE public.sentry_option OWNER TO postgres;

--
-- Name: sentry_option_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_option_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_option_id_seq OWNER TO postgres;

--
-- Name: sentry_option_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_option_id_seq OWNED BY public.sentry_option.id;


--
-- Name: sentry_organization; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_organization (
    id bigint NOT NULL,
    name character varying(64) NOT NULL,
    status integer NOT NULL,
    date_added timestamp with time zone NOT NULL,
    slug character varying(50) NOT NULL,
    flags bigint NOT NULL,
    default_role character varying(32) NOT NULL,
    CONSTRAINT sentry_organization_status_check CHECK ((status >= 0))
);


ALTER TABLE public.sentry_organization OWNER TO postgres;

--
-- Name: sentry_organization_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_organization_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_organization_id_seq OWNER TO postgres;

--
-- Name: sentry_organization_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_organization_id_seq OWNED BY public.sentry_organization.id;


--
-- Name: sentry_organizationaccessrequest; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_organizationaccessrequest (
    id bigint NOT NULL,
    team_id bigint NOT NULL,
    member_id bigint NOT NULL
);


ALTER TABLE public.sentry_organizationaccessrequest OWNER TO postgres;

--
-- Name: sentry_organizationaccessrequest_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_organizationaccessrequest_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_organizationaccessrequest_id_seq OWNER TO postgres;

--
-- Name: sentry_organizationaccessrequest_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_organizationaccessrequest_id_seq OWNED BY public.sentry_organizationaccessrequest.id;


--
-- Name: sentry_organizationavatar; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_organizationavatar (
    id bigint NOT NULL,
    file_id bigint,
    ident character varying(32) NOT NULL,
    organization_id bigint NOT NULL,
    avatar_type smallint NOT NULL,
    CONSTRAINT sentry_organizationavatar_avatar_type_check CHECK ((avatar_type >= 0))
);


ALTER TABLE public.sentry_organizationavatar OWNER TO postgres;

--
-- Name: sentry_organizationavatar_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_organizationavatar_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_organizationavatar_id_seq OWNER TO postgres;

--
-- Name: sentry_organizationavatar_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_organizationavatar_id_seq OWNED BY public.sentry_organizationavatar.id;


--
-- Name: sentry_organizationintegration; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_organizationintegration (
    id bigint NOT NULL,
    organization_id bigint NOT NULL,
    integration_id bigint NOT NULL,
    config text NOT NULL,
    default_auth_id integer,
    CONSTRAINT ck_default_auth_id_pstv_69e09714ab85b548 CHECK ((default_auth_id >= 0)),
    CONSTRAINT sentry_organizationintegration_default_auth_id_check CHECK ((default_auth_id >= 0))
);


ALTER TABLE public.sentry_organizationintegration OWNER TO postgres;

--
-- Name: sentry_organizationintegration_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_organizationintegration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_organizationintegration_id_seq OWNER TO postgres;

--
-- Name: sentry_organizationintegration_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_organizationintegration_id_seq OWNED BY public.sentry_organizationintegration.id;


--
-- Name: sentry_organizationmember; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_organizationmember (
    id bigint NOT NULL,
    organization_id bigint NOT NULL,
    user_id integer,
    type integer NOT NULL,
    date_added timestamp with time zone NOT NULL,
    email character varying(75),
    has_global_access boolean NOT NULL,
    flags bigint NOT NULL,
    role character varying(32) NOT NULL,
    token character varying(64),
    CONSTRAINT sentry_organizationmember_type_check CHECK ((type >= 0))
);


ALTER TABLE public.sentry_organizationmember OWNER TO postgres;

--
-- Name: sentry_organizationmember_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_organizationmember_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_organizationmember_id_seq OWNER TO postgres;

--
-- Name: sentry_organizationmember_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_organizationmember_id_seq OWNED BY public.sentry_organizationmember.id;


--
-- Name: sentry_organizationmember_teams; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_organizationmember_teams (
    id integer NOT NULL,
    organizationmember_id bigint NOT NULL,
    team_id bigint NOT NULL,
    is_active boolean NOT NULL
);


ALTER TABLE public.sentry_organizationmember_teams OWNER TO postgres;

--
-- Name: sentry_organizationmember_teams_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_organizationmember_teams_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_organizationmember_teams_id_seq OWNER TO postgres;

--
-- Name: sentry_organizationmember_teams_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_organizationmember_teams_id_seq OWNED BY public.sentry_organizationmember_teams.id;


--
-- Name: sentry_organizationonboardingtask; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_organizationonboardingtask (
    id bigint NOT NULL,
    organization_id bigint NOT NULL,
    user_id integer,
    task integer NOT NULL,
    status integer NOT NULL,
    date_completed timestamp with time zone NOT NULL,
    project_id bigint,
    data text NOT NULL,
    CONSTRAINT sentry_organizationonboardingtask_status_check CHECK ((status >= 0)),
    CONSTRAINT sentry_organizationonboardingtask_task_check CHECK ((task >= 0))
);


ALTER TABLE public.sentry_organizationonboardingtask OWNER TO postgres;

--
-- Name: sentry_organizationonboardingtask_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_organizationonboardingtask_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_organizationonboardingtask_id_seq OWNER TO postgres;

--
-- Name: sentry_organizationonboardingtask_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_organizationonboardingtask_id_seq OWNED BY public.sentry_organizationonboardingtask.id;


--
-- Name: sentry_organizationoptions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_organizationoptions (
    id bigint NOT NULL,
    organization_id bigint NOT NULL,
    key character varying(64) NOT NULL,
    value text NOT NULL
);


ALTER TABLE public.sentry_organizationoptions OWNER TO postgres;

--
-- Name: sentry_organizationoptions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_organizationoptions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_organizationoptions_id_seq OWNER TO postgres;

--
-- Name: sentry_organizationoptions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_organizationoptions_id_seq OWNED BY public.sentry_organizationoptions.id;


--
-- Name: sentry_processingissue; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_processingissue (
    id bigint NOT NULL,
    project_id bigint NOT NULL,
    checksum character varying(40) NOT NULL,
    type character varying(30) NOT NULL,
    data text NOT NULL,
    datetime timestamp with time zone NOT NULL
);


ALTER TABLE public.sentry_processingissue OWNER TO postgres;

--
-- Name: sentry_processingissue_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_processingissue_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_processingissue_id_seq OWNER TO postgres;

--
-- Name: sentry_processingissue_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_processingissue_id_seq OWNED BY public.sentry_processingissue.id;


--
-- Name: sentry_project; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_project (
    id bigint NOT NULL,
    name character varying(200) NOT NULL,
    public boolean NOT NULL,
    date_added timestamp with time zone NOT NULL,
    status integer NOT NULL,
    slug character varying(50),
    team_id bigint NOT NULL,
    organization_id bigint NOT NULL,
    first_event timestamp with time zone,
    forced_color character varying(6),
    flags bigint,
    platform character varying(64),
    CONSTRAINT ck_status_pstv_3af8360b8a37db73 CHECK ((status >= 0)),
    CONSTRAINT sentry_project_status_check CHECK ((status >= 0))
);


ALTER TABLE public.sentry_project OWNER TO postgres;

--
-- Name: sentry_project_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_project_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_project_id_seq OWNER TO postgres;

--
-- Name: sentry_project_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_project_id_seq OWNED BY public.sentry_project.id;


--
-- Name: sentry_projectbookmark; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_projectbookmark (
    id bigint NOT NULL,
    project_id bigint,
    user_id integer NOT NULL,
    date_added timestamp with time zone
);


ALTER TABLE public.sentry_projectbookmark OWNER TO postgres;

--
-- Name: sentry_projectbookmark_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_projectbookmark_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_projectbookmark_id_seq OWNER TO postgres;

--
-- Name: sentry_projectbookmark_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_projectbookmark_id_seq OWNED BY public.sentry_projectbookmark.id;


--
-- Name: sentry_projectcounter; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_projectcounter (
    id bigint NOT NULL,
    project_id bigint NOT NULL,
    value bigint NOT NULL
);


ALTER TABLE public.sentry_projectcounter OWNER TO postgres;

--
-- Name: sentry_projectcounter_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_projectcounter_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_projectcounter_id_seq OWNER TO postgres;

--
-- Name: sentry_projectcounter_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_projectcounter_id_seq OWNED BY public.sentry_projectcounter.id;


--
-- Name: sentry_projectdsymfile; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_projectdsymfile (
    id bigint NOT NULL,
    file_id bigint NOT NULL,
    object_name text NOT NULL,
    cpu_name character varying(40) NOT NULL,
    project_id bigint,
    uuid character varying(36) NOT NULL
);


ALTER TABLE public.sentry_projectdsymfile OWNER TO postgres;

--
-- Name: sentry_projectdsymfile_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_projectdsymfile_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_projectdsymfile_id_seq OWNER TO postgres;

--
-- Name: sentry_projectdsymfile_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_projectdsymfile_id_seq OWNED BY public.sentry_projectdsymfile.id;


--
-- Name: sentry_projectintegration; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_projectintegration (
    id bigint NOT NULL,
    project_id bigint NOT NULL,
    integration_id bigint NOT NULL,
    config text NOT NULL
);


ALTER TABLE public.sentry_projectintegration OWNER TO postgres;

--
-- Name: sentry_projectintegration_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_projectintegration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_projectintegration_id_seq OWNER TO postgres;

--
-- Name: sentry_projectintegration_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_projectintegration_id_seq OWNED BY public.sentry_projectintegration.id;


--
-- Name: sentry_projectkey; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_projectkey (
    id bigint NOT NULL,
    project_id bigint NOT NULL,
    public_key character varying(32),
    secret_key character varying(32),
    date_added timestamp with time zone,
    roles bigint NOT NULL,
    label character varying(64),
    status integer NOT NULL,
    rate_limit_count integer,
    rate_limit_window integer,
    CONSTRAINT ck_rate_limit_count_pstv_3e2d8378c08cd2b5 CHECK ((rate_limit_count >= 0)),
    CONSTRAINT ck_rate_limit_window_pstv_546e3067ebba7213 CHECK ((rate_limit_window >= 0)),
    CONSTRAINT ck_status_pstv_1f17c0d00e89ed63 CHECK ((status >= 0)),
    CONSTRAINT sentry_projectkey_rate_limit_count_check CHECK ((rate_limit_count >= 0)),
    CONSTRAINT sentry_projectkey_rate_limit_window_check CHECK ((rate_limit_window >= 0)),
    CONSTRAINT sentry_projectkey_status_check CHECK ((status >= 0))
);


ALTER TABLE public.sentry_projectkey OWNER TO postgres;

--
-- Name: sentry_projectkey_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_projectkey_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_projectkey_id_seq OWNER TO postgres;

--
-- Name: sentry_projectkey_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_projectkey_id_seq OWNED BY public.sentry_projectkey.id;


--
-- Name: sentry_projectoptions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_projectoptions (
    id bigint NOT NULL,
    project_id bigint NOT NULL,
    key character varying(64) NOT NULL,
    value text NOT NULL
);


ALTER TABLE public.sentry_projectoptions OWNER TO postgres;

--
-- Name: sentry_projectoptions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_projectoptions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_projectoptions_id_seq OWNER TO postgres;

--
-- Name: sentry_projectoptions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_projectoptions_id_seq OWNED BY public.sentry_projectoptions.id;


--
-- Name: sentry_projectplatform; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_projectplatform (
    id bigint NOT NULL,
    project_id bigint NOT NULL,
    platform character varying(64) NOT NULL,
    date_added timestamp with time zone NOT NULL,
    last_seen timestamp with time zone NOT NULL
);


ALTER TABLE public.sentry_projectplatform OWNER TO postgres;

--
-- Name: sentry_projectplatform_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_projectplatform_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_projectplatform_id_seq OWNER TO postgres;

--
-- Name: sentry_projectplatform_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_projectplatform_id_seq OWNED BY public.sentry_projectplatform.id;


--
-- Name: sentry_projectsymcachefile; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_projectsymcachefile (
    id bigint NOT NULL,
    project_id bigint,
    cache_file_id bigint NOT NULL,
    dsym_file_id bigint NOT NULL,
    checksum character varying(40) NOT NULL,
    version integer NOT NULL,
    CONSTRAINT sentry_projectsymcachefile_version_check CHECK ((version >= 0))
);


ALTER TABLE public.sentry_projectsymcachefile OWNER TO postgres;

--
-- Name: sentry_projectsymcachefile_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_projectsymcachefile_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_projectsymcachefile_id_seq OWNER TO postgres;

--
-- Name: sentry_projectsymcachefile_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_projectsymcachefile_id_seq OWNED BY public.sentry_projectsymcachefile.id;


--
-- Name: sentry_projectteam; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_projectteam (
    id bigint NOT NULL,
    project_id bigint NOT NULL,
    team_id bigint NOT NULL
);


ALTER TABLE public.sentry_projectteam OWNER TO postgres;

--
-- Name: sentry_projectteam_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_projectteam_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_projectteam_id_seq OWNER TO postgres;

--
-- Name: sentry_projectteam_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_projectteam_id_seq OWNED BY public.sentry_projectteam.id;


--
-- Name: sentry_rawevent; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_rawevent (
    id bigint NOT NULL,
    project_id bigint NOT NULL,
    event_id character varying(32),
    datetime timestamp with time zone NOT NULL,
    data text
);


ALTER TABLE public.sentry_rawevent OWNER TO postgres;

--
-- Name: sentry_rawevent_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_rawevent_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_rawevent_id_seq OWNER TO postgres;

--
-- Name: sentry_rawevent_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_rawevent_id_seq OWNED BY public.sentry_rawevent.id;


--
-- Name: sentry_release; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_release (
    id bigint NOT NULL,
    project_id bigint,
    version character varying(64) NOT NULL,
    date_added timestamp with time zone NOT NULL,
    date_released timestamp with time zone,
    ref character varying(64),
    url character varying(200),
    date_started timestamp with time zone,
    data text NOT NULL,
    new_groups integer NOT NULL,
    owner_id integer,
    organization_id bigint NOT NULL,
    commit_count integer,
    last_commit_id integer,
    authors text[],
    total_deploys integer,
    last_deploy_id integer,
    CONSTRAINT ck_commit_count_pstv_1e83b2bc4ee61de0 CHECK ((commit_count >= 0)),
    CONSTRAINT ck_last_commit_id_pstv_55474fbdf700412d CHECK ((last_commit_id >= 0)),
    CONSTRAINT ck_last_deploy_id_pstv_84d0c411d577507 CHECK ((last_deploy_id >= 0)),
    CONSTRAINT ck_new_groups_pstv_2cb74b3445ff4f0c CHECK ((new_groups >= 0)),
    CONSTRAINT ck_total_deploys_pstv_3619b358adfb4f75 CHECK ((total_deploys >= 0)),
    CONSTRAINT sentry_release_commit_count_check CHECK ((commit_count >= 0)),
    CONSTRAINT sentry_release_last_commit_id_check CHECK ((last_commit_id >= 0)),
    CONSTRAINT sentry_release_last_deploy_id_check CHECK ((last_deploy_id >= 0)),
    CONSTRAINT sentry_release_new_groups_check CHECK ((new_groups >= 0)),
    CONSTRAINT sentry_release_total_deploys_check CHECK ((total_deploys >= 0))
);


ALTER TABLE public.sentry_release OWNER TO postgres;

--
-- Name: sentry_release_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_release_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_release_id_seq OWNER TO postgres;

--
-- Name: sentry_release_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_release_id_seq OWNED BY public.sentry_release.id;


--
-- Name: sentry_release_project; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_release_project (
    id bigint NOT NULL,
    project_id bigint NOT NULL,
    release_id bigint NOT NULL,
    new_groups integer,
    CONSTRAINT ck_new_groups_pstv_35c7ce1fb5b5915e CHECK ((new_groups >= 0)),
    CONSTRAINT sentry_release_project_new_groups_check CHECK ((new_groups >= 0))
);


ALTER TABLE public.sentry_release_project OWNER TO postgres;

--
-- Name: sentry_release_project_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_release_project_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_release_project_id_seq OWNER TO postgres;

--
-- Name: sentry_release_project_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_release_project_id_seq OWNED BY public.sentry_release_project.id;


--
-- Name: sentry_releasecommit; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_releasecommit (
    id bigint NOT NULL,
    project_id integer,
    release_id bigint NOT NULL,
    commit_id bigint NOT NULL,
    "order" integer NOT NULL,
    organization_id integer NOT NULL,
    CONSTRAINT ck_organization_id_pstv_63c72b7b5009246 CHECK ((organization_id >= 0)),
    CONSTRAINT ck_project_id_pstv_559bbb746b5337db CHECK ((project_id >= 0)),
    CONSTRAINT sentry_releasecommit_order_check CHECK (("order" >= 0))
);


ALTER TABLE public.sentry_releasecommit OWNER TO postgres;

--
-- Name: sentry_releasecommit_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_releasecommit_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_releasecommit_id_seq OWNER TO postgres;

--
-- Name: sentry_releasecommit_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_releasecommit_id_seq OWNED BY public.sentry_releasecommit.id;


--
-- Name: sentry_releasefile; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_releasefile (
    id bigint NOT NULL,
    project_id bigint,
    release_id bigint NOT NULL,
    file_id bigint NOT NULL,
    ident character varying(40) NOT NULL,
    name text NOT NULL,
    organization_id bigint NOT NULL,
    dist_id bigint
);


ALTER TABLE public.sentry_releasefile OWNER TO postgres;

--
-- Name: sentry_releasefile_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_releasefile_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_releasefile_id_seq OWNER TO postgres;

--
-- Name: sentry_releasefile_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_releasefile_id_seq OWNED BY public.sentry_releasefile.id;


--
-- Name: sentry_releaseheadcommit; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_releaseheadcommit (
    id bigint NOT NULL,
    organization_id integer NOT NULL,
    repository_id integer NOT NULL,
    release_id bigint NOT NULL,
    commit_id bigint NOT NULL,
    CONSTRAINT sentry_releaseheadcommit_organization_id_check CHECK ((organization_id >= 0)),
    CONSTRAINT sentry_releaseheadcommit_repository_id_check CHECK ((repository_id >= 0))
);


ALTER TABLE public.sentry_releaseheadcommit OWNER TO postgres;

--
-- Name: sentry_releaseheadcommit_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_releaseheadcommit_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_releaseheadcommit_id_seq OWNER TO postgres;

--
-- Name: sentry_releaseheadcommit_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_releaseheadcommit_id_seq OWNED BY public.sentry_releaseheadcommit.id;


--
-- Name: sentry_repository; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_repository (
    id bigint NOT NULL,
    organization_id integer NOT NULL,
    name character varying(200) NOT NULL,
    date_added timestamp with time zone NOT NULL,
    url character varying(200),
    provider character varying(64),
    external_id character varying(64),
    config text NOT NULL,
    status integer NOT NULL,
    integration_id integer,
    CONSTRAINT ck_integration_id_pstv_4f3ef70e3e282bab CHECK ((integration_id >= 0)),
    CONSTRAINT ck_status_pstv_562b3ff4dae47f6b CHECK ((status >= 0)),
    CONSTRAINT sentry_repository_integration_id_check CHECK ((integration_id >= 0)),
    CONSTRAINT sentry_repository_organization_id_check CHECK ((organization_id >= 0)),
    CONSTRAINT sentry_repository_status_check CHECK ((status >= 0))
);


ALTER TABLE public.sentry_repository OWNER TO postgres;

--
-- Name: sentry_repository_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_repository_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_repository_id_seq OWNER TO postgres;

--
-- Name: sentry_repository_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_repository_id_seq OWNED BY public.sentry_repository.id;


--
-- Name: sentry_reprocessingreport; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_reprocessingreport (
    id bigint NOT NULL,
    project_id bigint NOT NULL,
    event_id character varying(32),
    datetime timestamp with time zone NOT NULL
);


ALTER TABLE public.sentry_reprocessingreport OWNER TO postgres;

--
-- Name: sentry_reprocessingreport_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_reprocessingreport_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_reprocessingreport_id_seq OWNER TO postgres;

--
-- Name: sentry_reprocessingreport_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_reprocessingreport_id_seq OWNED BY public.sentry_reprocessingreport.id;


--
-- Name: sentry_rule; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_rule (
    id bigint NOT NULL,
    project_id bigint NOT NULL,
    label character varying(64) NOT NULL,
    data text NOT NULL,
    date_added timestamp with time zone NOT NULL,
    status integer NOT NULL,
    CONSTRAINT ck_status_pstv_64efa876e92cb76d CHECK ((status >= 0)),
    CONSTRAINT sentry_rule_status_check CHECK ((status >= 0))
);


ALTER TABLE public.sentry_rule OWNER TO postgres;

--
-- Name: sentry_rule_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_rule_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_rule_id_seq OWNER TO postgres;

--
-- Name: sentry_rule_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_rule_id_seq OWNED BY public.sentry_rule.id;


--
-- Name: sentry_savedsearch; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_savedsearch (
    id bigint NOT NULL,
    project_id bigint NOT NULL,
    name character varying(128) NOT NULL,
    query text NOT NULL,
    date_added timestamp with time zone NOT NULL,
    is_default boolean NOT NULL,
    owner_id integer
);


ALTER TABLE public.sentry_savedsearch OWNER TO postgres;

--
-- Name: sentry_savedsearch_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_savedsearch_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_savedsearch_id_seq OWNER TO postgres;

--
-- Name: sentry_savedsearch_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_savedsearch_id_seq OWNED BY public.sentry_savedsearch.id;


--
-- Name: sentry_savedsearch_userdefault; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_savedsearch_userdefault (
    id bigint NOT NULL,
    savedsearch_id bigint NOT NULL,
    project_id bigint NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.sentry_savedsearch_userdefault OWNER TO postgres;

--
-- Name: sentry_savedsearch_userdefault_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_savedsearch_userdefault_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_savedsearch_userdefault_id_seq OWNER TO postgres;

--
-- Name: sentry_savedsearch_userdefault_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_savedsearch_userdefault_id_seq OWNED BY public.sentry_savedsearch_userdefault.id;


--
-- Name: sentry_scheduleddeletion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_scheduleddeletion (
    id bigint NOT NULL,
    guid character varying(32) NOT NULL,
    app_label character varying(64) NOT NULL,
    model_name character varying(64) NOT NULL,
    object_id bigint NOT NULL,
    date_added timestamp with time zone NOT NULL,
    date_scheduled timestamp with time zone NOT NULL,
    actor_id bigint,
    data text NOT NULL,
    in_progress boolean NOT NULL,
    aborted boolean NOT NULL
);


ALTER TABLE public.sentry_scheduleddeletion OWNER TO postgres;

--
-- Name: sentry_scheduleddeletion_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_scheduleddeletion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_scheduleddeletion_id_seq OWNER TO postgres;

--
-- Name: sentry_scheduleddeletion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_scheduleddeletion_id_seq OWNED BY public.sentry_scheduleddeletion.id;


--
-- Name: sentry_scheduledjob; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_scheduledjob (
    id bigint NOT NULL,
    name character varying(128) NOT NULL,
    payload text NOT NULL,
    date_added timestamp with time zone NOT NULL,
    date_scheduled timestamp with time zone NOT NULL
);


ALTER TABLE public.sentry_scheduledjob OWNER TO postgres;

--
-- Name: sentry_scheduledjob_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_scheduledjob_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_scheduledjob_id_seq OWNER TO postgres;

--
-- Name: sentry_scheduledjob_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_scheduledjob_id_seq OWNED BY public.sentry_scheduledjob.id;


--
-- Name: sentry_team; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_team (
    id bigint NOT NULL,
    slug character varying(50) NOT NULL,
    name character varying(64) NOT NULL,
    date_added timestamp with time zone,
    status integer NOT NULL,
    organization_id bigint NOT NULL,
    CONSTRAINT ck_status_pstv_1772e42d30eba7ba CHECK ((status >= 0)),
    CONSTRAINT sentry_team_status_check CHECK ((status >= 0))
);


ALTER TABLE public.sentry_team OWNER TO postgres;

--
-- Name: sentry_team_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_team_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_team_id_seq OWNER TO postgres;

--
-- Name: sentry_team_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_team_id_seq OWNED BY public.sentry_team.id;


--
-- Name: sentry_useravatar; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_useravatar (
    id bigint NOT NULL,
    user_id integer NOT NULL,
    file_id bigint,
    ident character varying(32) NOT NULL,
    avatar_type smallint NOT NULL,
    CONSTRAINT sentry_useravatar_avatar_type_check CHECK ((avatar_type >= 0))
);


ALTER TABLE public.sentry_useravatar OWNER TO postgres;

--
-- Name: sentry_useravatar_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_useravatar_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_useravatar_id_seq OWNER TO postgres;

--
-- Name: sentry_useravatar_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_useravatar_id_seq OWNED BY public.sentry_useravatar.id;


--
-- Name: sentry_useremail; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_useremail (
    id bigint NOT NULL,
    user_id integer NOT NULL,
    email character varying(75) NOT NULL,
    validation_hash character varying(32) NOT NULL,
    date_hash_added timestamp with time zone NOT NULL,
    is_verified boolean NOT NULL
);


ALTER TABLE public.sentry_useremail OWNER TO postgres;

--
-- Name: sentry_useremail_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_useremail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_useremail_id_seq OWNER TO postgres;

--
-- Name: sentry_useremail_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_useremail_id_seq OWNED BY public.sentry_useremail.id;


--
-- Name: sentry_useridentity; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_useridentity (
    id bigint NOT NULL,
    user_id integer NOT NULL,
    identity_id bigint NOT NULL,
    date_added timestamp with time zone NOT NULL
);


ALTER TABLE public.sentry_useridentity OWNER TO postgres;

--
-- Name: sentry_useridentity_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_useridentity_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_useridentity_id_seq OWNER TO postgres;

--
-- Name: sentry_useridentity_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_useridentity_id_seq OWNED BY public.sentry_useridentity.id;


--
-- Name: sentry_userip; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_userip (
    id bigint NOT NULL,
    user_id integer NOT NULL,
    ip_address inet NOT NULL,
    first_seen timestamp with time zone NOT NULL,
    last_seen timestamp with time zone NOT NULL
);


ALTER TABLE public.sentry_userip OWNER TO postgres;

--
-- Name: sentry_userip_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_userip_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_userip_id_seq OWNER TO postgres;

--
-- Name: sentry_userip_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_userip_id_seq OWNED BY public.sentry_userip.id;


--
-- Name: sentry_useroption; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_useroption (
    id bigint NOT NULL,
    user_id integer NOT NULL,
    project_id bigint,
    key character varying(64) NOT NULL,
    value text NOT NULL,
    organization_id bigint
);


ALTER TABLE public.sentry_useroption OWNER TO postgres;

--
-- Name: sentry_useroption_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_useroption_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_useroption_id_seq OWNER TO postgres;

--
-- Name: sentry_useroption_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_useroption_id_seq OWNED BY public.sentry_useroption.id;


--
-- Name: sentry_userreport; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_userreport (
    id bigint NOT NULL,
    project_id bigint NOT NULL,
    group_id bigint,
    event_id character varying(32) NOT NULL,
    name character varying(128) NOT NULL,
    email character varying(75) NOT NULL,
    comments text NOT NULL,
    date_added timestamp with time zone NOT NULL,
    event_user_id bigint
);


ALTER TABLE public.sentry_userreport OWNER TO postgres;

--
-- Name: sentry_userreport_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_userreport_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_userreport_id_seq OWNER TO postgres;

--
-- Name: sentry_userreport_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_userreport_id_seq OWNED BY public.sentry_userreport.id;


--
-- Name: sentry_versiondsymfile; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sentry_versiondsymfile (
    id bigint NOT NULL,
    dsym_file_id bigint,
    dsym_app_id bigint NOT NULL,
    version character varying(32) NOT NULL,
    build character varying(32),
    date_added timestamp with time zone NOT NULL
);


ALTER TABLE public.sentry_versiondsymfile OWNER TO postgres;

--
-- Name: sentry_versiondsymfile_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sentry_versiondsymfile_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sentry_versiondsymfile_id_seq OWNER TO postgres;

--
-- Name: sentry_versiondsymfile_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sentry_versiondsymfile_id_seq OWNED BY public.sentry_versiondsymfile.id;


--
-- Name: social_auth_usersocialauth; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.social_auth_usersocialauth (
    id integer NOT NULL,
    user_id integer NOT NULL,
    provider character varying(32) NOT NULL,
    uid character varying(255) NOT NULL,
    extra_data text NOT NULL
);


ALTER TABLE public.social_auth_usersocialauth OWNER TO postgres;

--
-- Name: social_auth_usersocialauth_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.social_auth_usersocialauth_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.social_auth_usersocialauth_id_seq OWNER TO postgres;

--
-- Name: social_auth_usersocialauth_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.social_auth_usersocialauth_id_seq OWNED BY public.social_auth_usersocialauth.id;


--
-- Name: south_migrationhistory; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.south_migrationhistory (
    id integer NOT NULL,
    app_name character varying(255) NOT NULL,
    migration character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.south_migrationhistory OWNER TO postgres;

--
-- Name: south_migrationhistory_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.south_migrationhistory_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.south_migrationhistory_id_seq OWNER TO postgres;

--
-- Name: south_migrationhistory_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.south_migrationhistory_id_seq OWNED BY public.south_migrationhistory.id;


--
-- Name: auth_authenticator id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_authenticator ALTER COLUMN id SET DEFAULT nextval('public.auth_authenticator_id_seq'::regclass);


--
-- Name: auth_group id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group ALTER COLUMN id SET DEFAULT nextval('public.auth_group_id_seq'::regclass);


--
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_group_permissions_id_seq'::regclass);


--
-- Name: auth_permission id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission ALTER COLUMN id SET DEFAULT nextval('public.auth_permission_id_seq'::regclass);


--
-- Name: auth_user id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user ALTER COLUMN id SET DEFAULT nextval('public.auth_user_id_seq'::regclass);


--
-- Name: django_admin_log id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log ALTER COLUMN id SET DEFAULT nextval('public.django_admin_log_id_seq'::regclass);


--
-- Name: django_content_type id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_content_type ALTER COLUMN id SET DEFAULT nextval('public.django_content_type_id_seq'::regclass);


--
-- Name: django_site id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_site ALTER COLUMN id SET DEFAULT nextval('public.django_site_id_seq'::regclass);


--
-- Name: jira_ac_tenant id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jira_ac_tenant ALTER COLUMN id SET DEFAULT nextval('public.jira_ac_tenant_id_seq'::regclass);


--
-- Name: sentry_activity id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_activity ALTER COLUMN id SET DEFAULT nextval('public.sentry_activity_id_seq'::regclass);


--
-- Name: sentry_apiapplication id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_apiapplication ALTER COLUMN id SET DEFAULT nextval('public.sentry_apiapplication_id_seq'::regclass);


--
-- Name: sentry_apiauthorization id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_apiauthorization ALTER COLUMN id SET DEFAULT nextval('public.sentry_apiauthorization_id_seq'::regclass);


--
-- Name: sentry_apigrant id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_apigrant ALTER COLUMN id SET DEFAULT nextval('public.sentry_apigrant_id_seq'::regclass);


--
-- Name: sentry_apikey id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_apikey ALTER COLUMN id SET DEFAULT nextval('public.sentry_apikey_id_seq'::regclass);


--
-- Name: sentry_apitoken id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_apitoken ALTER COLUMN id SET DEFAULT nextval('public.sentry_apitoken_id_seq'::regclass);


--
-- Name: sentry_auditlogentry id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_auditlogentry ALTER COLUMN id SET DEFAULT nextval('public.sentry_auditlogentry_id_seq'::regclass);


--
-- Name: sentry_authidentity id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_authidentity ALTER COLUMN id SET DEFAULT nextval('public.sentry_authidentity_id_seq'::regclass);


--
-- Name: sentry_authprovider id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_authprovider ALTER COLUMN id SET DEFAULT nextval('public.sentry_authprovider_id_seq'::regclass);


--
-- Name: sentry_authprovider_default_teams id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_authprovider_default_teams ALTER COLUMN id SET DEFAULT nextval('public.sentry_authprovider_default_teams_id_seq'::regclass);


--
-- Name: sentry_broadcast id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_broadcast ALTER COLUMN id SET DEFAULT nextval('public.sentry_broadcast_id_seq'::regclass);


--
-- Name: sentry_broadcastseen id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_broadcastseen ALTER COLUMN id SET DEFAULT nextval('public.sentry_broadcastseen_id_seq'::regclass);


--
-- Name: sentry_commit id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_commit ALTER COLUMN id SET DEFAULT nextval('public.sentry_commit_id_seq'::regclass);


--
-- Name: sentry_commitauthor id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_commitauthor ALTER COLUMN id SET DEFAULT nextval('public.sentry_commitauthor_id_seq'::regclass);


--
-- Name: sentry_commitfilechange id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_commitfilechange ALTER COLUMN id SET DEFAULT nextval('public.sentry_commitfilechange_id_seq'::regclass);


--
-- Name: sentry_deploy id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_deploy ALTER COLUMN id SET DEFAULT nextval('public.sentry_deploy_id_seq'::regclass);


--
-- Name: sentry_distribution id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_distribution ALTER COLUMN id SET DEFAULT nextval('public.sentry_distribution_id_seq'::regclass);


--
-- Name: sentry_dsymapp id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_dsymapp ALTER COLUMN id SET DEFAULT nextval('public.sentry_dsymapp_id_seq'::regclass);


--
-- Name: sentry_email id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_email ALTER COLUMN id SET DEFAULT nextval('public.sentry_email_id_seq'::regclass);


--
-- Name: sentry_environment id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_environment ALTER COLUMN id SET DEFAULT nextval('public.sentry_environment_id_seq'::regclass);


--
-- Name: sentry_environmentproject id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_environmentproject ALTER COLUMN id SET DEFAULT nextval('public.sentry_environmentproject_id_seq'::regclass);


--
-- Name: sentry_environmentrelease id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_environmentrelease ALTER COLUMN id SET DEFAULT nextval('public.sentry_environmentrelease_id_seq'::regclass);


--
-- Name: sentry_eventmapping id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_eventmapping ALTER COLUMN id SET DEFAULT nextval('public.sentry_eventmapping_id_seq'::regclass);


--
-- Name: sentry_eventprocessingissue id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_eventprocessingissue ALTER COLUMN id SET DEFAULT nextval('public.sentry_eventprocessingissue_id_seq'::regclass);


--
-- Name: sentry_eventtag id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_eventtag ALTER COLUMN id SET DEFAULT nextval('public.sentry_eventtag_id_seq'::regclass);


--
-- Name: sentry_eventuser id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_eventuser ALTER COLUMN id SET DEFAULT nextval('public.sentry_eventuser_id_seq'::regclass);


--
-- Name: sentry_featureadoption id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_featureadoption ALTER COLUMN id SET DEFAULT nextval('public.sentry_featureadoption_id_seq'::regclass);


--
-- Name: sentry_file id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_file ALTER COLUMN id SET DEFAULT nextval('public.sentry_file_id_seq'::regclass);


--
-- Name: sentry_fileblob id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_fileblob ALTER COLUMN id SET DEFAULT nextval('public.sentry_fileblob_id_seq'::regclass);


--
-- Name: sentry_fileblobindex id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_fileblobindex ALTER COLUMN id SET DEFAULT nextval('public.sentry_fileblobindex_id_seq'::regclass);


--
-- Name: sentry_filterkey id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_filterkey ALTER COLUMN id SET DEFAULT nextval('public.sentry_filterkey_id_seq'::regclass);


--
-- Name: sentry_filtervalue id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_filtervalue ALTER COLUMN id SET DEFAULT nextval('public.sentry_filtervalue_id_seq'::regclass);


--
-- Name: sentry_groupasignee id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupasignee ALTER COLUMN id SET DEFAULT nextval('public.sentry_groupasignee_id_seq'::regclass);


--
-- Name: sentry_groupbookmark id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupbookmark ALTER COLUMN id SET DEFAULT nextval('public.sentry_groupbookmark_id_seq'::regclass);


--
-- Name: sentry_groupcommitresolution id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupcommitresolution ALTER COLUMN id SET DEFAULT nextval('public.sentry_groupcommitresolution_id_seq'::regclass);


--
-- Name: sentry_groupedmessage id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupedmessage ALTER COLUMN id SET DEFAULT nextval('public.sentry_groupedmessage_id_seq'::regclass);


--
-- Name: sentry_groupemailthread id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupemailthread ALTER COLUMN id SET DEFAULT nextval('public.sentry_groupemailthread_id_seq'::regclass);


--
-- Name: sentry_grouphash id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_grouphash ALTER COLUMN id SET DEFAULT nextval('public.sentry_grouphash_id_seq'::regclass);


--
-- Name: sentry_grouplink id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_grouplink ALTER COLUMN id SET DEFAULT nextval('public.sentry_grouplink_id_seq'::regclass);


--
-- Name: sentry_groupmeta id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupmeta ALTER COLUMN id SET DEFAULT nextval('public.sentry_groupmeta_id_seq'::regclass);


--
-- Name: sentry_groupredirect id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupredirect ALTER COLUMN id SET DEFAULT nextval('public.sentry_groupredirect_id_seq'::regclass);


--
-- Name: sentry_grouprelease id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_grouprelease ALTER COLUMN id SET DEFAULT nextval('public.sentry_grouprelease_id_seq'::regclass);


--
-- Name: sentry_groupresolution id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupresolution ALTER COLUMN id SET DEFAULT nextval('public.sentry_groupresolution_id_seq'::regclass);


--
-- Name: sentry_grouprulestatus id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_grouprulestatus ALTER COLUMN id SET DEFAULT nextval('public.sentry_grouprulestatus_id_seq'::regclass);


--
-- Name: sentry_groupseen id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupseen ALTER COLUMN id SET DEFAULT nextval('public.sentry_groupseen_id_seq'::regclass);


--
-- Name: sentry_groupshare id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupshare ALTER COLUMN id SET DEFAULT nextval('public.sentry_groupshare_id_seq'::regclass);


--
-- Name: sentry_groupsnooze id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupsnooze ALTER COLUMN id SET DEFAULT nextval('public.sentry_groupsnooze_id_seq'::regclass);


--
-- Name: sentry_groupsubscription id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupsubscription ALTER COLUMN id SET DEFAULT nextval('public.sentry_groupsubscription_id_seq'::regclass);


--
-- Name: sentry_grouptagkey id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_grouptagkey ALTER COLUMN id SET DEFAULT nextval('public.sentry_grouptagkey_id_seq'::regclass);


--
-- Name: sentry_grouptombstone id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_grouptombstone ALTER COLUMN id SET DEFAULT nextval('public.sentry_grouptombstone_id_seq'::regclass);


--
-- Name: sentry_hipchat_ac_tenant_organizations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_hipchat_ac_tenant_organizations ALTER COLUMN id SET DEFAULT nextval('public.sentry_hipchat_ac_tenant_organizations_id_seq'::regclass);


--
-- Name: sentry_hipchat_ac_tenant_projects id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_hipchat_ac_tenant_projects ALTER COLUMN id SET DEFAULT nextval('public.sentry_hipchat_ac_tenant_projects_id_seq'::regclass);


--
-- Name: sentry_identity id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_identity ALTER COLUMN id SET DEFAULT nextval('public.sentry_identity_id_seq'::regclass);


--
-- Name: sentry_identityprovider id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_identityprovider ALTER COLUMN id SET DEFAULT nextval('public.sentry_identityprovider_id_seq'::regclass);


--
-- Name: sentry_integration id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_integration ALTER COLUMN id SET DEFAULT nextval('public.sentry_integration_id_seq'::regclass);


--
-- Name: sentry_lostpasswordhash id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_lostpasswordhash ALTER COLUMN id SET DEFAULT nextval('public.sentry_lostpasswordhash_id_seq'::regclass);


--
-- Name: sentry_message id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_message ALTER COLUMN id SET DEFAULT nextval('public.sentry_message_id_seq'::regclass);


--
-- Name: sentry_messagefiltervalue id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_messagefiltervalue ALTER COLUMN id SET DEFAULT nextval('public.sentry_messagefiltervalue_id_seq'::regclass);


--
-- Name: sentry_messageindex id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_messageindex ALTER COLUMN id SET DEFAULT nextval('public.sentry_messageindex_id_seq'::regclass);


--
-- Name: sentry_minidumpfile id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_minidumpfile ALTER COLUMN id SET DEFAULT nextval('public.sentry_minidumpfile_id_seq'::regclass);


--
-- Name: sentry_option id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_option ALTER COLUMN id SET DEFAULT nextval('public.sentry_option_id_seq'::regclass);


--
-- Name: sentry_organization id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_organization ALTER COLUMN id SET DEFAULT nextval('public.sentry_organization_id_seq'::regclass);


--
-- Name: sentry_organizationaccessrequest id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_organizationaccessrequest ALTER COLUMN id SET DEFAULT nextval('public.sentry_organizationaccessrequest_id_seq'::regclass);


--
-- Name: sentry_organizationavatar id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_organizationavatar ALTER COLUMN id SET DEFAULT nextval('public.sentry_organizationavatar_id_seq'::regclass);


--
-- Name: sentry_organizationintegration id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_organizationintegration ALTER COLUMN id SET DEFAULT nextval('public.sentry_organizationintegration_id_seq'::regclass);


--
-- Name: sentry_organizationmember id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_organizationmember ALTER COLUMN id SET DEFAULT nextval('public.sentry_organizationmember_id_seq'::regclass);


--
-- Name: sentry_organizationmember_teams id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_organizationmember_teams ALTER COLUMN id SET DEFAULT nextval('public.sentry_organizationmember_teams_id_seq'::regclass);


--
-- Name: sentry_organizationonboardingtask id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_organizationonboardingtask ALTER COLUMN id SET DEFAULT nextval('public.sentry_organizationonboardingtask_id_seq'::regclass);


--
-- Name: sentry_organizationoptions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_organizationoptions ALTER COLUMN id SET DEFAULT nextval('public.sentry_organizationoptions_id_seq'::regclass);


--
-- Name: sentry_processingissue id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_processingissue ALTER COLUMN id SET DEFAULT nextval('public.sentry_processingissue_id_seq'::regclass);


--
-- Name: sentry_project id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_project ALTER COLUMN id SET DEFAULT nextval('public.sentry_project_id_seq'::regclass);


--
-- Name: sentry_projectbookmark id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_projectbookmark ALTER COLUMN id SET DEFAULT nextval('public.sentry_projectbookmark_id_seq'::regclass);


--
-- Name: sentry_projectcounter id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_projectcounter ALTER COLUMN id SET DEFAULT nextval('public.sentry_projectcounter_id_seq'::regclass);


--
-- Name: sentry_projectdsymfile id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_projectdsymfile ALTER COLUMN id SET DEFAULT nextval('public.sentry_projectdsymfile_id_seq'::regclass);


--
-- Name: sentry_projectintegration id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_projectintegration ALTER COLUMN id SET DEFAULT nextval('public.sentry_projectintegration_id_seq'::regclass);


--
-- Name: sentry_projectkey id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_projectkey ALTER COLUMN id SET DEFAULT nextval('public.sentry_projectkey_id_seq'::regclass);


--
-- Name: sentry_projectoptions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_projectoptions ALTER COLUMN id SET DEFAULT nextval('public.sentry_projectoptions_id_seq'::regclass);


--
-- Name: sentry_projectplatform id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_projectplatform ALTER COLUMN id SET DEFAULT nextval('public.sentry_projectplatform_id_seq'::regclass);


--
-- Name: sentry_projectsymcachefile id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_projectsymcachefile ALTER COLUMN id SET DEFAULT nextval('public.sentry_projectsymcachefile_id_seq'::regclass);


--
-- Name: sentry_projectteam id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_projectteam ALTER COLUMN id SET DEFAULT nextval('public.sentry_projectteam_id_seq'::regclass);


--
-- Name: sentry_rawevent id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_rawevent ALTER COLUMN id SET DEFAULT nextval('public.sentry_rawevent_id_seq'::regclass);


--
-- Name: sentry_release id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_release ALTER COLUMN id SET DEFAULT nextval('public.sentry_release_id_seq'::regclass);


--
-- Name: sentry_release_project id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_release_project ALTER COLUMN id SET DEFAULT nextval('public.sentry_release_project_id_seq'::regclass);


--
-- Name: sentry_releasecommit id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_releasecommit ALTER COLUMN id SET DEFAULT nextval('public.sentry_releasecommit_id_seq'::regclass);


--
-- Name: sentry_releasefile id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_releasefile ALTER COLUMN id SET DEFAULT nextval('public.sentry_releasefile_id_seq'::regclass);


--
-- Name: sentry_releaseheadcommit id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_releaseheadcommit ALTER COLUMN id SET DEFAULT nextval('public.sentry_releaseheadcommit_id_seq'::regclass);


--
-- Name: sentry_repository id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_repository ALTER COLUMN id SET DEFAULT nextval('public.sentry_repository_id_seq'::regclass);


--
-- Name: sentry_reprocessingreport id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_reprocessingreport ALTER COLUMN id SET DEFAULT nextval('public.sentry_reprocessingreport_id_seq'::regclass);


--
-- Name: sentry_rule id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_rule ALTER COLUMN id SET DEFAULT nextval('public.sentry_rule_id_seq'::regclass);


--
-- Name: sentry_savedsearch id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_savedsearch ALTER COLUMN id SET DEFAULT nextval('public.sentry_savedsearch_id_seq'::regclass);


--
-- Name: sentry_savedsearch_userdefault id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_savedsearch_userdefault ALTER COLUMN id SET DEFAULT nextval('public.sentry_savedsearch_userdefault_id_seq'::regclass);


--
-- Name: sentry_scheduleddeletion id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_scheduleddeletion ALTER COLUMN id SET DEFAULT nextval('public.sentry_scheduleddeletion_id_seq'::regclass);


--
-- Name: sentry_scheduledjob id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_scheduledjob ALTER COLUMN id SET DEFAULT nextval('public.sentry_scheduledjob_id_seq'::regclass);


--
-- Name: sentry_team id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_team ALTER COLUMN id SET DEFAULT nextval('public.sentry_team_id_seq'::regclass);


--
-- Name: sentry_useravatar id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_useravatar ALTER COLUMN id SET DEFAULT nextval('public.sentry_useravatar_id_seq'::regclass);


--
-- Name: sentry_useremail id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_useremail ALTER COLUMN id SET DEFAULT nextval('public.sentry_useremail_id_seq'::regclass);


--
-- Name: sentry_useridentity id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_useridentity ALTER COLUMN id SET DEFAULT nextval('public.sentry_useridentity_id_seq'::regclass);


--
-- Name: sentry_userip id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_userip ALTER COLUMN id SET DEFAULT nextval('public.sentry_userip_id_seq'::regclass);


--
-- Name: sentry_useroption id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_useroption ALTER COLUMN id SET DEFAULT nextval('public.sentry_useroption_id_seq'::regclass);


--
-- Name: sentry_userreport id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_userreport ALTER COLUMN id SET DEFAULT nextval('public.sentry_userreport_id_seq'::regclass);


--
-- Name: sentry_versiondsymfile id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_versiondsymfile ALTER COLUMN id SET DEFAULT nextval('public.sentry_versiondsymfile_id_seq'::regclass);


--
-- Name: social_auth_usersocialauth id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.social_auth_usersocialauth ALTER COLUMN id SET DEFAULT nextval('public.social_auth_usersocialauth_id_seq'::regclass);


--
-- Name: south_migrationhistory id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.south_migrationhistory ALTER COLUMN id SET DEFAULT nextval('public.south_migrationhistory_id_seq'::regclass);


--
-- Data for Name: auth_authenticator; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_authenticator (id, user_id, created_at, last_used_at, type, config) FROM stdin;
\.


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_group (id, name) FROM stdin;
\.


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can add permission	2	add_permission
5	Can change permission	2	change_permission
6	Can delete permission	2	delete_permission
7	Can add group	3	add_group
8	Can change group	3	change_group
9	Can delete group	3	delete_group
10	Can add content type	4	add_contenttype
11	Can change content type	4	change_contenttype
12	Can delete content type	4	delete_contenttype
13	Can add session	5	add_session
14	Can change session	5	change_session
15	Can delete session	5	delete_session
16	Can add site	6	add_site
17	Can change site	6	change_site
18	Can delete site	6	delete_site
19	Can add migration history	7	add_migrationhistory
20	Can change migration history	7	change_migrationhistory
21	Can delete migration history	7	delete_migrationhistory
22	Can add activity	8	add_activity
23	Can change activity	8	change_activity
24	Can delete activity	8	delete_activity
25	Can add api application	9	add_apiapplication
26	Can change api application	9	change_apiapplication
27	Can delete api application	9	delete_apiapplication
28	Can add api authorization	10	add_apiauthorization
29	Can change api authorization	10	change_apiauthorization
30	Can delete api authorization	10	delete_apiauthorization
31	Can add api grant	11	add_apigrant
32	Can change api grant	11	change_apigrant
33	Can delete api grant	11	delete_apigrant
34	Can add api key	12	add_apikey
35	Can change api key	12	change_apikey
36	Can delete api key	12	delete_apikey
37	Can add api token	13	add_apitoken
38	Can change api token	13	change_apitoken
39	Can delete api token	13	delete_apitoken
40	Can add audit log entry	14	add_auditlogentry
41	Can change audit log entry	14	change_auditlogentry
42	Can delete audit log entry	14	delete_auditlogentry
43	Can add authenticator	15	add_authenticator
44	Can change authenticator	15	change_authenticator
45	Can delete authenticator	15	delete_authenticator
46	Can add auth identity	16	add_authidentity
47	Can change auth identity	16	change_authidentity
48	Can delete auth identity	16	delete_authidentity
49	Can add auth provider	17	add_authprovider
50	Can change auth provider	17	change_authprovider
51	Can delete auth provider	17	delete_authprovider
52	Can add broadcast	18	add_broadcast
53	Can change broadcast	18	change_broadcast
54	Can delete broadcast	18	delete_broadcast
55	Can add broadcast seen	19	add_broadcastseen
56	Can change broadcast seen	19	change_broadcastseen
57	Can delete broadcast seen	19	delete_broadcastseen
58	Can add commit	20	add_commit
59	Can change commit	20	change_commit
60	Can delete commit	20	delete_commit
61	Can add commit author	21	add_commitauthor
62	Can change commit author	21	change_commitauthor
63	Can delete commit author	21	delete_commitauthor
64	Can add commit file change	22	add_commitfilechange
65	Can change commit file change	22	change_commitfilechange
66	Can delete commit file change	22	delete_commitfilechange
67	Can add counter	23	add_counter
68	Can change counter	23	change_counter
69	Can delete counter	23	delete_counter
70	Can add deploy	24	add_deploy
71	Can change deploy	24	change_deploy
72	Can delete deploy	24	delete_deploy
73	Can add distribution	25	add_distribution
74	Can change distribution	25	change_distribution
75	Can delete distribution	25	delete_distribution
76	Can add file blob	26	add_fileblob
77	Can change file blob	26	change_fileblob
78	Can delete file blob	26	delete_fileblob
79	Can add file	27	add_file
80	Can change file	27	change_file
81	Can delete file	27	delete_file
82	Can add file blob index	28	add_fileblobindex
83	Can change file blob index	28	change_fileblobindex
84	Can delete file blob index	28	delete_fileblobindex
85	Can add version d sym file	29	add_versiondsymfile
86	Can change version d sym file	29	change_versiondsymfile
87	Can delete version d sym file	29	delete_versiondsymfile
88	Can add d sym app	30	add_dsymapp
89	Can change d sym app	30	change_dsymapp
90	Can delete d sym app	30	delete_dsymapp
91	Can add project d sym file	31	add_projectdsymfile
92	Can change project d sym file	31	change_projectdsymfile
93	Can delete project d sym file	31	delete_projectdsymfile
94	Can add project sym cache file	32	add_projectsymcachefile
95	Can change project sym cache file	32	change_projectsymcachefile
96	Can delete project sym cache file	32	delete_projectsymcachefile
97	Can add email	33	add_email
98	Can change email	33	change_email
99	Can delete email	33	delete_email
100	Can add environment project	34	add_environmentproject
101	Can change environment project	34	change_environmentproject
102	Can delete environment project	34	delete_environmentproject
103	Can add environment	35	add_environment
104	Can change environment	35	change_environment
105	Can delete environment	35	delete_environment
106	Can add message	36	add_event
107	Can change message	36	change_event
108	Can delete message	36	delete_event
109	Can add event mapping	37	add_eventmapping
110	Can change event mapping	37	change_eventmapping
111	Can delete event mapping	37	delete_eventmapping
112	Can add event user	38	add_eventuser
113	Can change event user	38	change_eventuser
114	Can delete event user	38	delete_eventuser
115	Can add feature adoption	39	add_featureadoption
116	Can change feature adoption	39	change_featureadoption
117	Can delete feature adoption	39	delete_featureadoption
118	Can add grouped message	40	add_group
119	Can change grouped message	40	change_group
120	Can delete grouped message	40	delete_group
121	Can view	40	can_view
122	Can add group assignee	41	add_groupassignee
123	Can change group assignee	41	change_groupassignee
124	Can delete group assignee	41	delete_groupassignee
125	Can add group bookmark	42	add_groupbookmark
126	Can change group bookmark	42	change_groupbookmark
127	Can delete group bookmark	42	delete_groupbookmark
128	Can add group commit resolution	43	add_groupcommitresolution
129	Can change group commit resolution	43	change_groupcommitresolution
130	Can delete group commit resolution	43	delete_groupcommitresolution
131	Can add group email thread	44	add_groupemailthread
132	Can change group email thread	44	change_groupemailthread
133	Can delete group email thread	44	delete_groupemailthread
134	Can add group hash	45	add_grouphash
135	Can change group hash	45	change_grouphash
136	Can delete group hash	45	delete_grouphash
137	Can add group link	46	add_grouplink
138	Can change group link	46	change_grouplink
139	Can delete group link	46	delete_grouplink
140	Can add group meta	47	add_groupmeta
141	Can change group meta	47	change_groupmeta
142	Can delete group meta	47	delete_groupmeta
143	Can add group redirect	48	add_groupredirect
144	Can change group redirect	48	change_groupredirect
145	Can delete group redirect	48	delete_groupredirect
146	Can add group release	49	add_grouprelease
147	Can change group release	49	change_grouprelease
148	Can delete group release	49	delete_grouprelease
149	Can add group resolution	50	add_groupresolution
150	Can change group resolution	50	change_groupresolution
151	Can delete group resolution	50	delete_groupresolution
152	Can add group rule status	51	add_grouprulestatus
153	Can change group rule status	51	change_grouprulestatus
154	Can delete group rule status	51	delete_grouprulestatus
155	Can add group seen	52	add_groupseen
156	Can change group seen	52	change_groupseen
157	Can delete group seen	52	delete_groupseen
158	Can add group share	53	add_groupshare
159	Can change group share	53	change_groupshare
160	Can delete group share	53	delete_groupshare
161	Can add group snooze	54	add_groupsnooze
162	Can change group snooze	54	change_groupsnooze
163	Can delete group snooze	54	delete_groupsnooze
164	Can add group subscription	55	add_groupsubscription
165	Can change group subscription	55	change_groupsubscription
166	Can delete group subscription	55	delete_groupsubscription
167	Can add group tombstone	56	add_grouptombstone
168	Can change group tombstone	56	change_grouptombstone
169	Can delete group tombstone	56	delete_grouptombstone
170	Can add identity provider	57	add_identityprovider
171	Can change identity provider	57	change_identityprovider
172	Can delete identity provider	57	delete_identityprovider
173	Can add identity	58	add_identity
174	Can change identity	58	change_identity
175	Can delete identity	58	delete_identity
176	Can add user identity	59	add_useridentity
177	Can change user identity	59	change_useridentity
178	Can delete user identity	59	delete_useridentity
179	Can add organization integration	60	add_organizationintegration
180	Can change organization integration	60	change_organizationintegration
181	Can delete organization integration	60	delete_organizationintegration
182	Can add project integration	61	add_projectintegration
183	Can change project integration	61	change_projectintegration
184	Can delete project integration	61	delete_projectintegration
185	Can add integration	62	add_integration
186	Can change integration	62	change_integration
187	Can delete integration	62	delete_integration
188	Can add lost password hash	63	add_lostpasswordhash
189	Can change lost password hash	63	change_lostpasswordhash
190	Can delete lost password hash	63	delete_lostpasswordhash
191	Can add minidump file	64	add_minidumpfile
192	Can change minidump file	64	change_minidumpfile
193	Can delete minidump file	64	delete_minidumpfile
194	Can add option	65	add_option
195	Can change option	65	change_option
196	Can delete option	65	delete_option
197	Can add organization	66	add_organization
198	Can change organization	66	change_organization
199	Can delete organization	66	delete_organization
200	Can add organization access request	67	add_organizationaccessrequest
201	Can change organization access request	67	change_organizationaccessrequest
202	Can delete organization access request	67	delete_organizationaccessrequest
203	Can add organization avatar	68	add_organizationavatar
204	Can change organization avatar	68	change_organizationavatar
205	Can delete organization avatar	68	delete_organizationavatar
206	Can add organization member team	69	add_organizationmemberteam
207	Can change organization member team	69	change_organizationmemberteam
208	Can delete organization member team	69	delete_organizationmemberteam
209	Can add organization member	70	add_organizationmember
210	Can change organization member	70	change_organizationmember
211	Can delete organization member	70	delete_organizationmember
212	Can add organization onboarding task	71	add_organizationonboardingtask
213	Can change organization onboarding task	71	change_organizationonboardingtask
214	Can delete organization onboarding task	71	delete_organizationonboardingtask
215	Can add organization option	72	add_organizationoption
216	Can change organization option	72	change_organizationoption
217	Can delete organization option	72	delete_organizationoption
218	Can add processing issue	73	add_processingissue
219	Can change processing issue	73	change_processingissue
220	Can delete processing issue	73	delete_processingissue
221	Can add event processing issue	74	add_eventprocessingissue
222	Can change event processing issue	74	change_eventprocessingissue
223	Can delete event processing issue	74	delete_eventprocessingissue
224	Can add project team	75	add_projectteam
225	Can change project team	75	change_projectteam
226	Can delete project team	75	delete_projectteam
227	Can add project	76	add_project
228	Can change project	76	change_project
229	Can delete project	76	delete_project
230	Can add project bookmark	77	add_projectbookmark
231	Can change project bookmark	77	change_projectbookmark
232	Can delete project bookmark	77	delete_projectbookmark
233	Can add project key	78	add_projectkey
234	Can change project key	78	change_projectkey
235	Can delete project key	78	delete_projectkey
236	Can add project option	79	add_projectoption
237	Can change project option	79	change_projectoption
238	Can delete project option	79	delete_projectoption
239	Can add project platform	80	add_projectplatform
240	Can change project platform	80	change_projectplatform
241	Can delete project platform	80	delete_projectplatform
242	Can add raw event	81	add_rawevent
243	Can change raw event	81	change_rawevent
244	Can delete raw event	81	delete_rawevent
245	Can add release project	82	add_releaseproject
246	Can change release project	82	change_releaseproject
247	Can delete release project	82	delete_releaseproject
248	Can add release	83	add_release
249	Can change release	83	change_release
250	Can delete release	83	delete_release
251	Can add release commit	84	add_releasecommit
252	Can change release commit	84	change_releasecommit
253	Can delete release commit	84	delete_releasecommit
254	Can add release environment	85	add_releaseenvironment
255	Can change release environment	85	change_releaseenvironment
256	Can delete release environment	85	delete_releaseenvironment
257	Can add release file	86	add_releasefile
258	Can change release file	86	change_releasefile
259	Can delete release file	86	delete_releasefile
260	Can add release head commit	87	add_releaseheadcommit
261	Can change release head commit	87	change_releaseheadcommit
262	Can delete release head commit	87	delete_releaseheadcommit
263	Can add repository	88	add_repository
264	Can change repository	88	change_repository
265	Can delete repository	88	delete_repository
266	Can add reprocessing report	89	add_reprocessingreport
267	Can change reprocessing report	89	change_reprocessingreport
268	Can delete reprocessing report	89	delete_reprocessingreport
269	Can add rule	90	add_rule
270	Can change rule	90	change_rule
271	Can delete rule	90	delete_rule
272	Can add saved search	91	add_savedsearch
273	Can change saved search	91	change_savedsearch
274	Can delete saved search	91	delete_savedsearch
275	Can add saved search user default	92	add_savedsearchuserdefault
276	Can change saved search user default	92	change_savedsearchuserdefault
277	Can delete saved search user default	92	delete_savedsearchuserdefault
278	Can add scheduled deletion	93	add_scheduleddeletion
279	Can change scheduled deletion	93	change_scheduleddeletion
280	Can delete scheduled deletion	93	delete_scheduleddeletion
281	Can add scheduled job	94	add_scheduledjob
282	Can change scheduled job	94	change_scheduledjob
283	Can delete scheduled job	94	delete_scheduledjob
284	Can add team	95	add_team
285	Can change team	95	change_team
286	Can delete team	95	delete_team
287	Can add user	96	add_user
288	Can change user	96	change_user
289	Can delete user	96	delete_user
290	Can add user avatar	97	add_useravatar
291	Can change user avatar	97	change_useravatar
292	Can delete user avatar	97	delete_useravatar
293	Can add user email	98	add_useremail
294	Can change user email	98	change_useremail
295	Can delete user email	98	delete_useremail
296	Can add user ip	99	add_userip
297	Can change user ip	99	change_userip
298	Can delete user ip	99	delete_userip
299	Can add user option	100	add_useroption
300	Can change user option	100	change_useroption
301	Can delete user option	100	delete_useroption
302	Can add user report	101	add_userreport
303	Can change user report	101	change_userreport
304	Can delete user report	101	delete_userreport
305	Can add event tag	102	add_eventtag
306	Can change event tag	102	change_eventtag
307	Can delete event tag	102	delete_eventtag
308	Can add group tag key	103	add_grouptagkey
309	Can change group tag key	103	change_grouptagkey
310	Can delete group tag key	103	delete_grouptagkey
311	Can add group tag value	104	add_grouptagvalue
312	Can change group tag value	104	change_grouptagvalue
313	Can delete group tag value	104	delete_grouptagvalue
314	Can add tag key	105	add_tagkey
315	Can change tag key	105	change_tagkey
316	Can delete tag key	105	delete_tagkey
317	Can add tag value	106	add_tagvalue
318	Can change tag value	106	change_tagvalue
319	Can delete tag value	106	delete_tagvalue
320	Can add node	107	add_node
321	Can change node	107	change_node
322	Can delete node	107	delete_node
323	Can add user social auth	108	add_usersocialauth
324	Can change user social auth	108	change_usersocialauth
325	Can delete user social auth	108	delete_usersocialauth
326	Can add jira tenant	109	add_jiratenant
327	Can change jira tenant	109	change_jiratenant
328	Can delete jira tenant	109	delete_jiratenant
329	Can add tenant	110	add_tenant
330	Can change tenant	110	change_tenant
331	Can delete tenant	110	delete_tenant
\.


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_user (password, last_login, id, username, first_name, email, is_staff, is_active, is_superuser, date_joined, is_managed, is_password_expired, last_password_change, session_nonce, last_active) FROM stdin;
pbkdf2_sha256$12000$S25jyjKdRinH$2rsnYqeTTeIXOjMv9CrqPlYjnopFVI366PgUf4DrE0Q=	2019-03-10 16:06:28.298489+00	1	andrew540i@yandex.ru		andrew540i@yandex.ru	t	t	t	2019-03-10 16:00:19.010905+00	f	f	2019-03-10 16:00:19.085953+00	\N	2019-03-10 16:49:04.815706+00
\.


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_admin_log (id, action_time, user_id, content_type_id, object_id, object_repr, action_flag, change_message) FROM stdin;
\.


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_content_type (id, name, app_label, model) FROM stdin;
1	log entry	admin	logentry
2	permission	auth	permission
3	group	auth	group
4	content type	contenttypes	contenttype
5	session	sessions	session
6	site	sites	site
7	migration history	south	migrationhistory
8	activity	sentry	activity
9	api application	sentry	apiapplication
10	api authorization	sentry	apiauthorization
11	api grant	sentry	apigrant
12	api key	sentry	apikey
13	api token	sentry	apitoken
14	audit log entry	sentry	auditlogentry
15	authenticator	sentry	authenticator
16	auth identity	sentry	authidentity
17	auth provider	sentry	authprovider
18	broadcast	sentry	broadcast
19	broadcast seen	sentry	broadcastseen
20	commit	sentry	commit
21	commit author	sentry	commitauthor
22	commit file change	sentry	commitfilechange
23	counter	sentry	counter
24	deploy	sentry	deploy
25	distribution	sentry	distribution
26	file blob	sentry	fileblob
27	file	sentry	file
28	file blob index	sentry	fileblobindex
29	version d sym file	sentry	versiondsymfile
30	d sym app	sentry	dsymapp
31	project d sym file	sentry	projectdsymfile
32	project sym cache file	sentry	projectsymcachefile
33	email	sentry	email
34	environment project	sentry	environmentproject
35	environment	sentry	environment
36	message	sentry	event
37	event mapping	sentry	eventmapping
38	event user	sentry	eventuser
39	feature adoption	sentry	featureadoption
40	grouped message	sentry	group
41	group assignee	sentry	groupassignee
42	group bookmark	sentry	groupbookmark
43	group commit resolution	sentry	groupcommitresolution
44	group email thread	sentry	groupemailthread
45	group hash	sentry	grouphash
46	group link	sentry	grouplink
47	group meta	sentry	groupmeta
48	group redirect	sentry	groupredirect
49	group release	sentry	grouprelease
50	group resolution	sentry	groupresolution
51	group rule status	sentry	grouprulestatus
52	group seen	sentry	groupseen
53	group share	sentry	groupshare
54	group snooze	sentry	groupsnooze
55	group subscription	sentry	groupsubscription
56	group tombstone	sentry	grouptombstone
57	identity provider	sentry	identityprovider
58	identity	sentry	identity
59	user identity	sentry	useridentity
60	organization integration	sentry	organizationintegration
61	project integration	sentry	projectintegration
62	integration	sentry	integration
63	lost password hash	sentry	lostpasswordhash
64	minidump file	sentry	minidumpfile
65	option	sentry	option
66	organization	sentry	organization
67	organization access request	sentry	organizationaccessrequest
68	organization avatar	sentry	organizationavatar
69	organization member team	sentry	organizationmemberteam
70	organization member	sentry	organizationmember
71	organization onboarding task	sentry	organizationonboardingtask
72	organization option	sentry	organizationoption
73	processing issue	sentry	processingissue
74	event processing issue	sentry	eventprocessingissue
75	project team	sentry	projectteam
76	project	sentry	project
77	project bookmark	sentry	projectbookmark
78	project key	sentry	projectkey
79	project option	sentry	projectoption
80	project platform	sentry	projectplatform
81	raw event	sentry	rawevent
82	release project	sentry	releaseproject
83	release	sentry	release
84	release commit	sentry	releasecommit
85	release environment	sentry	releaseenvironment
86	release file	sentry	releasefile
87	release head commit	sentry	releaseheadcommit
88	repository	sentry	repository
89	reprocessing report	sentry	reprocessingreport
90	rule	sentry	rule
91	saved search	sentry	savedsearch
92	saved search user default	sentry	savedsearchuserdefault
93	scheduled deletion	sentry	scheduleddeletion
94	scheduled job	sentry	scheduledjob
95	team	sentry	team
96	user	sentry	user
97	user avatar	sentry	useravatar
98	user email	sentry	useremail
99	user ip	sentry	userip
100	user option	sentry	useroption
101	user report	sentry	userreport
102	event tag	sentry	eventtag
103	group tag key	sentry	grouptagkey
104	group tag value	sentry	grouptagvalue
105	tag key	sentry	tagkey
106	tag value	sentry	tagvalue
107	node	nodestore	node
108	user social auth	social_auth	usersocialauth
109	jira tenant	jira_ac	jiratenant
110	tenant	hipchat_ac	tenant
\.


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_session (session_key, session_data, expire_date) FROM stdin;
\.


--
-- Data for Name: django_site; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_site (id, domain, name) FROM stdin;
1	example.com	example.com
\.


--
-- Data for Name: jira_ac_tenant; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.jira_ac_tenant (id, organization_id, client_key, secret, base_url, public_key) FROM stdin;
\.


--
-- Data for Name: nodestore_node; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.nodestore_node (id, data, "timestamp") FROM stdin;
CLXB+AmNRRqYl8ax1i/xDA==	eJzFW+lT28gS/+6/QsX7gNlnZM3o5oXUchjCciTh2k3eoZLlsa3YlhRJBsPW/u+vu0eS5QNCQqqWVECa6bn6+HX3zKjZS1jjajMTUZ4+qGGUi7TvByJTbzKRbjYS3mj2Eh0opvAe+RMBZQa8+lEvFffwYjayq80w8fweFGQZlFhQzWyuMlfVVAYFNpKIiR+O4cWpGpuGFv76AM9ipqZTqHKpqx48MQ372Gxk2bqpdWaBSPIwjpCS4QQZB3oxC7x4Eua5oC70xgW0vvPHU4GzYkajOU6YSeQ4xSz3g1GeQo9Ya1M5Tq6fwiqphYstuIY1HJnUn0ZBMSzHAcMokjzSceJ+N/MSPx9iAXKoPc3S9jgO/HF7HHbbyUM+jCNdtdq9MMu3ExjcH4is3fviR4O4HcSpaA+BGWORZm1RrlBNHrA/YnKSCi+IgQ2zHMssmp0NIynwE/iRkorxgxJHykDkeRgNFB9KsiSOMqGEUZYLv6fEfQUI591jR07Dl31sbGzgu1u+/3qf+knWhO68sqMtINC1kqAn+goxoZmKr8DmfGsH61lZjz8gOyrkDZ/EkSJrdZ3UCtlU7x1rTCh7042nUU+ZCGBZT9n3M/FOckb16uS4mjcBMC0S40wd5nmi7mWDsKBV4u4XEeSKnyvazO4bvX6gWa7oa2/f4jgWsrSYNr4jI99g80tZJtsx3XVt7jLbcB3dtqilQ+o8C/DZbVxtfBZpfBjehRkwtJOmcdrc7BWvSvdBeYTqzdbWBtiNRgqdxFleE6TBUJAGb8x5JuWjVHqu+BmWIR8Nvc5c/Km4sVs9ev049Sohl8JpYR8oQMOo95GKfJpGSk0GhlnWF3WlohtWw78lOfbDsSjRADn3Ei02iHGTuDcdUzu3aqdiO7Vsp4qafZsagULk+UmCr6xxolFRwUBvHEbYm1nn3xJb6jpTaSq2IbvFDqIYX43GCahk5oNqmmZjQDbuLemnCXozeLV9d0GlJVNMG/sjWzad2hLCfs14M+UijgSK33SXxY82moieB/MYd2FQWG8mxn114o+EdxeKe8/PARWDZkmAK7e05W4KM7XYcsUSL5eHmyvXL2WR56eDDN7nBaN7LKKB+XL/a3WdpgKang0kUljI8IX+kcBErlmFIWfx+E6k3sTPA4RgC7Xysig9x8ImwvcuujEV2SI93EnUj2/hraVgp7vNrZYiJ7v7518tZZqOPdTyXWQ/kCQJvWa7//5vS6GnBB0SvNLapH6HPZAx9CE8iV9Y466A2kGW9nHg84pcTdIYOstIaARthXHMe1QDaKWuNl0PdX7gENTZZC+oFPiCjuxHQBPbctDVlwOlTeZVSg0LUIpvSi+q1Pmv9NN4osyFU01AMM00Ast3qEfyg8sqiBXWa3u261qEBWCMG29uLs9KHVIAcSYTwDJQimxTaZJF4q8tZfN/7c23gO+2W19wofUY8lAI4bBiiBJKHA4RSjaQ+O/ojdu6afxDOYgnydgPI4SCfCgUUgsJyKJHgKA0fUVOShHod7bQnTsL4P4EjDjmshkCIXgsiBL8KBAVVrSU/CGBiOuo4Ow1vG3tyPkd7d9iT9Y6wCDcQQMBxCj7Uj0q8TxsZaNtP+055vjogFENnnYVSIhUwPlB6RsGK75gCcMIH0trq5yN133w5qbWBHuvuQqX+gUHwQCryUO47AkP4fKf6yFcvfIQrrFmYd1U+GgDrknO+Zuyd1ck9kMuxLWfcCGuM8dt120MJEwzkszfgMpMYzjw34rATEOdeA30Mk3/LuxlmoEjvhZw4Rm7eS26Mg2jph+BVKZhfCQBlGmIBQuoyTS38cJwgrGVmOf1sMAg/fR/Dm4zxl9gvIzpL0VOxoyXQCdj5vdg5yvjP8asCkiLUJtB7i2RtOwbOUa0zo8hKWliuydgvQBcwBQy2oIpboWmjOMuA+awWA8SF5MkbxLeoa5TLss4q2XH5346gqyaJFqZAyhYV2C6LTuQFoGCP7i6PJK0oFS5IGpV5tiMV8L+h1KNqNbmAay+TqdCuY+n457ij7MYHlNwyN1prsyXpiD2RGFAMQvjmBxuyF6l9j0oPUijcmXo3wklC3tCEf0+TAXkAz0W6Z2vRLUFqWh23Kjn+HXBNH8pxVtIVbLJnIM+usirzSL2YdyWvHZeDF24AYFbBSVbaDNH+2EI4hJAdQq/qpiM6bSzxWDKczTRS/9aX/AaqVD75SRZbpbM1adZ74R0X7eWbfc5TdXtJfOV/nFOTBMjSudJCy7mVs1qRXrU3i1s0ixN0tAKkyxN0WCvMcWBiEQaBnVwMvjcDg29IbW2Bn5DP/PzPG0iKLcU3CXa3FIAupQozpcrh8LvbW7toN4ubm2UP4TsSFWiPHaH1CthMNUWkEYbKuX+EDNWwiaiRSYCYalVxkpURFSS2UA31z+jFiYxEnMYhfm83pQ+z2RV5PBEkLQubvB7uik9r4l8Lo3Q/M4gAvVhUFqKSclXMJazs9CXB2M/y5TNJyYmHThucFQGtrDFQcZFmin7gVSh6Hxhk2NONmcQ0C5yCzc0lvxwDtErhYOkNr04yPIUYZrggkakhgsbHtOk5+fCk5abkjG3cFatoqYHsSiZDO5jrDPlNZpu6WvtuKCs3LBlPOuGC0MmbQJLSyh2nvveNVZtmYVV474EWbVlFVZddkBk9ndaNgQDuUeb5OiQ5LrLpTpzo7aWIzP8KYKP0gwxBC52STywaw/ihfhe7t7bywKVTKjikKKn51lgL0dnz2ysMpvXDNLWG6+xOgi8wVaKOVLB+m3tvSTEziA1RnX2smmXFNPz1DckiOwtQhWlIt89DzkRq7L4RbmVIKeWILCuH10zivXYNRSxnRoq4J5HZd4Qpy+wu0DTxQi7H0b+OHwUK1uyrUq+66XpsDXb1gtDEBWvyRyDl5gknRXO4kl9IR1w5tH1M2ruVIH1Ek+JiCiqmJqtN+ZFlkhFqe3YFxv1zCmjZQOye2nEThktLzci+u+NmJ9bZS1SdpcEW2y+19xpLWEq1L484mjSStwl4VUMWGmHi2+VI8i2/GU7G8ytbRkz12jIZPE1hoxsH3zfKQ9zrdr5BHUiDxddzH9lREVk7o/OitMJyEZ1erWBReyVNs5xjwI6JY9KPery+NOg4sLVUgWdoXLM6eVB2KL8qC97IVxfoZFOeJ31LETvrsvl9gfH1L+EGQ5J/+06bZofHM3jdL6Y9q8edvGFFB4BI/XDTHgwd386GNYUu0AQtM0dashfABac6d8AC86MZ8FCItxTcyqQgrPS2xtmgRScrXH3nP08d8/Z3N1z9ry7B4SoReylXkpPqKLTT5tbLeyTr92lWf15Ln4ghvCVkywxzggmOF85hFqc7DdiE45p9u0caTiw8xUYw/F0f/B3RQic/5QIgfNahMB5LULgvBYhcH3JkTwTkHF9RYIv9ZtcX/YYPzUS4fpLwgSufytM4PqLw4SXhLtcn0cLvMAAvYwWZMbL9XmAAClOG3fnRNRry93IxdnP3T83tKWZIUxCj9+IqTyJk8baM2XQ0JC4n8rEr1Q3Ok6ZV8rcsAnJl79bKiT8Th+8xAeGyoUbaw163okaZt4dPPaaEkcrhdlFLyG7WLlXISUgR6TGmP95OBHSn6rvxTrqy1jEBwNNu2AJ1VuymM4c4zQcoB56hYQMZ8F1onE/u8HVDxzpIg33VTsFHG9aVBZM2w6vRwXcfcCj55JZVIa3yerTOhb5VUUhJf3mI4r4MAzyHeXPv95KPZK7ES/eeeDoCCvggWYr6SRqHfKX3BONYNdg41caozrD56ZTDxI8AI9eqacixatf5DCb0GkxXbcCiTXmZWklOBRnIBUoWOxZUJBqSVFMXXW+aYrUNS8AAnc9CR8sfREfaBdiFR+Qz4vTN+foYBWslTKpy6FZ82CSKZa9FGytmToO5oU9Ot4rWjkLmzSyHsRWPsVpZamkEXSHkFtuzQZtrfET9Nl+3W4ct8keimlTgV5edCwV1TYWFbU4VeX2yl4ldgNMwNFVOSqpczMZ7RYjkNBta43DpUaHscgu4rwzg4BPjrGye0l4SdRAeITBCeU+ROxU+r2iHrgrILV7ziYsd7QndZu1NaJghYrirRWfLkwMYHoile05XeqUVzRrlz25g7c9oQavK9A7qvBKykY1tI05EXgOG2YTKrKo13yYCr9XCAbS7ItiCd1pOM7DSE6A7vjQpVJ6pW295TQQ71DSnZvUv/cWrplySIMvYFmUN0EhFckwyUVNyB4y3FG+o3K6sMohA4X0a+JHAC7IXUQ2OsKH/G8KwUGK91Wo0KZCTaV/O46mabIc5QQDQpTT94A4K26wQv55UsDz8gXbc5FlMNwm3vWkbWid0stJrZiv4++OsoYXOh5d05XBaCDSJIVxqJSWp2voG//8E7HAn45z5a+/qNKiI6DVmb2DqJwIbDktdJbTdExFKAuM2nfaMq0ZxqDWyAYJXyFYKNIxEg2eBkil0hkZng7pHP1BOewFaCXbZ340mBYrZnSyIKLtm6uWiP71dVdT3VY63b68oWcHnunBJmIwVp96s+a9daIg7gETiIAsbfAYJi1c+hjih5bSTanKKdvigg7iKBLlrWNdZkcjIZJtcDt3NDFMdIiecFyOReUU0eAeRHuYT8YtwPNxGPjYVXuGJf+cLZdOxsW6wgmsun0vuknx6CfRoPVL+xe5VureKIc1C3++vTcQUracIOc8fgzHY79tqprS/IOxfylnYTSdKTPH8ixjS9mDocXvonsa5m1Tt1XdUpqn767Pz1rKOBwJ5VgEo3hLORhC+CPaNge11i1uqYwz5crv+2lYNKMh7XI6qBLvYnmpV95lXtQGLMcrzESto14f+MFQbAOn8zQmVdIpnpz4s21Y+65soZctEFlukkEK+rN9AgYYTFOxXXgPebeZkBpA3fdpHygehUJWkL/UdVv+wYlm0x4ppU4THb23byc3XV/78iHaYUNdezB22MOHq/txHluB8AClDkd9/WH6ODjKz06woVGuxGB0F8mnQpr/8R5Tmco1W7O4ZjJ4MQ1Lt3Xb0YmoXJGBK5KmlhH46fJcTBW/zdJPp3H++eLjifmJfTg70H8LD5z8OuSnj9rj7PPgcXzduZ8+XM3OHq/c0zN+dfrp+GLw6WD4/vEqS76Gs9Hh5CY5G+m/fdne2z+Y/GFPjP54GH4aH08+Hpmd2/u9j7enN3F+Nrr78Jg9Xlzf36anjjF9uGEXB8PPh+fXo+T8cWRfAA9uB8MwumVfrya377N98+yPwGEXj+ez2f7ZaRAff/0UGnePJ7Nw3xyGv1uDzp7odnjnvmTk6Gj7/tPFx9+8yTS7u/xwdepd748ePx+fW5KRVskO9GgZ3e42KO6YTKb9rr5/b50l4Yn4wg67J8He5R/pdF98ZH8czYjSLVqbGjEzQxCUzDQp/XEezMTJJl+01JiG/S+PUewOrYj5Yc6se1MqAfjDohPEIDzdzWMIv6iK0MdIuoNu/nnw6bjTG53cdzuXjin22KV989G90s4PtfzG9a3Ol6Ozd2lgBMfhET8Vp+/3bh6TTzcTIe/Xl9CEgfTm9cFV5+rq5P3FySFVEiwddpixZ+9Zjs0OzQNHt45ch5m6c+QYzOFcUjpSvav4WDcRro471/hsEcaK6I5e6BMNMF2ov+pc3nYuvQ/vLyUdLrQ0SosOTQqKi73zDhUiusy/JpGEtMF62Tl/f93x9g4PL6nQbix9dqLjcdT6L0j20dMH6XTSJcOESBFniDGi/ExEt+WcbU73jnMxiFMMbHSK1yj/pDeacR6CY8z9CTkmCNOOmGlyyP5dQ1fB3lyNUTKlyfWfdQ6ulQ15FOgVirKhbhRP3kg8bLSeqcf0aC2BmCVhKrBebChHl+/PV2iU3991LjtK8xtt3yr4Fxellg9NrjG3pegthWnw32opnMN/eLcgJHOhKn9E97r75ub64C0g+8XhN5YIcet/vmkT/9mkz03oDEtGdEWUQKW2TGEAzjCyX5SLS9nrkigMzdDsuSj+fQRhnUhF77/YhCLTlUEwGq2CJyrgjTP9jKKm3ogK5Jcs1DCATDDKvZAUwVmjuA4p7jwG0x1UWvBoELDRK0WXxXcVOgWXqX8HMYfcnMVCV35DkopAgP8nw4OYcr5O0ki6VUaa7cqgxpVXsadjCL9oYRBcUsjrDySZKcnAQUFh6keZX8Ucri03i2shFISUOfwGzGtSdo8X78uvQTS6VGVzo9s3bN6zXSpk0MAAtmODsbgTYypFoYkiMAfpEI1JNDKmMzTrRTEdyJXaOsWE0Nx3pvIDNgNPC8LeDs0ADwOaKOG7kOJxg6HXfJ8PJSmEgEikE1GcUREKluIWejOJwCKCbhrfF2Mw5JEMVBQMVDaJzKEgVn7wIjsjnDHo3lklUoO+LQP9VHV6o5hcGg+9I5NAiyiwNDjJt7xDSiXmQr1Vrwet7YWSyiYqvRjDKa6i1xJhKndpJq7K8U0vcpg7ckEGhUqVrhoQI0HSWmjwZmE8A4OOi9Ar+MVumCETcIPipiI9M3T8vEZmYmUuZeh4XEHqrf4fK54hpA==	2019-03-10 16:22:24.64903+00
phuCSQSySH65A7OZtm7PPg==	eJzFW2tT20gW/e5foWI/YGaNrG692ZAaAiZheCTBwEyyD5Ust23FtqRIMhim5r/vvbclWX5ASEjVsLUZqft2q/s+zj39cLOfsEZ3OxNRnt6rYZSLdOAHIlOvM5FuNxLeaPYTHSRm8B75UwFlBrz6UT8Vd/BiNrLudph4fh8KsgxKLKhmNleZq2oqgwIbRcTUDyfw4lSNTUMLf72HZzFX0xlUudRVH56Yhn1sN7Js09A680AkeRhHKMlwgIyDvJgHXjwN81xQF3rjAlrf+pOZwFExo9GcJMwkcRxilvvBOE+hR6y1qRwHN0hhltTCxRZcwxqOShrMoqD4LMcPhlEkdaTjwP1e5iV+PsIC1FB7lqXtSRz4k/Yk7LWT+3wUR7pqtfthlu8m8HF/KLJ2/4sfDeN2EKeiPQJlTESatUU5QzW5x/5IyUkqvCAGNcxzLLNodDZ8SYG/wI+UVEzulThShiLPw2io+FCSJXGUCSWMslz4fSUeKCC46B47chq+7GNrawvf3fL917vUT7ImdOeVHe2AgK6VAn0xUEgJzVR8BTXnO3tYz8p6/APbUSFv+GSOFFWr6+RWqKZ671hjQtmrXjyL+spUgMr6yhs/E++kZlSvLo6zeRWA0iIxydRRnifqQTYMC1kl7n0RQa74uaLN7YHRHwSa5YqB9vo1fsdClRbDxndU5CtsfinLZDumu66t28wwOGcup5YOufM8wGe30d36LNL4KLwNM1BoJ03jtLndL16V3r3yANXbrZ0tiBuNHDqJs7xmSIOhIQ3eWOhM2kep/FzxMyxDPRp6Xbn4V2ljv3r0BnHqVUYujdPCPtCAhlHvIxX5LI2Umg0Ms6wv6kpHN6yGf0N2HIQTUaIBau45XmyQ4qZxfzahdm7VTsV2atlOFbX4NjUChcjzkwRfWeNEo6JCgd4kjLA3s66/FbXUfabyVGxDcYsdRDG+Go0TcMnMB9c0zcaQYtxb8U8T/Gb44vjugUtLpZg29kexbDq1KYSDWvBmykUcCTS/6a6aH2M0EX0PxjHpwUdhvpmYDNSpPxbebSjuPD8HVAyapQDO3NJWuynC1GKrFSu6XP3cwrl+KYs8Px1m8L4oGN9hEX2Yr/a/0ddpKODp2VAihYUKX+ofBUzUmlUEchZPbkXqTf08QAi20Csvi9JzLGwifO9jGlNRLTLDnUSD+AbeWgp2ut/caSlysPt//tVSZunEQy/fR/WDSJLQa7b/7/+2FHpKMCHBK81N+nfYBxtDH8KT+IU17hqoHWbpAD98XomrSRpDZxkZjaCtCI5Fj2oArdT1ppuhzg8cgjqb4gWdAl8wkf0IaGJbDr76fKC0KbxKq2EBWvFVmUWVuv6VQRpPlYVxqgEIpplGYPkO9Uh5cNUFscJ6ac923YuwAIJx69X15VnpQwogznQKWAZOkW0rTYpI/GdH2f5fe/s14Lvt1idceD1SHqIQDis+UUKJw4GhZEOJ/47euKmHxj+Uw3iaTPwwQijIR0Iht5CALPoECErTV+SgFIF5ZwfTubME7o/AiGOuhiEIQsYCluBHgaiwoqXk9wkwruNCs1fwtrMnx3f85gZ7sjYBBuEOBgggRtmX6lGJ52ErG2P78cyxwEcHgmr4eKpAQZQCzQ/L3DBcywUrGEb4WEZblWy83r23CLUmxHstVbjULyQIBlhNGcJlj2QIl//cDOHqVYZwjQ0T66XCxxhwTUrO37S9u2axH0ohrv1ICnGdBW67bmMoYZqRZf4GVGYaww//rQjMNPSJl0Av0/Tvwl6mGfjFlwIuPGM3L0VXpiFr+hFIZRryIwmgTEMsWEJNprmNZ9IJxtY4z8thgcHy0/85uM0Yf0bwMqY/FzkZM54DnYyZ34OdL+R/jFkVkBZUm8HaWyJp2TdqjGSdH0NS8sR2X8B8AbhAKRS0hVLcCk0Zx10GXMNiPVhcTJO8SXiHvk5rWcZZbXV87qdjWFWTRatwAAfrCVxuyw5kRKDhD7uXx1IWnCoXJK3KNTbjlbH/oVRfVGvjAFVfpTOh3MWzSV/xJ1kMjykk5N4sVxZTUxB7ojAgzsI4Lg63ZK/S++6VPiyjcmXk3wolC/tCEYMBDAXsAz0WyztfiWoTUjHsuFFf49cN0/ylNG9hVakmcwH6mCK72wX3YdyWunaeDV24AYFbBaVaaDNH+2EI4hJAdaJfFSdjOu1sMRjyAk30Mr/WJ7zBKtR+dZEsN0sW7tOsd0K+r1ursfuUp+r2SvjK/LgQpoGRpPNoBBdjq0a1Zj1q7xYxaZYhaWhFSJahaLCXhOJQRCINgzo4GXwRh4bekF5bA7+Rn/l5njYRlFsK7hJt7ygAXUoU56uVI+H3t3f20G+XtzbKP0J2lCpRHrtD6TUaTLUFpNGGSrk/xIw12kSyqEQQLL3KWGNFJCWVDXIL/zNqNImRmcMozBf1psx5JquYwyMkaRNv6DFwfnJ7E/VcBqH5nSQC/WFYRopJi69gIkdnYS4PJn6WKduPDEwmcNzgqAJsaYuDgos8U/YDS4Wi86VNjoXYQkEgu6wt3NBYycM5sFeig+Q2/TjI8hRhmuCCvkgNlzY8Zknfz4UnIzelYG7hqFpFTR+4KIUM7mNsCuUNnm7pG+O4kKzSsGU8mYaLQCZvgkhLiDsvcu+GqLbMIqpxX4Ki2rKKqC47IDH7OyMbyEDu0SY5JiQ573KqziKorVVmhn8F+SjDEClwsUviQVx7wBfiO7l7b68aVCqh4iFFT0+rwF5lZ09srDKb1wLS1hsviTog3hArxRipYPO29kESYmewNEZ39rJZjxzT89RXZIjsNUIVLUW+exxyIFYV8ct2K0FOLUFgYz/cKOZj11DEdmqogHseVXgDT19Sd4Gmywx7EEb+JHwQa1uyrcq+m63psA3b1kufICleszmSl5gsnRXJ4lF/IR9wFuz6CTd3KmK9olMSIomKU7PNwbysEukotR37YqOeOSVbNmB1L4PYKdnyaiOS/17G/NQsa0zZXTFssfleS6e1BVPh9uURR5Nm4q4Yr1LAWjucfKv8gmzLn7ezwdzaljFzjYZcLL4kkFHtw+875WGuVTufoE7k4aKL61/JqEjM/dFRcToB2apOr7awiL0wxjnuUUCnlFGpR10efxpUXKRaqqAzVI5renkQtmw/6steoutrMjIJb4qeJfbuunxQDM5ZwAyHRf/NJm9aHBwteDpfXvavH3bxpSU8Akbqh5nwYOz+bDiqOXaBIBibe9SQPwMsONO/ARacGU+ChUS4x8ZUIAVnZbY3zAIpONuQ7jn7eemes0W6B8r4ZLoHhKgx9tIvZSZUMemnzZ0W9sk37tKs/z3FH0ghfO0kS0wyggnO1w6hlgf7DW7CcZl9s0AaDup8AcZwPN0f/l0MgfOfwhA4rzEEzmsMgfMaQ+D6SiJ5gpBxfc2Cz82bXF/NGD+ViXD9OTSB69+iCVx/Nk14Dt3l+oIt8AID9JItyBUv1xcEAZY4bdydE1G/LXcjl0e/SP9g45WRIUxCj9/gVJ7ESWPjmTJ4aEjaT+XCr3Q3Ok5ZVMq1YRMWX/5+6ZDwb3rvJT4oVE7c2BjQi07UMPNu4bHflDhaOcw+ZgnZxdq9CmkB+UVqjOs/DwdC/lP1vVxHfRnL+GBgaBcqoXpLFtOZY5yGQ/RDr7CQ4SylTgzuJze4BoEjI9BwX7RTwPGmRRXBtO3wclTA3Qc8ei6VRWV4m6w+rLci71YS0tKvPqKJj8Ig31P+/Ou19CO5G/HsnQeOibACHmi2tpxEr0P9UnqiL9g12PiVvlGd4XPTqZMED8CjX/qpSPHqFyXMJnRaDNetQGJDeFlaCQ7FGUgFChZ7EhSkWxKLqbvON0ORuuYFQOCuJ+GDpS/jA+1CrOMD6nl5+OYCHaxCtdImdTs0axlMKsWyV8jWhqHjx7ywT8d7RStnaZNG1oPZyqc4rSKVPILuEHLLrcWgrTV+gj/bL9uN4zbFQzFsKtDLi46lo9rGsqMWp6rcXturxG5ACfh1VX6V3LmZjPeLL5DRbWtDwqVGR7HILuK8MwfCJ7+xtntJeEnSIHiM5ITWPiTsVP695h64KyC9e6EmLHe0R32btTWSYIWL4q0Vny5MDGF4IpXtOV3qlFc0a5c9uYO3PaEGryvQO7rw2pKNamgbcyrwHDbMplRkUa/5KBV+vzAMLLMviin0ZuEkDyM5ALrjQ5dK6ZW29VaXgXiHku7cpP6dt3TNlMMy+AKmResmKKQiSZNc9ITsPsMd5VsqpwurHFagsPya+hGAC2oXkY2O8GH9NwNykOJ9FSq0qVBT6X97jqZpshztBB8EljPwQDgrbrDC+vOkgOfVC7bnIsvgc9t415O2oXVaXk5rxXyTfveUDbrQ8eiargxGQ5EmKXyHSml6uoa58c8/EQv82SRX/vqLKi06Alof2Ttg5SRgy2FhspylEypCWyBr32vLZc0oBrdGNUj4CiFCUY6RafA0QDqVzijwdFjO0X/QDgcBRsnumR8NZ8WMGZ0siGj3utsS0b++7muq20pnu5fX9OzAMz3YJAzB6lNv1qK3ThTEfVACCVCkDR/CpIVTnwB/aCm9lKqcsi1O6DCOIlHeOtbl6mgsRLILaeeWBoYLHZInHJffonJiNLgH0R7l00kL8HwSBj521Z5jyT/nq6XTSTGvcAqzbt+JXlI8+kk0bP3S/kXOlbo3ys+aRT7fPRgKaVtOkHMeP4STid82VU1p/sHYv5SzMJrNlbljeZaxoxzAp8Xvonca5m1Tt1XdUpqn767Oz1rKJBwL5a0IxvGOcjgC+iPaNge31i1uqYwzpesP/DQsmtEn7XI46BLvYnmpV95lXvYGLMcrzCSto18f+sFI7IKm8zQmV9KJT079+S7MfV+20MsWiCzXyTAF/9k9gQAMZqnYLbKHvNtMSA2g7vu0DxSPQyErKF/qui3/gwPNZn1ySp0GOn5v30yve7725UO0x0a6dm/ssfsP3btJHluB8ACljsYD/X72MDzOz06woVHOxGB0F8mnQhr/2wOmMpVrtmZxzWTwYhqWbuu2o5NQOSMDZyRDLSPw0+W5mCp+m6efTuP888XHE/MT+3B2qP8WHjr5VchPH7SH+efhw+Sqcze7787PHrru6Rnvnn56ezH8dDh6/9DNkq/hfHw0vU7OxvpvX3YP3hxO/7CnxmAyCj9N3k4/Hpudm7uDjzen13F+Nr798JA9XFzd3aSnjjG7v2YXh6PPR+dX4+T8YWxfgA5uhqMwumFfu9Ob99kb8+yPwGEXD+fz+Zuz0yB++/VTaNw+nMzDN+Yo/N0adg5Er8M7d6Uix8e7d58uPv7mTWfZ7eWH7ql39Wb88PntuSUVaZXqwIyW0e1ug3jHdDob9PQ3d9ZZEp6IL+yodxIcXP6Rzt6Ij+yP4zlJukVrUyNlZgiCUpkmLX+cezNxsukXLTVm4eDLQxS7Iytifpgz686UTgD5sOgEMQhPd/MY6BdVEfoYSW/Yyz8PP73t9Mcnd73OpWOKA3ZpX390u9r5kZZfu77V+XJ89i4NjOBteMxPxen7g+uH5NP1VMj79SU0IZHevjrsdrrdk/cXJ0dUSbB01GHGgX1gOTY7Mg8d3Tp2HWbqzrFjMIdzKelI9674sW4iXL3tXOGzRRgrolt6oZ9oQOhCfbdzedO59D68v5RyONEyKC06NCkkLg7OO1SI6LL4NYkUpA3Wy875+6uOd3B0dEmFdmPlZyc6Hkdt/gXJG8z0QTqb9igwgSniCJEjyp+J6LYcs83p3nEuhnGKxEYnvkbrT3qjEechJMbcn1JiApp2zEyTw9pet23VYBBtyKllxux2zjqHV8qWPAn0Cj/ZUreKJ28s7rdaT9Tj6mijgJgnYSqwXmwpx5fvz9dklN/fdS47SvMbbV8r+F+ck1o+NLnG3JaitxSmwf+tlsLx1W4pBjBjHd7zB8yu+6+urw5fA7BfHH1jikBb//PNkPjPNv3ahI6wJKErSAKV2nIFA2iGxH7ZLC4tXlct4bomXWOQpvj3MbA6kYr+f7EJEdO1jyAZrbgTFfDGmX5GpKk/pgL5QxZqGMBCMMq9kPzA2eC3DvntgoLpDvosJDTga/RK5LL4WYVO3DL1b4FyyL1ZLHTlT0hSEQhI/xR3QClr86S4Qy5Gju1KTuPKm9izCbAvmhhwS2K8/lCKmVIM8hMUpn6U+RXlcG25V1xjUMAoc/gXIK9Ji3u8d1/+GESjO1U2N3oDw+Z926VCBg0MUDs2mIhbMaFSNJooeLkBfBBlTJKRlM7QrGdROgPYILZ1igFhtO/N5O/XDDwsCPt7NAI8C2iihW9DouMGw6T5Ph9JUWCAKKSTUJxRERqWaAu9mSRgkUAvje+KbzDUkeQpCvKUbRJziMPK37vIzghmDLp2VpnUoJ+W6aql6vRGlFwGD72jksCLiFcanACnvEJKJeZSvVWvB6/th1LKJim9+IZT3ESvrYOp3KWRuCrHN71YwtxSBjKIKVW+agBFgjVr4cHbRfAMDTotwqTgF5thhlx/G0SbitWZoeOva+RCrFxKAYxAGbm3+n//sCCI	2019-03-10 16:29:37.530563+00
QFAC5fKMQA6sJdq1V5R3sg==	eJzFW+lT28gS/+6/QsX7gNlnZM3o5oXUEjAJy5EEA7vJO1SyPbYVy5IiyWDY2v/9dfdIsnxASEjVshuQZnquPn7dPTNqDhLW6G5nIsrTezWIcpEO/b7I1OtMpNuNhDeag0QHihm8R/5UQJkBr340SMUdvJiNrLsdJJ4/gIIsgxILqpnNVeaqmsqgwEYSMfWDEF6cqrFpaMGv9/As5mo6gyqXuhrAE9Owj+1Glm2aWmfeF0kexBFSMpwg40Av5n0vngZ5LqgLvXEBrW/9cCZwVsxoNMOEmUSOU8xyvz/JU+gRa20qx8kNU1gltXCxBdewhiOThrOoXwzLccAgiiSPdJy438u8xM/HWIAcas+ytB3GfT9sh0Gvndzn4zjSVas9CLJ8N4HB/ZHI2oMvfjSK2/04Fe0xMCMUadYW5QrV5B77IyYnqfD6MbBhnmOZRbOzYSQFfvp+pKQivFfiSBmJPA+ikeJDSZbEUSaUIMpy4Q+UeKgA4aJ77Mhp+LKPra0tfHfL91/vUj/JmtCdV3a0AwS6VhIMxFAhJjRT8RXYnO/sYT0r6/EHZEeFvOGTOFJkra6TWiGb6r1jjQllr3rxLBooUwEsGyhv/Ey8k5xRvTo5ruZVH5gWiTBTx3meqAfZKCholbj3RfRzxc8VbW4PheZo3B/6rvb6NY5jIUuLaeM7MvIVNr+UZbIdMzRL44ZlWjYzDYNaOqTO8z4+u43u1meRxkfBbZABQztpGqfN7UHxqvTulQeo3m7tbIHdaKTQSZzlNUEaDAVp8MaCZ1I+SqXnip9hGfLR0OvMxZ+KG/vVozeMU68ScimcFvaBAjSMeh+pyGdppNRkYJhlfVFXKrphNfwbkuMwCEWJBsi552ixQYybxoNZSO3cqp2K7dSynSpq9m1qBAqR5ycJvrLGiUZFBQO9MIiwN7POvxW21HWm0lRsQ3aLHUQxvhqNE1DJzAfVNM3GiGzcW9FPE/Rm9GL77oFKS6aYNvZHtmw6tSUEw5rxZspFHAkUv+muih9tNBEDD+YR9mBQWG8mwqE69SfCuw3EnefngIr9ZkmAK7e01W4KM7XYasUKL1eHWyjXL2WR56ejDN4XBZM7LKKB+Wr/G3WdpgKano0kUljI8KX+kcBErlmFIWdxeCtSb+rnfYRgC7Xysig9x8Imwvc+ujEV2SI93Ek0jG/graVgp/vNnZYiJ7v/518tZZaGHmr5PrIfSJKEXrP9f/+3pdBTgg4JXmltUr+DAcgY+hCexC+scddA7TBLhzjweUWuJmkMnWUkNIK2wjgWPap9aKWuN90MdQ53COpsshdUCnxBR/YjoIltOejq84HSJvMqpYYFKMVXpRdV6vxXhmk8VRbCWUzA5TC86TrUI/nBVRXECuulPdt1LcICMMatV9eXZ6UOKYA40ylgGShFtq00ySLx146y/b/29mvAd9utL7jQegx5KIRwWDFECSUOhwglG0n8d/TGTd00/qEcxtMk9IMIoSAfC4XUQgKyGBAgKE1fkZNSBPqdHXTnzhK4PwIjjrlqhkAIHguiBD/qiworWkp+n0DEdVxw9gredvbk/I7f3GBP1ibAINxBAwHEKPtSPSrxPGxlo20/7jkW+OiAUY0edxVIiFTA+VHpG0ZrvmAFwwgfS2urnI3Xu/cWptYEe6+5Cpf6BQfBAKvJQ7jsEQ/h8p/rIVy98hCusWFhvVT4aAOuSc75m7J31yT2Qy7EtR9xIa6zwG3XbYwkTDOSzN+AykxjOPDfisBMQ514CfQyTf8u7GWagSO+FHCZZmI3L0VXpmHU9COQyjSMjySAMg2xYAk1meY2nhlOMLYW87wcFhikn/7PwW3G+DOMlzH9ucjJmPEc6GTM/B7sfGH8x5hVAWkRajPIvSWSln0jx4jW+TEkJU1sDwSsF4ALmEJGWzDFrdCUcdxlwBwW60HiYprkTcI71HXKZRlntez43E8nkFWTRCtzAAXrCUy3ZQfSIlDwh93LY0kLSpULolZljs14Jex/KNWIam0ewOqrdCaUu3gWDhQ/zGJ4TMEh92a5sliagtgTBX2KWRjH5HBL9iq1714ZQBqVK2P/VihZMBCKGA5hKiAf6LFI73wlqi1IRbPjRj3Hrwum+Usp3kKqkk3mAvTRRXa3i9iHcVvy2nk2dOEGBG4VlGyhzRzthyGISQDVKfyqYjKm084Wgykv0EQv/Wt9wRukQu1Xk2S5WbJQn2a9E9J93Vq13ac0VbdXzFf6xwUxTYwonUctuJhbNas16VF7t7BJszRJQytMsjRFg73EFEciEmnQr4OTwRd2aOgNqbU18Bv7mZ/naRNBuaXgLtH2jgLQpURxvlo5Fv5ge2cP9XZ5a6P8IWRHqhLlsTukXguDqbaANNpQKfeHmLEWNhEtMhEIS60y1qIiopLMBrqF/hm1MImRmIMoyBf1pvR5Jqsih0eCpLW4wdU00xxY0vOayOfSCM3vDCJQH0alpZiUfPVDOTsLfXk/9LNM2X5kYtKB4wZHZWBLWxxkXKSZsh9IFYrOlzY5FmQLBgHtMrdwQ2PFD+cQvVI4SGoziPtZniJME1zQiNRwacNjlgz8XHjSclMy5hbOqlXUDCAWJZPBfYxNprxB0y19ox0XlJUbtown3XBhyKRNYGkJxc4L37vBqi2zsGrclyCrtqzCqssOiMz+TsuGYCD3aJMcHZJcd7lUZ2HU1mpkhj9F8FGaIYbAxS6JB3btQbwQ38nde3tVoJIJVRxS9PQ0C+zV6OyJjVVm85pB2nrjJVYHgTfYSjFHKti8rX2QBNgZpMaozl4265Fiep76igSRvUaoolTku+chJ2JVFr8stxLk1BIENmQftm8Oi/XYNRSxnRoq4J5HZd4Qpy+xu0DT5Qh7GER+GDyItS3ZViXfzdJ02IZt66UhiIrXZI7BS0ySzgpn8ai+kA44i+j6CTV3qsB6hadERBRVTM02G/MyS6Si1Hbsi4165pTRsgHZvTRip4yWVxsR/fdGzE+tshYpuyuCLTbfa+60ljAVal8ecTRpJe6K8CoGrLXDxbfKEWRb/rydDebWtoyZazRksvgSQ0a2j77vlIe5Vu18gjqRh4su5r8yoiIy90dnxekEZKs6vdrCIvZCG+e4RwGdkkelHnV5/GlQceFqqYLOUDnm9PIgbFl+1Je9FK6v0UgnvMl6lqJ3x+Hlip0FzHBI+m82adPi4GgRp/PltH/9sIsvpfAIGKkfZMKDufuz0bim2AWCoG3uUUP+DLDgTP8GWHBmPAkWEuEem1OBFJyV3t4wC6TgbIO75+znuXvOFu6es6fdPSBELWIv9VJ6QhWdftrcaWGffOMuzfrPU/EDMYSvnWSJMCOY4HztEGp5st+ITTim2TcLpOHAzhdgDMfT/dHfFSFw/lMiBM5rEQLntQiB81qEwPUVR/JEQMb1NQk+129yfdVj/NRIhOvPCRO4/q0wgevPDhOeE+5yfREt8AID9DJakBkv1xcBAqQ4bdydE9GgLXcjl2e/cP/c0FZmhjAJPX4jpvIkThobz5RBQwPifioTv1Ld6DhlUSlzwyYkX/5+qZDwO733Eh8YKhdubDToRSdqkHm38DhoShytFGYfvYTsYu1ehZSAHJEaY/7n4URIf6q+l+uoL2MZHww07YIlVG/JYjpzjNNghHroFRIynCXXicb91AZXz+4VFmi4L9op4HjTorJg2nZ4OSrg7gMePZfMojK8TVaf1luRdysKKelXH1HER0E/31P+/Ou11CO5G/HsnQeOjrACHmi2lk6i1iF/yT3RCHYNNn6lMaozfG469SDBA/AYlHoqUrz6RQ6zCZ0W03UrkNhgXpZWgkNxBlKBgsWeBAWplhTF1FXnm6ZIXfMCIHDXk/DB0pfxgXYh1vEB+bw8fXOBDlbBWimTuhyaNQ8mmWLZK8HWhqnjYF4woOO9opWztEkj60Fs5VOcVpZKGkF3CLnl1mzQ1ho/QZ/tl+3GcZvsoZg2FejlRcdSUW1jWVGLU1Vur+1VYjfABBxdlaOSOjeTyX4xAgndtjY4XGp0FIvsIs47cwj45Bhru5eEl0QNhMcYnFDuQ8ROpd9r6oG7AlK7F2zCckd7VLdZWyMKVqgo3lrx6cLECKYnUtme06VOeUWzdtmTO3jbE2rwugK9owqvpWxUQ9uYU4HnsEE2pSKLes3HqfAHhWAgzb4oltCbBWEeRHICdMeHLpXSK23rraaBeIeS7tyk/p23dM2UQxp8AcuivAkKqUiGSS5qQnaf4Y7yLZXThVUOGSikX1M/AnBB7iKy0RE+5H8zCA5SvK9ChTYVair9t+domibLUU4wIEQ5Qw+Is+IGK+SfJwU8r16wPRdZBsNt411P2obWKb2c1or5Jv7uKRt4oePRNV0ZjEYiTVIYh0ppebqGvvHPPxEL/FmYK3/9RZUWHQGtz+wdROVEYMtpobOcpSEVoSwwat9ry7RmHINaIxskfAVgoUjHSDR4GiCVSmdkeDqkc/QH5XDQRyvZPfOj0axYMaOTBRHtXndbIvrX131NdVvpbPfymp4deKYHm4jBWH3qzVr01on68QCYQARkaaOHIGnh0kOIH1pKL6Uqp2yLCzqMo0iUt451mR1NhEh2we3c0sQw0SF6wnE5FpVTRIN7EO1xPg1bgOdh0Pexq/YcS/45Xy2dhsW6gimsun0neknx6CfRqPVL+xe5VureKIc1C3++ezASUracIOc8fgjC0G+bqqY0/2DsX8pZEM3mytyxPMvYUQ5gaPG76J0GedvUbVW3lObpu6vzs5YSBhOhvBX9SbyjHI4h/BFtm4Na6xa3VMaZ0vWHfhoUzWhIu5wOqsS7WF7qlXeZl7UBy/EKM1HrqNeHfn8sdoHTeRqTKukUT079+S6sfV+20MsWiCzXySgF/dk9AQPsz1KxW3gPebeZkBpA3fdpHyieBEJWkL/UdVv+wYlmswEppU4Tnby3b6bXPV/78iHaY2Nduzf22P2H7l2Yx1ZfeIBSR5Ohfj97GB3nZyfY0ChXYjC6i+RTIc3/7QFTmco1W7O4ZjJ4MQ1Lt3Xb0YmoXJGBK5KmlhH46fJcTBW/zdNPp3H++eLjifmJfTg71H8LDp38KuCnD9rD/PPoIbzq3M3uu/Ozh657esa7p5/eXow+HY7fP3Sz5GswnxxNr5Ozif7bl92DN4fTP+ypMQzHwafw7fTjsdm5uTv4eHN6Hednk9sPD9nDxdXdTXrqGLP7a3ZxOP58dH41Sc4fJvYF8OBmNA6iG/a1O715n70xz/7oO+zi4Xw+f3N22o/ffv0UGLcPJ/PgjTkOfrdGnQPR6/DOXcnIyfHu3aeLj79501l2e/mhe+pdvZk8fH57bklGWiU70KNldLvboLhjOp0Ne/qbO+ssCU7EF3bUO+kfXP6Rzt6Ij+yP4zlRukVrUyNmZgiCkpkmpT/OvZk42fSLlhqzYPjlIYrdsRUxP8iZdWdKJQB/WHSCGISnu3kM4RdVEfoYSW/Uyz+PPr3tDCYnd73OpWOKA3ZpX390u9r5kZZfu77V+XJ89i7tG/23wTE/FafvD64fkk/XUyHv15fQhIH09tVht9Ptnry/ODmiSoKlow4zDuwDy7HZkXno6Nax6zBTd44dgzmcS0pHqncVH+smwtXbzhU+W4SxIrqlF/pEA0wX6rudy5vOpffh/aWkw4WWRmnRoUlBcXFw3qFCRJfF1ySSkDZYLzvn76863sHR0SUV2o2Vz050PI7a/AXJG/T0/XQ27ZFhQqSIM8QYUX4mottyzjane8e5GMUpBjY6xWuUf9IbzTgPwDHm/pQcE4Rpx8w0OeT2hqapJvh1gw7Ppcvsds46h1fKljwK9ApF2VK3iidvIu63Wk/UY3q0kUDMkyAVWC+2lOPL9+drNMrv7zqXHaX5jbavFfyLi1LLhybXmNtS9JbCNPhnwSP8hf9NbmgGbyn5A3rX/VfXV4evAdgvjr6xQghb//NNk/jPNn1tQkdYMqArggQqtWUGA2iGgf2yWFxKXlckAZ5dtxaS+PcxRHUiFYP/YhMKTNcGwWC0ip2ogDfO9DMKmgYTKpAfslDDPiSCUe4FpAfOBr11SG8XIZjuoM6CQ4N4jV4puCw+q9Aptkz9Wwg55N4sFrryE5JU9AW4f7I7CCkX66RLLnSpjBTblTGNK29iz0KIvmhhEFtSxOuPJJkpycA/QWHqR5lfhRyuLfeKaxEURJQ5/AbIa1Jyj/fuy49BNMQ/fWi7mmFxcyAcKmTQwAC2Y4NQ3IqQSlFooojLQY+IxiQaGdIZmvWskM6AaBDbOsWE0Nr3ZvL7NQMPC4LBHs0AzwKaKOHbgMJxg6HTfJ+PJSlEgEikE1GcUREKlsIWejOJwCKCXhrfFWMw5JGMUxSMU7aJzKEYVn7vIjsjmDHo2lklUoM+LdNVS9XpjUJyaTz0jkwCLaK40uAEOOUVUioxl+qtej1o7SCQVDZR6cUYTnETvZYHU7lLM3FVjm96kcLckgcyKFKqdNWAEAly1kKDtwvjGRl0WoROwS82wwyZfxsUNhXZmaHj1zUyEStTKUPH0wpSb/X/QLAaQg==	2019-03-10 16:30:01.307193+00
\.


--
-- Data for Name: sentry_activity; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_activity (id, project_id, group_id, type, ident, user_id, datetime, data) FROM stdin;
1	3	1	1	\N	1	2019-03-10 16:22:33.365232+00	\N
\.


--
-- Data for Name: sentry_apiapplication; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_apiapplication (id, client_id, client_secret, owner_id, name, status, allowed_origins, redirect_uris, homepage_url, privacy_url, terms_url, date_added) FROM stdin;
\.


--
-- Data for Name: sentry_apiauthorization; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_apiauthorization (id, application_id, user_id, scopes, date_added, scope_list) FROM stdin;
\.


--
-- Data for Name: sentry_apigrant; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_apigrant (id, user_id, application_id, code, expires_at, redirect_uri, scopes, scope_list) FROM stdin;
\.


--
-- Data for Name: sentry_apikey; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_apikey (id, organization_id, label, key, scopes, status, date_added, allowed_origins, scope_list) FROM stdin;
\.


--
-- Data for Name: sentry_apitoken; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_apitoken (id, user_id, token, scopes, date_added, application_id, refresh_token, expires_at, scope_list) FROM stdin;
\.


--
-- Data for Name: sentry_auditlogentry; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_auditlogentry (id, organization_id, actor_id, target_object, target_user_id, event, data, datetime, ip_address, actor_label, actor_key_id) FROM stdin;
1	1	1	2	\N	30	eJzTSCkw5ApWLy5JLCktVucqMOLyNOAqDlYvKE3KyUwGChgDBcAimSlAngmXj5EPiJeXmJsK5JtyhblkJeal53MVmIGEi3NK04HC5lxhKVBhC65iPQCkLxyh	2019-03-10 16:07:41.119802+00	172.19.0.1	andrew540i@yandex.ru	\N
2	1	1	3	\N	30	eJzTSCkw5ApWLy5JLCktVucqMOLyNOAqDlYvKE3KyUwGChgDBcAimSlAngmXj7EPiJeXmJsK5JtyhSUlJmen5qVwFZiBxItzStOB4uZI4hZcxXoA5J8dbA==	2019-03-10 16:07:54.574658+00	172.19.0.1	andrew540i@yandex.ru	\N
3	1	1	2	\N	32	eJzTSCkw5ApWLy5JLCktVucqMOLyNOAqDlYvKE3KyUwGChgDBcAimSlAngmXj5EPiJeXmJsK5JtyhblkJeal53MVmIGEi3NK04HC5lxhKVBhC65iPQCkLxyh	2019-03-10 16:11:33.399259+00	172.19.0.1	andrew540i@yandex.ru	\N
4	1	1	4	\N	30	eJzTSCkw5ApWLy5JLCktVucqMOLyNOAqDlYvKE3KyUwGChgDBcAimSlAngmXj4kPiJeXmJsK5JtyhSUWFHAVmIHEinNK04Fi5lAxC65iPQD2Fxpf	2019-03-10 16:27:29.613642+00	172.19.0.1	andrew540i@yandex.ru	\N
\.


--
-- Data for Name: sentry_authidentity; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_authidentity (id, user_id, auth_provider_id, ident, data, date_added, last_verified, last_synced) FROM stdin;
\.


--
-- Data for Name: sentry_authprovider; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_authprovider (id, organization_id, provider, config, date_added, sync_time, last_sync, default_role, default_global_access, flags) FROM stdin;
\.


--
-- Data for Name: sentry_authprovider_default_teams; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_authprovider_default_teams (id, authprovider_id, team_id) FROM stdin;
\.


--
-- Data for Name: sentry_broadcast; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_broadcast (id, message, link, is_active, date_added, title, upstream_id, date_expires) FROM stdin;
\.


--
-- Data for Name: sentry_broadcastseen; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_broadcastseen (id, broadcast_id, user_id, date_seen) FROM stdin;
\.


--
-- Data for Name: sentry_commit; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_commit (id, organization_id, repository_id, key, date_added, author_id, message) FROM stdin;
\.


--
-- Data for Name: sentry_commitauthor; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_commitauthor (id, organization_id, name, email, external_id) FROM stdin;
\.


--
-- Data for Name: sentry_commitfilechange; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_commitfilechange (id, organization_id, commit_id, filename, type) FROM stdin;
\.


--
-- Data for Name: sentry_deploy; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_deploy (id, organization_id, release_id, environment_id, date_finished, date_started, name, url, notified) FROM stdin;
\.


--
-- Data for Name: sentry_distribution; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_distribution (id, organization_id, release_id, name, date_added) FROM stdin;
\.


--
-- Data for Name: sentry_dsymapp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_dsymapp (id, project_id, app_id, sync_id, data, platform, last_synced, date_added) FROM stdin;
\.


--
-- Data for Name: sentry_email; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_email (id, email, date_added) FROM stdin;
1	andrew540i@yandex.ru	2019-03-10 16:00:19.096639+00
\.


--
-- Data for Name: sentry_environment; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_environment (id, project_id, name, date_added, organization_id) FROM stdin;
1	\N		2019-03-10 16:22:24.63958+00	1
\.


--
-- Data for Name: sentry_environmentproject; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_environmentproject (id, project_id, environment_id) FROM stdin;
1	3	1
\.


--
-- Data for Name: sentry_environmentrelease; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_environmentrelease (id, project_id, release_id, environment_id, first_seen, last_seen, organization_id) FROM stdin;
\.


--
-- Data for Name: sentry_eventmapping; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_eventmapping (id, project_id, group_id, event_id, date_added) FROM stdin;
1	3	1	625230f2ca2f4d30a2d0fc0d0079eedc	2019-03-10 16:22:24.634689+00
2	3	2	b89d2102db224d6e97bd24383e2e8219	2019-03-10 16:29:37.520758+00
3	3	2	85d96df4a9e34ee7aa1c55cb2dec022b	2019-03-10 16:30:01.294897+00
\.


--
-- Data for Name: sentry_eventprocessingissue; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_eventprocessingissue (id, raw_event_id, processing_issue_id) FROM stdin;
\.


--
-- Data for Name: sentry_eventtag; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_eventtag (id, project_id, event_id, key_id, value_id, date_added, group_id) FROM stdin;
1	3	1	1	1	2019-03-10 16:22:24.693165+00	1
2	3	1	2	2	2019-03-10 16:22:24.702857+00	1
3	3	1	3	3	2019-03-10 16:22:24.713564+00	1
4	3	1	4	4	2019-03-10 16:22:24.722723+00	1
5	3	1	5	5	2019-03-10 16:22:24.731932+00	1
6	3	1	6	6	2019-03-10 16:22:24.741346+00	1
7	3	1	7	7	2019-03-10 16:22:24.749959+00	1
8	3	1	8	8	2019-03-10 16:22:24.760697+00	1
9	3	2	1	1	2019-03-10 16:29:37.559827+00	2
10	3	2	2	2	2019-03-10 16:29:37.564875+00	2
11	3	2	3	3	2019-03-10 16:29:37.569664+00	2
12	3	2	4	4	2019-03-10 16:29:37.574119+00	2
13	3	2	5	5	2019-03-10 16:29:37.580487+00	2
14	3	2	6	6	2019-03-10 16:29:37.585928+00	2
15	3	2	7	7	2019-03-10 16:29:37.590247+00	2
16	3	2	8	8	2019-03-10 16:29:37.596398+00	2
17	3	3	1	1	2019-03-10 16:30:01.335963+00	2
18	3	3	2	9	2019-03-10 16:30:01.344298+00	2
19	3	3	3	3	2019-03-10 16:30:01.350358+00	2
20	3	3	4	4	2019-03-10 16:30:01.35723+00	2
21	3	3	5	5	2019-03-10 16:30:01.362199+00	2
22	3	3	6	6	2019-03-10 16:30:01.36694+00	2
23	3	3	7	7	2019-03-10 16:30:01.372614+00	2
24	3	3	8	8	2019-03-10 16:30:01.377999+00	2
\.


--
-- Data for Name: sentry_eventuser; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_eventuser (id, project_id, ident, email, username, ip_address, date_added, hash, name) FROM stdin;
1	3	1	andrew540i@yandex.ru	andrew	172.19.0.1	2019-03-10 16:22:24.588913+00	c4ca4238a0b923820dcc509a6f75849b	\N
\.


--
-- Data for Name: sentry_featureadoption; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_featureadoption (id, organization_id, feature_id, date_completed, complete, applicable, data) FROM stdin;
1	1	60	2019-03-10 16:07:41.12588+00	t	t	{}
2	1	40	2019-03-10 16:22:24.691932+00	t	t	{}
3	1	0	2019-03-10 16:22:24.803164+00	t	t	{}
4	1	48	2019-03-10 16:22:24.803378+00	t	t	{}
5	1	43	2019-03-10 16:22:24.803492+00	t	t	{}
6	1	63	2019-03-10 16:22:33.443908+00	t	t	{}
\.


--
-- Data for Name: sentry_file; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_file (id, name, path, type, size, "timestamp", checksum, headers, blob_id) FROM stdin;
\.


--
-- Data for Name: sentry_fileblob; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_fileblob (id, path, size, checksum, "timestamp") FROM stdin;
\.


--
-- Data for Name: sentry_fileblobindex; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_fileblobindex (id, file_id, blob_id, "offset") FROM stdin;
\.


--
-- Data for Name: sentry_filterkey; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_filterkey (id, project_id, key, values_seen, label, status) FROM stdin;
1	3	transaction	0	\N	0
2	3	server_name	0	\N	0
3	3	level	0	\N	0
4	3	url	0	\N	0
5	3	sentry:user	0	\N	0
6	3	device	0	\N	0
7	3	os	0	\N	0
8	3	browser	0	\N	0
\.


--
-- Data for Name: sentry_filtervalue; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_filtervalue (id, key, value, project_id, times_seen, last_seen, first_seen, data) FROM stdin;
2	server_name	d724bf472d79	3	2	2019-03-10 16:29:37+00	2019-03-10 16:22:24.70081+00	\N
1	transaction	/user/info	3	3	2019-03-10 16:30:01+00	2019-03-10 16:22:24.690366+00	\N
4	url	http://localhost:8000/user/info	3	3	2019-03-10 16:30:01+00	2019-03-10 16:22:24.720456+00	\N
9	server_name	3f7904625de8	3	1	2019-03-10 16:30:01+00	2019-03-10 16:30:01.341833+00	\N
5	sentry:user	id:1	3	3	2019-03-10 16:30:01+00	2019-03-10 16:22:24.729295+00	\N
3	level	error	3	3	2019-03-10 16:30:01+00	2019-03-10 16:22:24.711498+00	\N
6	device	Other	3	3	2019-03-10 16:30:01+00	2019-03-10 16:22:24.73903+00	\N
7	os	Linux	3	3	2019-03-10 16:30:01+00	2019-03-10 16:22:24.747735+00	\N
8	browser	Chrome 72.0	3	3	2019-03-10 16:30:01+00	2019-03-10 16:22:24.758877+00	\N
\.


--
-- Data for Name: sentry_groupasignee; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_groupasignee (id, project_id, group_id, user_id, date_added) FROM stdin;
\.


--
-- Data for Name: sentry_groupbookmark; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_groupbookmark (id, project_id, group_id, user_id, date_added) FROM stdin;
\.


--
-- Data for Name: sentry_groupcommitresolution; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_groupcommitresolution (id, group_id, commit_id, datetime) FROM stdin;
\.


--
-- Data for Name: sentry_groupedmessage; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_groupedmessage (id, logger, level, message, view, status, times_seen, last_seen, first_seen, data, score, project_id, time_spent_total, time_spent_count, resolved_at, active_at, is_public, platform, num_comments, first_release_id, short_id) FROM stdin;
1		40	ZeroDivisionError: division by zero /user/info	/user/info	3	1	2019-03-10 16:22:24+00	2019-03-10 16:22:24+00	eJwtzDEOwjAMQNHdp+iWTkhNE6A7cIFuLChgC0UqJLJDpHJ6HNTN/np2j3mA2SxByo3pQbESGsgWLoP31o5ucg5kNmXNpH1US8yJdf73F5WAoQTdPfSY9/Bs5kqcTrFGiel93vyh+RqWT3t0VIQb6O5r99UDzROI7H7Ydi0o	1552234944	3	0	0	2019-03-10 16:22:33.356811+00	2019-03-10 16:22:24+00	f	python	0	\N	1
2		40	ZeroDivisionError: division by zero /user/info	/user/info	3	2	2019-03-10 16:30:01+00	2019-03-10 16:29:37+00	eJwtzDsOwjAMgOHdp+iWToikDY+9cIFuLChgC0UqJLJDpHJ6HKmb/euze8wWZrMEKXemJ8VKaCA7uFrvnRv8uLcgsylrJu2DWmJOrPPY+ptKwFCC7h56zAd4NXMjTlOsUWL6XDZ/bL6G5dsenRThBrrH2v30QPMZRHZ/05AtHQ==	1552235401	3	0	0	\N	2019-03-10 16:29:37+00	f	python	0	\N	2
\.


--
-- Data for Name: sentry_groupemailthread; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_groupemailthread (id, email, project_id, group_id, msgid, date) FROM stdin;
1	andrew540i@yandex.ru	3	1	<20190310162226.20.30912@localhost>	2019-03-10 16:22:26.023257+00
2	andrew540i@yandex.ru	3	2	<20190310162937.20.5502@localhost>	2019-03-10 16:29:37.920973+00
\.


--
-- Data for Name: sentry_grouphash; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_grouphash (id, project_id, hash, group_id, state, group_tombstone_id) FROM stdin;
\.


--
-- Data for Name: sentry_grouplink; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_grouplink (id, group_id, project_id, linked_type, linked_id, relationship, data, datetime) FROM stdin;
\.


--
-- Data for Name: sentry_groupmeta; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_groupmeta (id, group_id, key, value) FROM stdin;
\.


--
-- Data for Name: sentry_groupredirect; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_groupredirect (id, group_id, previous_group_id) FROM stdin;
\.


--
-- Data for Name: sentry_grouprelease; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_grouprelease (id, project_id, group_id, release_id, environment, first_seen, last_seen) FROM stdin;
\.


--
-- Data for Name: sentry_groupresolution; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_groupresolution (id, group_id, release_id, datetime, status, type, actor_id) FROM stdin;
\.


--
-- Data for Name: sentry_grouprulestatus; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_grouprulestatus (id, project_id, rule_id, group_id, status, date_added, last_active) FROM stdin;
1	3	3	1	0	2019-03-10 16:22:24.7552+00	2019-03-10 16:22:24.759446+00
2	3	3	2	0	2019-03-10 16:29:37.586731+00	2019-03-10 16:29:37.589538+00
\.


--
-- Data for Name: sentry_groupseen; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_groupseen (id, project_id, group_id, user_id, last_seen) FROM stdin;
\.


--
-- Data for Name: sentry_groupshare; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_groupshare (id, project_id, group_id, uuid, user_id, date_added) FROM stdin;
\.


--
-- Data for Name: sentry_groupsnooze; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_groupsnooze (id, group_id, until, count, "window", user_count, user_window, state, actor_id) FROM stdin;
\.


--
-- Data for Name: sentry_groupsubscription; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_groupsubscription (id, project_id, group_id, user_id, is_active, reason, date_added) FROM stdin;
1	3	1	1	t	4	2019-03-10 16:22:33.361356+00
\.


--
-- Data for Name: sentry_grouptagkey; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_grouptagkey (id, project_id, group_id, key, values_seen) FROM stdin;
1	3	1	level	1
2	3	1	sentry:user	1
4	3	1	transaction	1
3	3	1	browser	1
5	3	1	url	1
6	3	1	os	1
7	3	1	server_name	1
8	3	1	device	1
9	3	2	transaction	1
10	3	2	url	1
12	3	2	sentry:user	1
13	3	2	os	1
14	3	2	level	1
15	3	2	browser	1
16	3	2	device	1
11	3	2	server_name	2
\.


--
-- Data for Name: sentry_grouptombstone; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_grouptombstone (id, previous_group_id, project_id, level, message, culprit, data, actor_id) FROM stdin;
\.


--
-- Data for Name: sentry_hipchat_ac_tenant; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_hipchat_ac_tenant (id, room_id, room_name, room_owner_id, room_owner_name, secret, homepage, token_url, capabilities_url, api_base_url, installed_from, auth_user_id) FROM stdin;
\.


--
-- Data for Name: sentry_hipchat_ac_tenant_organizations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_hipchat_ac_tenant_organizations (id, tenant_id, organization_id) FROM stdin;
\.


--
-- Data for Name: sentry_hipchat_ac_tenant_projects; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_hipchat_ac_tenant_projects (id, tenant_id, project_id) FROM stdin;
\.


--
-- Data for Name: sentry_identity; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_identity (id, idp_id, external_id, data, status, scopes, date_verified, date_added) FROM stdin;
\.


--
-- Data for Name: sentry_identityprovider; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_identityprovider (id, type, instance) FROM stdin;
\.


--
-- Data for Name: sentry_integration; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_integration (id, provider, external_id, name, metadata) FROM stdin;
\.


--
-- Data for Name: sentry_lostpasswordhash; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_lostpasswordhash (id, user_id, hash, date_added) FROM stdin;
\.


--
-- Data for Name: sentry_message; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_message (id, message, datetime, data, group_id, message_id, project_id, time_spent, platform) FROM stdin;
1	ZeroDivisionError: division by zero /user/info	2019-03-10 16:22:24+00	eJzTSCkw5ApWz8tPSY3PTFHnKjAC8px9Ipy0HXP9goIKI3MsEisMM/UrXBxtbYHSxlzFegCQfg7V	1	625230f2ca2f4d30a2d0fc0d0079eedc	3	\N	python
2	ZeroDivisionError: division by zero /user/info	2019-03-10 16:29:37+00	eJzTSCkw5ApWz8tPSY3PTFHnKjAC8goySp2DA4Mrgz3MTB3N/aNKcs0DAtJtbYHSxlzFegCZ4A8a	2	b89d2102db224d6e97bd24383e2e8219	3	\N	python
3	ZeroDivisionError: division by zero /user/info	2019-03-10 16:30:01+00	eJzTSCkw5ApWz8tPSY3PTFHnKjAC8gLdHJ1N07x9Ax3Nir1SCg3DTIOMi9NtbYHSxlzFegCKmA6X	2	85d96df4a9e34ee7aa1c55cb2dec022b	3	\N	python
\.


--
-- Data for Name: sentry_messagefiltervalue; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_messagefiltervalue (id, group_id, times_seen, key, value, project_id, last_seen, first_seen) FROM stdin;
1	1	1	transaction	/user/info	3	2019-03-10 16:22:24+00	2019-03-10 16:22:24.963597+00
2	1	1	server_name	d724bf472d79	3	2019-03-10 16:22:24+00	2019-03-10 16:22:24.968835+00
3	1	1	level	error	3	2019-03-10 16:22:24+00	2019-03-10 16:22:24.97483+00
4	1	1	url	http://localhost:8000/user/info	3	2019-03-10 16:22:24+00	2019-03-10 16:22:24.975237+00
5	1	1	sentry:user	id:1	3	2019-03-10 16:22:24+00	2019-03-10 16:22:24.98119+00
6	1	1	os	Linux	3	2019-03-10 16:22:24+00	2019-03-10 16:22:24.989116+00
7	1	1	browser	Chrome 72.0	3	2019-03-10 16:22:24+00	2019-03-10 16:22:24.994945+00
8	1	1	device	Other	3	2019-03-10 16:22:24+00	2019-03-10 16:22:26.613526+00
10	2	1	server_name	d724bf472d79	3	2019-03-10 16:29:37+00	2019-03-10 16:29:45.229755+00
9	2	2	transaction	/user/info	3	2019-03-10 16:30:01+00	2019-03-10 16:29:45.217773+00
13	2	2	level	error	3	2019-03-10 16:30:01+00	2019-03-10 16:29:45.241138+00
17	2	1	server_name	3f7904625de8	3	2019-03-10 16:30:01+00	2019-03-10 16:30:05.216101+00
11	2	2	url	http://localhost:8000/user/info	3	2019-03-10 16:30:01+00	2019-03-10 16:29:45.233329+00
12	2	2	sentry:user	id:1	3	2019-03-10 16:30:01+00	2019-03-10 16:29:45.240246+00
14	2	2	device	Other	3	2019-03-10 16:30:01+00	2019-03-10 16:29:45.246448+00
15	2	2	browser	Chrome 72.0	3	2019-03-10 16:30:01+00	2019-03-10 16:29:45.252172+00
16	2	2	os	Linux	3	2019-03-10 16:30:01+00	2019-03-10 16:29:45.255114+00
\.


--
-- Data for Name: sentry_messageindex; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_messageindex (id, object_id, "column", value) FROM stdin;
\.


--
-- Data for Name: sentry_minidumpfile; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_minidumpfile (id, file_id, event_id) FROM stdin;
\.


--
-- Data for Name: sentry_option; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_option (id, key, value, last_updated) FROM stdin;
3	mail.use-tls	gAKJLg==	2019-03-10 16:07:23.847544+00
4	mail.username	gAJYAAAAAC4=	2019-03-10 16:07:23.851538+00
5	mail.port	gAJLGS4=	2019-03-10 16:07:23.855057+00
6	system.admin-email	gAJYFAAAAGFuZHJldzU0MGlAeWFuZGV4LnJ1cQEu	2019-03-10 16:07:23.858161+00
7	mail.password	gAJYAAAAAC4=	2019-03-10 16:07:23.861918+00
8	system.url-prefix	gAJYGwAAAGh0dHA6Ly8xNzMuMjQ5LjQ1LjE3Nzo5MDAwL3EBLg==	2019-03-10 16:07:23.865523+00
9	auth.allow-registration	gAKJLg==	2019-03-10 16:07:23.868812+00
10	beacon.anonymous	gAKJLg==	2019-03-10 16:07:23.871732+00
11	mail.host	gAJYCQAAAGxvY2FsaG9zdHEBLg==	2019-03-10 16:07:23.874517+00
12	sentry:version-configured	gAJYBgAAADguMjIuMHEBLg==	2019-03-10 16:07:23.877634+00
1	sentry:last_worker_ping	gAJHQdchUE2iCU4u	2019-03-10 16:54:14.531997+00
2	sentry:last_worker_version	gAJYBgAAADguMjIuMHEBLg==	2019-03-10 16:54:14.544891+00
\.


--
-- Data for Name: sentry_organization; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_organization (id, name, status, date_added, slug, flags, default_role) FROM stdin;
1	Sentry	0	2019-03-10 15:59:49.158469+00	sentry	1	member
\.


--
-- Data for Name: sentry_organizationaccessrequest; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_organizationaccessrequest (id, team_id, member_id) FROM stdin;
\.


--
-- Data for Name: sentry_organizationavatar; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_organizationavatar (id, file_id, ident, organization_id, avatar_type) FROM stdin;
\.


--
-- Data for Name: sentry_organizationintegration; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_organizationintegration (id, organization_id, integration_id, config, default_auth_id) FROM stdin;
\.


--
-- Data for Name: sentry_organizationmember; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_organizationmember (id, organization_id, user_id, type, date_added, email, has_global_access, flags, role, token) FROM stdin;
1	1	1	50	2019-03-10 16:00:19.100258+00	\N	t	0	owner	\N
\.


--
-- Data for Name: sentry_organizationmember_teams; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_organizationmember_teams (id, organizationmember_id, team_id, is_active) FROM stdin;
1	1	1	t
\.


--
-- Data for Name: sentry_organizationonboardingtask; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_organizationonboardingtask (id, organization_id, user_id, task, status, date_completed, project_id, data) FROM stdin;
1	1	1	1	1	2019-03-10 16:07:41.128506+00	2	{}
3	1	1	4	2	2019-03-10 16:07:54.583725+00	3	{}
4	1	\N	2	1	2019-03-10 16:22:24+00	3	{"platform": "python"}
5	1	\N	5	1	2019-03-10 16:22:24.810422+00	3	{}
\.


--
-- Data for Name: sentry_organizationoptions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_organizationoptions (id, organization_id, key, value) FROM stdin;
\.


--
-- Data for Name: sentry_processingissue; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_processingissue (id, project_id, checksum, type, data, datetime) FROM stdin;
\.


--
-- Data for Name: sentry_project; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_project (id, name, public, date_added, status, slug, team_id, organization_id, first_event, forced_color, flags, platform) FROM stdin;
1	Internal	f	2019-03-10 15:59:49.163008+00	0	internal	1	1	\N	\N	0	\N
2	Django	f	2019-03-10 16:07:41.084328+00	2	django	1	1	\N	\N	0	python-django
3	backend	f	2019-03-10 16:07:54.542212+00	0	backend	1	1	2019-03-10 16:22:24+00	\N	0	python-django
4	app	f	2019-03-10 16:27:29.590414+00	0	app	1	1	\N	\N	0	
\.


--
-- Data for Name: sentry_projectbookmark; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_projectbookmark (id, project_id, user_id, date_added) FROM stdin;
\.


--
-- Data for Name: sentry_projectcounter; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_projectcounter (id, project_id, value) FROM stdin;
1	3	2
\.


--
-- Data for Name: sentry_projectdsymfile; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_projectdsymfile (id, file_id, object_name, cpu_name, project_id, uuid) FROM stdin;
\.


--
-- Data for Name: sentry_projectintegration; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_projectintegration (id, project_id, integration_id, config) FROM stdin;
\.


--
-- Data for Name: sentry_projectkey; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_projectkey (id, project_id, public_key, secret_key, date_added, roles, label, status, rate_limit_count, rate_limit_window) FROM stdin;
1	1	8546a98479b24fd28b304276c735c1c1	54c18e2f97734074a3500013ef60b378	2019-03-10 15:59:49.166049+00	1	Default	0	\N	\N
2	2	a7c921c2a94b429789fbbbbf564e8909	163d9bccf492455da674c7fe67e8fe8d	2019-03-10 16:07:41.101705+00	1	Default	0	\N	\N
3	3	242136359e14472fb9a654abe6abf862	3e7958a4c17845a6ab4402ec7b13d7ac	2019-03-10 16:07:54.552393+00	1	Default	0	\N	\N
4	4	1f3fb51d3ed6498bb7811b28052d719a	0e3d014d21124f05b84c978844dc6064	2019-03-10 16:27:29.596161+00	1	Default	0	\N	\N
\.


--
-- Data for Name: sentry_projectoptions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_projectoptions (id, project_id, key, value) FROM stdin;
1	1	sentry:origins	gAJdcQFVASphLg==
2	2	sentry:token	gAJYIAAAADI4ODhlNTY0NDM0ZjExZTlhNjA5MDI0MmFjMTMwMDA3cQEu
3	3	sentry:token	gAJYIAAAADMwZjBkMjVjNDM0ZjExZTk4MWM4MDI0MmFjMTMwMDA3cQEu
4	4	sentry:token	gAJYIAAAADZhMDNmZDU2NDM1MTExZTliM2ZiMDI0MmFjMTMwMDA3cQEu
\.


--
-- Data for Name: sentry_projectplatform; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_projectplatform (id, project_id, platform, date_added, last_seen) FROM stdin;
\.


--
-- Data for Name: sentry_projectsymcachefile; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_projectsymcachefile (id, project_id, cache_file_id, dsym_file_id, checksum, version) FROM stdin;
\.


--
-- Data for Name: sentry_projectteam; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_projectteam (id, project_id, team_id) FROM stdin;
1	1	1
2	2	1
3	3	1
4	4	1
\.


--
-- Data for Name: sentry_rawevent; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_rawevent (id, project_id, event_id, datetime, data) FROM stdin;
\.


--
-- Data for Name: sentry_release; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_release (id, project_id, version, date_added, date_released, ref, url, date_started, data, new_groups, owner_id, organization_id, commit_count, last_commit_id, authors, total_deploys, last_deploy_id) FROM stdin;
\.


--
-- Data for Name: sentry_release_project; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_release_project (id, project_id, release_id, new_groups) FROM stdin;
\.


--
-- Data for Name: sentry_releasecommit; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_releasecommit (id, project_id, release_id, commit_id, "order", organization_id) FROM stdin;
\.


--
-- Data for Name: sentry_releasefile; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_releasefile (id, project_id, release_id, file_id, ident, name, organization_id, dist_id) FROM stdin;
\.


--
-- Data for Name: sentry_releaseheadcommit; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_releaseheadcommit (id, organization_id, repository_id, release_id, commit_id) FROM stdin;
\.


--
-- Data for Name: sentry_repository; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_repository (id, organization_id, name, date_added, url, provider, external_id, config, status, integration_id) FROM stdin;
\.


--
-- Data for Name: sentry_reprocessingreport; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_reprocessingreport (id, project_id, event_id, datetime) FROM stdin;
\.


--
-- Data for Name: sentry_rule; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_rule (id, project_id, label, data, date_added, status) FROM stdin;
1	1	Send a notification for new issues	eJxlj80OAiEMhO99ETgRd/0/GqNHLzzAhgCrJAiEosm+vXRl48FbZzr9JuUmdSCZjsG44mJABqkH7tMauEmbunKmWts6oA0lTyK/vEXxOxCjy1gGtDYM9l0z4kqGrPpC8rwkK2YHqFAypZeqPVUdqOoI97+SlhMhFjdOjX6bxYw+6cbtVl/wUxX9IE0/Ke9p7AHFBx5HTWU=	2019-03-10 15:59:49.167658+00	0
2	2	Send a notification for new issues	eJxlj80OAiEMhO99ETgRd/0/GqNHLzzAhgCrJAiEosm+vXRl48FbZzr9JuUmdSCZjsG44mJABqkH7tMauEmbunKmWts6oA0lTyK/vEXxOxCjy1gGtDYM9l0z4kqGrPpC8rwkK2YHqFAypZeqPVUdqOoI97+SlhMhFjdOjX6bxYw+6cbtVl/wUxX9IE0/Ke9p7AHFBx5HTWU=	2019-03-10 16:07:41.105891+00	0
3	3	Send a notification for new issues	eJxlj80OAiEMhO99ETgRd/0/GqNHLzzAhgCrJAiEosm+vXRl48FbZzr9JuUmdSCZjsG44mJABqkH7tMauEmbunKmWts6oA0lTyK/vEXxOxCjy1gGtDYM9l0z4kqGrPpC8rwkK2YHqFAypZeqPVUdqOoI97+SlhMhFjdOjX6bxYw+6cbtVl/wUxX9IE0/Ke9p7AHFBx5HTWU=	2019-03-10 16:07:54.555851+00	0
4	4	Send a notification for new issues	eJxlj80OAiEMhO99ETgRd/0/GqNHLzzAhgCrJAiEosm+vXRl48FbZzr9JuUmdSCZjsG44mJABqkH7tMauEmbunKmWts6oA0lTyK/vEXxOxCjy1gGtDYM9l0z4kqGrPpC8rwkK2YHqFAypZeqPVUdqOoI97+SlhMhFjdOjX6bxYw+6cbtVl/wUxX9IE0/Ke9p7AHFBx5HTWU=	2019-03-10 16:27:29.600329+00	0
\.


--
-- Data for Name: sentry_savedsearch; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_savedsearch (id, project_id, name, query, date_added, is_default, owner_id) FROM stdin;
1	1	Unresolved Issues	is:unresolved	2019-03-10 15:59:49.168762+00	t	\N
2	1	Needs Triage	is:unresolved is:unassigned	2019-03-10 15:59:49.169991+00	f	\N
3	1	Assigned To Me	is:unresolved assigned:me	2019-03-10 15:59:49.170736+00	f	\N
4	1	My Bookmarks	is:unresolved bookmarks:me	2019-03-10 15:59:49.171906+00	f	\N
5	1	New Today	is:unresolved age:-24h	2019-03-10 15:59:49.172747+00	f	\N
6	2	Unresolved Issues	is:unresolved	2019-03-10 16:07:41.108157+00	t	\N
7	2	Needs Triage	is:unresolved is:unassigned	2019-03-10 16:07:41.110052+00	f	\N
8	2	Assigned To Me	is:unresolved assigned:me	2019-03-10 16:07:41.111318+00	f	\N
9	2	My Bookmarks	is:unresolved bookmarks:me	2019-03-10 16:07:41.112677+00	f	\N
10	2	New Today	is:unresolved age:-24h	2019-03-10 16:07:41.113844+00	f	\N
11	3	Unresolved Issues	is:unresolved	2019-03-10 16:07:54.558646+00	t	\N
12	3	Needs Triage	is:unresolved is:unassigned	2019-03-10 16:07:54.561156+00	f	\N
13	3	Assigned To Me	is:unresolved assigned:me	2019-03-10 16:07:54.562956+00	f	\N
14	3	My Bookmarks	is:unresolved bookmarks:me	2019-03-10 16:07:54.564715+00	f	\N
15	3	New Today	is:unresolved age:-24h	2019-03-10 16:07:54.566516+00	f	\N
16	4	Unresolved Issues	is:unresolved	2019-03-10 16:27:29.602774+00	t	\N
17	4	Needs Triage	is:unresolved is:unassigned	2019-03-10 16:27:29.604217+00	f	\N
18	4	Assigned To Me	is:unresolved assigned:me	2019-03-10 16:27:29.6053+00	f	\N
19	4	My Bookmarks	is:unresolved bookmarks:me	2019-03-10 16:27:29.606484+00	f	\N
20	4	New Today	is:unresolved age:-24h	2019-03-10 16:27:29.607666+00	f	\N
\.


--
-- Data for Name: sentry_savedsearch_userdefault; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_savedsearch_userdefault (id, savedsearch_id, project_id, user_id) FROM stdin;
\.


--
-- Data for Name: sentry_scheduleddeletion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_scheduleddeletion (id, guid, app_label, model_name, object_id, date_added, date_scheduled, actor_id, data, in_progress, aborted) FROM stdin;
\.


--
-- Data for Name: sentry_scheduledjob; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_scheduledjob (id, name, payload, date_added, date_scheduled) FROM stdin;
\.


--
-- Data for Name: sentry_team; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_team (id, slug, name, date_added, status, organization_id) FROM stdin;
1	sentry	Sentry	2019-03-10 15:59:49.161442+00	0	1
\.


--
-- Data for Name: sentry_useravatar; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_useravatar (id, user_id, file_id, ident, avatar_type) FROM stdin;
\.


--
-- Data for Name: sentry_useremail; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_useremail (id, user_id, email, validation_hash, date_hash_added, is_verified) FROM stdin;
1	1	andrew540i@yandex.ru	Le5uzczbhIJ6Yqei9Enj5RaMEwTJL9Bw	2019-03-10 16:00:19.094557+00	f
\.


--
-- Data for Name: sentry_useridentity; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_useridentity (id, user_id, identity_id, date_added) FROM stdin;
\.


--
-- Data for Name: sentry_userip; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_userip (id, user_id, ip_address, first_seen, last_seen) FROM stdin;
1	1	172.19.0.1	2019-03-10 16:06:28.397562+00	2019-03-10 16:53:56.807045+00
\.


--
-- Data for Name: sentry_useroption; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_useroption (id, user_id, project_id, key, value, organization_id) FROM stdin;
\.


--
-- Data for Name: sentry_userreport; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_userreport (id, project_id, group_id, event_id, name, email, comments, date_added, event_user_id) FROM stdin;
\.


--
-- Data for Name: sentry_versiondsymfile; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sentry_versiondsymfile (id, dsym_file_id, dsym_app_id, version, build, date_added) FROM stdin;
\.


--
-- Data for Name: social_auth_usersocialauth; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.social_auth_usersocialauth (id, user_id, provider, uid, extra_data) FROM stdin;
\.


--
-- Data for Name: south_migrationhistory; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.south_migrationhistory (id, app_name, migration, applied) FROM stdin;
1	sentry	0001_initial	2019-03-10 15:58:41.922731+00
2	sentry	0002_auto__del_field_groupedmessage_url__chg_field_groupedmessage_view__chg	2019-03-10 15:58:41.973533+00
3	sentry	0003_auto__add_field_message_group__del_field_groupedmessage_server_name	2019-03-10 15:58:41.986595+00
4	sentry	0004_auto__add_filtervalue__add_unique_filtervalue_key_value	2019-03-10 15:58:42.006639+00
5	sentry	0005_auto	2019-03-10 15:58:42.017139+00
6	sentry	0006_auto	2019-03-10 15:58:42.025375+00
7	sentry	0007_auto__add_field_message_site	2019-03-10 15:58:42.046234+00
8	sentry	0008_auto__chg_field_message_view__add_field_groupedmessage_data__chg_field	2019-03-10 15:58:42.101706+00
9	sentry	0009_auto__add_field_message_message_id	2019-03-10 15:58:42.117789+00
10	sentry	0010_auto__add_messageindex__add_unique_messageindex_column_value_object_id	2019-03-10 15:58:42.138227+00
11	sentry	0011_auto__add_field_groupedmessage_score	2019-03-10 15:58:42.191522+00
12	sentry	0012_auto	2019-03-10 15:58:42.209462+00
13	sentry	0013_auto__add_messagecountbyminute__add_unique_messagecountbyminute_group_	2019-03-10 15:58:42.267084+00
14	sentry	0014_auto	2019-03-10 15:58:42.300303+00
15	sentry	0014_auto__add_project__add_projectmember__add_unique_projectmember_project	2019-03-10 15:58:42.346439+00
16	sentry	0015_auto__add_field_message_project__add_field_messagecountbyminute_projec	2019-03-10 15:58:42.473716+00
17	sentry	0016_auto__add_field_projectmember_is_superuser	2019-03-10 15:58:42.527074+00
18	sentry	0017_auto__add_field_projectmember_api_key	2019-03-10 15:58:42.554475+00
19	sentry	0018_auto__chg_field_project_owner	2019-03-10 15:58:42.60498+00
20	sentry	0019_auto__del_field_projectmember_api_key__add_field_projectmember_public_	2019-03-10 15:58:42.636177+00
21	sentry	0020_auto__add_projectdomain__add_unique_projectdomain_project_domain	2019-03-10 15:58:42.665449+00
22	sentry	0021_auto__del_message__del_groupedmessage__del_unique_groupedmessage_proje	2019-03-10 15:58:42.685468+00
23	sentry	0022_auto__del_field_group_class_name__del_field_group_traceback__del_field	2019-03-10 15:58:42.708622+00
24	sentry	0023_auto__add_field_event_time_spent	2019-03-10 15:58:42.727441+00
25	sentry	0024_auto__add_field_group_time_spent_total__add_field_group_time_spent_cou	2019-03-10 15:58:42.823788+00
26	sentry	0025_auto__add_field_messagecountbyminute_time_spent_total__add_field_messa	2019-03-10 15:58:42.875725+00
27	sentry	0026_auto__add_field_project_status	2019-03-10 15:58:43.04522+00
28	sentry	0027_auto__chg_field_event_server_name	2019-03-10 15:58:43.08287+00
29	sentry	0028_auto__add_projectoptions__add_unique_projectoptions_project_key_value	2019-03-10 15:58:43.115275+00
30	sentry	0029_auto__del_field_projectmember_is_superuser__del_field_projectmember_pe	2019-03-10 15:58:43.164148+00
31	sentry	0030_auto__add_view__chg_field_event_group	2019-03-10 15:58:43.246981+00
32	sentry	0031_auto__add_field_view_verbose_name__add_field_view_verbose_name_plural_	2019-03-10 15:58:43.276718+00
33	sentry	0032_auto__add_eventmeta	2019-03-10 15:58:43.312894+00
34	sentry	0033_auto__add_option__add_unique_option_key_value	2019-03-10 15:58:43.342918+00
35	sentry	0034_auto__add_groupbookmark__add_unique_groupbookmark_project_user_group	2019-03-10 15:58:43.384481+00
36	sentry	0034_auto__add_unique_option_key__del_unique_option_value_key__del_unique_g	2019-03-10 15:58:43.433486+00
37	sentry	0036_auto__chg_field_option_value__chg_field_projectoption_value	2019-03-10 15:58:43.51012+00
38	sentry	0037_auto__add_unique_option_key__del_unique_option_value_key__del_unique_g	2019-03-10 15:58:43.556564+00
39	sentry	0038_auto__add_searchtoken__add_unique_searchtoken_document_field_token__ad	2019-03-10 15:58:43.621297+00
40	sentry	0039_auto__add_field_searchdocument_status	2019-03-10 15:58:43.680832+00
41	sentry	0040_auto__del_unique_event_event_id__add_unique_event_project_event_id	2019-03-10 15:58:43.734164+00
42	sentry	0041_auto__add_field_messagefiltervalue_last_seen__add_field_messagefilterv	2019-03-10 15:58:43.806578+00
43	sentry	0042_auto__add_projectcountbyminute__add_unique_projectcountbyminute_projec	2019-03-10 15:58:43.872426+00
44	sentry	0043_auto__chg_field_option_value__chg_field_projectoption_value	2019-03-10 15:58:43.930112+00
45	sentry	0044_auto__add_field_projectmember_is_active	2019-03-10 15:58:43.98364+00
46	sentry	0045_auto__add_pendingprojectmember__add_unique_pendingprojectmember_projec	2019-03-10 15:58:44.027204+00
47	sentry	0046_auto__add_teammember__add_unique_teammember_team_user__add_team__add_p	2019-03-10 15:58:44.16623+00
48	sentry	0047_migrate_project_slugs	2019-03-10 15:58:44.229417+00
49	sentry	0048_migrate_project_keys	2019-03-10 15:58:44.268382+00
50	sentry	0049_create_default_project_keys	2019-03-10 15:58:44.307892+00
51	sentry	0050_remove_project_keys_from_members	2019-03-10 15:58:44.34574+00
52	sentry	0051_auto__del_pendingprojectmember__del_unique_pendingprojectmember_projec	2019-03-10 15:58:44.401775+00
53	sentry	0052_migrate_project_members	2019-03-10 15:58:44.440995+00
54	sentry	0053_auto__del_projectmember__del_unique_projectmember_project_user	2019-03-10 15:58:44.484323+00
55	sentry	0054_fix_project_keys	2019-03-10 15:58:44.521358+00
56	sentry	0055_auto__del_projectdomain__del_unique_projectdomain_project_domain	2019-03-10 15:58:44.563188+00
57	sentry	0056_auto__add_field_group_resolved_at	2019-03-10 15:58:44.606115+00
58	sentry	0057_auto__add_field_group_active_at	2019-03-10 15:58:44.645324+00
59	sentry	0058_auto__add_useroption__add_unique_useroption_user_project_key	2019-03-10 15:58:44.699899+00
60	sentry	0059_auto__add_filterkey__add_unique_filterkey_project_key	2019-03-10 15:58:44.749901+00
61	sentry	0060_fill_filter_key	2019-03-10 15:58:44.797024+00
62	sentry	0061_auto__add_field_group_group_id__add_field_group_is_public	2019-03-10 15:58:44.880168+00
63	sentry	0062_correct_del_index_sentry_groupedmessage_logger__view__checksum	2019-03-10 15:58:44.934168+00
64	sentry	0063_auto	2019-03-10 15:58:44.975474+00
65	sentry	0064_index_checksum	2019-03-10 15:58:45.016754+00
66	sentry	0065_create_default_project_key	2019-03-10 15:58:45.204495+00
67	sentry	0066_auto__del_view	2019-03-10 15:58:45.246066+00
68	sentry	0067_auto__add_field_group_platform__add_field_event_platform	2019-03-10 15:58:45.284775+00
69	sentry	0068_auto__add_field_projectkey_user_added__add_field_projectkey_date_added	2019-03-10 15:58:45.35463+00
70	sentry	0069_auto__add_lostpasswordhash	2019-03-10 15:58:45.419426+00
71	sentry	0070_projectoption_key_length	2019-03-10 15:58:45.472614+00
72	sentry	0071_auto__add_field_group_users_seen	2019-03-10 15:58:45.571144+00
73	sentry	0072_auto__add_affecteduserbygroup__add_unique_affecteduserbygroup_project_	2019-03-10 15:58:45.664636+00
74	sentry	0073_auto__add_field_project_platform	2019-03-10 15:58:45.706394+00
75	sentry	0074_correct_filtervalue_index	2019-03-10 15:58:45.755861+00
76	sentry	0075_add_groupbookmark_index	2019-03-10 15:58:45.800228+00
77	sentry	0076_add_groupmeta_index	2019-03-10 15:58:45.85274+00
78	sentry	0077_auto__add_trackeduser__add_unique_trackeduser_project_ident	2019-03-10 15:58:45.922084+00
79	sentry	0078_auto__add_field_affecteduserbygroup_tuser	2019-03-10 15:58:45.971192+00
80	sentry	0079_auto__del_unique_affecteduserbygroup_project_ident_group__add_unique_a	2019-03-10 15:58:46.048691+00
81	sentry	0080_auto__chg_field_affecteduserbygroup_ident	2019-03-10 15:58:46.129737+00
82	sentry	0081_fill_trackeduser	2019-03-10 15:58:46.172963+00
83	sentry	0082_auto__add_activity__add_field_group_num_comments__add_field_event_num_	2019-03-10 15:58:46.400176+00
84	sentry	0083_migrate_dupe_groups	2019-03-10 15:58:46.476112+00
85	sentry	0084_auto__del_unique_group_project_checksum_logger_culprit__add_unique_gro	2019-03-10 15:58:46.536889+00
86	sentry	0085_auto__del_unique_project_slug__add_unique_project_slug_team	2019-03-10 15:58:46.59668+00
87	sentry	0086_auto__add_field_team_date_added	2019-03-10 15:58:46.656522+00
88	sentry	0087_auto__del_messagefiltervalue__del_unique_messagefiltervalue_project_ke	2019-03-10 15:58:46.703019+00
89	sentry	0088_auto__del_messagecountbyminute__del_unique_messagecountbyminute_projec	2019-03-10 15:58:46.750636+00
90	sentry	0089_auto__add_accessgroup__add_unique_accessgroup_team_name	2019-03-10 15:58:46.863356+00
91	sentry	0090_auto__add_grouptagkey__add_unique_grouptagkey_project_group_key__add_f	2019-03-10 15:58:47.009664+00
92	sentry	0091_auto__add_alert	2019-03-10 15:58:47.103414+00
93	sentry	0092_auto__add_alertrelatedgroup__add_unique_alertrelatedgroup_group_alert	2019-03-10 15:58:47.181097+00
94	sentry	0093_auto__add_field_alert_status	2019-03-10 15:58:47.255053+00
95	sentry	0094_auto__add_eventmapping__add_unique_eventmapping_project_event_id	2019-03-10 15:58:47.329079+00
96	sentry	0095_rebase	2019-03-10 15:58:47.386969+00
97	sentry	0096_auto__add_field_tagvalue_data	2019-03-10 15:58:47.449088+00
98	sentry	0097_auto__del_affecteduserbygroup__del_unique_affecteduserbygroup_project_	2019-03-10 15:58:47.527134+00
99	sentry	0098_auto__add_user__chg_field_team_owner__chg_field_activity_user__chg_fie	2019-03-10 15:58:47.592101+00
100	sentry	0099_auto__del_field_teammember_is_active	2019-03-10 15:58:47.861149+00
101	sentry	0100_auto__add_field_tagkey_label	2019-03-10 15:58:47.931589+00
102	sentry	0101_ensure_teams	2019-03-10 15:58:47.993937+00
103	sentry	0102_ensure_slugs	2019-03-10 15:58:48.06118+00
104	sentry	0103_ensure_non_empty_slugs	2019-03-10 15:58:48.125355+00
105	sentry	0104_auto__add_groupseen__add_unique_groupseen_group_user	2019-03-10 15:58:48.210442+00
106	sentry	0105_auto__chg_field_projectcountbyminute_time_spent_total__chg_field_group	2019-03-10 15:58:48.477181+00
107	sentry	0106_auto__del_searchtoken__del_unique_searchtoken_document_field_token__de	2019-03-10 15:58:48.541709+00
108	sentry	0107_expand_user	2019-03-10 15:58:48.624953+00
109	sentry	0108_fix_user	2019-03-10 15:58:48.685312+00
110	sentry	0109_index_filtervalue_times_seen	2019-03-10 15:58:48.74474+00
111	sentry	0110_index_filtervalue_last_seen	2019-03-10 15:58:48.803666+00
112	sentry	0111_index_filtervalue_first_seen	2019-03-10 15:58:48.859409+00
113	sentry	0112_auto__chg_field_option_value__chg_field_useroption_value__chg_field_pr	2019-03-10 15:58:48.910807+00
114	sentry	0113_auto__add_field_team_status	2019-03-10 15:58:48.975586+00
115	sentry	0114_auto__add_field_projectkey_roles	2019-03-10 15:58:49.050887+00
116	sentry	0115_auto__del_projectcountbyminute__del_unique_projectcountbyminute_projec	2019-03-10 15:58:49.111768+00
117	sentry	0116_auto__del_field_event_server_name__del_field_event_culprit__del_field_	2019-03-10 15:58:49.164495+00
118	sentry	0117_auto__add_rule	2019-03-10 15:58:49.227219+00
119	sentry	0118_create_default_rules	2019-03-10 15:58:49.278619+00
120	sentry	0119_auto__add_field_projectkey_label	2019-03-10 15:58:49.327997+00
121	sentry	0120_auto__add_grouprulestatus	2019-03-10 15:58:49.401271+00
122	sentry	0121_auto__add_unique_grouprulestatus_rule_group	2019-03-10 15:58:49.461607+00
123	sentry	0122_add_event_group_id_datetime_index	2019-03-10 15:58:49.516417+00
124	sentry	0123_auto__add_groupassignee__add_index_event_group_datetime	2019-03-10 15:58:49.590566+00
125	sentry	0124_auto__add_grouphash__add_unique_grouphash_project_hash	2019-03-10 15:58:49.695848+00
126	sentry	0125_auto__add_field_user_is_managed	2019-03-10 15:58:49.762787+00
127	sentry	0126_auto__add_field_option_last_updated	2019-03-10 15:58:49.829937+00
128	sentry	0127_auto__add_release__add_unique_release_project_version	2019-03-10 15:58:49.915169+00
129	sentry	0128_auto__add_broadcast	2019-03-10 15:58:49.984438+00
130	sentry	0129_auto__chg_field_release_id__chg_field_pendingteammember_id__chg_field_	2019-03-10 15:58:50.057663+00
131	sentry	0130_auto__del_field_project_owner	2019-03-10 15:58:50.12066+00
132	sentry	0131_auto__add_organizationmember__add_unique_organizationmember_organizati	2019-03-10 15:58:50.224454+00
133	sentry	0132_add_default_orgs	2019-03-10 15:58:50.310026+00
134	sentry	0133_add_org_members	2019-03-10 15:58:50.375876+00
135	sentry	0134_auto__chg_field_team_organization	2019-03-10 15:58:50.46129+00
136	sentry	0135_auto__chg_field_project_team	2019-03-10 15:58:50.801595+00
137	sentry	0136_auto__add_field_organizationmember_email__chg_field_organizationmember	2019-03-10 15:58:50.895778+00
138	sentry	0137_auto__add_field_organizationmember_has_global_access	2019-03-10 15:58:51.003617+00
139	sentry	0138_migrate_team_members	2019-03-10 15:58:51.074064+00
140	sentry	0139_auto__add_auditlogentry	2019-03-10 15:58:51.169537+00
141	sentry	0140_auto__add_field_organization_slug	2019-03-10 15:58:51.256067+00
142	sentry	0141_fill_org_slugs	2019-03-10 15:58:51.325395+00
143	sentry	0142_auto__add_field_project_organization__add_unique_project_organization_	2019-03-10 15:58:51.401991+00
144	sentry	0143_fill_project_orgs	2019-03-10 15:58:51.470392+00
145	sentry	0144_auto__chg_field_project_organization	2019-03-10 15:58:51.561193+00
146	sentry	0145_auto__chg_field_organization_slug	2019-03-10 15:58:51.650384+00
147	sentry	0146_auto__add_field_auditlogentry_ip_address	2019-03-10 15:58:51.720526+00
148	sentry	0147_auto__del_unique_team_slug__add_unique_team_organization_slug	2019-03-10 15:58:51.805759+00
149	sentry	0148_auto__add_helppage	2019-03-10 15:58:51.893687+00
150	sentry	0149_auto__chg_field_groupseen_project__chg_field_groupseen_user__chg_field	2019-03-10 15:58:51.972758+00
151	sentry	0150_fix_broken_rules	2019-03-10 15:58:52.048635+00
152	sentry	0151_auto__add_file	2019-03-10 15:58:52.144734+00
153	sentry	0152_auto__add_field_file_checksum__chg_field_file_name__add_unique_file_na	2019-03-10 15:58:52.248714+00
154	sentry	0153_auto__add_field_grouprulestatus_last_active	2019-03-10 15:58:52.324773+00
155	sentry	0154_auto__add_field_tagkey_status	2019-03-10 15:58:52.410817+00
156	sentry	0155_auto__add_field_projectkey_status	2019-03-10 15:58:52.513514+00
157	sentry	0156_auto__add_apikey	2019-03-10 15:58:52.617916+00
158	sentry	0157_auto__add_authidentity__add_unique_authidentity_auth_provider_ident__a	2019-03-10 15:58:52.730513+00
159	sentry	0158_auto__add_unique_authidentity_auth_provider_user	2019-03-10 15:58:52.820174+00
160	sentry	0159_auto__add_field_authidentity_last_verified__add_field_organizationmemb	2019-03-10 15:58:52.934692+00
161	sentry	0160_auto__add_field_authprovider_default_global_access	2019-03-10 15:58:53.057626+00
162	sentry	0161_auto__chg_field_authprovider_config	2019-03-10 15:58:53.167196+00
163	sentry	0162_auto__chg_field_authidentity_data	2019-03-10 15:58:53.27663+00
164	sentry	0163_auto__add_field_authidentity_last_synced	2019-03-10 15:58:53.380233+00
165	sentry	0164_auto__add_releasefile__add_unique_releasefile_release_ident__add_field	2019-03-10 15:58:53.558766+00
166	sentry	0165_auto__del_unique_file_name_checksum	2019-03-10 15:58:53.656871+00
167	sentry	0166_auto__chg_field_user_id__add_field_apikey_allowed_origins	2019-03-10 15:58:53.762206+00
168	sentry	0167_auto__add_field_authprovider_flags	2019-03-10 15:58:53.871872+00
169	sentry	0168_unfill_projectkey_user	2019-03-10 15:58:53.969227+00
170	sentry	0169_auto__del_field_projectkey_user	2019-03-10 15:58:54.38692+00
171	sentry	0170_auto__add_organizationmemberteam__add_unique_organizationmemberteam_te	2019-03-10 15:58:54.533831+00
172	sentry	0171_auto__chg_field_team_owner	2019-03-10 15:58:54.673503+00
173	sentry	0172_auto__del_field_team_owner	2019-03-10 15:58:54.770895+00
174	sentry	0173_auto__del_teammember__del_unique_teammember_team_user	2019-03-10 15:58:54.87652+00
175	sentry	0174_auto__del_field_projectkey_user_added	2019-03-10 15:58:54.970047+00
176	sentry	0175_auto__del_pendingteammember__del_unique_pendingteammember_team_email	2019-03-10 15:58:55.071968+00
177	sentry	0176_auto__add_field_organizationmember_counter__add_unique_organizationmem	2019-03-10 15:58:55.16953+00
178	sentry	0177_fill_member_counters	2019-03-10 15:58:55.260585+00
179	sentry	0178_auto__del_unique_organizationmember_organization_counter	2019-03-10 15:58:55.361733+00
180	sentry	0179_auto__add_field_release_date_released	2019-03-10 15:58:55.451695+00
181	sentry	0180_auto__add_field_release_environment__add_field_release_ref__add_field_	2019-03-10 15:58:55.595238+00
182	sentry	0181_auto__del_field_release_environment__del_unique_release_project_versio	2019-03-10 15:58:55.718746+00
183	sentry	0182_auto__add_field_auditlogentry_actor_label__add_field_auditlogentry_act	2019-03-10 15:58:55.84134+00
184	sentry	0183_auto__del_index_grouphash_hash	2019-03-10 15:58:55.932945+00
185	sentry	0184_auto__del_field_group_checksum__del_unique_group_project_checksum__del	2019-03-10 15:58:56.039563+00
186	sentry	0185_auto__add_savedsearch__add_unique_savedsearch_project_name	2019-03-10 15:58:56.155126+00
187	sentry	0186_auto__add_field_group_first_release	2019-03-10 15:58:56.258381+00
188	sentry	0187_auto__add_index_group_project_first_release	2019-03-10 15:58:56.363329+00
189	sentry	0188_auto__add_userreport	2019-03-10 15:58:56.477102+00
190	sentry	0189_auto__add_index_userreport_project_event_id	2019-03-10 15:58:56.58528+00
191	sentry	0190_auto__add_field_release_new_groups	2019-03-10 15:58:56.699017+00
192	sentry	0191_auto__del_alert__del_alertrelatedgroup__del_unique_alertrelatedgroup_g	2019-03-10 15:58:56.810845+00
193	sentry	0192_add_model_groupemailthread	2019-03-10 15:58:56.936141+00
194	sentry	0193_auto__del_unique_groupemailthread_msgid__add_unique_groupemailthread_e	2019-03-10 15:58:57.05435+00
195	sentry	0194_auto__del_field_project_platform	2019-03-10 15:58:57.158002+00
196	sentry	0195_auto__chg_field_organization_owner	2019-03-10 15:58:57.281343+00
197	sentry	0196_auto__del_field_organization_owner	2019-03-10 15:58:57.382755+00
198	sentry	0197_auto__del_accessgroup__del_unique_accessgroup_team_name	2019-03-10 15:58:57.492621+00
199	sentry	0198_auto__add_field_release_primary_owner	2019-03-10 15:58:57.595957+00
200	sentry	0199_auto__add_field_project_first_event	2019-03-10 15:58:57.69143+00
201	sentry	0200_backfill_first_event	2019-03-10 15:58:57.787894+00
202	sentry	0201_auto__add_eventuser__add_unique_eventuser_project_ident__add_index_eve	2019-03-10 15:58:57.92104+00
203	sentry	0202_auto__add_field_eventuser_hash__add_unique_eventuser_project_hash	2019-03-10 15:58:58.05305+00
204	sentry	0203_auto__chg_field_eventuser_username__chg_field_eventuser_ident	2019-03-10 15:58:58.192912+00
205	sentry	0204_backfill_team_membership	2019-03-10 15:58:58.28986+00
206	sentry	0205_auto__add_field_organizationmember_role	2019-03-10 15:58:58.424749+00
207	sentry	0206_backfill_member_role	2019-03-10 15:58:58.531724+00
208	sentry	0207_auto__add_field_organization_default_role	2019-03-10 15:58:58.650305+00
209	sentry	0208_backfill_default_role	2019-03-10 15:58:58.761845+00
210	sentry	0209_auto__add_broadcastseen__add_unique_broadcastseen_broadcast_user	2019-03-10 15:58:59.305677+00
211	sentry	0210_auto__del_field_broadcast_badge	2019-03-10 15:58:59.416943+00
212	sentry	0211_auto__add_field_broadcast_title	2019-03-10 15:58:59.534534+00
213	sentry	0212_auto__add_fileblob__add_field_file_blob	2019-03-10 15:58:59.671219+00
214	sentry	0212_auto__add_organizationoption__add_unique_organizationoption_organizati	2019-03-10 15:58:59.795673+00
215	sentry	0213_migrate_file_blobs	2019-03-10 15:58:59.910513+00
216	sentry	0214_auto__add_field_broadcast_upstream_id	2019-03-10 15:59:00.030499+00
217	sentry	0215_auto__add_field_broadcast_date_expires	2019-03-10 15:59:00.162598+00
218	sentry	0216_auto__add_groupsnooze	2019-03-10 15:59:00.292236+00
219	sentry	0217_auto__add_groupresolution	2019-03-10 15:59:00.435703+00
220	sentry	0218_auto__add_field_groupresolution_status	2019-03-10 15:59:00.582212+00
221	sentry	0219_auto__add_field_groupbookmark_date_added	2019-03-10 15:59:00.722748+00
222	sentry	0220_auto__del_field_fileblob_storage_options__del_field_fileblob_storage__	2019-03-10 15:59:00.850854+00
223	sentry	0221_auto__chg_field_user_first_name	2019-03-10 15:59:01.000967+00
224	sentry	0222_auto__del_field_user_last_name__del_field_user_first_name__add_field_u	2019-03-10 15:59:01.123056+00
225	sentry	0223_delete_old_sentry_docs_options	2019-03-10 15:59:01.243816+00
226	sentry	0224_auto__add_index_userreport_project_date_added	2019-03-10 15:59:01.368643+00
227	sentry	0225_auto__add_fileblobindex__add_unique_fileblobindex_file_blob_offset	2019-03-10 15:59:01.510168+00
228	sentry	0226_backfill_file_size	2019-03-10 15:59:01.636798+00
229	sentry	0227_auto__del_field_activity_event	2019-03-10 15:59:01.762972+00
230	sentry	0228_auto__del_field_event_num_comments	2019-03-10 15:59:01.889406+00
231	sentry	0229_drop_event_constraints	2019-03-10 15:59:02.065806+00
232	sentry	0230_auto__del_field_eventmapping_group__del_field_eventmapping_project__ad	2019-03-10 15:59:02.189316+00
233	sentry	0231_auto__add_field_savedsearch_is_default	2019-03-10 15:59:02.325847+00
234	sentry	0232_default_savedsearch	2019-03-10 15:59:02.447648+00
235	sentry	0233_add_new_savedsearch	2019-03-10 15:59:02.572587+00
236	sentry	0234_auto__add_savedsearchuserdefault__add_unique_savedsearchuserdefault_pr	2019-03-10 15:59:02.724243+00
237	sentry	0235_auto__add_projectbookmark__add_unique_projectbookmark_project_id_user_	2019-03-10 15:59:02.873208+00
238	sentry	0236_auto__add_organizationonboardingtask__add_unique_organizationonboardin	2019-03-10 15:59:03.031593+00
239	sentry	0237_auto__add_eventtag__add_unique_eventtag_event_id_key_id_value_id	2019-03-10 15:59:03.183829+00
240	sentry	0238_fill_org_onboarding_tasks	2019-03-10 15:59:03.326794+00
241	sentry	0239_auto__add_projectdsymfile__add_unique_projectdsymfile_project_uuid__ad	2019-03-10 15:59:03.512935+00
242	sentry	0240_fill_onboarding_option	2019-03-10 15:59:03.660796+00
243	sentry	0241_auto__add_counter__add_unique_counter_project_ident__add_field_group_s	2019-03-10 15:59:03.832073+00
244	sentry	0242_auto__add_field_project_forced_color	2019-03-10 15:59:03.990524+00
245	sentry	0243_remove_inactive_members	2019-03-10 15:59:04.141538+00
246	sentry	0244_auto__add_groupredirect	2019-03-10 15:59:04.308782+00
247	sentry	0245_auto__del_field_project_callsign__del_unique_project_organization_call	2019-03-10 15:59:04.466466+00
248	sentry	0246_auto__add_dsymsymbol__add_unique_dsymsymbol_object_address__add_dsymsd	2019-03-10 15:59:04.72939+00
249	sentry	0247_migrate_file_blobs	2019-03-10 15:59:04.921621+00
250	sentry	0248_auto__add_projectplatform__add_unique_projectplatform_project_id_platf	2019-03-10 15:59:05.107594+00
251	sentry	0249_auto__add_index_eventtag_project_id_key_id_value_id	2019-03-10 15:59:05.291509+00
252	sentry	0250_auto__add_unique_userreport_project_event_id	2019-03-10 15:59:05.996119+00
253	sentry	0251_auto__add_useravatar	2019-03-10 15:59:06.223342+00
254	sentry	0252_default_users_to_gravatar	2019-03-10 15:59:06.501352+00
255	sentry	0253_auto__add_field_eventtag_group_id	2019-03-10 15:59:06.78837+00
256	sentry	0254_auto__add_index_eventtag_group_id_key_id_value_id	2019-03-10 15:59:07.020899+00
257	sentry	0255_auto__add_apitoken	2019-03-10 15:59:07.270667+00
258	sentry	0256_auto__add_authenticator	2019-03-10 15:59:07.512125+00
259	sentry	0257_repair_activity	2019-03-10 15:59:07.708445+00
260	sentry	0258_auto__add_field_user_is_password_expired__add_field_user_last_password	2019-03-10 15:59:07.942609+00
261	sentry	0259_auto__add_useremail__add_unique_useremail_user_email	2019-03-10 15:59:08.159629+00
262	sentry	0260_populate_email_addresses	2019-03-10 15:59:08.364715+00
263	sentry	0261_auto__add_groupsubscription__add_unique_groupsubscription_group_user	2019-03-10 15:59:08.602834+00
264	sentry	0262_fix_tag_indexes	2019-03-10 15:59:08.840014+00
265	sentry	0263_remove_default_regression_rule	2019-03-10 15:59:09.062808+00
266	sentry	0264_drop_grouptagvalue_project_index	2019-03-10 15:59:09.281493+00
267	sentry	0265_auto__add_field_rule_status	2019-03-10 15:59:09.531487+00
268	sentry	0266_auto__add_grouprelease__add_unique_grouprelease_group_id_release_id_en	2019-03-10 15:59:09.769976+00
269	sentry	0267_auto__add_environment__add_unique_environment_project_id_name__add_rel	2019-03-10 15:59:10.025506+00
270	sentry	0268_fill_environment	2019-03-10 15:59:10.290544+00
271	sentry	0269_auto__del_helppage	2019-03-10 15:59:10.536388+00
272	sentry	0270_auto__add_field_organizationmember_token	2019-03-10 15:59:10.792598+00
273	sentry	0271_auto__del_field_organizationmember_counter	2019-03-10 15:59:11.024345+00
274	sentry	0272_auto__add_unique_authenticator_user_type	2019-03-10 15:59:11.365743+00
275	sentry	0273_auto__add_repository__add_unique_repository_organization_id_name__add_	2019-03-10 15:59:11.732546+00
276	sentry	0274_auto__add_index_commit_repository_id_date_added	2019-03-10 15:59:12.010239+00
277	sentry	0275_auto__del_index_grouptagvalue_project_key_value__add_index_grouptagval	2019-03-10 15:59:12.29374+00
278	sentry	0276_auto__add_field_user_session_nonce	2019-03-10 15:59:12.585241+00
279	sentry	0277_auto__add_commitfilechange__add_unique_commitfilechange_commit_filenam	2019-03-10 15:59:12.931024+00
280	sentry	0278_auto__add_releaseproject__add_unique_releaseproject_project_release__a	2019-03-10 15:59:13.240673+00
281	sentry	0279_populate_release_orgs_and_projects	2019-03-10 15:59:13.517677+00
282	sentry	0280_auto__add_field_releasecommit_organization_id	2019-03-10 15:59:13.790228+00
283	sentry	0281_populate_release_commit_organization_id	2019-03-10 15:59:14.059814+00
284	sentry	0282_auto__add_field_releasefile_organization__add_field_releaseenvironment	2019-03-10 15:59:14.334119+00
285	sentry	0283_populate_release_environment_and_release_file_organization	2019-03-10 15:59:14.600676+00
286	sentry	0284_auto__del_field_release_project__add_field_release_project_id__chg_fie	2019-03-10 15:59:15.017738+00
287	sentry	0285_auto__chg_field_release_project_id__chg_field_releasefile_project_id	2019-03-10 15:59:15.288348+00
288	sentry	0286_drop_project_fk_release_release_file	2019-03-10 15:59:15.570181+00
289	sentry	0287_auto__add_field_releaseproject_new_groups	2019-03-10 15:59:15.841176+00
290	sentry	0288_set_release_project_new_groups_to_zero	2019-03-10 15:59:16.108789+00
291	sentry	0289_auto__add_organizationavatar	2019-03-10 15:59:16.403742+00
292	sentry	0290_populate_release_project_new_groups	2019-03-10 15:59:17.348684+00
293	sentry	0291_merge_legacy_releases	2019-03-10 15:59:17.626758+00
294	sentry	0292_auto__add_unique_release_organization_version	2019-03-10 15:59:17.903287+00
295	sentry	0293_auto__del_unique_release_project_id_version	2019-03-10 15:59:18.190026+00
296	sentry	0294_auto__add_groupcommitresolution__add_unique_groupcommitresolution_grou	2019-03-10 15:59:18.47489+00
297	sentry	0295_auto__add_environmentproject__add_unique_environmentproject_project_en	2019-03-10 15:59:18.773962+00
298	sentry	0296_populate_environment_organization_and_projects	2019-03-10 15:59:19.060465+00
299	sentry	0297_auto__add_field_project_flags	2019-03-10 15:59:19.371833+00
300	sentry	0298_backfill_project_has_releases	2019-03-10 15:59:19.664356+00
301	sentry	0299_auto__chg_field_environment_organization_id	2019-03-10 15:59:19.980601+00
302	sentry	0300_auto__add_processingissue__add_unique_processingissue_project_checksum	2019-03-10 15:59:20.351899+00
303	sentry	0301_auto__chg_field_environment_project_id__chg_field_releaseenvironment_p	2019-03-10 15:59:20.67456+00
304	sentry	0302_merge_environments	2019-03-10 15:59:20.987748+00
305	sentry	0303_fix_release_new_group_counts	2019-03-10 15:59:21.300946+00
306	sentry	0304_auto__add_deploy	2019-03-10 15:59:21.635869+00
307	sentry	0305_auto__chg_field_authidentity_data__chg_field_useroption_value__chg_fie	2019-03-10 15:59:21.95707+00
308	sentry	0306_auto__add_apigrant__add_apiauthorization__add_unique_apiauthorization_	2019-03-10 15:59:22.396012+00
309	sentry	0307_auto__add_field_apigrant_scope_list__add_field_apitoken_scope_list__ad	2019-03-10 15:59:22.775411+00
310	sentry	0308_auto__add_versiondsymfile__add_unique_versiondsymfile_dsym_file_versio	2019-03-10 15:59:23.161809+00
311	sentry	0308_backfill_scopes_list	2019-03-10 15:59:23.52766+00
312	sentry	0309_fix_application_state	2019-03-10 15:59:23.877273+00
313	sentry	0310_auto__add_field_savedsearch_owner	2019-03-10 15:59:24.237204+00
314	sentry	0311_auto__add_releaseheadcommit__add_unique_releaseheadcommit_repository_i	2019-03-10 15:59:24.632119+00
315	sentry	0312_create_missing_emails	2019-03-10 15:59:25.012773+00
316	sentry	0313_auto__add_field_commitauthor_external_id__add_unique_commitauthor_orga	2019-03-10 15:59:25.386816+00
317	sentry	0314_auto__add_distribution__add_unique_distribution_release_name__add_fiel	2019-03-10 15:59:25.778113+00
318	sentry	0315_auto__add_field_useroption_organization__add_unique_useroption_user_or	2019-03-10 15:59:26.16637+00
319	sentry	0316_auto__del_field_grouptagvalue_project__del_field_grouptagvalue_group__	2019-03-10 15:59:26.539596+00
320	sentry	0317_drop_grouptagvalue_constraints	2019-03-10 15:59:26.949336+00
321	sentry	0318_auto__add_field_deploy_notified	2019-03-10 15:59:27.325636+00
322	sentry	0319_auto__add_index_deploy_notified	2019-03-10 15:59:27.710142+00
323	sentry	0320_auto__add_index_eventtag_date_added	2019-03-10 15:59:28.097387+00
324	sentry	0321_auto__add_field_projectkey_rate_limit_count__add_field_projectkey_rate	2019-03-10 15:59:28.481018+00
325	sentry	0321_auto__add_unique_environment_organization_id_name	2019-03-10 15:59:28.86917+00
326	sentry	0322_merge_0321_migrations	2019-03-10 15:59:29.248252+00
327	sentry	0323_auto__add_unique_releaseenvironment_organization_id_release_id_environ	2019-03-10 15:59:29.637606+00
328	sentry	0324_auto__add_field_eventuser_name__add_field_userreport_event_user_id	2019-03-10 15:59:30.025959+00
329	sentry	0325_auto__add_scheduleddeletion__add_unique_scheduleddeletion_app_label_mo	2019-03-10 15:59:30.437127+00
330	sentry	0326_auto__add_field_groupsnooze_count__add_field_groupsnooze_window__add_f	2019-03-10 15:59:30.876643+00
331	sentry	0327_auto__add_field_release_commit_count__add_field_release_last_commit_id	2019-03-10 15:59:31.278111+00
332	sentry	0328_backfill_release_stats	2019-03-10 15:59:32.49566+00
333	sentry	0329_auto__del_dsymsymbol__del_unique_dsymsymbol_object_address__del_global	2019-03-10 15:59:32.882903+00
334	sentry	0330_auto__add_field_grouphash_state	2019-03-10 15:59:33.256439+00
335	sentry	0331_auto__del_index_releasecommit_project_id__del_index_releaseenvironment	2019-03-10 15:59:33.652868+00
336	sentry	0332_auto__add_featureadoption__add_unique_featureadoption_organization_fea	2019-03-10 15:59:34.046672+00
337	sentry	0333_auto__add_field_groupresolution_type__add_field_groupresolution_actor_	2019-03-10 15:59:34.43301+00
338	sentry	0334_auto__add_field_project_platform	2019-03-10 15:59:34.820458+00
339	sentry	0334_auto__add_scheduledjob	2019-03-10 15:59:35.223434+00
340	sentry	0335_auto__add_field_groupsnooze_actor_id	2019-03-10 15:59:35.614339+00
341	sentry	0336_auto__add_field_user_last_active	2019-03-10 15:59:36.011268+00
342	sentry	0337_fix_out_of_order_migrations	2019-03-10 15:59:36.410974+00
343	sentry	0338_fix_null_user_last_active	2019-03-10 15:59:36.801845+00
344	sentry	0339_backfill_first_project_feature	2019-03-10 15:59:37.190834+00
345	sentry	0340_auto__add_grouptombstone__add_field_grouphash_group_tombstone_id	2019-03-10 15:59:37.621339+00
346	sentry	0341_auto__add_organizationintegration__add_unique_organizationintegration_	2019-03-10 15:59:38.127423+00
347	sentry	0342_projectplatform	2019-03-10 15:59:38.574699+00
348	sentry	0343_auto__add_index_groupcommitresolution_commit_id	2019-03-10 15:59:39.019827+00
349	sentry	0344_add_index_ProjectPlatform_last_seen	2019-03-10 15:59:39.457721+00
350	sentry	0345_add_citext	2019-03-10 15:59:39.89263+00
351	sentry	0346_auto__del_field_tagkey_project__add_field_tagkey_project_id__del_uniqu	2019-03-10 15:59:40.32521+00
352	sentry	0347_auto__add_index_grouptagvalue_project_id__add_index_grouptagvalue_grou	2019-03-10 15:59:40.761743+00
353	sentry	0348_fix_project_key_rate_limit_window_unit	2019-03-10 15:59:41.195765+00
354	sentry	0349_drop_constraints_filterkey_filtervalue_grouptagkey	2019-03-10 15:59:41.695381+00
355	sentry	0350_auto__add_email	2019-03-10 15:59:42.139715+00
356	sentry	0351_backfillemail	2019-03-10 15:59:42.581001+00
357	sentry	0352_add_index_release_coalesce_date_released_date_added	2019-03-10 15:59:43.025879+00
358	sentry	0353_auto__del_field_eventuser_project__add_field_eventuser_project_id__del	2019-03-10 15:59:43.468272+00
359	sentry	0354_auto__chg_field_commitfilechange_filename	2019-03-10 15:59:43.917934+00
360	sentry	0355_auto__add_field_organizationintegration_config__add_field_organization	2019-03-10 15:59:44.437692+00
361	sentry	0356_auto__add_useridentity__add_unique_useridentity_user_identity__add_ide	2019-03-10 15:59:44.949125+00
362	sentry	0357_auto__add_projectteam__add_unique_projectteam_project_team	2019-03-10 15:59:45.451723+00
363	sentry	0358_auto__add_projectsymcachefile__add_unique_projectsymcachefile_project_	2019-03-10 15:59:45.96937+00
364	sentry	0359_auto__add_index_tagvalue_project_id_key_last_seen	2019-03-10 15:59:46.459849+00
365	sentry	0360_auto__add_groupshare	2019-03-10 15:59:46.991956+00
366	sentry	0361_auto__add_minidumpfile	2019-03-10 15:59:47.541771+00
367	sentry	0362_auto__add_userip__add_unique_userip_user_ip_address	2019-03-10 15:59:48.073662+00
368	sentry	0363_auto__add_grouplink__add_unique_grouplink_group_id_linked_type_linked_	2019-03-10 15:59:48.612553+00
369	sentry	0364_backfill_grouplink_from_groupcommitresolution	2019-03-10 15:59:49.149716+00
370	nodestore	0001_initial	2019-03-10 16:00:20.723741+00
371	search	0001_initial	2019-03-10 16:00:21.073353+00
372	search	0002_auto__del_searchtoken__del_unique_searchtoken_document_field_token__de	2019-03-10 16:00:21.105352+00
373	social_auth	0001_initial	2019-03-10 16:00:21.501192+00
374	social_auth	0002_auto__add_unique_nonce_timestamp_salt_server_url__add_unique_associati	2019-03-10 16:00:21.670128+00
375	social_auth	0003_auto__del_nonce__del_unique_nonce_server_url_timestamp_salt__del_assoc	2019-03-10 16:00:21.702784+00
376	social_auth	0004_auto__del_unique_usersocialauth_provider_uid__add_unique_usersocialaut	2019-03-10 16:00:21.72513+00
377	jira_ac	0001_initial	2019-03-10 16:00:23.271436+00
378	hipchat_ac	0001_initial	2019-03-10 16:00:23.641742+00
379	hipchat_ac	0002_auto__del_mentionedevent	2019-03-10 16:00:23.689707+00
\.


--
-- Name: auth_authenticator_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_authenticator_id_seq', 1, false);


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_group_id_seq', 1, false);


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 1, false);


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_permission_id_seq', 331, true);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_user_id_seq', 1, true);


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.django_admin_log_id_seq', 1, false);


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.django_content_type_id_seq', 110, true);


--
-- Name: django_site_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.django_site_id_seq', 1, true);


--
-- Name: jira_ac_tenant_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.jira_ac_tenant_id_seq', 1, false);


--
-- Name: sentry_activity_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_activity_id_seq', 1, true);


--
-- Name: sentry_apiapplication_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_apiapplication_id_seq', 1, false);


--
-- Name: sentry_apiauthorization_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_apiauthorization_id_seq', 1, false);


--
-- Name: sentry_apigrant_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_apigrant_id_seq', 1, false);


--
-- Name: sentry_apikey_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_apikey_id_seq', 1, false);


--
-- Name: sentry_apitoken_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_apitoken_id_seq', 1, false);


--
-- Name: sentry_auditlogentry_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_auditlogentry_id_seq', 4, true);


--
-- Name: sentry_authidentity_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_authidentity_id_seq', 1, false);


--
-- Name: sentry_authprovider_default_teams_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_authprovider_default_teams_id_seq', 1, false);


--
-- Name: sentry_authprovider_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_authprovider_id_seq', 1, false);


--
-- Name: sentry_broadcast_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_broadcast_id_seq', 1, false);


--
-- Name: sentry_broadcastseen_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_broadcastseen_id_seq', 1, false);


--
-- Name: sentry_commit_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_commit_id_seq', 1, false);


--
-- Name: sentry_commitauthor_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_commitauthor_id_seq', 1, false);


--
-- Name: sentry_commitfilechange_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_commitfilechange_id_seq', 1, false);


--
-- Name: sentry_deploy_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_deploy_id_seq', 1, false);


--
-- Name: sentry_distribution_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_distribution_id_seq', 1, false);


--
-- Name: sentry_dsymapp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_dsymapp_id_seq', 1, false);


--
-- Name: sentry_email_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_email_id_seq', 1, true);


--
-- Name: sentry_environment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_environment_id_seq', 1, true);


--
-- Name: sentry_environmentproject_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_environmentproject_id_seq', 3, true);


--
-- Name: sentry_environmentrelease_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_environmentrelease_id_seq', 1, false);


--
-- Name: sentry_eventmapping_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_eventmapping_id_seq', 3, true);


--
-- Name: sentry_eventprocessingissue_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_eventprocessingissue_id_seq', 1, false);


--
-- Name: sentry_eventtag_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_eventtag_id_seq', 24, true);


--
-- Name: sentry_eventuser_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_eventuser_id_seq', 3, true);


--
-- Name: sentry_featureadoption_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_featureadoption_id_seq', 6, true);


--
-- Name: sentry_file_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_file_id_seq', 1, false);


--
-- Name: sentry_fileblob_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_fileblob_id_seq', 1, false);


--
-- Name: sentry_fileblobindex_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_fileblobindex_id_seq', 1, false);


--
-- Name: sentry_filterkey_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_filterkey_id_seq', 8, true);


--
-- Name: sentry_filtervalue_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_filtervalue_id_seq', 9, true);


--
-- Name: sentry_groupasignee_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_groupasignee_id_seq', 1, false);


--
-- Name: sentry_groupbookmark_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_groupbookmark_id_seq', 1, false);


--
-- Name: sentry_groupcommitresolution_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_groupcommitresolution_id_seq', 1, false);


--
-- Name: sentry_groupedmessage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_groupedmessage_id_seq', 2, true);


--
-- Name: sentry_groupemailthread_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_groupemailthread_id_seq', 2, true);


--
-- Name: sentry_grouphash_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_grouphash_id_seq', 4, true);


--
-- Name: sentry_grouplink_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_grouplink_id_seq', 1, false);


--
-- Name: sentry_groupmeta_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_groupmeta_id_seq', 1, false);


--
-- Name: sentry_groupredirect_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_groupredirect_id_seq', 1, false);


--
-- Name: sentry_grouprelease_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_grouprelease_id_seq', 1, false);


--
-- Name: sentry_groupresolution_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_groupresolution_id_seq', 1, false);


--
-- Name: sentry_grouprulestatus_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_grouprulestatus_id_seq', 2, true);


--
-- Name: sentry_groupseen_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_groupseen_id_seq', 1, false);


--
-- Name: sentry_groupshare_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_groupshare_id_seq', 1, false);


--
-- Name: sentry_groupsnooze_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_groupsnooze_id_seq', 1, false);


--
-- Name: sentry_groupsubscription_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_groupsubscription_id_seq', 1, true);


--
-- Name: sentry_grouptagkey_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_grouptagkey_id_seq', 16, true);


--
-- Name: sentry_grouptombstone_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_grouptombstone_id_seq', 1, false);


--
-- Name: sentry_hipchat_ac_tenant_organizations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_hipchat_ac_tenant_organizations_id_seq', 1, false);


--
-- Name: sentry_hipchat_ac_tenant_projects_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_hipchat_ac_tenant_projects_id_seq', 1, false);


--
-- Name: sentry_identity_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_identity_id_seq', 1, false);


--
-- Name: sentry_identityprovider_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_identityprovider_id_seq', 1, false);


--
-- Name: sentry_integration_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_integration_id_seq', 1, false);


--
-- Name: sentry_lostpasswordhash_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_lostpasswordhash_id_seq', 1, false);


--
-- Name: sentry_message_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_message_id_seq', 3, true);


--
-- Name: sentry_messagefiltervalue_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_messagefiltervalue_id_seq', 17, true);


--
-- Name: sentry_messageindex_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_messageindex_id_seq', 1, false);


--
-- Name: sentry_minidumpfile_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_minidumpfile_id_seq', 1, false);


--
-- Name: sentry_option_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_option_id_seq', 12, true);


--
-- Name: sentry_organization_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_organization_id_seq', 1, true);


--
-- Name: sentry_organizationaccessrequest_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_organizationaccessrequest_id_seq', 1, false);


--
-- Name: sentry_organizationavatar_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_organizationavatar_id_seq', 1, false);


--
-- Name: sentry_organizationintegration_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_organizationintegration_id_seq', 1, false);


--
-- Name: sentry_organizationmember_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_organizationmember_id_seq', 1, true);


--
-- Name: sentry_organizationmember_teams_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_organizationmember_teams_id_seq', 1, true);


--
-- Name: sentry_organizationonboardingtask_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_organizationonboardingtask_id_seq', 9, true);


--
-- Name: sentry_organizationoptions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_organizationoptions_id_seq', 1, false);


--
-- Name: sentry_processingissue_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_processingissue_id_seq', 1, false);


--
-- Name: sentry_project_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_project_id_seq', 4, true);


--
-- Name: sentry_projectbookmark_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_projectbookmark_id_seq', 1, false);


--
-- Name: sentry_projectcounter_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_projectcounter_id_seq', 1, true);


--
-- Name: sentry_projectdsymfile_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_projectdsymfile_id_seq', 1, false);


--
-- Name: sentry_projectintegration_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_projectintegration_id_seq', 1, false);


--
-- Name: sentry_projectkey_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_projectkey_id_seq', 4, true);


--
-- Name: sentry_projectoptions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_projectoptions_id_seq', 4, true);


--
-- Name: sentry_projectplatform_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_projectplatform_id_seq', 1, false);


--
-- Name: sentry_projectsymcachefile_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_projectsymcachefile_id_seq', 1, false);


--
-- Name: sentry_projectteam_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_projectteam_id_seq', 4, true);


--
-- Name: sentry_rawevent_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_rawevent_id_seq', 1, false);


--
-- Name: sentry_release_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_release_id_seq', 1, false);


--
-- Name: sentry_release_project_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_release_project_id_seq', 1, false);


--
-- Name: sentry_releasecommit_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_releasecommit_id_seq', 1, false);


--
-- Name: sentry_releasefile_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_releasefile_id_seq', 1, false);


--
-- Name: sentry_releaseheadcommit_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_releaseheadcommit_id_seq', 1, false);


--
-- Name: sentry_repository_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_repository_id_seq', 1, false);


--
-- Name: sentry_reprocessingreport_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_reprocessingreport_id_seq', 1, false);


--
-- Name: sentry_rule_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_rule_id_seq', 4, true);


--
-- Name: sentry_savedsearch_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_savedsearch_id_seq', 20, true);


--
-- Name: sentry_savedsearch_userdefault_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_savedsearch_userdefault_id_seq', 1, false);


--
-- Name: sentry_scheduleddeletion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_scheduleddeletion_id_seq', 1, false);


--
-- Name: sentry_scheduledjob_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_scheduledjob_id_seq', 1, false);


--
-- Name: sentry_team_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_team_id_seq', 1, true);


--
-- Name: sentry_useravatar_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_useravatar_id_seq', 1, false);


--
-- Name: sentry_useremail_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_useremail_id_seq', 1, true);


--
-- Name: sentry_useridentity_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_useridentity_id_seq', 1, false);


--
-- Name: sentry_userip_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_userip_id_seq', 1, true);


--
-- Name: sentry_useroption_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_useroption_id_seq', 1, false);


--
-- Name: sentry_userreport_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_userreport_id_seq', 1, false);


--
-- Name: sentry_versiondsymfile_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sentry_versiondsymfile_id_seq', 1, false);


--
-- Name: social_auth_usersocialauth_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.social_auth_usersocialauth_id_seq', 1, false);


--
-- Name: south_migrationhistory_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.south_migrationhistory_id_seq', 379, true);


--
-- Name: auth_authenticator auth_authenticator_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_authenticator
    ADD CONSTRAINT auth_authenticator_pkey PRIMARY KEY (id);


--
-- Name: auth_authenticator auth_authenticator_user_id_5774ed51577668d4_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_authenticator
    ADD CONSTRAINT auth_authenticator_user_id_5774ed51577668d4_uniq UNIQUE (user_id, type);


--
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_key UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission auth_permission_content_type_id_codename_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_key UNIQUE (content_type_id, codename);


--
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type django_content_type_app_label_model_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_key UNIQUE (app_label, model);


--
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: django_site django_site_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_site
    ADD CONSTRAINT django_site_pkey PRIMARY KEY (id);


--
-- Name: jira_ac_tenant jira_ac_tenant_client_key_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jira_ac_tenant
    ADD CONSTRAINT jira_ac_tenant_client_key_key UNIQUE (client_key);


--
-- Name: jira_ac_tenant jira_ac_tenant_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jira_ac_tenant
    ADD CONSTRAINT jira_ac_tenant_pkey PRIMARY KEY (id);


--
-- Name: nodestore_node nodestore_node_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nodestore_node
    ADD CONSTRAINT nodestore_node_pkey PRIMARY KEY (id);


--
-- Name: sentry_activity sentry_activity_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_activity
    ADD CONSTRAINT sentry_activity_pkey PRIMARY KEY (id);


--
-- Name: sentry_apiapplication sentry_apiapplication_client_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_apiapplication
    ADD CONSTRAINT sentry_apiapplication_client_id_key UNIQUE (client_id);


--
-- Name: sentry_apiapplication sentry_apiapplication_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_apiapplication
    ADD CONSTRAINT sentry_apiapplication_pkey PRIMARY KEY (id);


--
-- Name: sentry_apiauthorization sentry_apiauthorization_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_apiauthorization
    ADD CONSTRAINT sentry_apiauthorization_pkey PRIMARY KEY (id);


--
-- Name: sentry_apiauthorization sentry_apiauthorization_user_id_eb16c64a7b6db1c_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_apiauthorization
    ADD CONSTRAINT sentry_apiauthorization_user_id_eb16c64a7b6db1c_uniq UNIQUE (user_id, application_id);


--
-- Name: sentry_apigrant sentry_apigrant_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_apigrant
    ADD CONSTRAINT sentry_apigrant_pkey PRIMARY KEY (id);


--
-- Name: sentry_apikey sentry_apikey_key_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_apikey
    ADD CONSTRAINT sentry_apikey_key_key UNIQUE (key);


--
-- Name: sentry_apikey sentry_apikey_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_apikey
    ADD CONSTRAINT sentry_apikey_pkey PRIMARY KEY (id);


--
-- Name: sentry_apitoken sentry_apitoken_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_apitoken
    ADD CONSTRAINT sentry_apitoken_pkey PRIMARY KEY (id);


--
-- Name: sentry_apitoken sentry_apitoken_refresh_token_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_apitoken
    ADD CONSTRAINT sentry_apitoken_refresh_token_key UNIQUE (refresh_token);


--
-- Name: sentry_apitoken sentry_apitoken_token_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_apitoken
    ADD CONSTRAINT sentry_apitoken_token_key UNIQUE (token);


--
-- Name: sentry_auditlogentry sentry_auditlogentry_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_auditlogentry
    ADD CONSTRAINT sentry_auditlogentry_pkey PRIMARY KEY (id);


--
-- Name: sentry_authidentity sentry_authidentity_auth_provider_id_2ac89deececdc9d7_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_authidentity
    ADD CONSTRAINT sentry_authidentity_auth_provider_id_2ac89deececdc9d7_uniq UNIQUE (auth_provider_id, user_id);


--
-- Name: sentry_authidentity sentry_authidentity_auth_provider_id_72ab4375ecd728ba_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_authidentity
    ADD CONSTRAINT sentry_authidentity_auth_provider_id_72ab4375ecd728ba_uniq UNIQUE (auth_provider_id, ident);


--
-- Name: sentry_authidentity sentry_authidentity_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_authidentity
    ADD CONSTRAINT sentry_authidentity_pkey PRIMARY KEY (id);


--
-- Name: sentry_authprovider_default_teams sentry_authprovider_defau_authprovider_id_352ee7f2584f4caf_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_authprovider_default_teams
    ADD CONSTRAINT sentry_authprovider_defau_authprovider_id_352ee7f2584f4caf_uniq UNIQUE (authprovider_id, team_id);


--
-- Name: sentry_authprovider_default_teams sentry_authprovider_default_teams_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_authprovider_default_teams
    ADD CONSTRAINT sentry_authprovider_default_teams_pkey PRIMARY KEY (id);


--
-- Name: sentry_authprovider sentry_authprovider_organization_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_authprovider
    ADD CONSTRAINT sentry_authprovider_organization_id_key UNIQUE (organization_id);


--
-- Name: sentry_authprovider sentry_authprovider_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_authprovider
    ADD CONSTRAINT sentry_authprovider_pkey PRIMARY KEY (id);


--
-- Name: sentry_broadcast sentry_broadcast_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_broadcast
    ADD CONSTRAINT sentry_broadcast_pkey PRIMARY KEY (id);


--
-- Name: sentry_broadcastseen sentry_broadcastseen_broadcast_id_352c833420c70bd9_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_broadcastseen
    ADD CONSTRAINT sentry_broadcastseen_broadcast_id_352c833420c70bd9_uniq UNIQUE (broadcast_id, user_id);


--
-- Name: sentry_broadcastseen sentry_broadcastseen_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_broadcastseen
    ADD CONSTRAINT sentry_broadcastseen_pkey PRIMARY KEY (id);


--
-- Name: sentry_commit sentry_commit_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_commit
    ADD CONSTRAINT sentry_commit_pkey PRIMARY KEY (id);


--
-- Name: sentry_commit sentry_commit_repository_id_2d25b4d8949fca93_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_commit
    ADD CONSTRAINT sentry_commit_repository_id_2d25b4d8949fca93_uniq UNIQUE (repository_id, key);


--
-- Name: sentry_commitauthor sentry_commitauthor_organization_id_3cdc85e9f09bf3f3_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_commitauthor
    ADD CONSTRAINT sentry_commitauthor_organization_id_3cdc85e9f09bf3f3_uniq UNIQUE (organization_id, external_id);


--
-- Name: sentry_commitauthor sentry_commitauthor_organization_id_5656e6a6baa5f6c_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_commitauthor
    ADD CONSTRAINT sentry_commitauthor_organization_id_5656e6a6baa5f6c_uniq UNIQUE (organization_id, email);


--
-- Name: sentry_commitauthor sentry_commitauthor_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_commitauthor
    ADD CONSTRAINT sentry_commitauthor_pkey PRIMARY KEY (id);


--
-- Name: sentry_commitfilechange sentry_commitfilechange_commit_id_4c6f7ec25af34227_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_commitfilechange
    ADD CONSTRAINT sentry_commitfilechange_commit_id_4c6f7ec25af34227_uniq UNIQUE (commit_id, filename);


--
-- Name: sentry_commitfilechange sentry_commitfilechange_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_commitfilechange
    ADD CONSTRAINT sentry_commitfilechange_pkey PRIMARY KEY (id);


--
-- Name: sentry_deploy sentry_deploy_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_deploy
    ADD CONSTRAINT sentry_deploy_pkey PRIMARY KEY (id);


--
-- Name: sentry_distribution sentry_distribution_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_distribution
    ADD CONSTRAINT sentry_distribution_pkey PRIMARY KEY (id);


--
-- Name: sentry_distribution sentry_distribution_release_id_42bfea790c978c1b_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_distribution
    ADD CONSTRAINT sentry_distribution_release_id_42bfea790c978c1b_uniq UNIQUE (release_id, name);


--
-- Name: sentry_dsymapp sentry_dsymapp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_dsymapp
    ADD CONSTRAINT sentry_dsymapp_pkey PRIMARY KEY (id);


--
-- Name: sentry_dsymapp sentry_dsymapp_project_id_decebaf381e09fe_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_dsymapp
    ADD CONSTRAINT sentry_dsymapp_project_id_decebaf381e09fe_uniq UNIQUE (project_id, platform, app_id);


--
-- Name: sentry_email sentry_email_email_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_email
    ADD CONSTRAINT sentry_email_email_key UNIQUE (email);


--
-- Name: sentry_email sentry_email_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_email
    ADD CONSTRAINT sentry_email_pkey PRIMARY KEY (id);


--
-- Name: sentry_environment sentry_environment_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_environment
    ADD CONSTRAINT sentry_environment_pkey PRIMARY KEY (id);


--
-- Name: sentry_environment sentry_environment_project_id_1fbf3bfa87c819df_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_environment
    ADD CONSTRAINT sentry_environment_project_id_1fbf3bfa87c819df_uniq UNIQUE (project_id, name);


--
-- Name: sentry_environmentproject sentry_environmentproject_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_environmentproject
    ADD CONSTRAINT sentry_environmentproject_pkey PRIMARY KEY (id);


--
-- Name: sentry_environmentproject sentry_environmentproject_project_id_29250c1307d3722b_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_environmentproject
    ADD CONSTRAINT sentry_environmentproject_project_id_29250c1307d3722b_uniq UNIQUE (project_id, environment_id);


--
-- Name: sentry_environmentrelease sentry_environmentrelease_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_environmentrelease
    ADD CONSTRAINT sentry_environmentrelease_pkey PRIMARY KEY (id);


--
-- Name: sentry_eventmapping sentry_eventmapping_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_eventmapping
    ADD CONSTRAINT sentry_eventmapping_pkey PRIMARY KEY (id);


--
-- Name: sentry_eventmapping sentry_eventmapping_project_id_eb6c54bf8930ba6_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_eventmapping
    ADD CONSTRAINT sentry_eventmapping_project_id_eb6c54bf8930ba6_uniq UNIQUE (project_id, event_id);


--
-- Name: sentry_eventprocessingissue sentry_eventprocessingissue_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_eventprocessingissue
    ADD CONSTRAINT sentry_eventprocessingissue_pkey PRIMARY KEY (id);


--
-- Name: sentry_eventprocessingissue sentry_eventprocessingissue_raw_event_id_7751571083fd0f14_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_eventprocessingissue
    ADD CONSTRAINT sentry_eventprocessingissue_raw_event_id_7751571083fd0f14_uniq UNIQUE (raw_event_id, processing_issue_id);


--
-- Name: sentry_eventtag sentry_eventtag_event_id_430cef8ef4186908_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_eventtag
    ADD CONSTRAINT sentry_eventtag_event_id_430cef8ef4186908_uniq UNIQUE (event_id, key_id, value_id);


--
-- Name: sentry_eventtag sentry_eventtag_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_eventtag
    ADD CONSTRAINT sentry_eventtag_pkey PRIMARY KEY (id);


--
-- Name: sentry_eventuser sentry_eventuser_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_eventuser
    ADD CONSTRAINT sentry_eventuser_pkey PRIMARY KEY (id);


--
-- Name: sentry_eventuser sentry_eventuser_project_id_1a96e3b719e55f9a_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_eventuser
    ADD CONSTRAINT sentry_eventuser_project_id_1a96e3b719e55f9a_uniq UNIQUE (project_id, hash);


--
-- Name: sentry_eventuser sentry_eventuser_project_id_1dcb94833e2de5cf_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_eventuser
    ADD CONSTRAINT sentry_eventuser_project_id_1dcb94833e2de5cf_uniq UNIQUE (project_id, ident);


--
-- Name: sentry_featureadoption sentry_featureadoption_organization_id_78451b8747a9e638_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_featureadoption
    ADD CONSTRAINT sentry_featureadoption_organization_id_78451b8747a9e638_uniq UNIQUE (organization_id, feature_id);


--
-- Name: sentry_featureadoption sentry_featureadoption_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_featureadoption
    ADD CONSTRAINT sentry_featureadoption_pkey PRIMARY KEY (id);


--
-- Name: sentry_file sentry_file_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_file
    ADD CONSTRAINT sentry_file_pkey PRIMARY KEY (id);


--
-- Name: sentry_fileblob sentry_fileblob_checksum_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_fileblob
    ADD CONSTRAINT sentry_fileblob_checksum_key UNIQUE (checksum);


--
-- Name: sentry_fileblob sentry_fileblob_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_fileblob
    ADD CONSTRAINT sentry_fileblob_pkey PRIMARY KEY (id);


--
-- Name: sentry_fileblobindex sentry_fileblobindex_file_id_56d11844195e33b2_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_fileblobindex
    ADD CONSTRAINT sentry_fileblobindex_file_id_56d11844195e33b2_uniq UNIQUE (file_id, blob_id, "offset");


--
-- Name: sentry_fileblobindex sentry_fileblobindex_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_fileblobindex
    ADD CONSTRAINT sentry_fileblobindex_pkey PRIMARY KEY (id);


--
-- Name: sentry_filterkey sentry_filterkey_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_filterkey
    ADD CONSTRAINT sentry_filterkey_pkey PRIMARY KEY (id);


--
-- Name: sentry_filterkey sentry_filterkey_project_id_67551b8e28dda5a_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_filterkey
    ADD CONSTRAINT sentry_filterkey_project_id_67551b8e28dda5a_uniq UNIQUE (project_id, key);


--
-- Name: sentry_filtervalue sentry_filtervalue_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_filtervalue
    ADD CONSTRAINT sentry_filtervalue_pkey PRIMARY KEY (id);


--
-- Name: sentry_filtervalue sentry_filtervalue_project_id_201b156195347397_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_filtervalue
    ADD CONSTRAINT sentry_filtervalue_project_id_201b156195347397_uniq UNIQUE (project_id, key, value);


--
-- Name: sentry_groupasignee sentry_groupasignee_group_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupasignee
    ADD CONSTRAINT sentry_groupasignee_group_id_key UNIQUE (group_id);


--
-- Name: sentry_groupasignee sentry_groupasignee_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupasignee
    ADD CONSTRAINT sentry_groupasignee_pkey PRIMARY KEY (id);


--
-- Name: sentry_groupbookmark sentry_groupbookmark_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupbookmark
    ADD CONSTRAINT sentry_groupbookmark_pkey PRIMARY KEY (id);


--
-- Name: sentry_groupbookmark sentry_groupbookmark_project_id_6d2bb88ad3832208_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupbookmark
    ADD CONSTRAINT sentry_groupbookmark_project_id_6d2bb88ad3832208_uniq UNIQUE (project_id, user_id, group_id);


--
-- Name: sentry_groupcommitresolution sentry_groupcommitresolution_group_id_c46e4845d76b4f_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupcommitresolution
    ADD CONSTRAINT sentry_groupcommitresolution_group_id_c46e4845d76b4f_uniq UNIQUE (group_id, commit_id);


--
-- Name: sentry_groupcommitresolution sentry_groupcommitresolution_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupcommitresolution
    ADD CONSTRAINT sentry_groupcommitresolution_pkey PRIMARY KEY (id);


--
-- Name: sentry_groupedmessage sentry_groupedmessage_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupedmessage
    ADD CONSTRAINT sentry_groupedmessage_pkey PRIMARY KEY (id);


--
-- Name: sentry_groupedmessage sentry_groupedmessage_project_id_680bfe5607002523_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupedmessage
    ADD CONSTRAINT sentry_groupedmessage_project_id_680bfe5607002523_uniq UNIQUE (project_id, short_id);


--
-- Name: sentry_groupemailthread sentry_groupemailthread_email_456f4d17524b316_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupemailthread
    ADD CONSTRAINT sentry_groupemailthread_email_456f4d17524b316_uniq UNIQUE (email, msgid);


--
-- Name: sentry_groupemailthread sentry_groupemailthread_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupemailthread
    ADD CONSTRAINT sentry_groupemailthread_pkey PRIMARY KEY (id);


--
-- Name: sentry_grouphash sentry_grouphash_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_grouphash
    ADD CONSTRAINT sentry_grouphash_pkey PRIMARY KEY (id);


--
-- Name: sentry_grouphash sentry_grouphash_project_id_4a293f96a363c9a2_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_grouphash
    ADD CONSTRAINT sentry_grouphash_project_id_4a293f96a363c9a2_uniq UNIQUE (project_id, hash);


--
-- Name: sentry_grouplink sentry_grouplink_group_id_73ee52490ebedd34_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_grouplink
    ADD CONSTRAINT sentry_grouplink_group_id_73ee52490ebedd34_uniq UNIQUE (group_id, linked_type, linked_id);


--
-- Name: sentry_grouplink sentry_grouplink_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_grouplink
    ADD CONSTRAINT sentry_grouplink_pkey PRIMARY KEY (id);


--
-- Name: sentry_groupmeta sentry_groupmeta_key_5d9d7a3c6538b14d_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupmeta
    ADD CONSTRAINT sentry_groupmeta_key_5d9d7a3c6538b14d_uniq UNIQUE (key, group_id);


--
-- Name: sentry_groupmeta sentry_groupmeta_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupmeta
    ADD CONSTRAINT sentry_groupmeta_pkey PRIMARY KEY (id);


--
-- Name: sentry_groupredirect sentry_groupredirect_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupredirect
    ADD CONSTRAINT sentry_groupredirect_pkey PRIMARY KEY (id);


--
-- Name: sentry_groupredirect sentry_groupredirect_previous_group_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupredirect
    ADD CONSTRAINT sentry_groupredirect_previous_group_id_key UNIQUE (previous_group_id);


--
-- Name: sentry_grouprelease sentry_grouprelease_group_id_46ba6e430d088d04_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_grouprelease
    ADD CONSTRAINT sentry_grouprelease_group_id_46ba6e430d088d04_uniq UNIQUE (group_id, release_id, environment);


--
-- Name: sentry_grouprelease sentry_grouprelease_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_grouprelease
    ADD CONSTRAINT sentry_grouprelease_pkey PRIMARY KEY (id);


--
-- Name: sentry_groupresolution sentry_groupresolution_group_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupresolution
    ADD CONSTRAINT sentry_groupresolution_group_id_key UNIQUE (group_id);


--
-- Name: sentry_groupresolution sentry_groupresolution_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupresolution
    ADD CONSTRAINT sentry_groupresolution_pkey PRIMARY KEY (id);


--
-- Name: sentry_grouprulestatus sentry_grouprulestatus_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_grouprulestatus
    ADD CONSTRAINT sentry_grouprulestatus_pkey PRIMARY KEY (id);


--
-- Name: sentry_grouprulestatus sentry_grouprulestatus_rule_id_329bb0edaad3880f_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_grouprulestatus
    ADD CONSTRAINT sentry_grouprulestatus_rule_id_329bb0edaad3880f_uniq UNIQUE (rule_id, group_id);


--
-- Name: sentry_groupseen sentry_groupseen_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupseen
    ADD CONSTRAINT sentry_groupseen_pkey PRIMARY KEY (id);


--
-- Name: sentry_groupseen sentry_groupseen_user_id_179917bc9974d91b_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupseen
    ADD CONSTRAINT sentry_groupseen_user_id_179917bc9974d91b_uniq UNIQUE (user_id, group_id);


--
-- Name: sentry_groupshare sentry_groupshare_group_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupshare
    ADD CONSTRAINT sentry_groupshare_group_id_key UNIQUE (group_id);


--
-- Name: sentry_groupshare sentry_groupshare_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupshare
    ADD CONSTRAINT sentry_groupshare_pkey PRIMARY KEY (id);


--
-- Name: sentry_groupshare sentry_groupshare_uuid_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupshare
    ADD CONSTRAINT sentry_groupshare_uuid_key UNIQUE (uuid);


--
-- Name: sentry_groupsnooze sentry_groupsnooze_group_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupsnooze
    ADD CONSTRAINT sentry_groupsnooze_group_id_key UNIQUE (group_id);


--
-- Name: sentry_groupsnooze sentry_groupsnooze_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupsnooze
    ADD CONSTRAINT sentry_groupsnooze_pkey PRIMARY KEY (id);


--
-- Name: sentry_groupsubscription sentry_groupsubscription_group_id_7e18bedd5058ccc3_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupsubscription
    ADD CONSTRAINT sentry_groupsubscription_group_id_7e18bedd5058ccc3_uniq UNIQUE (group_id, user_id);


--
-- Name: sentry_groupsubscription sentry_groupsubscription_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupsubscription
    ADD CONSTRAINT sentry_groupsubscription_pkey PRIMARY KEY (id);


--
-- Name: sentry_grouptagkey sentry_grouptagkey_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_grouptagkey
    ADD CONSTRAINT sentry_grouptagkey_pkey PRIMARY KEY (id);


--
-- Name: sentry_grouptagkey sentry_grouptagkey_project_id_7b0c8092f47b509f_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_grouptagkey
    ADD CONSTRAINT sentry_grouptagkey_project_id_7b0c8092f47b509f_uniq UNIQUE (project_id, group_id, key);


--
-- Name: sentry_grouptombstone sentry_grouptombstone_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_grouptombstone
    ADD CONSTRAINT sentry_grouptombstone_pkey PRIMARY KEY (id);


--
-- Name: sentry_grouptombstone sentry_grouptombstone_previous_group_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_grouptombstone
    ADD CONSTRAINT sentry_grouptombstone_previous_group_id_key UNIQUE (previous_group_id);


--
-- Name: sentry_hipchat_ac_tenant_organizations sentry_hipchat_ac_tenant_organi_tenant_id_277f40009a2aa417_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_hipchat_ac_tenant_organizations
    ADD CONSTRAINT sentry_hipchat_ac_tenant_organi_tenant_id_277f40009a2aa417_uniq UNIQUE (tenant_id, organization_id);


--
-- Name: sentry_hipchat_ac_tenant_organizations sentry_hipchat_ac_tenant_organizations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_hipchat_ac_tenant_organizations
    ADD CONSTRAINT sentry_hipchat_ac_tenant_organizations_pkey PRIMARY KEY (id);


--
-- Name: sentry_hipchat_ac_tenant sentry_hipchat_ac_tenant_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_hipchat_ac_tenant
    ADD CONSTRAINT sentry_hipchat_ac_tenant_pkey PRIMARY KEY (id);


--
-- Name: sentry_hipchat_ac_tenant_projects sentry_hipchat_ac_tenant_projec_tenant_id_5308544b484f49a9_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_hipchat_ac_tenant_projects
    ADD CONSTRAINT sentry_hipchat_ac_tenant_projec_tenant_id_5308544b484f49a9_uniq UNIQUE (tenant_id, project_id);


--
-- Name: sentry_hipchat_ac_tenant_projects sentry_hipchat_ac_tenant_projects_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_hipchat_ac_tenant_projects
    ADD CONSTRAINT sentry_hipchat_ac_tenant_projects_pkey PRIMARY KEY (id);


--
-- Name: sentry_identity sentry_identity_idp_id_47d379630426f630_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_identity
    ADD CONSTRAINT sentry_identity_idp_id_47d379630426f630_uniq UNIQUE (idp_id, external_id);


--
-- Name: sentry_identity sentry_identity_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_identity
    ADD CONSTRAINT sentry_identity_pkey PRIMARY KEY (id);


--
-- Name: sentry_identityprovider sentry_identityprovider_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_identityprovider
    ADD CONSTRAINT sentry_identityprovider_pkey PRIMARY KEY (id);


--
-- Name: sentry_identityprovider sentry_identityprovider_type_5aba23d550d1f197_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_identityprovider
    ADD CONSTRAINT sentry_identityprovider_type_5aba23d550d1f197_uniq UNIQUE (type, instance);


--
-- Name: sentry_integration sentry_integration_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_integration
    ADD CONSTRAINT sentry_integration_pkey PRIMARY KEY (id);


--
-- Name: sentry_integration sentry_integration_provider_e9944c77818d5f5_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_integration
    ADD CONSTRAINT sentry_integration_provider_e9944c77818d5f5_uniq UNIQUE (provider, external_id);


--
-- Name: sentry_lostpasswordhash sentry_lostpasswordhash_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_lostpasswordhash
    ADD CONSTRAINT sentry_lostpasswordhash_pkey PRIMARY KEY (id);


--
-- Name: sentry_lostpasswordhash sentry_lostpasswordhash_user_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_lostpasswordhash
    ADD CONSTRAINT sentry_lostpasswordhash_user_id_key UNIQUE (user_id);


--
-- Name: sentry_message sentry_message_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_message
    ADD CONSTRAINT sentry_message_pkey PRIMARY KEY (id);


--
-- Name: sentry_message sentry_message_project_id_b6b4e75e438ca83_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_message
    ADD CONSTRAINT sentry_message_project_id_b6b4e75e438ca83_uniq UNIQUE (project_id, message_id);


--
-- Name: sentry_messagefiltervalue sentry_messagefiltervalue_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_messagefiltervalue
    ADD CONSTRAINT sentry_messagefiltervalue_pkey PRIMARY KEY (id);


--
-- Name: sentry_messageindex sentry_messageindex_column_23431fca14e385c1_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_messageindex
    ADD CONSTRAINT sentry_messageindex_column_23431fca14e385c1_uniq UNIQUE ("column", value, object_id);


--
-- Name: sentry_messageindex sentry_messageindex_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_messageindex
    ADD CONSTRAINT sentry_messageindex_pkey PRIMARY KEY (id);


--
-- Name: sentry_minidumpfile sentry_minidumpfile_event_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_minidumpfile
    ADD CONSTRAINT sentry_minidumpfile_event_id_key UNIQUE (event_id);


--
-- Name: sentry_minidumpfile sentry_minidumpfile_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_minidumpfile
    ADD CONSTRAINT sentry_minidumpfile_pkey PRIMARY KEY (id);


--
-- Name: sentry_option sentry_option_key_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_option
    ADD CONSTRAINT sentry_option_key_uniq UNIQUE (key);


--
-- Name: sentry_option sentry_option_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_option
    ADD CONSTRAINT sentry_option_pkey PRIMARY KEY (id);


--
-- Name: sentry_organizationmember_teams sentry_organization_organizationmember_id_1634015042409685_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_organizationmember_teams
    ADD CONSTRAINT sentry_organization_organizationmember_id_1634015042409685_uniq UNIQUE (organizationmember_id, team_id);


--
-- Name: sentry_organization sentry_organization_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_organization
    ADD CONSTRAINT sentry_organization_pkey PRIMARY KEY (id);


--
-- Name: sentry_organization sentry_organization_slug_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_organization
    ADD CONSTRAINT sentry_organization_slug_key UNIQUE (slug);


--
-- Name: sentry_organizationaccessrequest sentry_organizationaccessrequest_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_organizationaccessrequest
    ADD CONSTRAINT sentry_organizationaccessrequest_pkey PRIMARY KEY (id);


--
-- Name: sentry_organizationaccessrequest sentry_organizationaccessrequest_team_id_2a38219fe738f1d7_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_organizationaccessrequest
    ADD CONSTRAINT sentry_organizationaccessrequest_team_id_2a38219fe738f1d7_uniq UNIQUE (team_id, member_id);


--
-- Name: sentry_organizationavatar sentry_organizationavatar_file_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_organizationavatar
    ADD CONSTRAINT sentry_organizationavatar_file_id_key UNIQUE (file_id);


--
-- Name: sentry_organizationavatar sentry_organizationavatar_ident_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_organizationavatar
    ADD CONSTRAINT sentry_organizationavatar_ident_key UNIQUE (ident);


--
-- Name: sentry_organizationavatar sentry_organizationavatar_organization_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_organizationavatar
    ADD CONSTRAINT sentry_organizationavatar_organization_id_key UNIQUE (organization_id);


--
-- Name: sentry_organizationavatar sentry_organizationavatar_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_organizationavatar
    ADD CONSTRAINT sentry_organizationavatar_pkey PRIMARY KEY (id);


--
-- Name: sentry_organizationintegration sentry_organizationintegr_organization_id_77bd763ea752b4b7_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_organizationintegration
    ADD CONSTRAINT sentry_organizationintegr_organization_id_77bd763ea752b4b7_uniq UNIQUE (organization_id, integration_id);


--
-- Name: sentry_organizationintegration sentry_organizationintegration_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_organizationintegration
    ADD CONSTRAINT sentry_organizationintegration_pkey PRIMARY KEY (id);


--
-- Name: sentry_organizationmember sentry_organizationmember_organization_id_404770fc5e3a794_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_organizationmember
    ADD CONSTRAINT sentry_organizationmember_organization_id_404770fc5e3a794_uniq UNIQUE (organization_id, user_id);


--
-- Name: sentry_organizationmember sentry_organizationmember_organization_id_59ee8d99c683b0e7_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_organizationmember
    ADD CONSTRAINT sentry_organizationmember_organization_id_59ee8d99c683b0e7_uniq UNIQUE (organization_id, email);


--
-- Name: sentry_organizationmember sentry_organizationmember_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_organizationmember
    ADD CONSTRAINT sentry_organizationmember_pkey PRIMARY KEY (id);


--
-- Name: sentry_organizationmember_teams sentry_organizationmember_teams_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_organizationmember_teams
    ADD CONSTRAINT sentry_organizationmember_teams_pkey PRIMARY KEY (id);


--
-- Name: sentry_organizationmember sentry_organizationmember_token_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_organizationmember
    ADD CONSTRAINT sentry_organizationmember_token_key UNIQUE (token);


--
-- Name: sentry_organizationonboardingtask sentry_organizationonboar_organization_id_47e98e05cae29cf3_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_organizationonboardingtask
    ADD CONSTRAINT sentry_organizationonboar_organization_id_47e98e05cae29cf3_uniq UNIQUE (organization_id, task);


--
-- Name: sentry_organizationonboardingtask sentry_organizationonboardingtask_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_organizationonboardingtask
    ADD CONSTRAINT sentry_organizationonboardingtask_pkey PRIMARY KEY (id);


--
-- Name: sentry_organizationoptions sentry_organizationoption_organization_id_613ac9b501bd6e71_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_organizationoptions
    ADD CONSTRAINT sentry_organizationoption_organization_id_613ac9b501bd6e71_uniq UNIQUE (organization_id, key);


--
-- Name: sentry_organizationoptions sentry_organizationoptions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_organizationoptions
    ADD CONSTRAINT sentry_organizationoptions_pkey PRIMARY KEY (id);


--
-- Name: sentry_processingissue sentry_processingissue_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_processingissue
    ADD CONSTRAINT sentry_processingissue_pkey PRIMARY KEY (id);


--
-- Name: sentry_processingissue sentry_processingissue_project_id_4cf2c364095eb2b9_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_processingissue
    ADD CONSTRAINT sentry_processingissue_project_id_4cf2c364095eb2b9_uniq UNIQUE (project_id, checksum, type);


--
-- Name: sentry_project sentry_project_organization_id_3017a54aeb676236_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_project
    ADD CONSTRAINT sentry_project_organization_id_3017a54aeb676236_uniq UNIQUE (organization_id, slug);


--
-- Name: sentry_project sentry_project_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_project
    ADD CONSTRAINT sentry_project_pkey PRIMARY KEY (id);


--
-- Name: sentry_project sentry_project_slug_7e0cc0d379eb3e42_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_project
    ADD CONSTRAINT sentry_project_slug_7e0cc0d379eb3e42_uniq UNIQUE (slug, team_id);


--
-- Name: sentry_projectbookmark sentry_projectbookmark_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_projectbookmark
    ADD CONSTRAINT sentry_projectbookmark_pkey PRIMARY KEY (id);


--
-- Name: sentry_projectbookmark sentry_projectbookmark_project_id_450321e77adb9106_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_projectbookmark
    ADD CONSTRAINT sentry_projectbookmark_project_id_450321e77adb9106_uniq UNIQUE (project_id, user_id);


--
-- Name: sentry_projectcounter sentry_projectcounter_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_projectcounter
    ADD CONSTRAINT sentry_projectcounter_pkey PRIMARY KEY (id);


--
-- Name: sentry_projectcounter sentry_projectcounter_project_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_projectcounter
    ADD CONSTRAINT sentry_projectcounter_project_id_key UNIQUE (project_id);


--
-- Name: sentry_projectdsymfile sentry_projectdsymfile_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_projectdsymfile
    ADD CONSTRAINT sentry_projectdsymfile_pkey PRIMARY KEY (id);


--
-- Name: sentry_projectdsymfile sentry_projectdsymfile_project_id_52cf645985146f12_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_projectdsymfile
    ADD CONSTRAINT sentry_projectdsymfile_project_id_52cf645985146f12_uniq UNIQUE (project_id, uuid);


--
-- Name: sentry_projectintegration sentry_projectintegration_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_projectintegration
    ADD CONSTRAINT sentry_projectintegration_pkey PRIMARY KEY (id);


--
-- Name: sentry_projectintegration sentry_projectintegration_project_id_b772982487a62fd_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_projectintegration
    ADD CONSTRAINT sentry_projectintegration_project_id_b772982487a62fd_uniq UNIQUE (project_id, integration_id);


--
-- Name: sentry_projectkey sentry_projectkey_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_projectkey
    ADD CONSTRAINT sentry_projectkey_pkey PRIMARY KEY (id);


--
-- Name: sentry_projectkey sentry_projectkey_public_key_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_projectkey
    ADD CONSTRAINT sentry_projectkey_public_key_key UNIQUE (public_key);


--
-- Name: sentry_projectkey sentry_projectkey_secret_key_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_projectkey
    ADD CONSTRAINT sentry_projectkey_secret_key_key UNIQUE (secret_key);


--
-- Name: sentry_projectoptions sentry_projectoptions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_projectoptions
    ADD CONSTRAINT sentry_projectoptions_pkey PRIMARY KEY (id);


--
-- Name: sentry_projectoptions sentry_projectoptions_project_id_2d0b5c5d84cdbe8f_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_projectoptions
    ADD CONSTRAINT sentry_projectoptions_project_id_2d0b5c5d84cdbe8f_uniq UNIQUE (project_id, key);


--
-- Name: sentry_projectplatform sentry_projectplatform_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_projectplatform
    ADD CONSTRAINT sentry_projectplatform_pkey PRIMARY KEY (id);


--
-- Name: sentry_projectplatform sentry_projectplatform_project_id_4750cc420a30bf84_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_projectplatform
    ADD CONSTRAINT sentry_projectplatform_project_id_4750cc420a30bf84_uniq UNIQUE (project_id, platform);


--
-- Name: sentry_projectsymcachefile sentry_projectsymcachefile_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_projectsymcachefile
    ADD CONSTRAINT sentry_projectsymcachefile_pkey PRIMARY KEY (id);


--
-- Name: sentry_projectsymcachefile sentry_projectsymcachefile_project_id_1d82672e636477c9_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_projectsymcachefile
    ADD CONSTRAINT sentry_projectsymcachefile_project_id_1d82672e636477c9_uniq UNIQUE (project_id, dsym_file_id);


--
-- Name: sentry_projectteam sentry_projectteam_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_projectteam
    ADD CONSTRAINT sentry_projectteam_pkey PRIMARY KEY (id);


--
-- Name: sentry_projectteam sentry_projectteam_project_id_4b99b03421a3c6e9_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_projectteam
    ADD CONSTRAINT sentry_projectteam_project_id_4b99b03421a3c6e9_uniq UNIQUE (project_id, team_id);


--
-- Name: sentry_rawevent sentry_rawevent_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_rawevent
    ADD CONSTRAINT sentry_rawevent_pkey PRIMARY KEY (id);


--
-- Name: sentry_rawevent sentry_rawevent_project_id_67074d89f7075a2e_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_rawevent
    ADD CONSTRAINT sentry_rawevent_project_id_67074d89f7075a2e_uniq UNIQUE (project_id, event_id);


--
-- Name: sentry_release sentry_release_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_release
    ADD CONSTRAINT sentry_release_pkey PRIMARY KEY (id);


--
-- Name: sentry_release_project sentry_release_project_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_release_project
    ADD CONSTRAINT sentry_release_project_pkey PRIMARY KEY (id);


--
-- Name: sentry_release_project sentry_release_project_project_id_35add08b8e678ec7_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_release_project
    ADD CONSTRAINT sentry_release_project_project_id_35add08b8e678ec7_uniq UNIQUE (project_id, release_id);


--
-- Name: sentry_releasecommit sentry_releasecommit_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_releasecommit
    ADD CONSTRAINT sentry_releasecommit_pkey PRIMARY KEY (id);


--
-- Name: sentry_releasecommit sentry_releasecommit_release_id_4394bda1d741e954_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_releasecommit
    ADD CONSTRAINT sentry_releasecommit_release_id_4394bda1d741e954_uniq UNIQUE (release_id, "order");


--
-- Name: sentry_releasecommit sentry_releasecommit_release_id_4ce87699e8e032b3_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_releasecommit
    ADD CONSTRAINT sentry_releasecommit_release_id_4ce87699e8e032b3_uniq UNIQUE (release_id, commit_id);


--
-- Name: sentry_releasefile sentry_releasefile_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_releasefile
    ADD CONSTRAINT sentry_releasefile_pkey PRIMARY KEY (id);


--
-- Name: sentry_releasefile sentry_releasefile_release_id_7809ae7ca24c9589_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_releasefile
    ADD CONSTRAINT sentry_releasefile_release_id_7809ae7ca24c9589_uniq UNIQUE (release_id, ident);


--
-- Name: sentry_releaseheadcommit sentry_releaseheadcommit_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_releaseheadcommit
    ADD CONSTRAINT sentry_releaseheadcommit_pkey PRIMARY KEY (id);


--
-- Name: sentry_releaseheadcommit sentry_releaseheadcommit_repository_id_401c869de623265e_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_releaseheadcommit
    ADD CONSTRAINT sentry_releaseheadcommit_repository_id_401c869de623265e_uniq UNIQUE (repository_id, release_id);


--
-- Name: sentry_repository sentry_repository_organization_id_2bbb7c67744745b6_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_repository
    ADD CONSTRAINT sentry_repository_organization_id_2bbb7c67744745b6_uniq UNIQUE (organization_id, name);


--
-- Name: sentry_repository sentry_repository_organization_id_6369691ee795aeaf_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_repository
    ADD CONSTRAINT sentry_repository_organization_id_6369691ee795aeaf_uniq UNIQUE (organization_id, provider, external_id);


--
-- Name: sentry_repository sentry_repository_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_repository
    ADD CONSTRAINT sentry_repository_pkey PRIMARY KEY (id);


--
-- Name: sentry_reprocessingreport sentry_reprocessingreport_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_reprocessingreport
    ADD CONSTRAINT sentry_reprocessingreport_pkey PRIMARY KEY (id);


--
-- Name: sentry_reprocessingreport sentry_reprocessingreport_project_id_1b8c4565a54fb40e_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_reprocessingreport
    ADD CONSTRAINT sentry_reprocessingreport_project_id_1b8c4565a54fb40e_uniq UNIQUE (project_id, event_id);


--
-- Name: sentry_rule sentry_rule_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_rule
    ADD CONSTRAINT sentry_rule_pkey PRIMARY KEY (id);


--
-- Name: sentry_savedsearch sentry_savedsearch_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_savedsearch
    ADD CONSTRAINT sentry_savedsearch_pkey PRIMARY KEY (id);


--
-- Name: sentry_savedsearch sentry_savedsearch_project_id_4a2cf58e27d0cc59_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_savedsearch
    ADD CONSTRAINT sentry_savedsearch_project_id_4a2cf58e27d0cc59_uniq UNIQUE (project_id, name);


--
-- Name: sentry_savedsearch_userdefault sentry_savedsearch_userdefault_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_savedsearch_userdefault
    ADD CONSTRAINT sentry_savedsearch_userdefault_pkey PRIMARY KEY (id);


--
-- Name: sentry_savedsearch_userdefault sentry_savedsearch_userdefault_project_id_19fbb9813d6a20ef_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_savedsearch_userdefault
    ADD CONSTRAINT sentry_savedsearch_userdefault_project_id_19fbb9813d6a20ef_uniq UNIQUE (project_id, user_id);


--
-- Name: sentry_scheduleddeletion sentry_scheduleddeletion_app_label_740edc97d666ad4_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_scheduleddeletion
    ADD CONSTRAINT sentry_scheduleddeletion_app_label_740edc97d666ad4_uniq UNIQUE (app_label, model_name, object_id);


--
-- Name: sentry_scheduleddeletion sentry_scheduleddeletion_guid_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_scheduleddeletion
    ADD CONSTRAINT sentry_scheduleddeletion_guid_key UNIQUE (guid);


--
-- Name: sentry_scheduleddeletion sentry_scheduleddeletion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_scheduleddeletion
    ADD CONSTRAINT sentry_scheduleddeletion_pkey PRIMARY KEY (id);


--
-- Name: sentry_scheduledjob sentry_scheduledjob_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_scheduledjob
    ADD CONSTRAINT sentry_scheduledjob_pkey PRIMARY KEY (id);


--
-- Name: sentry_team sentry_team_organization_id_1e0ece47434a2ed_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_team
    ADD CONSTRAINT sentry_team_organization_id_1e0ece47434a2ed_uniq UNIQUE (organization_id, slug);


--
-- Name: sentry_team sentry_team_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_team
    ADD CONSTRAINT sentry_team_pkey PRIMARY KEY (id);


--
-- Name: sentry_useravatar sentry_useravatar_file_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_useravatar
    ADD CONSTRAINT sentry_useravatar_file_id_key UNIQUE (file_id);


--
-- Name: sentry_useravatar sentry_useravatar_ident_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_useravatar
    ADD CONSTRAINT sentry_useravatar_ident_key UNIQUE (ident);


--
-- Name: sentry_useravatar sentry_useravatar_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_useravatar
    ADD CONSTRAINT sentry_useravatar_pkey PRIMARY KEY (id);


--
-- Name: sentry_useravatar sentry_useravatar_user_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_useravatar
    ADD CONSTRAINT sentry_useravatar_user_id_key UNIQUE (user_id);


--
-- Name: sentry_useremail sentry_useremail_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_useremail
    ADD CONSTRAINT sentry_useremail_pkey PRIMARY KEY (id);


--
-- Name: sentry_useremail sentry_useremail_user_id_469ffbb142507df2_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_useremail
    ADD CONSTRAINT sentry_useremail_user_id_469ffbb142507df2_uniq UNIQUE (user_id, email);


--
-- Name: sentry_useridentity sentry_useridentity_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_useridentity
    ADD CONSTRAINT sentry_useridentity_pkey PRIMARY KEY (id);


--
-- Name: sentry_useridentity sentry_useridentity_user_id_637a2430fca09073_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_useridentity
    ADD CONSTRAINT sentry_useridentity_user_id_637a2430fca09073_uniq UNIQUE (user_id, identity_id);


--
-- Name: sentry_userip sentry_userip_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_userip
    ADD CONSTRAINT sentry_userip_pkey PRIMARY KEY (id);


--
-- Name: sentry_userip sentry_userip_user_id_5e5a95a35f9f6063_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_userip
    ADD CONSTRAINT sentry_userip_user_id_5e5a95a35f9f6063_uniq UNIQUE (user_id, ip_address);


--
-- Name: sentry_useroption sentry_useroption_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_useroption
    ADD CONSTRAINT sentry_useroption_pkey PRIMARY KEY (id);


--
-- Name: sentry_useroption sentry_useroption_user_id_4d4ce0b1f7bb578b_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_useroption
    ADD CONSTRAINT sentry_useroption_user_id_4d4ce0b1f7bb578b_uniq UNIQUE (user_id, project_id, key);


--
-- Name: sentry_useroption sentry_useroption_user_id_7d51ec93e4fc570e_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_useroption
    ADD CONSTRAINT sentry_useroption_user_id_7d51ec93e4fc570e_uniq UNIQUE (user_id, organization_id, key);


--
-- Name: sentry_userreport sentry_userreport_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_userreport
    ADD CONSTRAINT sentry_userreport_pkey PRIMARY KEY (id);


--
-- Name: sentry_userreport sentry_userreport_project_id_1ac377e052723c91_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_userreport
    ADD CONSTRAINT sentry_userreport_project_id_1ac377e052723c91_uniq UNIQUE (project_id, event_id);


--
-- Name: sentry_versiondsymfile sentry_versiondsymfile_dsym_file_id_208187f6f9f8d2a6_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_versiondsymfile
    ADD CONSTRAINT sentry_versiondsymfile_dsym_file_id_208187f6f9f8d2a6_uniq UNIQUE (dsym_file_id, version, build);


--
-- Name: sentry_versiondsymfile sentry_versiondsymfile_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_versiondsymfile
    ADD CONSTRAINT sentry_versiondsymfile_pkey PRIMARY KEY (id);


--
-- Name: social_auth_usersocialauth social_auth_usersocialauth_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.social_auth_usersocialauth
    ADD CONSTRAINT social_auth_usersocialauth_pkey PRIMARY KEY (id);


--
-- Name: social_auth_usersocialauth social_auth_usersocialauth_provider_69933d2ea493fc8c_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.social_auth_usersocialauth
    ADD CONSTRAINT social_auth_usersocialauth_provider_69933d2ea493fc8c_uniq UNIQUE (provider, uid, user_id);


--
-- Name: south_migrationhistory south_migrationhistory_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.south_migrationhistory
    ADD CONSTRAINT south_migrationhistory_pkey PRIMARY KEY (id);


--
-- Name: auth_authenticator_user_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_authenticator_user_id ON public.auth_authenticator USING btree (user_id);


--
-- Name: auth_group_name_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_name_like ON public.auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_permissions_group_id ON public.auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_permissions_permission_id ON public.auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_permission_content_type_id ON public.auth_permission USING btree (content_type_id);


--
-- Name: auth_user_username_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_user_username_like ON public.auth_user USING btree (username varchar_pattern_ops);


--
-- Name: django_admin_log_content_type_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_admin_log_content_type_id ON public.django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_admin_log_user_id ON public.django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_session_expire_date ON public.django_session USING btree (expire_date);


--
-- Name: django_session_session_key_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_session_session_key_like ON public.django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: jira_ac_tenant_client_key_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX jira_ac_tenant_client_key_like ON public.jira_ac_tenant USING btree (client_key varchar_pattern_ops);


--
-- Name: jira_ac_tenant_organization_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX jira_ac_tenant_organization_id ON public.jira_ac_tenant USING btree (organization_id);


--
-- Name: nodestore_node_id_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX nodestore_node_id_like ON public.nodestore_node USING btree (id varchar_pattern_ops);


--
-- Name: nodestore_node_timestamp; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX nodestore_node_timestamp ON public.nodestore_node USING btree ("timestamp");


--
-- Name: sentry_activity_group_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_activity_group_id ON public.sentry_activity USING btree (group_id);


--
-- Name: sentry_activity_project_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_activity_project_id ON public.sentry_activity USING btree (project_id);


--
-- Name: sentry_activity_user_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_activity_user_id ON public.sentry_activity USING btree (user_id);


--
-- Name: sentry_apiapplication_client_id_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_apiapplication_client_id_like ON public.sentry_apiapplication USING btree (client_id varchar_pattern_ops);


--
-- Name: sentry_apiapplication_owner_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_apiapplication_owner_id ON public.sentry_apiapplication USING btree (owner_id);


--
-- Name: sentry_apiapplication_status; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_apiapplication_status ON public.sentry_apiapplication USING btree (status);


--
-- Name: sentry_apiauthorization_application_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_apiauthorization_application_id ON public.sentry_apiauthorization USING btree (application_id);


--
-- Name: sentry_apiauthorization_user_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_apiauthorization_user_id ON public.sentry_apiauthorization USING btree (user_id);


--
-- Name: sentry_apigrant_application_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_apigrant_application_id ON public.sentry_apigrant USING btree (application_id);


--
-- Name: sentry_apigrant_code; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_apigrant_code ON public.sentry_apigrant USING btree (code);


--
-- Name: sentry_apigrant_code_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_apigrant_code_like ON public.sentry_apigrant USING btree (code varchar_pattern_ops);


--
-- Name: sentry_apigrant_expires_at; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_apigrant_expires_at ON public.sentry_apigrant USING btree (expires_at);


--
-- Name: sentry_apigrant_user_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_apigrant_user_id ON public.sentry_apigrant USING btree (user_id);


--
-- Name: sentry_apikey_key_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_apikey_key_like ON public.sentry_apikey USING btree (key varchar_pattern_ops);


--
-- Name: sentry_apikey_organization_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_apikey_organization_id ON public.sentry_apikey USING btree (organization_id);


--
-- Name: sentry_apikey_status; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_apikey_status ON public.sentry_apikey USING btree (status);


--
-- Name: sentry_apitoken_application_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_apitoken_application_id ON public.sentry_apitoken USING btree (application_id);


--
-- Name: sentry_apitoken_refresh_token_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_apitoken_refresh_token_like ON public.sentry_apitoken USING btree (refresh_token varchar_pattern_ops);


--
-- Name: sentry_apitoken_token_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_apitoken_token_like ON public.sentry_apitoken USING btree (token varchar_pattern_ops);


--
-- Name: sentry_apitoken_user_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_apitoken_user_id ON public.sentry_apitoken USING btree (user_id);


--
-- Name: sentry_auditlogentry_actor_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_auditlogentry_actor_id ON public.sentry_auditlogentry USING btree (actor_id);


--
-- Name: sentry_auditlogentry_actor_key_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_auditlogentry_actor_key_id ON public.sentry_auditlogentry USING btree (actor_key_id);


--
-- Name: sentry_auditlogentry_organization_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_auditlogentry_organization_id ON public.sentry_auditlogentry USING btree (organization_id);


--
-- Name: sentry_auditlogentry_target_user_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_auditlogentry_target_user_id ON public.sentry_auditlogentry USING btree (target_user_id);


--
-- Name: sentry_authidentity_auth_provider_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_authidentity_auth_provider_id ON public.sentry_authidentity USING btree (auth_provider_id);


--
-- Name: sentry_authidentity_user_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_authidentity_user_id ON public.sentry_authidentity USING btree (user_id);


--
-- Name: sentry_authprovider_default_teams_authprovider_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_authprovider_default_teams_authprovider_id ON public.sentry_authprovider_default_teams USING btree (authprovider_id);


--
-- Name: sentry_authprovider_default_teams_team_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_authprovider_default_teams_team_id ON public.sentry_authprovider_default_teams USING btree (team_id);


--
-- Name: sentry_broadcast_is_active; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_broadcast_is_active ON public.sentry_broadcast USING btree (is_active);


--
-- Name: sentry_broadcastseen_broadcast_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_broadcastseen_broadcast_id ON public.sentry_broadcastseen USING btree (broadcast_id);


--
-- Name: sentry_broadcastseen_user_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_broadcastseen_user_id ON public.sentry_broadcastseen USING btree (user_id);


--
-- Name: sentry_commit_author_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_commit_author_id ON public.sentry_commit USING btree (author_id);


--
-- Name: sentry_commit_organization_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_commit_organization_id ON public.sentry_commit USING btree (organization_id);


--
-- Name: sentry_commit_repository_id_5b0d06238a42bbfc; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_commit_repository_id_5b0d06238a42bbfc ON public.sentry_commit USING btree (repository_id, date_added);


--
-- Name: sentry_commitauthor_organization_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_commitauthor_organization_id ON public.sentry_commitauthor USING btree (organization_id);


--
-- Name: sentry_commitfilechange_commit_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_commitfilechange_commit_id ON public.sentry_commitfilechange USING btree (commit_id);


--
-- Name: sentry_commitfilechange_organization_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_commitfilechange_organization_id ON public.sentry_commitfilechange USING btree (organization_id);


--
-- Name: sentry_deploy_environment_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_deploy_environment_id ON public.sentry_deploy USING btree (environment_id);


--
-- Name: sentry_deploy_notified; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_deploy_notified ON public.sentry_deploy USING btree (notified);


--
-- Name: sentry_deploy_organization_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_deploy_organization_id ON public.sentry_deploy USING btree (organization_id);


--
-- Name: sentry_deploy_release_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_deploy_release_id ON public.sentry_deploy USING btree (release_id);


--
-- Name: sentry_distribution_organization_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_distribution_organization_id ON public.sentry_distribution USING btree (organization_id);


--
-- Name: sentry_distribution_release_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_distribution_release_id ON public.sentry_distribution USING btree (release_id);


--
-- Name: sentry_dsymapp_project_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_dsymapp_project_id ON public.sentry_dsymapp USING btree (project_id);


--
-- Name: sentry_environment_organization_id_6c9098a3d53d6a9a; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX sentry_environment_organization_id_6c9098a3d53d6a9a ON public.sentry_environment USING btree (organization_id, name);


--
-- Name: sentry_environmentproject_environment_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_environmentproject_environment_id ON public.sentry_environmentproject USING btree (environment_id);


--
-- Name: sentry_environmentproject_project_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_environmentproject_project_id ON public.sentry_environmentproject USING btree (project_id);


--
-- Name: sentry_environmentrelease_environment_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_environmentrelease_environment_id ON public.sentry_environmentrelease USING btree (environment_id);


--
-- Name: sentry_environmentrelease_last_seen; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_environmentrelease_last_seen ON public.sentry_environmentrelease USING btree (last_seen);


--
-- Name: sentry_environmentrelease_organization_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_environmentrelease_organization_id ON public.sentry_environmentrelease USING btree (organization_id);


--
-- Name: sentry_environmentrelease_organization_id_394c1c5e67253784; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX sentry_environmentrelease_organization_id_394c1c5e67253784 ON public.sentry_environmentrelease USING btree (organization_id, release_id, environment_id);


--
-- Name: sentry_environmentrelease_release_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_environmentrelease_release_id ON public.sentry_environmentrelease USING btree (release_id);


--
-- Name: sentry_eventmapping_group_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_eventmapping_group_id ON public.sentry_eventmapping USING btree (group_id);


--
-- Name: sentry_eventmapping_project_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_eventmapping_project_id ON public.sentry_eventmapping USING btree (project_id);


--
-- Name: sentry_eventprocessingissue_processing_issue_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_eventprocessingissue_processing_issue_id ON public.sentry_eventprocessingissue USING btree (processing_issue_id);


--
-- Name: sentry_eventprocessingissue_raw_event_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_eventprocessingissue_raw_event_id ON public.sentry_eventprocessingissue USING btree (raw_event_id);


--
-- Name: sentry_eventtag_date_added; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_eventtag_date_added ON public.sentry_eventtag USING btree (date_added);


--
-- Name: sentry_eventtag_group_id_5ad9abfe8e1fa62b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_eventtag_group_id_5ad9abfe8e1fa62b ON public.sentry_eventtag USING btree (group_id, key_id, value_id);


--
-- Name: sentry_eventtag_project_id_42979ba214ba3c43; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_eventtag_project_id_42979ba214ba3c43 ON public.sentry_eventtag USING btree (project_id, key_id, value_id);


--
-- Name: sentry_eventuser_date_added; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_eventuser_date_added ON public.sentry_eventuser USING btree (date_added);


--
-- Name: sentry_eventuser_project_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_eventuser_project_id ON public.sentry_eventuser USING btree (project_id);


--
-- Name: sentry_eventuser_project_id_58b4a7f2595290e6; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_eventuser_project_id_58b4a7f2595290e6 ON public.sentry_eventuser USING btree (project_id, ip_address);


--
-- Name: sentry_eventuser_project_id_7684267daffc292f; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_eventuser_project_id_7684267daffc292f ON public.sentry_eventuser USING btree (project_id, email);


--
-- Name: sentry_eventuser_project_id_8868307f60b6a92; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_eventuser_project_id_8868307f60b6a92 ON public.sentry_eventuser USING btree (project_id, username);


--
-- Name: sentry_featureadoption_organization_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_featureadoption_organization_id ON public.sentry_featureadoption USING btree (organization_id);


--
-- Name: sentry_file_blob_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_file_blob_id ON public.sentry_file USING btree (blob_id);


--
-- Name: sentry_file_timestamp; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_file_timestamp ON public.sentry_file USING btree ("timestamp");


--
-- Name: sentry_fileblob_checksum_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_fileblob_checksum_like ON public.sentry_fileblob USING btree (checksum varchar_pattern_ops);


--
-- Name: sentry_fileblob_timestamp; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_fileblob_timestamp ON public.sentry_fileblob USING btree ("timestamp");


--
-- Name: sentry_fileblobindex_blob_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_fileblobindex_blob_id ON public.sentry_fileblobindex USING btree (blob_id);


--
-- Name: sentry_fileblobindex_file_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_fileblobindex_file_id ON public.sentry_fileblobindex USING btree (file_id);


--
-- Name: sentry_filterkey_project_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_filterkey_project_id ON public.sentry_filterkey USING btree (project_id);


--
-- Name: sentry_filtervalue_first_seen; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_filtervalue_first_seen ON public.sentry_filtervalue USING btree (first_seen);


--
-- Name: sentry_filtervalue_last_seen; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_filtervalue_last_seen ON public.sentry_filtervalue USING btree (last_seen);


--
-- Name: sentry_filtervalue_project_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_filtervalue_project_id ON public.sentry_filtervalue USING btree (project_id);


--
-- Name: sentry_filtervalue_project_id_20cb80e47b504ee6; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_filtervalue_project_id_20cb80e47b504ee6 ON public.sentry_filtervalue USING btree (project_id, key, last_seen);


--
-- Name: sentry_filtervalue_project_id_27377f6151fcab56; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_filtervalue_project_id_27377f6151fcab56 ON public.sentry_filtervalue USING btree (project_id, value, last_seen);


--
-- Name: sentry_filtervalue_project_id_2b3fdfeac62987c7; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_filtervalue_project_id_2b3fdfeac62987c7 ON public.sentry_filtervalue USING btree (project_id, value, first_seen);


--
-- Name: sentry_filtervalue_project_id_737632cad2909511; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_filtervalue_project_id_737632cad2909511 ON public.sentry_filtervalue USING btree (project_id, value, times_seen);


--
-- Name: sentry_groupasignee_project_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_groupasignee_project_id ON public.sentry_groupasignee USING btree (project_id);


--
-- Name: sentry_groupasignee_user_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_groupasignee_user_id ON public.sentry_groupasignee USING btree (user_id);


--
-- Name: sentry_groupbookmark_group_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_groupbookmark_group_id ON public.sentry_groupbookmark USING btree (group_id);


--
-- Name: sentry_groupbookmark_project_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_groupbookmark_project_id ON public.sentry_groupbookmark USING btree (project_id);


--
-- Name: sentry_groupbookmark_user_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_groupbookmark_user_id ON public.sentry_groupbookmark USING btree (user_id);


--
-- Name: sentry_groupbookmark_user_id_5eedb134f529cf58; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_groupbookmark_user_id_5eedb134f529cf58 ON public.sentry_groupbookmark USING btree (user_id, group_id);


--
-- Name: sentry_groupcommitresolution_commit_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_groupcommitresolution_commit_id ON public.sentry_groupcommitresolution USING btree (commit_id);


--
-- Name: sentry_groupcommitresolution_datetime; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_groupcommitresolution_datetime ON public.sentry_groupcommitresolution USING btree (datetime);


--
-- Name: sentry_groupedmessage_active_at; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_groupedmessage_active_at ON public.sentry_groupedmessage USING btree (active_at);


--
-- Name: sentry_groupedmessage_first_release_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_groupedmessage_first_release_id ON public.sentry_groupedmessage USING btree (first_release_id);


--
-- Name: sentry_groupedmessage_first_seen; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_groupedmessage_first_seen ON public.sentry_groupedmessage USING btree (first_seen);


--
-- Name: sentry_groupedmessage_last_seen; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_groupedmessage_last_seen ON public.sentry_groupedmessage USING btree (last_seen);


--
-- Name: sentry_groupedmessage_level; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_groupedmessage_level ON public.sentry_groupedmessage USING btree (level);


--
-- Name: sentry_groupedmessage_logger; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_groupedmessage_logger ON public.sentry_groupedmessage USING btree (logger);


--
-- Name: sentry_groupedmessage_logger_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_groupedmessage_logger_like ON public.sentry_groupedmessage USING btree (logger varchar_pattern_ops);


--
-- Name: sentry_groupedmessage_project_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_groupedmessage_project_id ON public.sentry_groupedmessage USING btree (project_id);


--
-- Name: sentry_groupedmessage_project_id_31335ae34c8ef983; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_groupedmessage_project_id_31335ae34c8ef983 ON public.sentry_groupedmessage USING btree (project_id, first_release_id);


--
-- Name: sentry_groupedmessage_resolved_at; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_groupedmessage_resolved_at ON public.sentry_groupedmessage USING btree (resolved_at);


--
-- Name: sentry_groupedmessage_status; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_groupedmessage_status ON public.sentry_groupedmessage USING btree (status);


--
-- Name: sentry_groupedmessage_times_seen; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_groupedmessage_times_seen ON public.sentry_groupedmessage USING btree (times_seen);


--
-- Name: sentry_groupedmessage_view; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_groupedmessage_view ON public.sentry_groupedmessage USING btree (view);


--
-- Name: sentry_groupedmessage_view_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_groupedmessage_view_like ON public.sentry_groupedmessage USING btree (view varchar_pattern_ops);


--
-- Name: sentry_groupemailthread_date; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_groupemailthread_date ON public.sentry_groupemailthread USING btree (date);


--
-- Name: sentry_groupemailthread_group_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_groupemailthread_group_id ON public.sentry_groupemailthread USING btree (group_id);


--
-- Name: sentry_groupemailthread_msgid_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_groupemailthread_msgid_like ON public.sentry_groupemailthread USING btree (msgid varchar_pattern_ops);


--
-- Name: sentry_groupemailthread_project_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_groupemailthread_project_id ON public.sentry_groupemailthread USING btree (project_id);


--
-- Name: sentry_grouphash_group_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_grouphash_group_id ON public.sentry_grouphash USING btree (group_id);


--
-- Name: sentry_grouphash_group_tombstone_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_grouphash_group_tombstone_id ON public.sentry_grouphash USING btree (group_tombstone_id);


--
-- Name: sentry_grouphash_hash_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_grouphash_hash_like ON public.sentry_grouphash USING btree (hash varchar_pattern_ops);


--
-- Name: sentry_grouphash_project_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_grouphash_project_id ON public.sentry_grouphash USING btree (project_id);


--
-- Name: sentry_grouplink_datetime; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_grouplink_datetime ON public.sentry_grouplink USING btree (datetime);


--
-- Name: sentry_grouplink_project_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_grouplink_project_id ON public.sentry_grouplink USING btree (project_id);


--
-- Name: sentry_groupmeta_group_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_groupmeta_group_id ON public.sentry_groupmeta USING btree (group_id);


--
-- Name: sentry_groupredirect_group_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_groupredirect_group_id ON public.sentry_groupredirect USING btree (group_id);


--
-- Name: sentry_grouprelease_last_seen; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_grouprelease_last_seen ON public.sentry_grouprelease USING btree (last_seen);


--
-- Name: sentry_grouprelease_project_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_grouprelease_project_id ON public.sentry_grouprelease USING btree (project_id);


--
-- Name: sentry_grouprelease_release_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_grouprelease_release_id ON public.sentry_grouprelease USING btree (release_id);


--
-- Name: sentry_groupresolution_datetime; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_groupresolution_datetime ON public.sentry_groupresolution USING btree (datetime);


--
-- Name: sentry_groupresolution_release_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_groupresolution_release_id ON public.sentry_groupresolution USING btree (release_id);


--
-- Name: sentry_grouprulestatus_group_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_grouprulestatus_group_id ON public.sentry_grouprulestatus USING btree (group_id);


--
-- Name: sentry_grouprulestatus_project_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_grouprulestatus_project_id ON public.sentry_grouprulestatus USING btree (project_id);


--
-- Name: sentry_grouprulestatus_rule_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_grouprulestatus_rule_id ON public.sentry_grouprulestatus USING btree (rule_id);


--
-- Name: sentry_groupseen_group_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_groupseen_group_id ON public.sentry_groupseen USING btree (group_id);


--
-- Name: sentry_groupseen_project_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_groupseen_project_id ON public.sentry_groupseen USING btree (project_id);


--
-- Name: sentry_groupshare_project_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_groupshare_project_id ON public.sentry_groupshare USING btree (project_id);


--
-- Name: sentry_groupshare_user_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_groupshare_user_id ON public.sentry_groupshare USING btree (user_id);


--
-- Name: sentry_groupshare_uuid_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_groupshare_uuid_like ON public.sentry_groupshare USING btree (uuid varchar_pattern_ops);


--
-- Name: sentry_groupsubscription_group_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_groupsubscription_group_id ON public.sentry_groupsubscription USING btree (group_id);


--
-- Name: sentry_groupsubscription_project_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_groupsubscription_project_id ON public.sentry_groupsubscription USING btree (project_id);


--
-- Name: sentry_groupsubscription_user_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_groupsubscription_user_id ON public.sentry_groupsubscription USING btree (user_id);


--
-- Name: sentry_grouptagkey_group_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_grouptagkey_group_id ON public.sentry_grouptagkey USING btree (group_id);


--
-- Name: sentry_grouptagkey_project_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_grouptagkey_project_id ON public.sentry_grouptagkey USING btree (project_id);


--
-- Name: sentry_grouptombstone_project_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_grouptombstone_project_id ON public.sentry_grouptombstone USING btree (project_id);


--
-- Name: sentry_hipchat_ac_tenant_auth_user_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_hipchat_ac_tenant_auth_user_id ON public.sentry_hipchat_ac_tenant USING btree (auth_user_id);


--
-- Name: sentry_hipchat_ac_tenant_id_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_hipchat_ac_tenant_id_like ON public.sentry_hipchat_ac_tenant USING btree (id varchar_pattern_ops);


--
-- Name: sentry_hipchat_ac_tenant_organizations_organization_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_hipchat_ac_tenant_organizations_organization_id ON public.sentry_hipchat_ac_tenant_organizations USING btree (organization_id);


--
-- Name: sentry_hipchat_ac_tenant_organizations_tenant_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_hipchat_ac_tenant_organizations_tenant_id ON public.sentry_hipchat_ac_tenant_organizations USING btree (tenant_id);


--
-- Name: sentry_hipchat_ac_tenant_organizations_tenant_id_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_hipchat_ac_tenant_organizations_tenant_id_like ON public.sentry_hipchat_ac_tenant_organizations USING btree (tenant_id varchar_pattern_ops);


--
-- Name: sentry_hipchat_ac_tenant_projects_project_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_hipchat_ac_tenant_projects_project_id ON public.sentry_hipchat_ac_tenant_projects USING btree (project_id);


--
-- Name: sentry_hipchat_ac_tenant_projects_tenant_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_hipchat_ac_tenant_projects_tenant_id ON public.sentry_hipchat_ac_tenant_projects USING btree (tenant_id);


--
-- Name: sentry_hipchat_ac_tenant_projects_tenant_id_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_hipchat_ac_tenant_projects_tenant_id_like ON public.sentry_hipchat_ac_tenant_projects USING btree (tenant_id varchar_pattern_ops);


--
-- Name: sentry_identity_idp_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_identity_idp_id ON public.sentry_identity USING btree (idp_id);


--
-- Name: sentry_message_datetime; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_message_datetime ON public.sentry_message USING btree (datetime);


--
-- Name: sentry_message_group_id_5f63ffbd9aac1141; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_message_group_id_5f63ffbd9aac1141 ON public.sentry_message USING btree (group_id, datetime);


--
-- Name: sentry_message_message_id_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_message_message_id_like ON public.sentry_message USING btree (message_id varchar_pattern_ops);


--
-- Name: sentry_message_project_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_message_project_id ON public.sentry_message USING btree (project_id);


--
-- Name: sentry_messagefiltervalue_first_seen; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_messagefiltervalue_first_seen ON public.sentry_messagefiltervalue USING btree (first_seen);


--
-- Name: sentry_messagefiltervalue_group_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_messagefiltervalue_group_id ON public.sentry_messagefiltervalue USING btree (group_id);


--
-- Name: sentry_messagefiltervalue_group_id_59490523e6ee451f; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX sentry_messagefiltervalue_group_id_59490523e6ee451f ON public.sentry_messagefiltervalue USING btree (group_id, key, value);


--
-- Name: sentry_messagefiltervalue_last_seen; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_messagefiltervalue_last_seen ON public.sentry_messagefiltervalue USING btree (last_seen);


--
-- Name: sentry_messagefiltervalue_project_id_6852dd47401b2d7d; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_messagefiltervalue_project_id_6852dd47401b2d7d ON public.sentry_messagefiltervalue USING btree (project_id, key, value, last_seen);


--
-- Name: sentry_minidumpfile_event_id_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_minidumpfile_event_id_like ON public.sentry_minidumpfile USING btree (event_id varchar_pattern_ops);


--
-- Name: sentry_minidumpfile_file_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_minidumpfile_file_id ON public.sentry_minidumpfile USING btree (file_id);


--
-- Name: sentry_organization_slug_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_organization_slug_like ON public.sentry_organization USING btree (slug varchar_pattern_ops);


--
-- Name: sentry_organizationaccessrequest_member_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_organizationaccessrequest_member_id ON public.sentry_organizationaccessrequest USING btree (member_id);


--
-- Name: sentry_organizationaccessrequest_team_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_organizationaccessrequest_team_id ON public.sentry_organizationaccessrequest USING btree (team_id);


--
-- Name: sentry_organizationavatar_ident_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_organizationavatar_ident_like ON public.sentry_organizationavatar USING btree (ident varchar_pattern_ops);


--
-- Name: sentry_organizationintegration_default_auth_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_organizationintegration_default_auth_id ON public.sentry_organizationintegration USING btree (default_auth_id);


--
-- Name: sentry_organizationintegration_integration_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_organizationintegration_integration_id ON public.sentry_organizationintegration USING btree (integration_id);


--
-- Name: sentry_organizationintegration_organization_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_organizationintegration_organization_id ON public.sentry_organizationintegration USING btree (organization_id);


--
-- Name: sentry_organizationmember_organization_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_organizationmember_organization_id ON public.sentry_organizationmember USING btree (organization_id);


--
-- Name: sentry_organizationmember_teams_organizationmember_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_organizationmember_teams_organizationmember_id ON public.sentry_organizationmember_teams USING btree (organizationmember_id);


--
-- Name: sentry_organizationmember_teams_team_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_organizationmember_teams_team_id ON public.sentry_organizationmember_teams USING btree (team_id);


--
-- Name: sentry_organizationmember_token_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_organizationmember_token_like ON public.sentry_organizationmember USING btree (token varchar_pattern_ops);


--
-- Name: sentry_organizationmember_user_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_organizationmember_user_id ON public.sentry_organizationmember USING btree (user_id);


--
-- Name: sentry_organizationonboardingtask_organization_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_organizationonboardingtask_organization_id ON public.sentry_organizationonboardingtask USING btree (organization_id);


--
-- Name: sentry_organizationonboardingtask_user_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_organizationonboardingtask_user_id ON public.sentry_organizationonboardingtask USING btree (user_id);


--
-- Name: sentry_organizationoptions_organization_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_organizationoptions_organization_id ON public.sentry_organizationoptions USING btree (organization_id);


--
-- Name: sentry_processingissue_checksum; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_processingissue_checksum ON public.sentry_processingissue USING btree (checksum);


--
-- Name: sentry_processingissue_checksum_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_processingissue_checksum_like ON public.sentry_processingissue USING btree (checksum varchar_pattern_ops);


--
-- Name: sentry_processingissue_project_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_processingissue_project_id ON public.sentry_processingissue USING btree (project_id);


--
-- Name: sentry_project_organization_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_project_organization_id ON public.sentry_project USING btree (organization_id);


--
-- Name: sentry_project_slug_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_project_slug_like ON public.sentry_project USING btree (slug varchar_pattern_ops);


--
-- Name: sentry_project_status; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_project_status ON public.sentry_project USING btree (status);


--
-- Name: sentry_project_team_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_project_team_id ON public.sentry_project USING btree (team_id);


--
-- Name: sentry_projectbookmark_user_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_projectbookmark_user_id ON public.sentry_projectbookmark USING btree (user_id);


--
-- Name: sentry_projectdsymfile_file_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_projectdsymfile_file_id ON public.sentry_projectdsymfile USING btree (file_id);


--
-- Name: sentry_projectdsymfile_project_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_projectdsymfile_project_id ON public.sentry_projectdsymfile USING btree (project_id);


--
-- Name: sentry_projectintegration_integration_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_projectintegration_integration_id ON public.sentry_projectintegration USING btree (integration_id);


--
-- Name: sentry_projectintegration_project_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_projectintegration_project_id ON public.sentry_projectintegration USING btree (project_id);


--
-- Name: sentry_projectkey_project_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_projectkey_project_id ON public.sentry_projectkey USING btree (project_id);


--
-- Name: sentry_projectkey_public_key_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_projectkey_public_key_like ON public.sentry_projectkey USING btree (public_key varchar_pattern_ops);


--
-- Name: sentry_projectkey_secret_key_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_projectkey_secret_key_like ON public.sentry_projectkey USING btree (secret_key varchar_pattern_ops);


--
-- Name: sentry_projectkey_status; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_projectkey_status ON public.sentry_projectkey USING btree (status);


--
-- Name: sentry_projectoptions_project_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_projectoptions_project_id ON public.sentry_projectoptions USING btree (project_id);


--
-- Name: sentry_projectplatform_last_seen; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_projectplatform_last_seen ON public.sentry_projectplatform USING btree (last_seen);


--
-- Name: sentry_projectsymcachefile_cache_file_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_projectsymcachefile_cache_file_id ON public.sentry_projectsymcachefile USING btree (cache_file_id);


--
-- Name: sentry_projectsymcachefile_dsym_file_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_projectsymcachefile_dsym_file_id ON public.sentry_projectsymcachefile USING btree (dsym_file_id);


--
-- Name: sentry_projectsymcachefile_project_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_projectsymcachefile_project_id ON public.sentry_projectsymcachefile USING btree (project_id);


--
-- Name: sentry_projectteam_project_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_projectteam_project_id ON public.sentry_projectteam USING btree (project_id);


--
-- Name: sentry_projectteam_team_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_projectteam_team_id ON public.sentry_projectteam USING btree (team_id);


--
-- Name: sentry_rawevent_project_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_rawevent_project_id ON public.sentry_rawevent USING btree (project_id);


--
-- Name: sentry_release_organization_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_release_organization_id ON public.sentry_release USING btree (organization_id);


--
-- Name: sentry_release_organization_id_b3241759a1649e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_release_organization_id_b3241759a1649e ON public.sentry_release USING btree (organization_id, COALESCE(date_released, date_added));


--
-- Name: sentry_release_organization_id_f0a7ec9ba96de76; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX sentry_release_organization_id_f0a7ec9ba96de76 ON public.sentry_release USING btree (organization_id, version);


--
-- Name: sentry_release_owner_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_release_owner_id ON public.sentry_release USING btree (owner_id);


--
-- Name: sentry_release_project_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_release_project_id ON public.sentry_release USING btree (project_id);


--
-- Name: sentry_release_project_project_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_release_project_project_id ON public.sentry_release_project USING btree (project_id);


--
-- Name: sentry_release_project_release_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_release_project_release_id ON public.sentry_release_project USING btree (release_id);


--
-- Name: sentry_releasecommit_commit_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_releasecommit_commit_id ON public.sentry_releasecommit USING btree (commit_id);


--
-- Name: sentry_releasecommit_organization_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_releasecommit_organization_id ON public.sentry_releasecommit USING btree (organization_id);


--
-- Name: sentry_releasecommit_release_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_releasecommit_release_id ON public.sentry_releasecommit USING btree (release_id);


--
-- Name: sentry_releasefile_dist_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_releasefile_dist_id ON public.sentry_releasefile USING btree (dist_id);


--
-- Name: sentry_releasefile_file_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_releasefile_file_id ON public.sentry_releasefile USING btree (file_id);


--
-- Name: sentry_releasefile_organization_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_releasefile_organization_id ON public.sentry_releasefile USING btree (organization_id);


--
-- Name: sentry_releasefile_project_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_releasefile_project_id ON public.sentry_releasefile USING btree (project_id);


--
-- Name: sentry_releasefile_release_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_releasefile_release_id ON public.sentry_releasefile USING btree (release_id);


--
-- Name: sentry_releaseheadcommit_commit_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_releaseheadcommit_commit_id ON public.sentry_releaseheadcommit USING btree (commit_id);


--
-- Name: sentry_releaseheadcommit_organization_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_releaseheadcommit_organization_id ON public.sentry_releaseheadcommit USING btree (organization_id);


--
-- Name: sentry_releaseheadcommit_release_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_releaseheadcommit_release_id ON public.sentry_releaseheadcommit USING btree (release_id);


--
-- Name: sentry_repository_integration_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_repository_integration_id ON public.sentry_repository USING btree (integration_id);


--
-- Name: sentry_repository_organization_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_repository_organization_id ON public.sentry_repository USING btree (organization_id);


--
-- Name: sentry_repository_status; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_repository_status ON public.sentry_repository USING btree (status);


--
-- Name: sentry_reprocessingreport_project_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_reprocessingreport_project_id ON public.sentry_reprocessingreport USING btree (project_id);


--
-- Name: sentry_rule_project_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_rule_project_id ON public.sentry_rule USING btree (project_id);


--
-- Name: sentry_rule_status; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_rule_status ON public.sentry_rule USING btree (status);


--
-- Name: sentry_savedsearch_owner_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_savedsearch_owner_id ON public.sentry_savedsearch USING btree (owner_id);


--
-- Name: sentry_savedsearch_project_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_savedsearch_project_id ON public.sentry_savedsearch USING btree (project_id);


--
-- Name: sentry_savedsearch_userdefault_project_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_savedsearch_userdefault_project_id ON public.sentry_savedsearch_userdefault USING btree (project_id);


--
-- Name: sentry_savedsearch_userdefault_savedsearch_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_savedsearch_userdefault_savedsearch_id ON public.sentry_savedsearch_userdefault USING btree (savedsearch_id);


--
-- Name: sentry_savedsearch_userdefault_user_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_savedsearch_userdefault_user_id ON public.sentry_savedsearch_userdefault USING btree (user_id);


--
-- Name: sentry_scheduleddeletion_guid_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_scheduleddeletion_guid_like ON public.sentry_scheduleddeletion USING btree (guid varchar_pattern_ops);


--
-- Name: sentry_team_organization_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_team_organization_id ON public.sentry_team USING btree (organization_id);


--
-- Name: sentry_team_slug_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_team_slug_like ON public.sentry_team USING btree (slug varchar_pattern_ops);


--
-- Name: sentry_useravatar_ident_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_useravatar_ident_like ON public.sentry_useravatar USING btree (ident varchar_pattern_ops);


--
-- Name: sentry_useremail_user_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_useremail_user_id ON public.sentry_useremail USING btree (user_id);


--
-- Name: sentry_useridentity_identity_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_useridentity_identity_id ON public.sentry_useridentity USING btree (identity_id);


--
-- Name: sentry_useridentity_user_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_useridentity_user_id ON public.sentry_useridentity USING btree (user_id);


--
-- Name: sentry_userip_user_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_userip_user_id ON public.sentry_userip USING btree (user_id);


--
-- Name: sentry_useroption_organization_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_useroption_organization_id ON public.sentry_useroption USING btree (organization_id);


--
-- Name: sentry_useroption_project_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_useroption_project_id ON public.sentry_useroption USING btree (project_id);


--
-- Name: sentry_useroption_user_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_useroption_user_id ON public.sentry_useroption USING btree (user_id);


--
-- Name: sentry_userreport_group_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_userreport_group_id ON public.sentry_userreport USING btree (group_id);


--
-- Name: sentry_userreport_project_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_userreport_project_id ON public.sentry_userreport USING btree (project_id);


--
-- Name: sentry_userreport_project_id_1ac377e052723c91; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_userreport_project_id_1ac377e052723c91 ON public.sentry_userreport USING btree (project_id, event_id);


--
-- Name: sentry_userreport_project_id_1c06c9ecc190b2e6; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_userreport_project_id_1c06c9ecc190b2e6 ON public.sentry_userreport USING btree (project_id, date_added);


--
-- Name: sentry_versiondsymfile_dsym_app_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_versiondsymfile_dsym_app_id ON public.sentry_versiondsymfile USING btree (dsym_app_id);


--
-- Name: sentry_versiondsymfile_dsym_file_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sentry_versiondsymfile_dsym_file_id ON public.sentry_versiondsymfile USING btree (dsym_file_id);


--
-- Name: social_auth_usersocialauth_user_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX social_auth_usersocialauth_user_id ON public.social_auth_usersocialauth USING btree (user_id);


--
-- Name: sentry_auditlogentry actor_id_refs_id_cac0f7f5; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_auditlogentry
    ADD CONSTRAINT actor_id_refs_id_cac0f7f5 FOREIGN KEY (actor_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_auditlogentry actor_key_id_refs_id_cc2fc30c; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_auditlogentry
    ADD CONSTRAINT actor_key_id_refs_id_cc2fc30c FOREIGN KEY (actor_key_id) REFERENCES public.sentry_apikey(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_apitoken application_id_refs_id_153d42f0; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_apitoken
    ADD CONSTRAINT application_id_refs_id_153d42f0 FOREIGN KEY (application_id) REFERENCES public.sentry_apiapplication(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_apiauthorization application_id_refs_id_4607bb14; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_apiauthorization
    ADD CONSTRAINT application_id_refs_id_4607bb14 FOREIGN KEY (application_id) REFERENCES public.sentry_apiapplication(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_apigrant application_id_refs_id_fe5530d5; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_apigrant
    ADD CONSTRAINT application_id_refs_id_fe5530d5 FOREIGN KEY (application_id) REFERENCES public.sentry_apiapplication(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissions_permission_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_permission_id_fkey FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_authidentity auth_provider_id_refs_id_d9990f1d; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_authidentity
    ADD CONSTRAINT auth_provider_id_refs_id_d9990f1d FOREIGN KEY (auth_provider_id) REFERENCES public.sentry_authprovider(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_hipchat_ac_tenant auth_user_id_refs_id_615fc607; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_hipchat_ac_tenant
    ADD CONSTRAINT auth_user_id_refs_id_615fc607 FOREIGN KEY (auth_user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_commit author_id_refs_id_2f962e87; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_commit
    ADD CONSTRAINT author_id_refs_id_2f962e87 FOREIGN KEY (author_id) REFERENCES public.sentry_commitauthor(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_authprovider_default_teams authprovider_id_refs_id_9e7068be; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_authprovider_default_teams
    ADD CONSTRAINT authprovider_id_refs_id_9e7068be FOREIGN KEY (authprovider_id) REFERENCES public.sentry_authprovider(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_fileblobindex blob_id_refs_id_5732bcfb; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_fileblobindex
    ADD CONSTRAINT blob_id_refs_id_5732bcfb FOREIGN KEY (blob_id) REFERENCES public.sentry_fileblob(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_file blob_id_refs_id_912b0028; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_file
    ADD CONSTRAINT blob_id_refs_id_912b0028 FOREIGN KEY (blob_id) REFERENCES public.sentry_fileblob(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_broadcastseen broadcast_id_refs_id_e214087a; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_broadcastseen
    ADD CONSTRAINT broadcast_id_refs_id_e214087a FOREIGN KEY (broadcast_id) REFERENCES public.sentry_broadcast(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_projectsymcachefile cache_file_id_refs_id_7cf3a92b; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_projectsymcachefile
    ADD CONSTRAINT cache_file_id_refs_id_7cf3a92b FOREIGN KEY (cache_file_id) REFERENCES public.sentry_file(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_releaseheadcommit commit_id_refs_id_66ff0ace; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_releaseheadcommit
    ADD CONSTRAINT commit_id_refs_id_66ff0ace FOREIGN KEY (commit_id) REFERENCES public.sentry_commit(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_releasecommit commit_id_refs_id_a0857449; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_releasecommit
    ADD CONSTRAINT commit_id_refs_id_a0857449 FOREIGN KEY (commit_id) REFERENCES public.sentry_commit(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_commitfilechange commit_id_refs_id_f9a55f94; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_commitfilechange
    ADD CONSTRAINT commit_id_refs_id_f9a55f94 FOREIGN KEY (commit_id) REFERENCES public.sentry_commit(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log content_type_id_refs_id_93d2d1f8; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT content_type_id_refs_id_93d2d1f8 FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission content_type_id_refs_id_d043b34a; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT content_type_id_refs_id_d043b34a FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_releasefile dist_id_refs_id_13fab125; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_releasefile
    ADD CONSTRAINT dist_id_refs_id_13fab125 FOREIGN KEY (dist_id) REFERENCES public.sentry_distribution(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_versiondsymfile dsym_app_id_refs_id_2e9c299e; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_versiondsymfile
    ADD CONSTRAINT dsym_app_id_refs_id_2e9c299e FOREIGN KEY (dsym_app_id) REFERENCES public.sentry_dsymapp(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_versiondsymfile dsym_file_id_refs_id_a38d7c87; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_versiondsymfile
    ADD CONSTRAINT dsym_file_id_refs_id_a38d7c87 FOREIGN KEY (dsym_file_id) REFERENCES public.sentry_projectdsymfile(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_projectsymcachefile dsym_file_id_refs_id_f03d3b01; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_projectsymcachefile
    ADD CONSTRAINT dsym_file_id_refs_id_f03d3b01 FOREIGN KEY (dsym_file_id) REFERENCES public.sentry_projectdsymfile(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_environmentproject environment_id_refs_id_ab2491b4; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_environmentproject
    ADD CONSTRAINT environment_id_refs_id_ab2491b4 FOREIGN KEY (environment_id) REFERENCES public.sentry_environment(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_useravatar file_id_refs_id_0c8678bd; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_useravatar
    ADD CONSTRAINT file_id_refs_id_0c8678bd FOREIGN KEY (file_id) REFERENCES public.sentry_file(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_organizationavatar file_id_refs_id_2ced8ba5; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_organizationavatar
    ADD CONSTRAINT file_id_refs_id_2ced8ba5 FOREIGN KEY (file_id) REFERENCES public.sentry_file(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_minidumpfile file_id_refs_id_57956ab2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_minidumpfile
    ADD CONSTRAINT file_id_refs_id_57956ab2 FOREIGN KEY (file_id) REFERENCES public.sentry_file(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_fileblobindex file_id_refs_id_82747ec9; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_fileblobindex
    ADD CONSTRAINT file_id_refs_id_82747ec9 FOREIGN KEY (file_id) REFERENCES public.sentry_file(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_projectdsymfile file_id_refs_id_cc76204b; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_projectdsymfile
    ADD CONSTRAINT file_id_refs_id_cc76204b FOREIGN KEY (file_id) REFERENCES public.sentry_file(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_releasefile file_id_refs_id_fb71e922; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_releasefile
    ADD CONSTRAINT file_id_refs_id_fb71e922 FOREIGN KEY (file_id) REFERENCES public.sentry_file(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_groupedmessage first_release_id_refs_id_d035a570; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupedmessage
    ADD CONSTRAINT first_release_id_refs_id_d035a570 FOREIGN KEY (first_release_id) REFERENCES public.sentry_release(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_groupseen group_id_refs_id_09b2694a; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupseen
    ADD CONSTRAINT group_id_refs_id_09b2694a FOREIGN KEY (group_id) REFERENCES public.sentry_groupedmessage(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_groupbookmark group_id_refs_id_3738447a; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupbookmark
    ADD CONSTRAINT group_id_refs_id_3738447a FOREIGN KEY (group_id) REFERENCES public.sentry_groupedmessage(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_groupemailthread group_id_refs_id_3c3dd283; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupemailthread
    ADD CONSTRAINT group_id_refs_id_3c3dd283 FOREIGN KEY (group_id) REFERENCES public.sentry_groupedmessage(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_groupshare group_id_refs_id_45b11130; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupshare
    ADD CONSTRAINT group_id_refs_id_45b11130 FOREIGN KEY (group_id) REFERENCES public.sentry_groupedmessage(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_groupasignee group_id_refs_id_47b32b76; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupasignee
    ADD CONSTRAINT group_id_refs_id_47b32b76 FOREIGN KEY (group_id) REFERENCES public.sentry_groupedmessage(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_grouprulestatus group_id_refs_id_66981850; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_grouprulestatus
    ADD CONSTRAINT group_id_refs_id_66981850 FOREIGN KEY (group_id) REFERENCES public.sentry_groupedmessage(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_userreport group_id_refs_id_6b3d43d4; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_userreport
    ADD CONSTRAINT group_id_refs_id_6b3d43d4 FOREIGN KEY (group_id) REFERENCES public.sentry_groupedmessage(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_groupmeta group_id_refs_id_6dc57728; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupmeta
    ADD CONSTRAINT group_id_refs_id_6dc57728 FOREIGN KEY (group_id) REFERENCES public.sentry_groupedmessage(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_groupsnooze group_id_refs_id_7d70660e; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupsnooze
    ADD CONSTRAINT group_id_refs_id_7d70660e FOREIGN KEY (group_id) REFERENCES public.sentry_groupedmessage(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_groupsubscription group_id_refs_id_901a3390; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupsubscription
    ADD CONSTRAINT group_id_refs_id_901a3390 FOREIGN KEY (group_id) REFERENCES public.sentry_groupedmessage(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_grouphash group_id_refs_id_9603f6ba; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_grouphash
    ADD CONSTRAINT group_id_refs_id_9603f6ba FOREIGN KEY (group_id) REFERENCES public.sentry_groupedmessage(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_activity group_id_refs_id_b84d67ec; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_activity
    ADD CONSTRAINT group_id_refs_id_b84d67ec FOREIGN KEY (group_id) REFERENCES public.sentry_groupedmessage(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_groupresolution group_id_refs_id_ed32932f; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupresolution
    ADD CONSTRAINT group_id_refs_id_ed32932f FOREIGN KEY (group_id) REFERENCES public.sentry_groupedmessage(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions group_id_refs_id_f4b32aac; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT group_id_refs_id_f4b32aac FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_useridentity identity_id_refs_id_60b730ac; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_useridentity
    ADD CONSTRAINT identity_id_refs_id_60b730ac FOREIGN KEY (identity_id) REFERENCES public.sentry_identity(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_identity idp_id_refs_id_f0c91862; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_identity
    ADD CONSTRAINT idp_id_refs_id_f0c91862 FOREIGN KEY (idp_id) REFERENCES public.sentry_identityprovider(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_projectintegration integration_id_refs_id_13d343b7; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_projectintegration
    ADD CONSTRAINT integration_id_refs_id_13d343b7 FOREIGN KEY (integration_id) REFERENCES public.sentry_integration(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_organizationintegration integration_id_refs_id_fdcbef56; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_organizationintegration
    ADD CONSTRAINT integration_id_refs_id_fdcbef56 FOREIGN KEY (integration_id) REFERENCES public.sentry_integration(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_organizationaccessrequest member_id_refs_id_7c8ccc01; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_organizationaccessrequest
    ADD CONSTRAINT member_id_refs_id_7c8ccc01 FOREIGN KEY (member_id) REFERENCES public.sentry_organizationmember(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_organizationonboardingtask organization_id_refs_id_2203c68b; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_organizationonboardingtask
    ADD CONSTRAINT organization_id_refs_id_2203c68b FOREIGN KEY (organization_id) REFERENCES public.sentry_organization(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_organizationmember organization_id_refs_id_42dc8e8f; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_organizationmember
    ADD CONSTRAINT organization_id_refs_id_42dc8e8f FOREIGN KEY (organization_id) REFERENCES public.sentry_organization(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: jira_ac_tenant organization_id_refs_id_49689eb3; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jira_ac_tenant
    ADD CONSTRAINT organization_id_refs_id_49689eb3 FOREIGN KEY (organization_id) REFERENCES public.sentry_organization(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_useroption organization_id_refs_id_56961afd; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_useroption
    ADD CONSTRAINT organization_id_refs_id_56961afd FOREIGN KEY (organization_id) REFERENCES public.sentry_organization(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_team organization_id_refs_id_61038a42; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_team
    ADD CONSTRAINT organization_id_refs_id_61038a42 FOREIGN KEY (organization_id) REFERENCES public.sentry_organization(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_project organization_id_refs_id_6874e5b7; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_project
    ADD CONSTRAINT organization_id_refs_id_6874e5b7 FOREIGN KEY (organization_id) REFERENCES public.sentry_organization(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_authprovider organization_id_refs_id_6a37632f; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_authprovider
    ADD CONSTRAINT organization_id_refs_id_6a37632f FOREIGN KEY (organization_id) REFERENCES public.sentry_organization(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_organizationoptions organization_id_refs_id_83d34346; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_organizationoptions
    ADD CONSTRAINT organization_id_refs_id_83d34346 FOREIGN KEY (organization_id) REFERENCES public.sentry_organization(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_apikey organization_id_refs_id_961ec303; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_apikey
    ADD CONSTRAINT organization_id_refs_id_961ec303 FOREIGN KEY (organization_id) REFERENCES public.sentry_organization(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_organizationavatar organization_id_refs_id_ac0fa6a7; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_organizationavatar
    ADD CONSTRAINT organization_id_refs_id_ac0fa6a7 FOREIGN KEY (organization_id) REFERENCES public.sentry_organization(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_hipchat_ac_tenant_organizations organization_id_refs_id_af26d69f; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_hipchat_ac_tenant_organizations
    ADD CONSTRAINT organization_id_refs_id_af26d69f FOREIGN KEY (organization_id) REFERENCES public.sentry_organization(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_organizationintegration organization_id_refs_id_af8cad03; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_organizationintegration
    ADD CONSTRAINT organization_id_refs_id_af8cad03 FOREIGN KEY (organization_id) REFERENCES public.sentry_organization(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_release organization_id_refs_id_ba7f8e42; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_release
    ADD CONSTRAINT organization_id_refs_id_ba7f8e42 FOREIGN KEY (organization_id) REFERENCES public.sentry_organization(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_featureadoption organization_id_refs_id_e6c64c1d; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_featureadoption
    ADD CONSTRAINT organization_id_refs_id_e6c64c1d FOREIGN KEY (organization_id) REFERENCES public.sentry_organization(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_releasefile organization_id_refs_id_ef2843cb; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_releasefile
    ADD CONSTRAINT organization_id_refs_id_ef2843cb FOREIGN KEY (organization_id) REFERENCES public.sentry_organization(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_auditlogentry organization_id_refs_id_f5b1844e; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_auditlogentry
    ADD CONSTRAINT organization_id_refs_id_f5b1844e FOREIGN KEY (organization_id) REFERENCES public.sentry_organization(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_organizationmember_teams organizationmember_id_refs_id_878802f4; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_organizationmember_teams
    ADD CONSTRAINT organizationmember_id_refs_id_878802f4 FOREIGN KEY (organizationmember_id) REFERENCES public.sentry_organizationmember(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_release owner_id_refs_id_65604067; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_release
    ADD CONSTRAINT owner_id_refs_id_65604067 FOREIGN KEY (owner_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_savedsearch owner_id_refs_id_865787fc; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_savedsearch
    ADD CONSTRAINT owner_id_refs_id_865787fc FOREIGN KEY (owner_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_apiapplication owner_id_refs_id_f68b4574; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_apiapplication
    ADD CONSTRAINT owner_id_refs_id_f68b4574 FOREIGN KEY (owner_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_eventprocessingissue processing_issue_id_refs_id_0df012da; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_eventprocessingissue
    ADD CONSTRAINT processing_issue_id_refs_id_0df012da FOREIGN KEY (processing_issue_id) REFERENCES public.sentry_processingissue(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_grouprulestatus project_id_refs_id_09c5b95d; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_grouprulestatus
    ADD CONSTRAINT project_id_refs_id_09c5b95d FOREIGN KEY (project_id) REFERENCES public.sentry_project(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_activity project_id_refs_id_0c94d99e; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_activity
    ADD CONSTRAINT project_id_refs_id_0c94d99e FOREIGN KEY (project_id) REFERENCES public.sentry_project(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_reprocessingreport project_id_refs_id_11918af3; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_reprocessingreport
    ADD CONSTRAINT project_id_refs_id_11918af3 FOREIGN KEY (project_id) REFERENCES public.sentry_project(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_groupbookmark project_id_refs_id_18390fbc; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupbookmark
    ADD CONSTRAINT project_id_refs_id_18390fbc FOREIGN KEY (project_id) REFERENCES public.sentry_project(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_groupasignee project_id_refs_id_1b5200f8; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupasignee
    ADD CONSTRAINT project_id_refs_id_1b5200f8 FOREIGN KEY (project_id) REFERENCES public.sentry_project(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_projectsymcachefile project_id_refs_id_3a95c5b5; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_projectsymcachefile
    ADD CONSTRAINT project_id_refs_id_3a95c5b5 FOREIGN KEY (project_id) REFERENCES public.sentry_project(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_projectintegration project_id_refs_id_41efed36; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_projectintegration
    ADD CONSTRAINT project_id_refs_id_41efed36 FOREIGN KEY (project_id) REFERENCES public.sentry_project(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_savedsearch_userdefault project_id_refs_id_4bc1c005; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_savedsearch_userdefault
    ADD CONSTRAINT project_id_refs_id_4bc1c005 FOREIGN KEY (project_id) REFERENCES public.sentry_project(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_projectcounter project_id_refs_id_58200d0a; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_projectcounter
    ADD CONSTRAINT project_id_refs_id_58200d0a FOREIGN KEY (project_id) REFERENCES public.sentry_project(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_groupseen project_id_refs_id_67db0efd; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupseen
    ADD CONSTRAINT project_id_refs_id_67db0efd FOREIGN KEY (project_id) REFERENCES public.sentry_project(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_grouphash project_id_refs_id_6f0a9434; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_grouphash
    ADD CONSTRAINT project_id_refs_id_6f0a9434 FOREIGN KEY (project_id) REFERENCES public.sentry_project(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_userreport project_id_refs_id_723e0b3c; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_userreport
    ADD CONSTRAINT project_id_refs_id_723e0b3c FOREIGN KEY (project_id) REFERENCES public.sentry_project(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_groupedmessage project_id_refs_id_77344b57; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupedmessage
    ADD CONSTRAINT project_id_refs_id_77344b57 FOREIGN KEY (project_id) REFERENCES public.sentry_project(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_release_project project_id_refs_id_80894a1c; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_release_project
    ADD CONSTRAINT project_id_refs_id_80894a1c FOREIGN KEY (project_id) REFERENCES public.sentry_project(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_groupemailthread project_id_refs_id_8419ea36; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupemailthread
    ADD CONSTRAINT project_id_refs_id_8419ea36 FOREIGN KEY (project_id) REFERENCES public.sentry_project(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_grouptombstone project_id_refs_id_8e12ecf7; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_grouptombstone
    ADD CONSTRAINT project_id_refs_id_8e12ecf7 FOREIGN KEY (project_id) REFERENCES public.sentry_project(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_projectdsymfile project_id_refs_id_94d40917; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_projectdsymfile
    ADD CONSTRAINT project_id_refs_id_94d40917 FOREIGN KEY (project_id) REFERENCES public.sentry_project(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_projectoptions project_id_refs_id_9b845024; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_projectoptions
    ADD CONSTRAINT project_id_refs_id_9b845024 FOREIGN KEY (project_id) REFERENCES public.sentry_project(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_groupsubscription project_id_refs_id_a564d25b; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupsubscription
    ADD CONSTRAINT project_id_refs_id_a564d25b FOREIGN KEY (project_id) REFERENCES public.sentry_project(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_hipchat_ac_tenant_projects project_id_refs_id_a7eeaf92; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_hipchat_ac_tenant_projects
    ADD CONSTRAINT project_id_refs_id_a7eeaf92 FOREIGN KEY (project_id) REFERENCES public.sentry_project(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_savedsearch project_id_refs_id_b18120e7; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_savedsearch
    ADD CONSTRAINT project_id_refs_id_b18120e7 FOREIGN KEY (project_id) REFERENCES public.sentry_project(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_dsymapp project_id_refs_id_bdc0d6eb; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_dsymapp
    ADD CONSTRAINT project_id_refs_id_bdc0d6eb FOREIGN KEY (project_id) REFERENCES public.sentry_project(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_rule project_id_refs_id_c96b69eb; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_rule
    ADD CONSTRAINT project_id_refs_id_c96b69eb FOREIGN KEY (project_id) REFERENCES public.sentry_project(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_environmentproject project_id_refs_id_cf2e01df; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_environmentproject
    ADD CONSTRAINT project_id_refs_id_cf2e01df FOREIGN KEY (project_id) REFERENCES public.sentry_project(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_groupshare project_id_refs_id_d3771efc; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupshare
    ADD CONSTRAINT project_id_refs_id_d3771efc FOREIGN KEY (project_id) REFERENCES public.sentry_project(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_rawevent project_id_refs_id_d849fb4d; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_rawevent
    ADD CONSTRAINT project_id_refs_id_d849fb4d FOREIGN KEY (project_id) REFERENCES public.sentry_project(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_projectkey project_id_refs_id_e4d8a857; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_projectkey
    ADD CONSTRAINT project_id_refs_id_e4d8a857 FOREIGN KEY (project_id) REFERENCES public.sentry_project(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_useroption project_id_refs_id_eb596317; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_useroption
    ADD CONSTRAINT project_id_refs_id_eb596317 FOREIGN KEY (project_id) REFERENCES public.sentry_project(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_processingissue project_id_refs_id_f04dda9c; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_processingissue
    ADD CONSTRAINT project_id_refs_id_f04dda9c FOREIGN KEY (project_id) REFERENCES public.sentry_project(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_projectteam project_id_refs_id_f5d7021b; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_projectteam
    ADD CONSTRAINT project_id_refs_id_f5d7021b FOREIGN KEY (project_id) REFERENCES public.sentry_project(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_eventprocessingissue raw_event_id_refs_id_c533ed8a; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_eventprocessingissue
    ADD CONSTRAINT raw_event_id_refs_id_c533ed8a FOREIGN KEY (raw_event_id) REFERENCES public.sentry_rawevent(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_deploy release_id_refs_id_056a8a23; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_deploy
    ADD CONSTRAINT release_id_refs_id_056a8a23 FOREIGN KEY (release_id) REFERENCES public.sentry_release(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_groupresolution release_id_refs_id_0599bf90; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupresolution
    ADD CONSTRAINT release_id_refs_id_0599bf90 FOREIGN KEY (release_id) REFERENCES public.sentry_release(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_releasecommit release_id_refs_id_26c8c7a0; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_releasecommit
    ADD CONSTRAINT release_id_refs_id_26c8c7a0 FOREIGN KEY (release_id) REFERENCES public.sentry_release(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_releasefile release_id_refs_id_8c214aaf; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_releasefile
    ADD CONSTRAINT release_id_refs_id_8c214aaf FOREIGN KEY (release_id) REFERENCES public.sentry_release(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_distribution release_id_refs_id_a8524557; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_distribution
    ADD CONSTRAINT release_id_refs_id_a8524557 FOREIGN KEY (release_id) REFERENCES public.sentry_release(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_release_project release_id_refs_id_add4a457; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_release_project
    ADD CONSTRAINT release_id_refs_id_add4a457 FOREIGN KEY (release_id) REFERENCES public.sentry_release(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_releaseheadcommit release_id_refs_id_b02d8da1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_releaseheadcommit
    ADD CONSTRAINT release_id_refs_id_b02d8da1 FOREIGN KEY (release_id) REFERENCES public.sentry_release(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_grouprulestatus rule_id_refs_id_39ff91f8; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_grouprulestatus
    ADD CONSTRAINT rule_id_refs_id_39ff91f8 FOREIGN KEY (rule_id) REFERENCES public.sentry_rule(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_savedsearch_userdefault savedsearch_id_refs_id_8d85995b; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_savedsearch_userdefault
    ADD CONSTRAINT savedsearch_id_refs_id_8d85995b FOREIGN KEY (savedsearch_id) REFERENCES public.sentry_savedsearch(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_auditlogentry target_user_id_refs_id_cac0f7f5; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_auditlogentry
    ADD CONSTRAINT target_user_id_refs_id_cac0f7f5 FOREIGN KEY (target_user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_authprovider_default_teams team_id_refs_id_10a85f7b; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_authprovider_default_teams
    ADD CONSTRAINT team_id_refs_id_10a85f7b FOREIGN KEY (team_id) REFERENCES public.sentry_team(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_projectteam team_id_refs_id_1d6cecd2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_projectteam
    ADD CONSTRAINT team_id_refs_id_1d6cecd2 FOREIGN KEY (team_id) REFERENCES public.sentry_team(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_project team_id_refs_id_78750968; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_project
    ADD CONSTRAINT team_id_refs_id_78750968 FOREIGN KEY (team_id) REFERENCES public.sentry_team(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_organizationmember_teams team_id_refs_id_d98f2858; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_organizationmember_teams
    ADD CONSTRAINT team_id_refs_id_d98f2858 FOREIGN KEY (team_id) REFERENCES public.sentry_team(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_organizationaccessrequest team_id_refs_id_ea6e538b; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_organizationaccessrequest
    ADD CONSTRAINT team_id_refs_id_ea6e538b FOREIGN KEY (team_id) REFERENCES public.sentry_team(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_hipchat_ac_tenant_projects tenant_id_refs_id_6c1ae0ea; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_hipchat_ac_tenant_projects
    ADD CONSTRAINT tenant_id_refs_id_6c1ae0ea FOREIGN KEY (tenant_id) REFERENCES public.sentry_hipchat_ac_tenant(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_hipchat_ac_tenant_organizations tenant_id_refs_id_f26e0c12; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_hipchat_ac_tenant_organizations
    ADD CONSTRAINT tenant_id_refs_id_f26e0c12 FOREIGN KEY (tenant_id) REFERENCES public.sentry_hipchat_ac_tenant(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_groupbookmark user_id_refs_id_05ac45cc; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupbookmark
    ADD CONSTRAINT user_id_refs_id_05ac45cc FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_useravatar user_id_refs_id_1a689f2e; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_useravatar
    ADD CONSTRAINT user_id_refs_id_1a689f2e FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_organizationonboardingtask user_id_refs_id_22c181a4; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_organizationonboardingtask
    ADD CONSTRAINT user_id_refs_id_22c181a4 FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_groupseen user_id_refs_id_270b7315; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupseen
    ADD CONSTRAINT user_id_refs_id_270b7315 FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_projectbookmark user_id_refs_id_32679665; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_projectbookmark
    ADD CONSTRAINT user_id_refs_id_32679665 FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_savedsearch_userdefault user_id_refs_id_3f7101ca; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_savedsearch_userdefault
    ADD CONSTRAINT user_id_refs_id_3f7101ca FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_apigrant user_id_refs_id_5368c652; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_apigrant
    ADD CONSTRAINT user_id_refs_id_5368c652 FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_apiauthorization user_id_refs_id_55597d94; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_apiauthorization
    ADD CONSTRAINT user_id_refs_id_55597d94 FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_broadcastseen user_id_refs_id_5d9e5ad9; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_broadcastseen
    ADD CONSTRAINT user_id_refs_id_5d9e5ad9 FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_activity user_id_refs_id_6caec40e; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_activity
    ADD CONSTRAINT user_id_refs_id_6caec40e FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_useridentity user_id_refs_id_6dc79229; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_useridentity
    ADD CONSTRAINT user_id_refs_id_6dc79229 FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_useroption user_id_refs_id_73734413; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_useroption
    ADD CONSTRAINT user_id_refs_id_73734413 FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_authidentity user_id_refs_id_78163ab5; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_authidentity
    ADD CONSTRAINT user_id_refs_id_78163ab5 FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_apitoken user_id_refs_id_78c75ee2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_apitoken
    ADD CONSTRAINT user_id_refs_id_78c75ee2 FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_authenticator user_id_refs_id_8e85b45f; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_authenticator
    ADD CONSTRAINT user_id_refs_id_8e85b45f FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_userip user_id_refs_id_96273ab1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_userip
    ADD CONSTRAINT user_id_refs_id_96273ab1 FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_useremail user_id_refs_id_ae956867; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_useremail
    ADD CONSTRAINT user_id_refs_id_ae956867 FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_organizationmember user_id_refs_id_be455e60; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_organizationmember
    ADD CONSTRAINT user_id_refs_id_be455e60 FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_lostpasswordhash user_id_refs_id_c60bdf9b; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_lostpasswordhash
    ADD CONSTRAINT user_id_refs_id_c60bdf9b FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: social_auth_usersocialauth user_id_refs_id_e6cbdf29; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.social_auth_usersocialauth
    ADD CONSTRAINT user_id_refs_id_e6cbdf29 FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_groupshare user_id_refs_id_e7ef4954; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupshare
    ADD CONSTRAINT user_id_refs_id_e7ef4954 FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_groupsubscription user_id_refs_id_efb4b379; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupsubscription
    ADD CONSTRAINT user_id_refs_id_efb4b379 FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sentry_groupasignee user_id_refs_id_f4dcb8d1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sentry_groupasignee
    ADD CONSTRAINT user_id_refs_id_f4dcb8d1 FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

