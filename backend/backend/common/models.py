from django.db import models, connections, DEFAULT_DB_ALIAS


class Comment(models.Model):

    class Meta:
        abstract = True

    user = models.ForeignKey('user.User', on_delete=models.DO_NOTHING)
    body = models.CharField(max_length=1024, null=False, blank=False)
    create_time = models.DateTimeField(auto_now_add=True)
    modify_time = models.DateTimeField(auto_now=True)


class CommonMessage(models.Model):

    class Meta:

        abstract = True

    body = models.CharField(max_length=1024)
    create_time = models.DateTimeField(auto_now_add=True)
    modify_time = models.DateTimeField(auto_now=True)

    def save(self, *args, **kwargs):  # TODO: move this to decorator
        using = kwargs.get('using', DEFAULT_DB_ALIAS)
        original_value = connections[using].features.can_return_id_from_insert
        connections[using].features.can_return_id_from_insert = False
        result = super(CommonMessage, self).save(*args, **kwargs)
        connections[using].features.can_return_id_from_insert = original_value

        return result
