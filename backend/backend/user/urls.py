from django.urls import path

from user.views import UserSignInView, UserInfoView, UploadAvatarView


urlpatterns = [
    path('sign_in', UserSignInView.as_view()),
    path('info/<int:user_id>', UserInfoView.as_view()),
    path('info', UserInfoView.as_view()),
    path('upload_avatar', UploadAvatarView.as_view()),
]
