from channels.db import database_sync_to_async

from chat.packets import (
    GetChatMessagesPacket, SendChatMessagePacket, CommonChatPingPacket,
    GetCommonChatInfoPacket, GetPrivateChatMessagesPacket, SendPrivateChatMessagePacket,
)
from common.consumers import BaseConsumer
from chat.models import Room, Message
from common.packets import Packet
from user.models import User, Message as UserMessage
from user.packets import AuthenticatePacket


class ChatConsumer(BaseConsumer):

    packet_types = [
        GetChatMessagesPacket.packet_type,
        SendChatMessagePacket.packet_type,
        CommonChatPingPacket.packet_type,
        GetCommonChatInfoPacket.packet_type,
    ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.room_id = None
        self.room = None

    @database_sync_to_async
    def get_room(self, **kwargs):
        return Room.objects.get(**kwargs)

    def get_room_id(self):
        return self.scope['url_route']['kwargs'].get('room_id', 0)

    async def connect(self):
        self.room_id = self.get_room_id()
        await self.accept()

    async def disconnect(self, close_code):
        await self.channel_layer.group_discard(self.group_name, self.channel_name)
        await super().disconnect(close_code)

        if self.room:
            await database_sync_to_async(self.redis_client.delete)(f'common_chat:{self.room.pk}:{self.user.pk}')
            await self.channel_layer.group_send(self.group_name, {'type': 'update_info'})

    async def update_messages(self, _):
        offset = 0
        limit = 50
        messages = Message.objects.filter(room=self.room).order_by('-id')[offset:offset + limit][::-1]

        packet = GetChatMessagesPacket(traffic_type=Packet.TYPE_TRAFFIC_OUTCOMING, status=Packet.STATUS_SUCCESS, messages=messages)
        await self.send_packet(packet)

    async def update_info(self, _):
        online_count = await self.online_count()
        packet = GetCommonChatInfoPacket(
            traffic_type=Packet.TYPE_TRAFFIC_OUTCOMING,
            online_count=online_count,
            name=self.room.name,
            status=Packet.STATUS_SUCCESS,
        )
        await self.send_packet(packet)

    async def authentication_success(self):
        try:
            self.room = await self.get_room(pk=self.room_id)
        except Room.DoesNotExist:
            self.room = await self.get_room(name='default')

        await self.channel_layer.group_add(self.group_name, self.channel_name)

        # Removing online user and telling about it to anothers
        await database_sync_to_async(self.redis_client.setex)(f'common_chat:{self.room.pk}:{self.user.pk}', 10, 1)
        await self.channel_layer.group_send(self.group_name, {'type': 'update_info'})

    async def receive_packet(self, packet):
        packet.room = self.room
        await super().receive_packet(packet)

    @property
    def group_name(self):
        try:
            return f'common_chat_{self.room.pk}'
        except AttributeError:  # 'NoneType' object has no attribute 'pk'
            return 'common_chat_0'

    async def online_count(self):
        users = await database_sync_to_async(self.redis_client.keys)(f'common_chat:{self.room.pk}:*')

        return len(users)


class PrivateChatConsumer(BaseConsumer):

    packet_types = [
        GetPrivateChatMessagesPacket.packet_type,
        SendPrivateChatMessagePacket.packet_type,
    ]

    def __init__(self, *args, **kwargs):
        super(PrivateChatConsumer, self).__init__(*args, **kwargs)

    async def connect(self):
        await self.accept()

    async def disconnect(self, close_code):
        await self.channel_layer.group_discard(self.group_name(self.user.pk), self.channel_name)
        await super().disconnect(close_code)

    async def authentication_success(self):
        await self.channel_layer.group_add(self.group_name(self.user.pk), self.channel_name)

    async def update_messages(self, event):
        offset = 0
        limit = 25
        messages = UserMessage.objects.filter(thread=event['thread']).order_by('-id')[offset:offset + limit][::-1]

        packet = GetPrivateChatMessagesPacket(traffic_type=Packet.TYPE_TRAFFIC_OUTCOMING, status=Packet.STATUS_SUCCESS, messages=messages)
        await self.send_packet(packet)

    def group_name(self, user_id: int) -> str:
        return f'private_chat_{user_id}'
