import uuid
import importlib
from datetime import datetime
from importlib import util as importlib_util
from typing import List, Tuple, Any, Dict

from boltons.strutils import camel2under


class PacketMeta(type):

    registered_types = {}

    def __new__(mcs, name: str, bases: Tuple[type, ...], namespace: Dict[str, Any]) -> Any:
        cls = super().__new__(mcs, name, bases, namespace)
        classname = cls.__name__

        if classname == 'Packet':
            return cls

        packet_type = camel2under(classname.replace('Packet', ''))
        cls.packet_type = packet_type

        if 'PingPacket' in classname:
            from common.serializers.packets import BasePingPacketSerializer

            mcs.registered_types[packet_type] = (BasePingPacketSerializer, BasePingPacketSerializer)
        else:
            mcs.registered_types[packet_type] = mcs._get_serializers(cls)

        mcs.registered_types[f'{packet_type}_class'] = cls
        # mcs._patch_process_method(cls)

        return cls

    @classmethod
    def _patch_process_method(mcs, cls):
        original_process = cls.process

        def process(*args, **kwargs):
            res = original_process(*args, **kwargs)

            if isinstance(res, Packet):
                return res.serialize()

            return res

        cls.process = process

    @classmethod
    def _find_serialiers_package(mcs, cls):
        """
        Finds serializers.views module in the same django app as a cls
        :return:
        """
        module = cls.__module__

        def check(path):
            serializers_module = '.'.join([path, 'serializers', 'packets'])

            try:
                found = importlib_util.find_spec(serializers_module)
            except ModuleNotFoundError:
                found = None

            if found:
                return serializers_module

        module_path = None

        for m in module.split('.'):
            if module_path:
                module_path = '.'.join([module_path, m])
            else:
                module_path = m

            result = check(module_path)

            if result:
                return result

    @classmethod
    def _get_serializers(mcs, cls) -> List[Any]:
        classname = cls.__name__
        serializers_package_path = mcs._find_serialiers_package(cls)
        serializer_classes = []

        for serializer_type in ['out', 'in']:
            serializer_class = None

            if serializers_package_path:
                serializers_package = importlib.import_module(serializers_package_path)
                serializer_classname = '{}{}{}'.format(classname, serializer_type.capitalize(), 'Serializer')
                serializer_class = getattr(serializers_package, serializer_classname, None)

            if not serializer_class:
                raise NotImplementedError(f'Serializer class not found for packet {classname} and type {serializer_type}')

            serializer_classes.append(serializer_class)

        return serializer_classes

    @classmethod
    def get_packet_class(mcs, packet_type):
        return mcs.registered_types[f'{packet_type}_class']


class Packet(metaclass=PacketMeta):

    TYPE_TRAFFIC_OUTCOMING = 1
    TYPE_TRAFFIC_INCOMING = 2

    STATUS_SUCCESS = 'success'
    STATUS_FAILURE = 'failure'

    def __init__(self, **kwargs):
        self.traffic_type = kwargs.pop('traffic_type', self.TYPE_TRAFFIC_OUTCOMING)
        self.uuid = kwargs.pop('uuid', uuid.uuid4())
        self.parent_uuid = None
        self.__dict__.update(kwargs)

    def process(self, *args, **kwargs):
        raise NotImplementedError

    @classmethod
    def timestamp(cls):
        return int(datetime.now().timestamp())

    @classmethod
    def statuses(cls) -> List[str]:
        return [v for k, v in cls.__dict__.items() if k.startswith('STATUS_')]

    def serialize(self):
        out_serializer_class, in_serializer_class = PacketMeta.registered_types[self.packet_type]

        if self.traffic_type == self.TYPE_TRAFFIC_OUTCOMING:
            serialized_packet = out_serializer_class(self).data
        elif self.traffic_type == self.TYPE_TRAFFIC_INCOMING:
            serialized_packet = in_serializer_class(self).data
        else:
            raise ValueError(f'Unknown traffic_type {self.traffic_type} for packet: {self.__class__.__name__}')

        return serialized_packet

    @classmethod
    def deserialize(cls, **kwargs):
        packet_type = kwargs.get('type')
        traffic_type = kwargs.pop('traffic_type', None)

        if packet_type not in PacketMeta.registered_types:
            raise ValueError(f'Unknown packet_type: {packet_type}')

        out_serilizer_class, in_serilizer_class = PacketMeta.registered_types[packet_type]

        if traffic_type and traffic_type == cls.TYPE_TRAFFIC_OUTCOMING:
            serilizer = out_serilizer_class(data=kwargs)
        else:
            serilizer = in_serilizer_class(data=kwargs)

        serilizer.is_valid(raise_exception=True)
        packet_class = PacketMeta.get_packet_class(packet_type)

        return packet_class(**serilizer.validated_data)
